/**
 * @preserve YourJS v2.23.0.yn - Your Very Own JS Library
 * Home Page - http://yourjs.com/
 * Download - http://yourjs.com/download/2.23.0.yn
 *
 * Copyright (c) 2015-2019 Christopher West
 * Licensed under the MIT license.
 */
(function(__VERSION, __VARIABLE_NAME, undefined) {
  var YourJS,
      __GLOBAL = this,
      __EMPTY_ARRAY = [],
      __EMPTY_OBJECT = {},
      __DOCUMENT = __GLOBAL.document,
      __callsAfterDefs = [];
  
  /**
   * Finds the bitwise <code>and</code> of two numbers.
   * @name andBits
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/andBits/2.23.0}
   * @param {number} number1
   *   Number to be <code>and</code>ed against <code>number2</code>.
   * @param {number} [number2]
   *   Number to be <code>and</code>ed against <code>number1</code>.
   * @returns {number|Function}
   *   If <code>number1</code> and <code>number2</code> are given a bitwise <code>and</code> will be executed on
   *   both and returned.  Otherwise a partial function will be returned which
   *   when called will accept one number and will return a number which is the
   *   result of using bitwise <code>and</code> on <code>number1</code> and the number passed to this
   *   partial function.
   */
  
  /**
   * Finds the quotient of two numbers.
   * @name divide
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/divide/2.23.0}
   * @param {number} number1
   *   Number to be divided by <code>number2</code>.
   * @param {number} [number2]
   *   Number to divide by <code>number1</code>.
   * @returns {number|Function}
   *   If <code>number1</code> and <code>number2</code> are given the quotient of the two will be
   *   returned.  Otherwise a partial function will be returned which when
   *   called will accept one number and will return the number passed to the
   *   partial function divided by <code>number1</code>.
   */
  
  /**
   * Determines if one value is equal to (<code>==</code>) another value.
   * @name eq
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/eq/2.23.0}
   * @param {*} value1
   *   Value to be checked to see if it is equal to (<code>==</code>) <code>value2</code>.
   * @param {*} [value2]
   *   Value to be checked to see if it is equal to (<code>==</code>) <code>value1</code>.
   * @returns {boolean|Function}
   *   If <code>value1</code> and <code>value2</code> are given a boolean indicating if <code>value1</code> is
   *   equal to (<code>==</code>) <code>value2</code> will be returned.  Otherwise a partial function
   *   will be returned which when called will accept one value and will return
   *   a boolean indicating if that value is equal to (<code>==</code>) <code>value1</code>.
   */
  
  /**
   * Determines if one value is strictly equal to (<code>===</code>) another value.
   * @name eqs
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/eqs/2.23.0}
   * @param {*} value1
   *   Value to be checked to see if it is strictly equal to (<code>===</code>) <code>value2</code>.
   * @param {*} [value2]
   *   Value to be checked to see if it is strictly equal to (<code>===</code>) <code>value1</code>.
   * @returns {boolean|Function}
   *   If <code>value1</code> and <code>value2</code> are given a boolean indicating if <code>value1</code> is
   *   strictly equal to (<code>===</code>) <code>value2</code> will be returned.  Otherwise a partial
   *   function will be returned which when called will accept one value and
   *   will return a boolean indicating if that value is strictly equal to
   *   (<code>===</code>) <code>value1</code>.
   */
  
  /**
   * Determines if one value is greater than another value.
   * @name gt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/gt/2.23.0}
   * @param {*} value1
   *   Value to be checked to see if it is less than <code>value2</code>.
   * @param {*} [value2]
   *   Value to be checked to see if it is greater than <code>value1</code>.
   * @returns {boolean|Function}
   *   If <code>value1</code> and <code>value2</code> are given a boolean indicating if <code>value1</code> is
   *   greater than <code>value2</code> will be returned.  Otherwise a partial function
   *   will be returned which when called will accept one value and will return
   *   a boolean indicating if that value is greater than <code>value1</code>.
   */
  
  /**
   * Determines if one value is greater than or equal to another value.
   * @name gte
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/gte/2.23.0}
   * @param {*} value1
   *   Value to be checked to see if it is less than or equal to <code>value2</code>.
   * @param {*} [value2]
   *   Value to be checked to see if it is greater than or equal to <code>value1</code>.
   * @returns {boolean|Function}
   *   If <code>value1</code> and <code>value2</code> are given a boolean indicating if <code>value1</code> is
   *   greater than or equal to <code>value2</code> will be returned.  Otherwise a partial
   *   function will be returned which when called will accept one value and
   *   will return a boolean indicating if that value is greater than or equal
   *   to <code>value1</code>.
   */
  
  /**
   * Determines if one value is less than or equal to another value.
   * @name lte
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lte/2.23.0}
   * @param {*} value1
   *   Value to be checked to see if it is greater than or equal to <code>value2</code>.
   * @param {*} [value2]
   *   Value to be checked to see if it is less than or equal to <code>value1</code>.
   * @returns {boolean|Function}
   *   If <code>value1</code> and <code>value2</code> are given a boolean indicating if <code>value1</code> is
   *   less than or equal to <code>value2</code> will be returned.  Otherwise a partial
   *   function will be returned which when called will accept one value and
   *   will return a boolean indicating if that value is less than or equal to
   *   <code>value1</code>.
   */
  
  /**
   * Finds the product of two numbers.
   * @name multiply
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/multiply/2.23.0}
   * @param {number} number1
   *   Number to be multiplied by <code>number2</code>.
   * @param {number} [number2]
   *   Number to be multiplied by <code>number1</code>.
   * @returns {number|Function}
   *   If <code>number1</code> and <code>number2</code> are given the product of the two will be
   *   returned.  Otherwise a partial function will be returned which when
   *   called will accept one number and will return the number passed to the
   *   partial function multiplied by <code>number1</code>.
   */
  
  /**
   * Determines if one value is not equal to (<code>!=</code>) another value.
   * @name ne
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ne/2.23.0}
   * @param {*} value1
   *   Value to be checked to see if it is not equal to (<code>!=</code>) <code>value2</code>.
   * @param {*} [value2]
   *   Value to be checked to see if it is not equal to (<code>!=</code>) <code>value1</code>.
   * @returns {boolean|Function}
   *   If <code>value1</code> and <code>value2</code> are given a boolean indicating if <code>value1</code> is
   *   not equal to (<code>!=</code>) <code>value2</code> will be returned.  Otherwise a partial
   *   function will be returned which when called will accept one value and
   *   will return a boolean indicating if that value is not equal to (<code>!=</code>)
   *   <code>value1</code>.
   */
  
  /**
   * Determines if one value is strictly not equal to (<code>!==</code>) another value.
   * @name nes
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/nes/2.23.0}
   * @param {*} value1
   *   Value to be checked to see if it is strictly not equal to (<code>!==</code>)
   *   <code>value2</code>.
   * @param {*} [value2]
   *   Value to be checked to see if it is strictly not equal to (<code>!==</code>)
   *   <code>value1</code>.
   * @returns {boolean|Function}
   *   If <code>value1</code> and <code>value2</code> are given a boolean indicating if <code>value1</code> is
   *   strictly not equal to (<code>!==</code>) <code>value2</code> will be returned.  Otherwise a
   *   partial function will be returned which when called will accept one value
   *   and will return a boolean indicating if that value is strictly not equal
   *   to (<code>!==</code>) <code>value1</code>.
   */
  
  /**
   * Finds the bitwise <code>or</code> of two numbers.
   * @name orBits
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/orBits/2.23.0}
   * @param {number} number1
   *   Number to be <code>or</code>ed against <code>number2</code>.
   * @param {number} [number2]
   *   Number to be <code>or</code>ed against <code>number1</code>.
   * @returns {number|Function}
   *   If <code>number1</code> and <code>number2</code> are given a bitwise <code>or</code> will be executed on
   *   both and returned.  Otherwise a partial function will be returned which
   *   when called will accept one number and will return a number which is the
   *   result of using bitwise <code>or</code> on <code>number1</code> and the number passed to this
   *   partial function.
   */
  
  /**
   * Finds the remainder after dividing two numbers.
   * @name rem
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/rem/2.23.0}
   * @param {number} number1
   *   Number to be divided by <code>number2</code>.
   * @param {number} [number2]
   *   Number to divide by <code>number1</code>.
   * @returns {number|Function}
   *   If <code>number1</code> and <code>number2</code> are given <code>number1</code> will be divided by
   *   <code>number2</code> and the remainder will be returned.  Otherwise a partial
   *   function will be returned which when called will accept one number and
   *   will return the remainder of dividing the number passed to the partial
   *   function by <code>number1</code>.
   */
  
  /**
   * Finds the difference of two numbers.
   * @name subtract
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/subtract/2.23.0}
   * @param {number} number1
   *   Number to be subtracted from <code>number2</code>.
   * @param {number} [number2]
   *   Number to subtract from <code>number1</code>.
   * @returns {number|Function}
   *   If <code>number1</code> and <code>number2</code> are given the difference of the two will be
   *   returned.  Otherwise a partial function will be returned which when
   *   called will accept one number and will return the number passed to the
   *   partial function subtracted from <code>number1</code>.
   */
  
  /**
   * Finds the bitwise <code>xor</code> of two numbers.
   * @name xorBits
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/xorBits/2.23.0}
   * @param {number} number1
   *   Number to be <code>xor</code>ed against <code>number2</code>.
   * @param {number} [number2]
   *   Number to be <code>xor</code>ed against <code>number1</code>.
   * @returns {number|Function}
   *   If <code>number1</code> and <code>number2</code> are given a bitwise <code>xor</code> will be executed on
   *   both and returned.  Otherwise a partial function will be returned which
   *   when called will accept one number and will return a number which is the
   *   result of using bitwise <code>xor</code> on <code>number1</code> and the number passed to this
   *   partial function.
   */
  
  /**
   * Determines if one value is less than another value.
   * @name lt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lt/2.23.0}
   * @param {*} value1
   *   Value to be checked to see if it is greater than <code>value2</code>.
   * @param {*} [value2]
   *   Value to be checked to see if it is less than <code>value1</code>.
   * @returns {boolean|Function}
   *   If <code>value1</code> and <code>value2</code> are given a boolean indicating if <code>value1</code> is
   *   less than <code>value2</code> will be returned.  Otherwise a partial function will
   *   be returned which when called will accept one value and will return a
   *   boolean indicating if that value is less than <code>value1</code>.
   */
  // Generates xorBits, orBits, andBits, add, subtract, multiply, divide, rem, lt, gt, eq, ne, lte, gte, eqs and nes
  eval('xorBits^orBits|andBits&add+subtract-multiply*divide/rem%lt<gt>eq==ne!=lte<=gte>=eqs===nes!=='.replace(/(\w+)(\W+)/g, 'function $1(a,b){return arguments.length-1?a$2b:function(b){return b$2a}}'));
  
  /**
   * Finds the sum of two numbers.
   * @name add
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/add/2.23.0}
   * @param {number} number1
   *   Number to be added to <code>number2</code>.
   * @param {number} [number2]
   *   Number to be added to <code>number1</code>.
   * @returns {number|Function}
   *   If <code>number1</code> and <code>number2</code> are given the sum of the two will be returned.
   *   Otherwise a partial function will be returned which when called will
   *   accept one number and will return the number passed to the partial
   *   function added to <code>number1</code>.
   */
  
  /**
   * Creates a new copy of a regular expression with modified flags.
   * @name modRegExp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/modRegExp/2.23.0}
   * @param {RegExp} rgx
   *   The regular expression to be duplicated with modified flags.
   * @param {string} [modifiers]
   *   Specifies how to modify the flags of the duplicate of <code>rgx</code>. The standard
   *   flags are of course <code>g</code> for global, <code>i</code> for ignoreCase and <code>m</code> for
   *   multiline. In some environments other flags are also available. In order
   *   to assure that the duplicate will have specified flag you can just add
   *   the flag character (optionally you can prefix it with a <code>+</code>) to the
   *   string. In order to assure that the duplicate will exclude a specified
   *   flag you can enter - followed by the flag character in this string. In
   *   order to toggle the flag in the duplicate you can prefix the flag
   *   character with !. For example <code>"g-i!m"</code> (which is the same as <code>"+g-i!m"</code>)
   *   will ensure the <code>g</code> flag is included, ensure the <code>i</code> flag is excluded and
   *   toggle <code>m</code>.
   * @returns {RegExp}
   *   Returns a duplicate of <code>rgx</code> with modified flags as specified by the
   *   <code>modifiers</code> string.
   */
  function modRegExp(rgx, modifiers) {
    var flags = (rgx + '').replace(/[\s\S]+\//, '');
    modifiers.replace(/([-+!]?)(\w)/g, function(index, op, flag) {
      index = flags.indexOf(flag)
      flags = op == '-' || (op == '!' && index >= 0)
        ? flags.replace(flag, '')
        : index < 0
          ? flags + flag
          : flags;
    });
    return new RegExp(rgx.source, flags);
  }
  
  
  /**
   * Turn any string into a regular expression that matches the exact string
   * without worrying about encoding the metacharacters yourself.
   * @name quoteRegExp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/quoteRegExp/2.23.0}
   * @param {string} str
   *   The string that should be modified so that any characters that would
   *   normally serve as metacharacters in a regular expression will be escaped.
   * @param {boolean|string} [flagsOrMakeRegExp=false]
   *   If not specified or if <code>false</code>, it will cause just the escaped version of
   *   <code>str</code> to be returned. If <code>true</code>, it will be as if the empty string was
   *   passed in. A string represents the flags to be set in the returned
   *   regular expression.
   * @returns {RegExp|string}
   *   If <code>flagsOrMakeRegExp</code> is <code>true</code> a regular expression without flags will
   *   be returned. If <code>flagsOrMakeRegExp</code> is a non-empty string, a regular
   *   expression with the specified characters representing the corresponding
   *   flags will be returned. In all other cases just the escaped string that
   *   can be used as a regular expression will be returned.
   */
  function quoteRegExp(str, opt_flagsOrMakeRegExp) {
    var ret = str.replace(/[[\](){}.+*^$|\\?-]/g, '\\$&');
    return (opt_flagsOrMakeRegExp === '' || opt_flagsOrMakeRegExp)
      ? new RegExp(ret, opt_flagsOrMakeRegExp === true ? '' : opt_flagsOrMakeRegExp)
      : ret;
  }
  
  /**
   * Determines if something is a primitive or not. If not a primitive it must
   * be some form of object.
   * @name isPrimitive
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isPrimitive/2.23.0}
   * @param {*} arg
   *   The argument which will be determined as being either a primitive (eg.
   *   boolean, number, string, etc.) or a non-primitive.
   * @returns {boolean}
   *   <code>true</code> will be returned if <code>arg</code> is simply a primitive, otherwise <code>false</code>
   *   will be returned.
   */
  function isPrimitive(arg) {
    var type = typeof arg;
    return arg == undefined || (type != "object" && type != "function");
  }
  
  /**
   * Determines if an object is a prototype object.
   * @name isPrototype
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isPrototype/2.23.0}
   * @param {*} obj
   *   Object to check to see if it is a prototype.
   * @returns {boolean}
   *   If <code>obj</code> is a prototype object <code>true</code> is returned, otherwise <code>false</code>.
   */
  function isPrototype(obj) {
    var c = obj && obj.constructor;
    return (c && 'function' === typeof c) ? c.prototype === obj : false;
  }
  
  /**
   * Retrieves all of the associated type names for a value.
   * @name kindsOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/kindsOf/2.23.0}
   * @param {*} value
   *   Value for which to get all of the associated type names.
   * @returns {Array<string>}
   *   An array of all of the types that <code>value</code> is.
   */
  
  /**
   * Either gets the type of a value or adds a constructor registering its
   * custom type name.
   * @name typeOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/typeOf/2.23.0}
   * @param {*} value
   *   A value whose type name will be returned.  Alternatively if
   *   <code>opt_typeNameToAdd</code> is passed this must be the constructor for the
   *   corresponding type name.
   * @param {string} [opt_typeNameToAdd]
   *   If specified, <code>value</code> will be looked for within an array of all known
   *   constructors and if not found a new entry will be added along with this
   *   given type name.
   * @returns {string|boolean}
   *   If <code>opt_typeNameToAdd</code> was omitted a string representation of <code>value</code>'s
   *   type usually capitalized unless <code>"null"</code> or <code>"undefined"</code> is returned.
   *   If <code>opt_typeNameToAdd</code> is given, the constructor passed as <code>value</code> will
   *   be searched and a boolean indicating whether or not it needed to be added
   *   will be returned.
   */
  var kindsOf, typeOf;
  (function (arrObjectTypes, arrLen) {
    kindsOf = function (value) {
      var isProto = isPrototype(value),
        result = [],
        typeName = __EMPTY_OBJECT.toString.call(value).slice(8, -1);
      if (isPrimitive(value)) {
        result.push('*primitive', typeName.toLowerCase());
      }
      else if (typeName !== 'Object') {
        result.push('Object');
      }
      if (value != undefined) {
        result.push(typeName);
      }
      if (typeName === 'Object') {
        value = value.constructor;
        typeName = (value && value.name) || '*unknown';
        for (i = 0; i < arrLen; i += 2) {
          if (value === arrObjectTypes[i]) {
            typeName = arrObjectTypes[i + 1];
            break;
          }
        }
        if (typeName !== 'Object') {
          result.push(typeName);
        }
      }
      if (isProto) {
        result.push('*prototype');
      }
      return result;
    };
    typeOf = function (value, opt_typeNameToAdd) {
      if (opt_typeNameToAdd) {
        for (var i = 0; i < arrLen; i += 2) {
          if (value === arrObjectTypes[i]) {
            return false;
          }
        }
        arrLen += 2;
        arrObjectTypes.push(value, opt_typeNameToAdd);
        return true;
      }
      value = kindsOf(value);
      return value[value.length - 1];
    };
  })([], 0);
  
  /**
   * Finds the substring before a specific target.
   * @name before
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/before/2.23.0}
   * @param {string} subject
   *   The string to search.
   * @param {string|RegExp} target
   *   The target to find and key off of within <code>subject</code>.
   * @param {number} [opt_occurrence=1]
   *   The occurrence of <code>target</code> that you want to key off of within <code>subject</code>.
   *   If negative, the occurrence will be counted from the end of <code>subject</code>.
   * @returns {string|null}
   *   Returns the string found before <code>target</code>. If <code>target</code> was not found or
   *   the specified occurrence (<code>opt_occurrence</code>) of <code>target</code> was not found,
   *   <code>null</code> will be returned.
   */
  
  /**
   * Gets the substrings around a specific target.
   * @name around
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/around/2.23.0}
   * @param {string} subject
   *   The string to search.
   * @param {string|RegExp} target
   *   The target to find and key off of within <code>subject</code>.
   * @param {number} [opt_occurrence=1]
   *   The occurrence of <code>target</code> that you want to key off of within <code>subject</code>.
   *   If negative, the occurrence will be counted from the end of <code>subject</code>.
   * @returns {Array}
   *   Returns an array with two values: the string found before <code>target</code> and
   *   the string found after <code>target</code>. If <code>target</code> was not found or the
   *   specified occurrence (<code>opt_occurrence</code>) of <code>target</code> was not found,
   *   <code>[null, null]</code> will be returned.
   */
  var around = Function('P,T,M,Q', 'return[ , [0], [1]]'.replace(/ ([^,]{3})?/g, 'function(a,b,d){var e,c=[];d=P(d||1,10);b=(T(b)==="RegExp"?M:Q)(b,"g");a.replace(b,function(a,b){e=arguments;c.push([b=e[e.length-2],a.length+b])});a=(c=c[d+(0<d?-1:c.length)])?[a.slice(0,c[0]),a.slice(c[1])]:[null,null];return a$1}'))(parseInt, typeOf, modRegExp, quoteRegExp),
      before = around[1],
      after = around[2];
  around = around[0];
  
  /**
   * Finds the substring after a specific target.
   * @name after
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/after/2.23.0}
   * @param {string} subject
   *   The string to search.
   * @param {string|RegExp} target
   *   The target to find and key off of within <code>subject</code>.
   * @param {number} [opt_occurrence=1]
   *   The occurrence of <code>target</code> that you want to key off of within <code>subject</code>.
   *   If negative, the occurrence will be counted from the end of <code>subject</code>.
   * @returns {string|null}
   *   Returns the string found after <code>target</code>. If <code>target</code> was not found or the
   *   specified occurrence (<code>opt_occurrence</code>) of <code>target</code> was not found, <code>null</code>
   *   will be returned.
   */
  
  /**
   * Calls a function after all of the objects in an array have had an action
   * carried out.
   * @name afterAll
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/afterAll/2.23.0}
   * @param {Array} objects
   *   Array of objects for which a function is sure to be called.
   * @param {Function|Array<string>} binder
   *   If this is a function it will be called for each object in <code>objects</code> and
   *   will be passed (1) a function that is to be called when action is to be
   *   taken (eg. on an event), (2) an object from <code>objects</code> and (3) the
   *   corresponding index of that object from <code>objects</code>.  Call If this is an
   *   array of strings the strings will be treated as event names that will be
   *   listened to.
   * @param {Function} callback
   *   Function to be called once either action was taken for all of the objects
   *   in <code>objects</code>.  If <code>binder</code> is an array of strings this means once at
   *   least one of the events was called for each object in <code>objects</code>.  This
   *   function will be called with (1) a copy of <code>objects</code> and (2) an array of
   *   objects that will have a <code>arguments</code> property containing the arguments
   *   passed when the action was taken and a <code>timeStamp</code> property containing
   *   the epoch value of when the action was taken.  The objects in the second
   *   argument passed will always correspond to the objects in the first
   *   argument.
   * @returns {Array<boolean>}
   *   Returns an array of booleans each one indicating if the corresponding
   *   object from <code>objects</code> has had an action take place.
   */
  function afterAll(objects, binder, callback) {
    objects = objects.slice();
    var arr = [], result = [], callsLeft = objects.length;
    objects.forEach(function(object, index) {
      result[index] = false;
      function registerAction() {
        if (callsLeft) {
          if (!arr[index]) {
            callsLeft--;
          }
          result[index] = true;
          arr[index] = { arguments: arguments, timeStamp: +new Date };
          if (!callsLeft) {
            callback(objects, arr);
          }
        }
      }
      if ('function' === typeof binder) {
        binder(registerAction, object, index);
      }
      else {
        binder.forEach(function(eventName) {
          object.addEventListener(eventName, registerAction);
        });
      }
    });
    return result;
  }
  
  /**
   * Binds a context to a given function within that context.
   * @name alias
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/alias/2.23.0}
   * @param {Object} context
   *   Context object containing the function to be called.
   * @param {string} name
   *   Name of the function to call within the context.
   * @returns {Function}
   *   Function that when called will execute the function with the given name
   *   under the given context.
   */
  function alias(context, name) { return context[name].bind(context); }
  
  /**
   * Gets the <code>call()</code> function for the specified function.
   * @name callOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/callOf/2.23.0}
   * @param {Function|string} fn
   *   The function or the function name for which to get the <code>call()</code> function.
   * @param {Object} [opt_owner]
   *   If <code>fn</code> is a string then this will be used as the parent object to
   *   reference the actual function.
   * @param {*} [opt_objThis]
   *   If given this will be the value that is always used as the <code>this</code> keyword
   *   by <code>fn</code> (the function).  If not given, the first argument passed to the
   *   returned function will be referenced as the <code>this</code> keyword.
   * @returns {Function}
   *   If <code>opt_objThis</code> is not passed, a version of <code>call()</code> will be returned
   *   which will use the first argument as the <code>this</code> keyword and all
   *   subsequent parameters will be normal arguments.  Otherwise a function
   *   which accepts only arguments (not the <code>this</code> keyword definition) will be
   *   returned.
   */
  /*******************************************************************************
  // Original code for callOf() and applyOf()
  function callOf(fn, opt_owner, opt_objThis) {
    if ('string' === typeof fn) {
      fn = opt_owner[fn];
    }
    return arguments.length < 3
      ? function(opt_objThis) { return fn.apply(opt_objThis, [].slice.call(arguments, 1)); }
      : function () { return fn.apply(opt_objThis, arguments); };
  }
  function applyOf(fn, opt_owner, opt_objThis) {
    if ('string' === typeof fn) {
      fn = opt_owner[fn];
    }
    return arguments.length < 3
      ? function(opt_objThis, args) { return fn.apply(opt_objThis, args); }
      : function (args) { return fn.apply(opt_objThis, args); };
  }
  *******************************************************************************/
  eval('0call2[].slice.call(arguments,1)3arguments)}}0apply2b3b)}}'.replace(/\d/g, function (i) {
    return [
      'function ',
      1,
      'Of(a,b,c){"string"===typeof a&&(a=b[a]);return 3>arguments.length?function(d,b){return a.apply(d,',
      ')}:function(b){return a.apply(c,'
    ][i];
  }));
  
  /**
   * Gets the <code>apply()</code> function for the specified function.
   * @name applyOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/applyOf/2.23.0}
   * @param {Function|string} fn
   *   The function or the function name for which to get the <code>apply()</code>
   *   function.
   * @param {Object} [opt_owner]
   *   If <code>fn</code> is a string then this will be used as the parent object to
   *   reference the actual function.
   * @param {*} [opt_objThis]
   *   If given this will be the value that is always used as the <code>this</code> keyword
   *   by <code>fn</code> (the function).  If not given, the first argument passed to the
   *   returned function will be referenced as the <code>this</code> keyword.
   * @returns {Function}
   *   If <code>opt_objThis</code> is not passed, a version of <code>apply()</code> will be returned
   *   which will use the first argument as the <code>this</code> keyword and the
   *   subsequent argument (which should be an array or an array-like object) as
   *   the normal arguments.  Otherwise a function which accepts only the array
   *   of arguments (not the <code>this</code> keyword definition) will be returned.
   */
  
  /**
   * Creates a new array with the given length and fills the array with the
   * given value.
   * @name array
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/array/2.23.0}
   * @param {number} length
   *   Integer that represents the length of the array (minimum of 0).
   * @param {Function|*} [opt_filler=undefined]
   *   Either a filler value or if this is a function and <code>opt_callFiller</code> is
   *   <code>true</code>-ish then this function will be called with the index to be filled
   *   and the return value will be used as the value added at that index.
   * @param {boolean} [opt_callFiller=false]
   *   If specified as <code>true</code>-ish and <code>opt_filler</code> is a function then
   *   <code>opt_filler</code> will be called as a function to generate the values at for
   *   each item in the new array.
   * @returns {Array}
   *   A new array of the specified length with the specified value used for
   *   each item.
   */
  function array(length, opt_filler, opt_callFiller) {
    opt_callFiller = !!opt_callFiller && 'function' === typeof opt_filler;
    for (var result = Array(length), i = 0; i < length; i++) {
      result[i] = opt_callFiller ? opt_filler(i) : opt_filler;
    }
    return result;
  }
  
  /**
   * Slice does not modify the original array, but instead returns a shallow
   * copy of elements from the original array.
   * @name slice
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/slice/2.23.0}
   * @param {Array|Arguments|Object} array
   *   Array or array-like object to shallow copy.
   * @param {number} [begin=0]
   *   Zero-based index at which to begin extraction.  A negative index can be
   *   used, indicating an offset from the end of the sequence.
   *   <code>slice(array,-2)</code> extracts the last two elements in the sequence.  If
   *   <code>begin</code> is <code>undefined</code>, slice begins from index <code>0</code>.
   * @param {number} [end=array.length]
   *   Zero-based index before which to end extraction. slice extracts up to but
   *   not including <code>end</code>.  For example, <code>slice(array,1,4)</code> extracts the second
   *   element through the fourth element (elements indexed 1, 2, and 3).  A
   *   negative index can be used, indicating an offset from the end of the
   *   sequence. <code>slice(array,2,-1)</code> extracts the third element through the
   *   second-to-last element in the sequence.  If <code>end</code> is omitted, <code>slice</code>
   *   extracts through the end of the sequence (<code>array.length</code>).  If <code>end</code> is
   *   greater than the length of the sequence, slice extracts through the end
   *   of the sequence (<code>array.length</code>).
   * @returns {Array}
   *   A new array containing the extracted elements.
   */
  var slice = alias(__EMPTY_ARRAY.slice, 'call');
  
  /**
   * Attempts to call a function if one exists at the given path starting at the
   * given root object.
   * @name attempt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/attempt/2.23.0}
   * @param {*} root
   *   The root object for which to begin traversing the <code>path</code> the find the
   *   corresponding function to be called.
   * @param {string|Array<string>} path
   *   If this is a string it represents the name of <code>root</code>'s function that will
   *   be called.  If this is an array it represents a path to the object and
   *   then the underlying function that will be called.
   * @param {...*} [opt_argX]
   *   Zero or more arguments to be passed to the function at <code>path</code>.
   * @returns {*}
   *   If there is a function found at <code>path</code> under root it will be executed and
   *   the return value will be returned.  Otherwise <code>undefined</code> is returned.
   */
  function attempt(root, path) {
    if ('string' === typeof path) {
      path = [path];
    }
    for (var fn = root, i = 0, l = path.length; fn != undefined && i < l;) {
      root = fn;
      fn = root[path[i++]];
    }
    if ('function' === typeof fn) {
      return fn.apply(root, slice(arguments, 2));
    }
  }
  
  /**
   * Every object descended from Object inherits the hasOwnProperty method. This
   * method can be used to determine whether an object has the specified
   * property as a direct property of that object; unlike the in operator, this
   * method does not check down the object's prototype chain.
   * @name has
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/has/2.23.0}
   * @param {Object} obj
   *   Object whose property will be tested.
   * @param {string} name
   *   Name of the property to test.
   * @returns {boolean}
   *   A boolean indicating whether the object has the specified property as own
   *   (not inherited) property.
   */
  var has = alias(__EMPTY_OBJECT.hasOwnProperty, 'call');
  
  /**
   * Finds the average of the number in an array.
   * @name avg
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/avg/2.23.0}
   * @param {Array<number>} array
   *   Array of numbers for which to find the average.
   * @returns {number}
   *   The average of the numbers in <code>array</code>.
   */
  function avg(array) {
    for (var sum = 0, count = 0, i = array.length; i--;) {
      if (has(array, i)) {
        sum += +array[i];
        count++;
      }
    }
    return count ? sum / count : undefined;
  }
  
  /**
   * Gives the best item in a list based on the return value of the given
   * iterator function.
   * @name best
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/best/2.23.0}
   * @param {Array|undefined|null} array
   *   Either the array to be iterated over to find the best value or
   *   <code>undefined</code> (or <code>null</code>) indicating that this is a partial function call.
   * @param {Function} iterator
   *   Function that will receive two values starting with the first two in
   *   <code>array</code> or in the array passed when calling the returned partial
   *   function.  When a <code>true</code>-ish is returned by this function the value in
   *   <code>array</code> passed as the first argument will be regarded as the best value,
   *   otherwise the value in <code>array</code> passed as the second argument will be
   *   regarded as the best value.
   * @returns {Function|*}
   *   If <code>array</code> is <code>undefined</code> or <code>null</code> a partial function that is simply
   *   awaiting the value of <code>array</code> will be returned. Otherwise the value that
   *   was determined to be the best will be returned.
   */
  function best(array, iterator) {
    function getBest(array) {
      for (var t, best = array[0], i = 1, l = array.length; i < l; i++) {
        if (iterator(t = array[i], best)) {
          best = t;
        }
      }
      return best;
    }
    return array == undefined ? getBest : getBest(array);
  }
  
  /**
   * Creates a function that when called will call <code>fn</code> with <code>this</code> referring to
   * <code>objThis</code> and any values in <code>opt_args</code> be passed as the first arguments to
   * <code>fn</code>.
   * @name bind
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/bind/2.23.0}
   * @param {Function} fn
   *   Function to invoke when the return function is called.
   * @param {*} objThis
   *   The value to which <code>this</code> will refer unless <code>undefined</code> or <code>null</code> is
   *   given.
   * @param {Array} [opt_args=[]]
   *   Array of arguments to prepend to the arguments passed to <code>fn</code> when the
   *   returned function is called.
   * @returns {Function}
   *   A function that when called will call <code>fn</code> with <code>this</code> referring to
   *   <code>objThis</code> and any values in <code>opt_args</code> be passed as the first arguments
   *   to <code>fn</code>.
   */
  function bind(fn, objThis, opt_args) {
    opt_args = slice(opt_args || []);
    return function() {
      return fn.apply(objThis, opt_args.concat(slice(arguments)));
    };
  }
  
  /**
   * Simple compare function typically use in sorting.
   * @name compare
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/compare/2.23.0}
   * @param {*} a
   *   First argument to compare.
   * @param {*} b
   *   Second argument to compare.
   * @returns {number}
   *   <code>-1</code> is returned if <code>a < b</code>.  <code>1</code> is returned if <code>a > b</code>.  In all other
   *   cases <code>0</code> is returned.
   */
  function compare(a, b) {
    return a < b ? -1 : a > b ? 1 : 0;
  }
  
  /**
   * Determines if the given value is of type <code>Boolean</code> as specified by
   * <code>typeOf()</code>.
   * @name isBoolean
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isBoolean/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>Boolean</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>Boolean</code> according to <code>typeOf()</code>.
   *   Otherwise <code>false</code>.
   */
  
  /**
   * Determines if the given value is of type <code>Date</code> as specified by <code>typeOf()</code>.
   * @name isDate
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isDate/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>Date</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>Date</code> according to <code>typeOf()</code>.  Otherwise
   *   <code>false</code>.
   */
  
  /**
   * Determines if the given value is of type <code>Function</code> as specified by
   * <code>typeOf()</code>.
   * @name isFunction
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isFunction/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>Function</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>Function</code> according to <code>typeOf()</code>.
   *   Otherwise <code>false</code>.
   */
  
  /**
   * Determines if the given value is <code>null</code> as specified by <code>typeOf()</code>.
   * @name isNull
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isNull/2.23.0}
   * @param {*} value
   *   Value to see if it is <code>null</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is <code>null</code> according to <code>typeOf()</code>.  Otherwise <code>false</code>.
   */
  
  /**
   * Determines if the given value is of type <code>Number</code> as specified by
   * <code>typeOf()</code>.
   * @name isNumber
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isNumber/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>Number</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>Number</code> according to <code>typeOf()</code>.  Otherwise
   *   <code>false</code>.
   */
  
  /**
   * Determines if the given value is of type <code>Object</code> as specified by
   * <code>typeOf()</code>.
   * @name isObject
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isObject/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>Object</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>Object</code> according to <code>typeOf()</code>.  Otherwise
   *   <code>false</code>.
   */
  
  /**
   * Determines if the given value is of type <code>RegExp</code> as specified by
   * <code>typeOf()</code>.
   * @name isRegExp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isRegExp/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>RegExp</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>RegExp</code> according to <code>typeOf()</code>.  Otherwise
   *   <code>false</code>.
   */
  
  /**
   * Determines if the given value is of type <code>String</code> as specified by
   * <code>typeOf()</code>.
   * @name isString
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isString/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>String</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>String</code> according to <code>typeOf()</code>.  Otherwise
   *   <code>false</code>.
   */
  
  /**
   * Determines if the given value is <code>undefined</code> as specified by <code>typeOf()</code>.
   * @name isUndefined
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isUndefined/2.23.0}
   * @param {*} value
   *   Value to see if it is <code>undefined</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is <code>undefined</code> according to <code>typeOf()</code>.  Otherwise
   *   <code>false</code>.
   */
  
  /**
   * Determines if the given value is <code>Unknown</code> as specified by <code>typeOf()</code>.
   * @name isUnknown
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isUnknown/2.23.0}
   * @param {*} value
   *   Value to see if it is <code>Unknown</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is <code>Unknown</code> according to <code>typeOf()</code>.  Otherwise
   *   <code>false</code>.
   */
  
  /**
   * Determines if the given value is of type <code>Arguments</code> as specified by
   * <code>typeOf()</code>.
   * @name isArguments
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isArguments/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>Arguments</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>Arguments</code> according to <code>typeOf()</code>.
   *   Otherwise <code>false</code>.
   */
  eval('var Arguments,Array,Boolean,Date,Function,Null,Number,Object,RegExp,String,Undefined,Unknown'.replace(/[A-Z]\w+/g, 'is$&'));
  'Arguments Array Boolean Date Function null Number Object RegExp String undefined *unknown'.replace(/\*?(\w)(\w+)/g, function(all, first, rest) {
    eval('(function(t){is' + first.toUpperCase() + rest + '=function(v){return t(v)==="' + all + '"}})')(typeOf);
  });
  
  /**
   * Determines if the given value is of type <code>Array</code> as specified by
   * <code>typeOf()</code>.
   * @name isArray
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isArray/2.23.0}
   * @param {*} value
   *   Value to see if it is of type <code>Array</code> according to <code>typeOf()</code>.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is of type <code>Array</code> according to <code>typeOf()</code>.  Otherwise
   *   <code>false</code>.
   */
  
  /**
   * Tests a given <code>path</code> under the given <code>root</code> object and returns the values
   * at each step of that <code>path</code>.
   * @name testAt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/testAt/2.23.0}
   * @param {*} root
   *   Object to start at as the root of the proposed <code>path</code>.
   * @param {Array} path
   *   An array of strings and/or numbers that represent the path to be tested
   *   under <code>root</code>.
   * @returns {Array}
   *   An array having a length less than or equal to that of <code>path</code> where each
   *   will be the value for the corresponding <code>path</code> value.
   */
  function testAt(root, path) {
    for (var result = [], i = 0, l = path.length; i < l; i++) {
      if (root != undefined && has(root, path[i])) {
        result[i] = root = root[path[i]];
      }
      else {
        break;
      }
    }
    return result;
  }
  
  /**
   * Gets the value under the given <code>root</code> object and <code>path</code>.
   * @name getAt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/getAt/2.23.0}
   * @param {*} root
   *   Object to start at as the root of the proposed <code>path</code>.
   * @param {Array} path
   *   An array of strings and/or numbers that represent the path to be
   *   traversed under <code>root</code>.
   * @param {*} [opt_defaultValue=undefined]
   *   The value to be returned if the given <code>path</code> doesn't fully exists under
   *   <code>root</code>.
   * @returns {*}
   *   If <code>path</code> is an empty array <code>root</code> will be returned.  Otherwise either
   *   the value found under the given <code>path</code> starting at <code>root</code> or
   *   <code>opt_defaultValue</code> if the <code>path</code> doesn't fully exist under <code>root</code>.
   */
  function getAt(root, path, opt_defaultValue) {
    var test = testAt(root, path),
        l = path.length;
    return l ? test.length == l ? test.slice(-1)[0] : opt_defaultValue : root;
  }
  
  /**
   * Creates a compare function which binds the namespaces within the two given
   * objects.
   * @name bindCompare
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/bindCompare/2.23.0}
   * @param {Array} namespaces
   *   An array of namespaces to compare.  Each namespace can be an array of
   *   strings and/or numbers, a string or a number.  The order of the
   *   namespaces will determine the order in which the comparison occurs.
   * @param {boolean} [opt_negate=false]
   *   Specifies whether or not the comparison value should be negated.
   * @returns {Function}
   *   A comparison function with the namespaces to be checked bound within the
   *   function.
   */
  function bindCompare(namespaces, opt_negate) {
    namespaces = namespaces.reduce(function(namespaces, ns) {
      var nsIsArray = isArray(ns), l = nsIsArray && ns.length;
      if (!nsIsArray || l) {
        namespaces.push(nsIsArray ? ns : [ns]);
      }
      return namespaces;
    }, []);
    return (Function(
      'c,g,n' + namespaces.map(function (_, i) { return i; }).join(',n'),
      'return function(a,b){return'
        + (opt_negate ? '-' : '')
        + '('
        + namespaces.map(function (_, i) { return 'c(g(a,n' + i + '),g(b,n' + i + '))'; }).join('||')
        + ')}'
    )).apply(namespaces, [compare, getAt].concat(namespaces));
  }
  
  /**
   * Determines if a string can be used as a variable name in JavaScript.
   * @name isValidVarName
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isValidVarName/2.23.0}
   * @param {string} varName
   *   The string to be tested to see if it is a valid variable name.
   * @returns {boolean}
   *   <code>true</code> if the string value of <code>varName</code> is a valid variable name in
   *   JavaScript, otherwise <code>false</code>.
   */
  function isValidVarName(varName) {
    try {
      Function((varName + '').replace(/[\{\[\s\xA0,\/\.=]|^$/g, '!'), '');
      return true;
    }
    catch (e) {
      return false;
    }
  }
  
  /**
   * Gets the native type name of a primitive or an object.  Unlike
   * YourJS.typeOf(), the types are solely determined by the system.
   * @name nativeType
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/nativeType/2.23.0}
   * @param {*} value
   *   Primitive or object whose native type name will be returned.
   * @returns {string}
   *   The native type name of the specified value.  All type names start with
   *   an uppercase character.
   */
  function nativeType(x) {
    return __EMPTY_OBJECT.toString.call(x).slice(8, -1);
  }
  
  /**
   * Creates global variables and when the returned code is executed via
   * <code>eval()</code> the global variables will be deleted and will only be included in
   * the block in which <code>eval()</code> executes.
   * @name blockify
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/blockify/2.23.0}
   * @param {Object} obj
   *   The object whose values should be added to the block's scope.
   * @param {Array|Function|RegExp|*} [opt_filter=undefined]
   *   If not given then all values will be setup to be added to the block's
   *   scope.  If this is an array, only the matching keys found in the array
   *   will be setup to be added to the block's scope.  If this is a function
   *   then it will iterate over all of the values in <code>obj</code> and will be passed
   *   (1) the value, (2) the key, (3) <code>obj</code> and the return value will be
   *   coerced to a boolean to determine if the key-value pair should be setup
   *   to be added to the block's scope.  If this is a regular expression then
   *   only the keys that match will be setup to be added to the block's scope.
   *   In all other cases, all key-value pairs will be added as long as the key
   *   isn't this value.
   * @param {boolean} [opt_overwrite=true]
   *   Defaults to <code>true</code>.  If <code>true</code> whenever <code>eval()</code> is called on the
   *   returned string any variables that could possibly be overwritten will be
   *   overwritten.
   * @returns {string}
   *   The JavaScript code that can be called by <code>eval()</code> to add the desired
   *   variables to the current scope.
   */
  function blockify(obj, opt_filter, opt_overwrite) {
    var filterType = nativeType(opt_filter);
    var filter = filterType === 'Function'
      ? opt_filter
      : filterType === 'Array'
        ? function(v, key) { return opt_filter.indexOf(key) >= 0; } // Array#indexOf() has IE9+ support
        : filterType === 'RegExp'
          ? function(v, key) { return opt_filter.test(key); }
          : function(v, key) { return opt_filter !== key; };
    opt_overwrite = opt_overwrite === undefined ? true : !!opt_overwrite;
    var code = ['try{if(@===undefined){throw 1}}catch(e){throw new Error("Specified block no longer exists.")}'];
    var global = (function() { return this; })();
    for (var id; has(global, id = '*yjsBlock' + Math.random()););
    global[id] = Object.keys(obj).reduce(function(carry, key) {
      if (isValidVarName(key) && filter(obj[key], key, obj)) {
        carry[key] = obj[key];
        code.push('var ' + key + (!opt_overwrite ? '=' + key + '===undefined?@.' + key + ':' : '=@.') + key);
      }
      return carry;
    }, {});
    code.push('delete @');
    return code.join(';').replace(/@/g, '(function(){return this})()["' + id + '"]');
  }
  
  /**
   * Calls a function whether it be a global prototype function or a YourJS
   * function or a custom bindable.
   * @name call
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/call/2.23.0}
   * @param {Function|string} fn
   *   If a function is supplied it will be called.  If a string, it can refer
   *   to a prototype function by using either the <code>"Array.prototype.slice"</code>
   *   (<code>"Prototype_Name.prototype.function_name"</code>) format, the <code>"Array#slice"</code>
   *   (<code>"Prototype_Name#function_name"</code>) format or the <code>"#slice"</code>
   *   (<code>"#function_name"</code> for <code>objThis</code>) format.  If not in the previous format
   *   but is a string it must be the string name of a function in the <code>YourJS</code>
   *   object.
   * @param {*} [objThis=global]
   *   If <code>fn</code> is a function or is a string referring to a prototypal function,
   *   this will be the value passed as the <code>this</code> object to <code>fn</code>.
   * @param {...*} [argX]
   *   Arguments to pass to <code>fn</code>.
   * @returns {*}
   *   Return value of calling <code>fn</code> with the specified arguments and using the
   *   specified context object.
   */
  function call(fn, objThis) {
    var t = typeof(fn) === 'string';
    if (t) {
      t = fn.match(/^([\w\$]+(?=\.|#))?(#|\.?prototype\.)(\w+)$/);
      fn = t ? (t[1] ? __GLOBAL[t[1]].prototype : objThis)[t[3]] : YourJS[fn];
    }
    return fn.apply(t ? objThis : undefined, slice(arguments, t ? 2 : 1));
  }
  
  /**
   * Makes multiple calls using the YourJS <code>call()</code> function.
   * @name calls
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/calls/2.23.0}
   * @param {*} value
   *   The value to start off with in the calling chain.
   * @param {...Array|Function|string} [callArgs]
   *   If any of these arguments are strings or functions that will be wrapped
   *   in arrays.  The items in each array argument will be passed as arguments
   *   to YourJS's <code>call()</code> function.
   * @returns {*}
   *   Return value of making the specified calls to YourJS's call function in
   *   the specified sequence.
   */
  function calls(value) {
    for (var callArgs, args = slice(arguments), i = 0, l = args.length, i = l; i < l; i++) {
      callArgs = args[i];
      if (nativeType(callArgs) !== 'Array') {
        callArgs = [callArgs];
      }
      value = call.apply(undefined, slice(callArgs, 0, 1).concat([value], slice(callArgs, 1)));
    }
    return value;
  }
  
  /**
   * Creates a partial function which will always default to another value when
   * non-values are passed to it.
   * @name defaulter
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/defaulter/2.23.0}
   * @param {*} defaultValue
   *   Value to default to if the value passed to the partial function is not
   *   sufficient.
   * @param {boolean} [opt_beStrict=false]
   *   Specifies whether or not to only check if the value passed to the partial
   *   function is <code>undefined</code>.
   * @returns {*}
   *   A partial function which will return either the value passed in or
   *   <code>defaultValue</code> based on the following criteria:  If <code>opt_beStrict</code> is
   *   <code>true</code>-ish and <code>value</code> is <code>undefined</code>, <code>defaultValue</code> will be returned.
   *   If <code>opt_beStrict</code> is <code>false</code>-ish and <code>value</code> is <code>undefined</code>, <code>null</code> or
   *   <code>NaN</code>, <code>defaultValue</code> will be returned.  In all other cases <code>value</code> will
   *   be returned.
   */
  
  /**
   * Defaults specific non-values to another given value.
   * @name defaultTo
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/defaultTo/2.23.0}
   * @param {*} value
   *   Value to test out.
   * @param {*} defaultValue
   *   Value to default to if <code>value</code> is not sufficient.
   * @param {boolean} [opt_beStrict=false]
   *   Specifies whether or not to only check if <code>value</code> is <code>undefined</code>.
   * @returns {*}
   *   If <code>opt_beStrict</code> is <code>true</code>-ish and <code>value</code> is <code>undefined</code>,
   *   <code>defaultValue</code> will be returned.  If <code>opt_beStrict</code> is <code>false</code>-ish and
   *   <code>value</code> is <code>undefined</code>, <code>null</code> or <code>NaN</code>, <code>defaultValue</code> will be returned.
   *   In all other cases <code>value</code> will be returned.
   */
  eval('function c?void 0===a:null==a||"number"===typeof a&&isNaN(a)return '.replace(/(.{9})(.+)(.{7})/, '$1canDefault(a,c){$3$2}$1defaultTo(a,b,c){$3($2)?b:a}$1defaulter(b,c){$3$1(a){$3($2)?b:a}}'));
  
  /**
   * Determines whether or not a value would be defaulted to another value.
   * @name canDefault
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/canDefault/2.23.0}
   * @param {*} value
   *   Value to test out.
   * @param {boolean} [opt_beStrict=false]
   *   Specifies whether or not to only check if <code>value</code> is <code>undefined</code>.
   * @returns {boolean}
   *   If <code>opt_beStrict</code> is <code>true</code>-ish and <code>value</code> is <code>undefined</code>, <code>true</code> will
   *   be returned.  If <code>opt_beStrict</code> is <code>false</code>-ish and <code>value</code> is
   *   <code>undefined</code>, <code>null</code> or <code>NaN</code>, <code>true</code> will be returned.  In all other
   *   cases <code>false</code> will be returned.
   */
  
  /**
   * Puts a cap (limit) on the amount of arguments that a function can receive.
   * @name cap
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/cap/2.23.0}
   * @param {Function|string} fn
   *   The function that will be called by the capped function.  If a string it
   *   be interpreted as the function within the YourJS object's context with
   *   that name.
   * @param {number} [opt_maxArity=fn.length]
   *   The max number of arguments that will be accepted by the returned capped
   *   function.
   * @param {Array} [opt_staticArgs=[]]
   *   Arguments that will always be passed to <code>fn</code>.
   * @param {number} [opt_indexInArgs=opt_staticArgs.length]
   *   Position within <code>opt_staticArgs</code> to place the limited additional
   *   arguments if any are passed to the capped function.  Negative position
   *   will be counted from the end of <code>opt_staticArgs</code>.
   * @returns {Function}
   *   Returns a function that will call <code>fn</code> with the passed in arguments when
   *   invoked, but it will limit the argument count to the value
   *   <code>opt_maxArity</code>.
   */
  function cap(fn, opt_maxArity, opt_staticArgs, opt_indexInArgs) {
    var leftArgs = slice(
          opt_staticArgs = opt_staticArgs || [],
          0,
          opt_indexInArgs = opt_indexInArgs == undefined ? opt_staticArgs.length : opt_indexInArgs
        ),
        rightArgs = slice(opt_staticArgs, opt_indexInArgs);
    if ('string' == typeof fn) {
      fn = YourJS[fn];
      if ('function' != typeof fn) {
        throw new Error('Cannot resolve cap function for "' + fn + '"');
      }
    }
    opt_maxArity = opt_maxArity != undefined ? opt_maxArity : fn.length;
    return function(arg1) {
      return fn.apply(
        this,
        leftArgs.concat(slice(arguments, 0, opt_maxArity), rightArgs)
      );
    };
  }
  
  /**
   * Creates a wrapper function for a prototype function that can be capped
   * (limited in the amount of arguments it can receive).
   * @name capProto
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/capProto/2.23.0}
   * @param {Function|string} fn
   *   The prototype function that will be called by the returned wrapper
   *   function.  If it is a string it can refer to a prototype function by
   *   using either the <code>"Array.prototype.slice"</code>
   *   (<code>"Prototype_Name.prototype.function_name"</code>) format, the <code>"Array#slice"</code>
   *   (<code>"Prototype_Name#function_name"</code>) format or the <code>"#slice"</code>
   *   (<code>"#function_name"</code> for the 1st argument) format.
   * @param {number} [opt_maxArity=fn.length]
   *   The max number of arguments that will be accepted by the returned capped
   *   function.
   * @param {Array} [opt_staticArgs=[]]
   *   Arguments that will always be passed to <code>fn</code>.
   * @param {number} [opt_indexInArgs=opt_staticArgs.length]
   *   Position within <code>opt_staticArgs</code> to place the limited additional
   *   arguments if any are passed to the capped function.  Negative position
   *   will be counted from the end of <code>opt_staticArgs</code>.
   * @returns {Function}
   *   Returns a function that will call <code>fn</code> with the passed in arguments when
   *   invoked, but it will limit the argument count to the value <code>opt_maxArity</code>
   *   (not including the 1st argument which is the context argument).
   */
  function capProto(fn, opt_maxArity, opt_staticArgs, opt_indexInArgs) {
    var fnIsString = 'string' === typeof fn,
        leftArgs = slice(
          opt_staticArgs = opt_staticArgs || [],
          0,
          opt_indexInArgs = opt_indexInArgs == undefined ? opt_staticArgs.length : opt_indexInArgs
        ),
        rightArgs = slice(opt_staticArgs, opt_indexInArgs);
    if (fnIsString) {
      fn.replace(/^([\w\$]+(?=\.|#))?(?:#|\.?prototype\.)(\w+)$|[\s\S]*/, function(m, globalClassName, fnName) {
        fnIsString = !globalClassName;
        if (globalClassName) {
          fn = __GLOBAL[globalClassName].prototype[fnName];
          if ('function' !== typeof fn) {
            fn = 0;
          }
        }
        else {
          fn = fnName;
        }
        if (!fn) {
          throw new Error('Cannot resolve prototype function for "' + m + '"');
        }
      });
    }
    return function(objThis) {
      var f = fn;
      if (fnIsString) {
        f = Object(objThis)[f];
        if ('function' !== typeof f) {
          throw new Error("Invalid value for this prototype function");
        }
      }
      return f.apply(objThis, leftArgs.concat(
        slice(arguments, 1, opt_maxArity == undefined ? f.length + 1 : opt_maxArity),
        rightArgs
      ));
    };
  }
  
  /**
   * Gets the cube root of a number.
   * @name cbrt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/cbrt/2.23.0}
   * @param {number} x
   *   A number.
   * @returns {number}
   *   The cube root of the given number.
   */
  function cbrt(x) {
    var y = Math.pow(Math.abs(x), 1 / 3);
    return x < 0 ? -y : y;
  }
  
  /**
   * Rounds towards <code>-Infinity</code> with the specified precision.
   * @name floor
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/floor/2.23.0}
   * @param {number} num
   *   The number to be rounded towards <code>Infinity</code>.
   * @param {number} [opt_precision=0]
   *   The precision (as an integer) with which to round the number towards
   *   <code>-Infinity</code>. Using the precision is similar to doing <code>Math.floor(num *
   *   Math.pow(10, precision)) / Math.pow(10, precision)</code>.
   * @returns {number}
   *   Returns <code>num</code> rounded towards <code>-Infinity</code> with the specified precision.
   */
  
  /**
   * Rounds with the specified precision.
   * @name round
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/round/2.23.0}
   * @param {number} num
   *   The number to be rounded.
   * @param {number} [opt_precision=0]
   *   The precision (as an integer) with which to round the number. Using the
   *   precision is similar to doing <code>Math.round(num * Math.pow(10, precision))
   *   / Math.pow(10, precision)</code>.
   * @returns {number}
   *   Returns <code>num</code> rounded with the specified precision.
   */
  eval('var ceil,floor,round');
  'ceil floor round'.replace(/\w+/g, function(fnName, RGX_NUM_BOUNDS, RGX_EXTRA_DOT) {
    RGX_NUM_BOUNDS = /^-?|$/g;
    RGX_EXTRA_DOT = /(\.\d*)\./;
    eval('(function(x){' + fnName + '=x})')(function(num, opt_precision) {
      opt_precision = opt_precision || 0;
      var absPrecision = Math.abs(opt_precision);
      var zeroesSub = '$&' + new Array(absPrecision + 2).join('0');
      var rgxGrow = new RegExp('(\\.)(\\d{' + absPrecision + '})');
      var rgxShrink = new RegExp('(\\d{' + absPrecision + '})(\\.)');
      num = (num + '.').replace(RGX_NUM_BOUNDS, zeroesSub)
        .replace(RGX_EXTRA_DOT, '$1')
        .replace(opt_precision < 0 ? rgxShrink : rgxGrow, '$2$1');
      num = (Math[fnName](+num) + '.').replace(RGX_NUM_BOUNDS, zeroesSub)
        .replace(RGX_EXTRA_DOT, '$1')
        .replace(opt_precision < 0 ? rgxGrow : rgxShrink, '$2$1');
      return +num;
    });
  });
  
  /**
   * Rounds towards <code>Infinity</code> with the specified precision.
   * @name ceil
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ceil/2.23.0}
   * @param {number} num
   *   The number to be rounded towards <code>Infinity</code>.
   * @param {number} [opt_precision=0]
   *   The precision (as an integer) with which to round the number towards
   *   <code>Infinity</code>. Using the precision is similar to doing <code>Math.ceil(num *
   *   Math.pow(10, precision)) / Math.pow(10, precision)</code>.
   * @returns {number}
   *   Returns <code>num</code> rounded towards <code>-Infinity</code> with the specified precision.
   */
  
  /**
   * Adds a new function to the YourJS object and to <code>YourJSChain</code>'s prototype.
   * @name mixin
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/mixin/2.23.0}
   * @param {Function} fn
   *   The function to add to the YourJS object.
   * @param {string} name
   *   The name to give to the function under the YourJS object and under the
   *   <code>YourJSChain</code>'s prototype.
   * @returns {boolean}
   *   Boolean value indicating if the function could be added.
   */
  
  /**
   * Runs through all of the properties of an object, executing a function for
   * each property found.
   * @name forIn
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/forIn/2.23.0}
   * @param {Object} obj
   *   Object whose properties will be traversed.
   * @param {Function} fn
   *   Function to call for each property in <code>obj</code>.  The parameters passed are
   *   as follows:  (1) value, (2) key, (3) <code>obj</code> and (4) ender function.  The
   *   <code>this</code> keyword will contain the number of times the function has been
   *   called.  The ender function may be used to immediately stop traversing
   *   <code>obj</code>.  Calling the ender function with a <code>true</code>-ish value will instead
   *   make the ender function return a boolean value indicating if the ender
   *   function has already been called (useful only in a <code>catch</code> or <code>finally</code>
   *   clause).
   * @param {boolean} [opt_traverseAll=false]
   *   Boolean indicating whether or not even the properties that are not owned
   *   by <code>obj</code> should be traversed.  If <code>true</code> all properties will be
   *   traversed.
   * @returns {number}
   *   Number of times <code>fn</code> was called.
   */
  function forIn(obj, fn, opt_traverseAll) {
    var endCalled, count = 0;
    function end(opt_getIfEnded) {
      if(opt_getIfEnded) {
        return !!endCalled;
      }
      throw endCalled=1;
    }
    for (var k in obj) {
      if (opt_traverseAll || has(obj, k)) {
        try {
          fn.call(++count, obj[k], k, obj, end);
        }
        catch (e) {
          if (endCalled) {
            break;
          }
          throw e;
        }
      }
    }
    return count;
  }
  
  /**
   * Tests to see if two or more values are not exactly the same. This is
   * similar to <code>!==</code> but will also work with <code>NaN</code> and can differentiate
   * between <code>-0</code> and <code>0</code>.
   * @name isNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isNot/2.23.0}
   * @param {*} x
   *   A value.
   * @param {*} y
   *   Value to be compared to <code>x</code> for inequality.
   * @param {...*} [otherValues]
   *   Any additional values to be compared to <code>x</code> and <code>y</code>.
   * @returns {boolean}
   *   <code>true</code> if any of the parameters passed are not exactly the same,
   *   otherwise <code>false</code> is returned.
   */
  
  /**
   * Tests to see if two or more values are exactly the same. This is similar to
   * <code>===</code> but will also indicate that <code>NaN</code> is <code>NaN</code> and will indicate that
   * <code>-0</code> is not <code>0</code>.
   * @name is
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/is/2.23.0}
   * @param {*} x
   *   A value.
   * @param {*} y
   *   Value to be compared to <code>x</code> for equality.
   * @param {...*} [otherValues]
   *   Any additional values to be compared to <code>x</code> and <code>y</code>.
   * @returns {boolean}
   *   <code>true</code> if all of the parameters passed in are exactly the same, otherwise
   *   <code>false</code> is returned.
   */
  // // Original is():
  // function is() {
  //   for (
  //     var a, b, args = arguments, i = args.length;
  //     a = args[--i], b = args[i - 1], i && (a === a ? a === 0 ? 1 / a === 1 / b : a === b : b !== b);
  //   );
  //   return i < 1;
  // }
  eval('>Not<='.replace(/(\w*)(.=?)/g, 'function is$1(){for(var a,b,d=arguments,c=d.length;a=d[--c],b=d[c-1],c&&(a===a?0===a?1/a===1/b:a===b:b!==b););return 1$2c}'));
  
  /**
   * Creates a chained version of a value.
   * @name chain
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/chain/2.23.0}
   * @param {*} [value]
   *   Value to make chainable.
   * @returns {YourJSChain}
   *   A <code>YourJSChain</code> object allows for chaining other YourJS functions calls
   *   together and thusly all of its functions except the <code>value()</code> function
   *   will return a <code>YourJSChain</code> object.  You can call the <code>YourJSChain</code>'s
   *   <code>value()</code> function to return the actual internal value.  The
   *   <code>YourJSChain</code>'s <code>end()</code> function will return the previous <code>YourJSChain</code>
   *   object.
   */
  var chain, mixin;
  (function(RGX_INVALID_PROP_NAMES) {
    function YourJSChain(value, previousWrap) {
      this.$$ = previousWrap;
      this.$ = value;
    }
  
    function value() { return this.$; }
  
    YourJSChain.prototype = {
      _: function(name) {
        return new YourJSChain(this.$[name].apply(this.$, slice(arguments, 1)), this);
      },
      value: value,
      end: function() { return this.$$; },
      valueOf: value
    };
  
    __callsAfterDefs.push(function() {
      forIn(YourJS, function(prop, name) {
        if ('function' === typeof prop && !RGX_INVALID_PROP_NAMES.test(name)) {
          YourJSChain.prototype[name] = function() {
            return new YourJSChain(prop.apply(undefined, [this.$].concat(slice(arguments))), this);
          };
        }
      });
    });
  
    typeOf(YourJSChain, 'YourJSChain');
  
    chain = function(value) {
      return new YourJSChain(value);
    };
  
    mixin = function(fn, name) {
      if (RGX_INVALID_PROP_NAMES.test(name) || has(YourJS, name)) {
        return false;
      }
  
      YourJS[name] = fn;
      YourJSChain.prototype[name] = function() {
        return new YourJSChain(fn.apply(undefined, [this.$].concat(slice(arguments))), this);
      };
      return true;
    };
  })(/^(value|end|_|mixin|\$\$?)$/);
  
  /**
   * Takes an array, splits it up into chunks of a specified size and returns a
   * new array containing said array chunks.
   * @name chunk
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/chunk/2.23.0}
   * @param {Array} array
   *   The array that will be used to make the chunks.  This array will not be
   *   modified.
   * @param {number} [opt_chunkLimit=1]
   *   The maximum number of items in each chunk.  All chunks will contain this
   *   amount of items with the exception of the final chunk.
   * @returns {Array}
   *   An array of arrays where each subarray contains a chunk of the values
   *   from <code>array</code>.
   */
  function chunk(array, opt_size) {
    opt_size = opt_size >= 1 ? ~~opt_size : 1;
    for (var chunks = [], i = Math.floor((array.length - 1) / opt_size) * opt_size; i >= 0; i -= opt_size) {
      chunks.unshift(array.slice(i, i + opt_size));
    }
    return chunks;
  }
  
  /**
   * Confine a given number to a specific range.
   * @name clamp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/clamp/2.23.0}
   * @param {number} x
   *   The number to be confined to the specified range.
   * @param {number} min
   *   The minimum number that can be returned.
   * @param {number} max
   *   The maximum number that can be returned.
   * @returns {number}
   *   The closest number to <code>x</code> that is greater than or equal to <code>min</code> and less
   *   than or equal to <code>max</code>.
   */
  function clamp(x, min, max) {
    return x < min ? min : x > max ? max : x;
  }
  
  /**
   * Finds the closest multiple of a number to another number.
   * @name closestMultOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/closestMultOf/2.23.0}
   * @param {number} factor
   *   The number to find a multiple of.
   * @param {number} target
   *   The number that the multiple of <code>factor</code> should approximate.
   * @returns {number}
   *   A multiple of <code>factor</code> that approximates <code>target</code>.  If <code>factor</code> is 0 or
   *   not a number, <code>NaN</code> will be returned.
   */
  function closestMultOf(factor, target) {
    return Math.round(target / factor) * factor;
  }
  
  /**
   * Gets the number of full code points in a string.
   * @name countCodePoints
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/countCodePoints/2.23.0}
   * @param {string} string
   *   The string whose full code points will be counted.
   * @returns {number}
   *   The number of full code points in the given <code>string</code>.  This differs from
   *   <code>string.length</code> which simply counts the amount of characters in a string
   *   even if it contains 2 characters that form a code point.
   */
  // // ORIGINAL CODE FOR countCodePoints() FOUND IN toCodePoints()
  // // Based on polyfill for String#codePointAt():
  // // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/codePointAt#Polyfill
  // function countCodePoints(string) {
  //   for (var x, y, l = string.length, position = 0, a = []; position < l; position++) {
  //     // Get the first code unit
  //     x = string.charCodeAt(position);
  //     if ( // check if it’s the start of a surrogate pair
  //       x >= 55296 && x <= 56319 && // high surrogate
  //       l > position + 1 // there is a next code unit
  //     ) {
  //       y = string.charCodeAt(position + 1);
  //       if (y >= 56320 && y <= 57343) { // low surrogate
  //         // https://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
  //         x = (x - 55296) * 1024 + y - 56320 + 65536;
  //         position++;
  //       }
  //     }
  //     a.push(x);
  //   }
  //   return a.length;
  // }
  
  /**
   * Gets the sequence of code points that make up a specified string.
   * @name toCodePoints
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/toCodePoints/2.23.0}
   * @param {string} string
   *   The string from which to pull the sequence of code points.
   * @returns {Array<number>}
   *   An array of code points that make of the specified <code>string</code>.
   */
  // // ORIGINAL CODE FOR toCodePoints()
  // // Based on polyfill for String#codePointAt():
  // // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/codePointAt#Polyfill
  // function toCodePoints(string) {
  //   for (var x, y, l = string.length, position = 0, a = []; position < l; position++) {
  //     // Get the first code unit
  //     x = string.charCodeAt(position);
  //     if ( // check if it’s the start of a surrogate pair
  //       x >= 55296 && x <= 56319 && // high surrogate
  //       l > position + 1 // there is a next code unit
  //     ) {
  //       y = string.charCodeAt(position + 1);
  //       if (y >= 56320 && y <= 57343) { // low surrogate
  //         // https://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
  //         x = (x - 55296) * 1024 + y - 56320 + 65536;
  //         position++;
  //       }
  //     }
  //     a.push(x);
  //   }
  //   return a;
  // }
  eval('ZtoCYUXTWSVe}ZcountCYUXTWSVe.length}ZcYAt(d,b){X;b=0>b?f+b:b||0;WVa}'.replace(
    /[S-Z]/g,
    function (c) {
      return {
        Z: 'function ',
        Y: 'odePoint',
        X: 'var a,c,f=d.length',
        W: 'a=d.charCodeAt(b),55296<=a&&56319>=a&&f>b+1&&(c=d.charCodeAt(b+1),56320<=c&&57343>=c&&(a=1024*(a-55296)+c-56320+65536,b++))',
        V: ';return ',
        U: 's(d){for(',
        T: ',b=0,e=[];b<f;b++)',
        S: ',e.push(a)'
      }[c];
    }
  ));
  
  /**
   * Gets the code point at a specific position in the given string.
   * @name codePointAt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/codePointAt/2.23.0}
   * @param {string} string
   *   The string from which to pull the code point.
   * @param {number} position
   *   The position of the code point that you want to identify.  If the
   *   specified number is negative then the position will be counted from the
   *   end of the string.
   * @returns {number}
   *   The code point at the specified <code>position</code> in the given <code>string</code>.
   */
  // // ORIGINAL CODE FOR codePointAt() FOUND IN toCodePoints() CODE:
  // // Allows for a negative position.  Similar to String#codePointAt():
  // // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/codePointAt#Polyfill
  // function codePointAt(string, position) {
  //   var x, y, l = string.length;
  //   position = position < 0 ? position + l : (position || 0);
  //   // Get the first code unit
  //   x = string.charCodeAt(position);
  //   if ( // check if it’s the start of a surrogate pair
  //     x >= 55296 && x <= 56319 && // high surrogate
  //     l > position + 1 // there is a next code unit
  //   ) {
  //     y = string.charCodeAt(position + 1);
  //     if (y >= 56320 && y <= 57343) { // low surrogate
  //       // https://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
  //       x = (x - 55296) * 1024 + y - 56320 + 65536;
  //       position++;
  //     }
  //   }
  //   return x;
  // }
  
  /**
   * Takes a number in any form and returns it as a string in its fullest form
   * (removing exponents).
   * @name fullNumber
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/fullNumber/2.23.0}
   * @param {number} num
   *   Number to return in its fullest form.
   * @returns {string|undefined}
   *   If <code>num</code> is a finite number it will be returned as a number in its
   *   fullest form without exponents of any kind.  If <code>num</code> is not a finite
   *   number <code>undefined</code> will be returned.
   */
  function fullNumber(num) {
    var realNum = +num;
    if (isFinite(realNum)) {
      var match = /(\d)(?:\.(\d+))?e(\+|\-)(\d+)/.exec(realNum + '');
      if (match) {
        var end = match[2] || '';
        var startAndEnd = match[1] + end;
        var digits = +match[4];
        num = match[3] === '+'
          ? startAndEnd + Array(digits + 1 - end.length).join('0')
          : ('0.' + Array(digits).join('0') + startAndEnd);
      }
      return num + '';
    }
  }
  
  /**
   * Turns a number into a string in its fullest form with commas separating the
   * integral part at the thousands mark, millions mark, billions mark, etc.
   * @name commaNumber
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/commaNumber/2.23.0}
   * @param {number|string} num
   *   Number that will be converted to a string and will have commas added to
   *   it to separate the hundreds from thousands, the thousands from the
   *   millions, etc.
   * @returns {string|undefined}
   *   A string representation of <code>num</code> with commas separating the hundreds from
   *   thousands, the thousands from the millions, etc.  If <code>num</code> is not a
   *   finite number <code>undefined</code> will be returned.
   */
  function commaNumber(num) {
    if (num = fullNumber(num)) {
      num = num.split('.');
      num[0] = num[0].replace(/\d(?=(\d{3})+$)/g, '$&,');
      return num.join('.');
    }
  }
  
  /**
   * Determines if an object is an array or at least array-like.
   * @name isArrayLike
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isArrayLike/2.23.0}
   * @param {*} o
   *   A value to test and see if it array-like.
   * @returns {boolean}
   *   A boolean indicating if the object was an array or array-like.
   */
  function isArrayLike(o) {
    var l, i, t = nativeType(o);
    return t === 'Array'
        || (!!o
          && !o.nodeName
          && typeof(l = o.length) === 'number'
          && 'Function' !== t && 'String' !== t
          && (!l || (l > 0 && (i = l - 1) % 1 == 0 && i in o)));
  }
  
  /**
   * Creates a new object or a new array based on the specified object array.
   * This new array will only have the values which fail the test function.
   * @name filterOut
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterOut/2.23.0}
   * @param {Array|Object} arrOrObj
   *   Array (or array-like object) or object which will be duplicated with only
   *   the values that fail the <code>test</code> function.
   * @param {Function} test
   *   Function that will be called for every value in <code>arrOrObj</code> and whose
   *   return value for each call will be coerced to a boolean and used to
   *   determine if the value will be kept in the resulting array or object
   *   returned by <code>filter()</code>.  Only return values that are <code>false</code>-ish will be
   *   kept. The first argument passed will be the value that is being tested.
   *   The second argument passed will be the index or key of the value that is
   *   being tested.  The third argument passed will be <code>arrOrObj</code>.  The <code>this</code>
   *   keyword will reference <code>opt_this</code>.
   * @param {*} [opt_this=global]
   *   If not given or <code>undefined</code> or <code>null</code>, the global object will be used.
   *   If given as a primitive, the object version of the value will be used.
   *   This object will be used as <code>this</code> within <code>test()</code>.
   * @returns {Array|Object}
   *   If <code>arrOrObj</code> is an array (or an array-like object) then an array with
   *   the filtered values will be returned.  Otherwise an object with the
   *   filtered values will be returned.
   */
  // // ORIGINAL CODE FOR filterOut()
  // function filterOut(arrOrObj, test, opt_this) {
  //   if (isArrayLike(arrOrObj = Object(arrOrObj))) {
  //     for (var result = [], i = 0, l = arrOrObj.length; i < l; i++) {
  //       if (has(arrOrObj, i) && !test.call(opt_this, arrOrObj[i], i, arrOrObj)) {
  //         result.push(arrOrObj[i]);
  //       }
  //     }
  //   }
  //   else {
  //     var k, result = {};
  //     for (k in arrOrObj) {
  //       if (has(arrOrObj, k) && !test.call(opt_this, arrOrObj[k], k, arrOrObj)) {
  //         result[k] = arrOrObj[k];
  //       }
  //     }
  //   }
  //   return result;
  // }
  
  /**
   * Creates a new object or a new array based on the specified object array.
   * This new array will only have the values which pass the test function.
   * @name filter
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filter/2.23.0}
   * @param {Array|Object} arrOrObj
   *   Array (or array-like object) or object which will be duplicated with only
   *   the values that pass the <code>test</code> function.
   * @param {Function} test
   *   Function that will be called for every value in <code>arrOrObj</code> and whose
   *   return value for each call will be coerced to a boolean and used to
   *   determine if the value will be kept in the resulting array or object
   *   returned by <code>filter()</code>.  Only return values that are <code>true</code>-ish will be
   *   kept. The first argument passed will be the value that is being tested.
   *   The second argument passed will be the index or key of the value that is
   *   being tested.  The third argument passed will be <code>arrOrObj</code>.  The <code>this</code>
   *   keyword will reference <code>opt_this</code>.
   * @param {*} [opt_this=global]
   *   If not given or <code>undefined</code> or <code>null</code>, the global object will be used.
   *   If given as a primitive, the object version of the value will be used.
   *   This object will be used as <code>this</code> within <code>test()</code>.
   * @returns {Array|Object}
   *   If <code>arrOrObj</code> is an array (or an array-like object) then an array with
   *   the filtered values will be returned.  Otherwise an object with the
   *   filtered values will be returned.
   */
  // // ORIGINAL CODE FOR filter()
  // function filter(arrOrObj, test, opt_this) {
  //   if (isArrayLike(arrOrObj = Object(arrOrObj))) {
  //     for (var result = [], i = 0, l = arrOrObj.length; i < l; i++) {
  //       if (has(arrOrObj, i) && test.call(opt_this, arrOrObj[i], i, arrOrObj)) {
  //         result.push(arrOrObj[i]);
  //       }
  //     }
  //   }
  //   else {
  //     var k, result = {};
  //     for (k in arrOrObj) {
  //       if (has(arrOrObj, k) && test.call(opt_this, arrOrObj[k], k, arrOrObj)) {
  //         result[k] = arrOrObj[k];
  //       }
  //     }
  //   }
  //   return result;
  // }
  eval(
    'var filter,filterOut;(function(A,H){#;#})'
      .replace(/#/g, 'filter=function(a,d,e){if(A(a=Object(a)))for(var c=[],b=0,f=a.length;b<f;b++)@c.push(a[b]);else for(b in c={},a)@(c[b]=a[b]);return c}')
      .replace(/@/g, 'H(a,b)&&d.call(e,a[b],b,a)&&')
      .replace(/(=.+?)(d.c.+?)d.c/, 'Out$1!$2!d.c')
  )(isArrayLike, has);
  
  /**
   * Create a new array or object with all of the <code>false</code>-ish values removed.
   * @name compact
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/compact/2.23.0}
   * @param {Array|Object} arrOrObj
   *   Array or object of values to comb through.
   * @returns {Array|Object}
   *   If <code>arrOrObj</code> is an array or is array-like then an array will be
   *   returned.  Otherwise an object will be returned.  The returned array or
   *   object will be similar to <code>arrOrObj</code> but will only have <code>true</code>-ish
   *   values.
   */
  function compact(arrOrObj) {
    return filter(arrOrObj, function(value) { return value; });
  }
  
  /**
   * Compares 2 titles by splitting the titles into sections where each section
   * is either full of digits or non-digits.
   * @name compareTitle
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/compareTitle/2.23.0}
   * @param {string} title1
   *   First title to compare to <code>title2</code>.
   * @param {string} title2
   *   Second title to compare to <code>title1</code>.
   * @returns {number}
   *   If <code>title1</code> comes before <code>title2</code> a negative number will be returned.  If
   *   <code>title1</code> comes after <code>title2</code> a positive number will be returned.  In all
   *   other cases <code>0</code> will be returned.
   */
  function compareTitle(title1, title2) {
    var parts1, parts2, part1, part2, i, l, isDigits = /^\d/.test(title1 += '');
    if (isDigits === /^\d/.test(title2 += '') || (!/\d/.test(title1) && !/\d/.test(title2))) {
      l = Math.min(
        (parts1 = title1.match(/\D+|\d+/g)).length,
        (parts2 = title2.match(/\D+|\d+/g)).length
      );
      for (i = 0; i < l; i++, isDigits = !isDigits) {
        part1 = parts1[i];
        part2 = parts2[i];
        if (part1 !== part2) {
          return isDigits ? Math.ceil(part1) - Math.ceil(part2) : part1 < part2 ? -1 : 1;
        }
      }
    }
    return title1 < title2 ? -1 : title1 > title2 ? 1 : 0;
  }
  
  /**
   * Create a new function which is basically the composition of two functions.
   * @name compose
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/compose/2.23.0}
   * @param {Function} fnWrapper
   *   Function that will be passed the return value of <code>fnToWrap</code> and whose
   *   return value will be returned by the composed function.
   * @param {Function} fnToWrap
   *   Function that will be called 1st by te composed function and whose return
   *   value will be passed to <code>fnWrapper</code>.
   * @param {boolean} [opt_applyArgs=false]
   *   Set to <code>true</code> if the return value from <code>fnToWrap</code> is an <code>Array</code> and each
   *   element should be passed a separate argument to <code>fnWrapper</code>.  Leave blank
   *   or set to <code>false</code> if the return value from <code>fnToWrap</code> should just be
   *   passed as the 1st argument to <code>fnWrapper</code>.
   * @returns {Function}
   *   The function that is composed of <code>fnToWrap</code> and <code>fnWrapper</code>.
   */
  function compose(fnWrapper, fnToWrap, opt_applyArgs) {
    return Function('x,y', 'return function(){return x.' + (opt_applyArgs ? 'apply' : 'call') + '(this,y.apply(this,arguments))}')(fnWrapper, fnToWrap);
  }
  
  /**
   * Constructs an object with an arbitrary number of arguments passed via an
   * array (or an array-like object).
   * @name construct
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/construct/2.23.0}
   * @param {Function} constructor
   *   The constructor function.
   * @param {...*} [args]
   *   Zero or more arguments with which <code>constructor</code> will be used to construct
   *   the desired object type.
   * @returns {*}
   *   Returns the result of calling <code>new constructor(...args)</code>.
   */
  function construct(constructor) {
    return new(constructor.bind.apply(constructor, [null].concat(slice(arguments, 1))));
  }
  
  /**
   * Copies the specified properties from the source object to the target
   * object.
   * @name copyProps
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/copyProps/2.23.0}
   * @param {*} target
   *   Target object to which the properties will be copied.
   * @param {*} source
   *   Source object from which the properties will be copied.
   * @param {Array} keys
   *   Array of keys for the properties that should be copied from source to
   *   target.
   * @param {boolean} [opt_dontDelete=false]
   *   Indicates whether properties that dont exist should be deleted between
   *   objects.  If <code>true</code>, non-existent properties will be set to <code>undefined</code>.
   * @returns {*}
   *   Returns a reference to <code>target</code>.
   */
  function copyProps(target, source, props, opt_dontDelete) {
    if (target) {
      source = Object(source);
      for (var prop, i = props.length; i--;) {
        prop = props[i];
        if ((prop in source) || opt_dontDelete) {
          target[prop] = source[prop];
        }
        else {
          delete target[prop];
        }
      }
    }
    return target;
  }
  
  /**
   * Takes the sign of the number from the second argument and uses it as the
   * sign of the number given in the first argument.
   * @name copySign
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/copySign/2.23.0}
   * @param {number} number1
   *   The number that will be returned with the sign specified by <code>number2</code>.
   * @param {number} number2
   *   The number that has the sign (positive or negative) to use as the sign
   *   for <code>number1</code> when it is returned from this function.
   * @returns {number}
   *   <code>number1</code> will be returned but with the same sign (positive or negative)
   *   as the one specified for <code>number2</code>.
   */
  function copySign(a, b) {
    return (a < 0 || 1 / a < 0) === (b < 0 || 1 / b < 0) ? a : -a;
  }
  
  /**
   * Determines the cotangent of a given number either in radians or in degrees.
   * @name cot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/cot/2.23.0}
   * @param {number} angle
   *   The angle whose cotangent value should be returned.
   * @param {boolean} [opt_usingDegrees=false]
   *   If <code>true</code> then <code>angle</code> will be interpreted in degrees, otherwise <code>angle</code>
   *   will be interpreted in radians.
   * @returns {number}
   *   The cotangent of <code>angle</code> in radians by default, but if <code>opt_usingDegrees</code>
   *   is a <code>true</code>-ish value then the cotangent of <code>angle</code> in degrees will be
   *   returned.
   */
  
  /**
   * Determines the cosecant of a given number either in radians or in degrees.
   * @name csc
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/csc/2.23.0}
   * @param {number} angle
   *   The angle whose cosecant value should be returned.
   * @param {boolean} [opt_usingDegrees=false]
   *   If <code>true</code> then <code>angle</code> will be interpreted in degrees, otherwise <code>angle</code>
   *   will be interpreted in radians.
   * @returns {number}
   *   The cosecant of <code>angle</code> in radians by default, but if <code>opt_usingDegrees</code>
   *   is a <code>true</code>-ish value then the cosecant of <code>angle</code> in degrees will be
   *   returned.
   */
  
  /**
   * Determines the sine of a given number either in radians or in degrees.
   * @name sin
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/sin/2.23.0}
   * @param {number} angle
   *   The angle whose sine value should be returned.
   * @param {boolean} [opt_usingDegrees=false]
   *   If <code>true</code> then <code>angle</code> will be interpreted in degrees, otherwise <code>angle</code>
   *   will be interpreted in radians.
   * @returns {number}
   *   The sine of <code>angle</code> in radians by default, but if <code>opt_usingDegrees</code> is a
   *   <code>true</code>-ish value then the sine of <code>angle</code> in degrees will be returned.
   */
  
  /**
   * Determines the tangent of a given number either in radians or in degrees.
   * @name tan
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/tan/2.23.0}
   * @param {number} angle
   *   The angle whose tangent value should be returned.
   * @param {boolean} [opt_usingDegrees=false]
   *   If <code>true</code> then <code>angle</code> will be interpreted in degrees, otherwise <code>angle</code>
   *   will be interpreted in radians.
   * @returns {number}
   *   The tangent of <code>angle</code> in radians by default, but if <code>opt_usingDegrees</code>
   *   is a <code>true</code>-ish value then the tangent of <code>angle</code> in degrees will be
   *   returned.
   */
  
  /**
   * Determines the secant of a given number either in radians or in degrees.
   * @name sec
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/sec/2.23.0}
   * @param {number} angle
   *   The angle whose secant value should be returned.
   * @param {boolean} [opt_usingDegrees=false]
   *   If <code>true</code> then <code>angle</code> will be interpreted in degrees, otherwise <code>angle</code>
   *   will be interpreted in radians.
   * @returns {number}
   *   The secant of <code>angle</code> in radians by default, but if <code>opt_usingDegrees</code> is
   *   a <code>true</code>-ish value then the secant of <code>angle</code> in degrees will be
   *   returned.
   */
  eval('seccos;cscsin;cottan;'.replace(/(...)(?=(...)?;)/g, function(m, name, base, i) {
    return 'function ' + name + '(a,b){return ' + (base ? '1/' : '') + 'Math.' + (base || name) + '(b?a*'+Math.PI/180+':a)}';
  }));
  
  /**
   * Determines the cosine of a given number either in radians or in degrees.
   * @name cos
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/cos/2.23.0}
   * @param {number} angle
   *   The angle whose cosine value should be returned.
   * @param {boolean} [opt_usingDegrees=false]
   *   If <code>true</code> then <code>angle</code> will be interpreted in degrees, otherwise <code>angle</code>
   *   will be interpreted in radians.
   * @returns {number}
   *   The cosine of <code>angle</code> in radians by default, but if <code>opt_usingDegrees</code> is
   *   a <code>true</code>-ish value then the cosine of <code>angle</code> in degrees will be
   *   returned.
   */
  
  /**
   * Traverses all the characters in a string or all of the elements in an array
   * or an array-like object, executing a function on each one.
   * @name forEach
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/forEach/2.23.0}
   * @param {Array|Arguments|string|Object} a
   *   Array or array-like object or string to traverse.
   * @param {Function} fn
   *   Function called on each item in <code>a</code>.  The parameters passed are as
   *   follows:  (1) value, (2) key, (3) <code>a</code> and (4) ender function.  The <code>this</code>
   *   keyword will contain the number of times the function has been called.
   *   The ender function may be used to immediately stop traversing <code>a</code>.
   *   Calling the ender function with a <code>true</code>-ish value will instead make the
   *   ender function return a boolean value indicating if the ender function
   *   has already been called (useful only in a <code>catch</code> or <code>finally</code> clause).
   * @param {boolean} [opt_traverseAll=false]
   *   Boolean indicating if all items in <code>a</code>, including those not owned by <code>a</code>,
   *   should be traversed.
   * @returns {number}
   *   The number of times <code>fn</code> was called.
   */
  function forEach(a, fn, opt_traverseAll) {
    function end(opt_getIfEnded) {
      if(opt_getIfEnded) {
        return !!endCalled;
      }
      throw endCalled = 1;
    }
    for (var endCalled, count = 0, i = 0, l = a.length, arr = nativeType(a) === 'String' ? a.split('') : a; i < l; i++) {
      if (opt_traverseAll || has(arr, i)) {
        try {
          fn.call(++count, arr[i], i, a, end);
        }
        catch (e) {
          if (endCalled) {
            break;
          }
          throw e;
        }
      }
    }
    return count;
  }
  
  /**
   * Traverses all the characters in a string or all of the items in an array or
   * an object, executing a function on each one.
   * @name forOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/forOf/2.23.0}
   * @param {Array|Object|string} obj
   *   If is an array, an array-like object, or an object, its items will be
   *   traversed.  If it is a string its characters will be individual
   *   traversed.
   * @param {Function} fn
   *   Function to call for each item traversed within <code>obj</code>.  The parameters
   *   passed are as follows:  (1) value, (2) key, (3) <code>obj</code> and (4) ender
   *   function.  The <code>this</code> keyword will contain the number of times this <code>fn</code>
   *   function has been called.  The ender function may be used to immediately
   *   stop traversing <code>obj</code>.  Calling the ender function with a <code>true</code>-ish
   *   value will instead make the ender function return a boolean value
   *   indicating if the ender function has already been called (useful only in
   *   a <code>catch</code> or <code>finally</code> clause).
   * @param {boolean} [opt_traverseAll=false]
   *   Boolean indicating whether or not even the properties that are not owned
   *   by <code>obj</code> should be traversed.  If <code>true</code> all properties will be
   *   traversed.
   * @returns {number}
   *   Number of times <code>fn</code> was called.
   */
  function forOf(obj, fn, opt_traverseAll) {
    return (
      (isArrayLike(obj) || nativeType(obj) === 'String') ? forEach : forIn
    )(Object(obj), fn, opt_traverseAll);
  }
  
  /**
   * Count how many items in an array or an object pass a given tester function.
   * @name count
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/count/2.23.0}
   * @param {Array|Object|string} arrOrObj
   *   If this is an array or an object all of the items that pass the <code>tester</code>
   *   function will be counted.  If this is a string all of the characters that
   *   pass the <code>tester</code> function will be counted.
   * @param {Function} tester
   *   Function that will take in (1) each value, (2) the corresponding key, and
   *   (3) a reference to <code>arrOrObj</code>.  If the function returns a <code>true</code>-ish
   *   value the count will be incremented.
   * @param {boolean} [opt_traverseAll=false]
   *   Indicates whether or not all items, including the non-owned ones, should
   *   be traversed and included in the count.
   * @returns {number}
   *   The number of times <code>tester</code> returned a <code>true</code>-ish value.
   */
  function count(arrOrObj, tester, opt_traverseAll) {
    var count = 0, isStrTester = nativeType(tester) === 'String';
    forOf(arrOrObj, function(value, key, arrOrObj) {
      if (tester(value, key, arrOrObj)) {
        count++;
      }
    }, opt_traverseAll);
    return count;
  }
  
  /**
   * Takes an array and groups the items together based on a given hasher
   * function.
   * @name groupBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/groupBy/2.23.0}
   * @param {Array} arr
   *   Array containing the items to be grouped by the <code>hasher</code> function.
   * @param {Function|Array|string} hasher
   *   If a function, it will be passed a value which will be used to group the
   *   items in <code>arr</code>.  If an array or a string it will be seen as the path
   *   within each item in <code>arr</code> to the property by which to group each item.
   * @param {*} [opt_initial={}]
   *   Object that will be augmented and returned.  The values at the keys
   *   produced by <code>hasher</code> will be arrays containing all of the corresponding
   *   items from <code>arr</code>.  If not specified the default is a new empty object.
   * @returns {*}
   *   <code>opt_initial</code> with all of the keys specified by <code>hasher</code> corresponding to
   *   arrays containing those values that come from <code>arr</code>.
   */
  
  /**
   * Traverses an array and returns an object with the values from the array
   * placed in the specified indices.
   * @name indexBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/indexBy/2.23.0}
   * @param {Array} arr
   *   Values to be placed in the returned object.
   * @param {Function|Array|string} indexer
   *   If a function, it will be passed a value which will be used to index the
   *   items in <code>arr</code>.  If an array or a string it will be seen as the path
   *   within each item in <code>arr</code> to the property by which to index each item.
   * @param {Object} [opt_initial={}]
   *   The object that will be augmented and returned.
   * @returns {Array}
   *   <code>opt_initial</code> with additional properties names coming from the return
   *   values of <code>indexer</code> and the items found in <code>arr</code>.
   */
  
  /**
   * Creates or augments an object, making new properties that correspond to
   * values in the given array.  Each property added will indicate how many
   * items in the array corresponding to those property names.
   * @name countBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/countBy/2.23.0}
   * @param {Array} arr
   *   Array or array-like object containing items to group together and count.
   * @param {Function|Array|string} grouper
   *   If a function, it will be passed a value which will be used to group the
   *   items in <code>arr</code>.  If an array or a string it will be seen as the path
   *   within each item in <code>arr</code> to the property by which to group each item.
   * @param {Object} [opt_initial={}]
   *   Object to augment with property names that will contain counts of how
   *   many items correspond to the values returned by <code>grouper</code>.
   * @returns {Object}
   *   <code>opt_initial</code> with property names that will contain counts of how many
   *   items correspond to the values returned by <code>grouper</code>.
   */
  var countBy = Function('G,H,T', ('return[@"number"!==typeof c[f=g?d(b[a],a,b):G(b[a],d)]&&(c[f]=0),c[f]++#,@"Array"!==T(c[f=g?d(b[a],a,b):G(b[a],d)])&&(c[f]=[]),c[f].push(b[a])#,@f=c[g?d(b[a],a,b):G(b[a],d)]=b[a]#]').replace(/@([^#]+)#/g, 'function(b,d,c){c=c||{};d="string"===typeof d?[d]:d;for(var f,g="function"===typeof d,a=0,h=b.length;a<h;a++)H(b,a)&&($1);return c}'))(getAt, has, nativeType),
      groupBy = countBy[1],
      indexBy = countBy[2];
  countBy = countBy[0];
  
  /**
   * Creates a CSS stylesheet from an JSON representation of a stylesheet.
   * @name css
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/css/2.23.0}
   * @param {Object} objStyles
   *   An object representing the CSS rules to be inserted into the document.
   *   Property names will be used as media queries if they start with <code>"@media
   *   "</code>. Property names will be used as rule selectors if the value is an
   *   object. If a property name is to be used as a selector, if any selectors
   *   don't contain <code>&</code>, <code>"& "</code> will be prepended to it. For all selectors, <code>&</code>
   *   will be replaced recursively with the selectors found in the parent. CSS
   *   property names will be uncamelcased by inserting dashes before each
   *   uppercased character and lower casing all characters. If a value is
   *   <code>null</code> or <code>undefined</code>, it will be turned into <code>"none"</code>. If a value is a
   *   number other than <code>0</code>, <code>"px"</code> will be appended to it. If a value is an
   *   array all of the items will be concatenated together, using <code>","</code> to
   *   delimit the values. If a value ends with <code>!</code> it will be replaced with
   *   <code>"!important"</code>.
   * @param {HTMLElement|Array.<HTMLElement>|string} opt_ancestors
   *   This can be an element or an array of elements which will get another
   *   class added to target all rules to it and its children. This can
   *   alternatively be a CSS path (selector) specifying the root on which all
   *   CSS rules created should be based.
   * @returns {HTMLStyleSheet}
   *   The stylesheet that is created and appended to the document is returned.
   */
  var css;
  (function(RGX_UPPER, RGX_AMP, RGX_NO_COMMAS_OR_NOTHING, RGX_NO_AMP, RGX_IND_SEL, RGX_CLS, RGX_TRIM_SELS) {
    css = function(obj, selAncestors) {
      if (typeof selAncestors != 'string') {
        if (selAncestors) {
          var className = ('_' + Math.random()).replace(RGX_CLS, +new Date);
          selAncestors = nativeType(selAncestors) == 'Array' ? selAncestors : [selAncestors];
          for (var i = selAncestors.length; i--;) {
            selAncestors[i].className += ' ' + className;
          }
        }
        selAncestors = className ? '.' + className : '';
      }
  
      var code = getCssCode(obj, selAncestors);
      var style = __DOCUMENT.createElement('style');
      style.type = 'text/css';
      if (style.styleSheet && !style.sheet) {
        style.styleSheet.cssText = code;
      }
      else {
        style.appendChild(__DOCUMENT.createTextNode(code));
      }
      (__DOCUMENT.getElementsByTagName('head')[0] || __DOCUMENT.body).appendChild(style);
      return style;
    }
  
    function getCssCode(obj, selAncestors) {
      var rules = [];
      var rule = [];
      for (var key in obj) {
        if (has(obj, key)) {
          var value = obj[key];
          var typeName = nativeType(value);
          if (!key.indexOf('@media ')) {
            rules.push(key + '{' + getCssCode(value, selAncestors) + '}');
          }
          else if (typeName === 'Object') {
            // Trim selectors
            key = key.replace(RGX_TRIM_SELS, '$1');
            // Return all selectors
            key = key.replace(RGX_IND_SEL, function(sel) {
              sel = selAncestors ? sel.replace(RGX_NO_AMP, '& $&') : sel;
              return selAncestors.replace(RGX_NO_COMMAS_OR_NOTHING, function(selAncestor) {
                return sel.replace(RGX_AMP, selAncestor);
              });
            });
            rules.push(getCssCode(value, key));
          }
          else {
            value = typeName !== 'Array'
              ? value != undefined
                ? value && typeof value == 'number'
                  ? value + 'px'
                  : ((value + '').slice(-1) == '!' ? value + 'important' : value)
                : 'none'
              : value.join(',');
            key = key.replace(RGX_UPPER, '-$&').toLowerCase();
            rule.push(key + ':' + value + ';');
          }
        }
      }
      if (rule[0]) {
          rules.unshift(selAncestors + '{' + rule.join('') + '}');
      }
      return rules.join('');
    }
  })(
    /[A-Z]/g,                                       // RGX_UPPER
    /&/g,                                           // RGX_AMP
    /[^,]+|^$/g,                                    // RGX_NO_COMMAS_OR_NOTHING
    /^[^&]+$/,                                      // RGX_NO_AMP
    /[^\s\xa0,][^,]*/g,                             // RGX_IND_SEL
    /0(.|$)/,                                       // RGX_CLS
    /^[\s\xa0]+|[\s\xa0]*(,)[\s\xa0]*|[\s\xa0]+$/g  // RGX_TRIM_SELS
  );
  
  /**
   * Creates a wrapper function that will only execute the given function once
   * the number of arguments passed is at least a given amount.  If the wrapper
   * is called with too few arguments, those arguments will be recursively
   * stored within a new wrapper function.
   * @name curry
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/curry/2.23.0}
   * @param {Function} fn
   *   Function to be executed once enough arguments have been passed.
   * @param {number} [opt_argsNeeded=fn.length]
   *   Number of arguments needed to successfully invoke this function.
   * @param {boolean} [opt_curryRight=false]
   *   Indicates whether returned partial functions will group the arguments
   *   recursively from right-to-left.
   * @param {Array} [opt_presetArgs=[]]
   *   Arguments that will always be passed to <code>fn</code>.  If <code>opt_curryRight</code> is
   *   <code>true</code> any arguments passed to the wrapper function will precede these
   *   arguments when invoking the function, otherwise any arguments passed to
   *   the wrapper function will come after these arguments when invoking the
   *   function.
   * @returns {Function}
   *   A wrapper function that will recursively create wrapper functions upon
   *   invocation until the cumulative number of arguments (excluding those
   *   specified in <code>opt_presetArgs</code>) amounts to <code>opt_argsNeeded</code>.  If the
   *   minimum number of arguments are passed to this function it will invoke
   *   <code>fn</code> with all of the arguments passed in.
   */
  function curry(fn, opt_argsNeeded, opt_curryRight, opt_presetArgs) {
    opt_presetArgs = opt_presetArgs || [];
    opt_argsNeeded = opt_argsNeeded == undefined ? fn.length - opt_presetArgs.length : opt_argsNeeded;
    return function() {
      var inArgs = slice(arguments),
          args = opt_curryRight ? inArgs.concat(opt_presetArgs) : opt_presetArgs.concat(inArgs),
          newArgsNeeded = opt_argsNeeded - inArgs.length;
      return newArgsNeeded > 0
        ? curry(fn, newArgsNeeded, opt_curryRight, args)
        : fn.apply(this, args);
    };
  }
  
  /**
   * Cut out a substring within a given range.
   * @name cut
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/cut/2.23.0}
   * @param {string} str
   *   String to cut from.
   * @param {number} startIndex
   *   Index within <code>str</code> where the substring that will be removed starts.
   * @param {number} [opt_endIndex=str.length]
   *   Index within <code>str</code> where the substring that will be removed ends.
   * @returns {string}
   *   <code>str</code> without the cut substring found between <code>startIndex</code> and
   *   <code>opt_endIndex</code>.
   */
  function cut(str, startIndex, opt_endIndex) {
    var len = str.length;
    if (opt_endIndex == undefined) {
      opt_endIndex = Infinity;
    }
    else if (opt_endIndex < 0) {
      opt_endIndex = opt_endIndex < -len ? 0 : (len + opt_endIndex);
    }
    if (startIndex < 0) {
      startIndex = startIndex < -len ? 0 : (len + startIndex);
    }
    if (opt_endIndex < startIndex) {
      opt_endIndex = startIndex;
    }
    return str.slice(0, startIndex) + str.slice(opt_endIndex);
  }
  
  /**
   * Creates a throttled version of a function which will only be executable
   * once per every specified wait period.
   * @name throttle
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/throttle/2.23.0}
   * @param {Function} fn
   *   The function which will esentially be rate-limited.
   * @param {number} msBetweenCalls
   *   The amount of time to wait between calls.
   * @param {boolean} [opt_leading=undefined]
   *   If <code>true</code> the <code>fn</code> function will only be invoked on the initial calls if
   *   the specified time has elapsed since the last invocation.  If <code>false</code> the
   *   <code>fn</code> function will only be invoked on the trailing calls.  If not
   *   specified or if specified as <code>null</code> or <code>undefined</code> the initial calls and
   *   the trailing calls will be used to invoke <code>fn</code>.
   * @returns {Function}
   *   The throttled version of <code>fn</code> function.
   */
  
  /**
   * Creates a debounced copy of the function which when called will delay the
   * execution until the specified amount of milliseconds have elapsed since the
   * last time it was called.
   * @name debounce
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/debounce/2.23.0}
   * @param {Function} fnCallback
   *   The function to debounce. The context and parameters sent to the
   *   debounced function will be sent to this function.
   * @param {number} msWait
   *   The amount of milliseconds that must pass since the last call to this
   *   function before really invoking <code>fnCallback</code>.
   * @param {boolean} [opt_initialAndAfter=undefined]
   *   Specify <code>true</code> if both the initial calls and the trailing calls should
   *   invoke the <code>fnCallback</code> function.  Specify <code>false</code> if only the initial
   *   calls should invoke the <code>fnCallback</code> function.  If not specified or
   *   specified as <code>null</code> or <code>undefined</code> only the trailing calls will invoke
   *   <code>fnCallback</code>.
   * @returns {Function}
   *   The debounced copy of <code>fnCallback</code>.
   */
  eval('debounce231k=b;b=@;##b-k##@-b#throttle312#k=@,#k-b#b=k,#k-b#'.replace(
    /(\D+)(.)(.)(.)([^#]*)#([^#]*)#([^#]*)#([^#]*)#([^#]*)#/g,
    'function $1(h,c,a){var u,d,e,f,g,k,b=0;a=u==a?$2:a?$3:$4;return function(){d=0;e=arguments;f=this;$5a&1&&($6d=$7>=c)&&($8g=h.apply(f,e));!d&&a&2&&setTimeout(function(){k=@,k-b>=c&&(b=k,g=h.apply(f,e))},c);return g}}'
  ).replace(/@/g, '+new Date'));
  
  /**
   * Converts supplementary latin letters into basic latin letters, removing the
   * diacritical marks.  In other words this normalizes a string.
   * @name deburr
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/deburr/2.23.0}
   * @param {string} str
   *   The string to be modified and returned without supplementary latin
   *   letters.
   * @returns {string}
   *   Returns a copy of <code>str</code> but with all of the following characters replaced
   *   by their corresponding basic latin letters:  <code>À</code>, <code>Á</code>, <code>Â</code>, <code>Ã</code>, <code>Ä</code>,
   *   <code>Å</code>, <code>Ā</code> convert to <code>A</code>.  <code>à</code>, <code>á</code>, <code>â</code>, <code>ã</code>, <code>ä</code>, <code>å</code>, <code>ā</code> convert to
   *   <code>a</code>.  <code>Ç</code>, <code>Ć</code>, <code>Č</code> convert to <code>C</code>.  <code>ç</code>, <code>ć</code>, <code>č</code> convert to <code>c</code>.  <code>Ð</code>,
   *   <code>Đ</code> convert to <code>D</code>.  <code>ð</code>, <code>đ</code> convert to <code>d</code>.  <code>È</code>, <code>É</code>, <code>Ê</code>, <code>Ë</code>, <code>Ē</code>,
   *   <code>Ė</code>, <code>Ę</code> convert to <code>E</code>.  <code>è</code>, <code>é</code>, <code>ê</code>, <code>ë</code>, <code>ē</code>, <code>ė</code>, <code>ę</code> convert to
   *   <code>e</code>.  <code>Ì</code>, <code>Í</code>, <code>Î</code>, <code>Ï</code>, <code>Ī</code>, <code>Į</code> convert to <code>I</code>.  <code>ì</code>, <code>í</code>, <code>î</code>, <code>ï</code>,
   *   <code>ī</code>, <code>į</code> convert to <code>i</code>.  <code>Ñ</code>, <code>Ń</code> convert to <code>N</code>.  <code>ñ</code>, <code>ń</code> convert to
   *   <code>n</code>.  <code>Ò</code>, <code>Ó</code>, <code>Ô</code>, <code>Õ</code>, <code>Ö</code>, <code>Ø</code>, <code>Ō</code> convert to <code>O</code>.  <code>ò</code>, <code>ó</code>, <code>ô</code>,
   *   <code>õ</code>, <code>ö</code>, <code>ø</code>, <code>ō</code> convert to <code>o</code>.  <code>Ù</code>, <code>Ú</code>, <code>Û</code>, <code>Ü</code>, <code>Ū</code> convert to
   *   <code>U</code>.  <code>ù</code>, <code>ú</code>, <code>û</code>, <code>ü</code>, <code>ū</code> convert to <code>u</code>.  <code>Ý</code>, <code>Ÿ</code> convert to <code>Y</code>.
   *   <code>ý</code>, <code>ÿ</code> convert to <code>y</code>.  <code>Æ</code> converts to <code>Ae</code>.  <code>æ</code> converts to <code>ae</code>.
   *   <code>Þ</code> converts to <code>Th</code>.  <code>þ</code> converts to <code>th</code>.  <code>ß</code> converts to <code>ss</code>.  <code>Œ</code>
   *   converts to <code>Oe</code>.  <code>œ</code> converts to <code>oe</code>.  <code>Ł</code> converts to <code>L</code>.  <code>ł</code>
   *   converts to <code>l</code>.
   */
  var deburr;
  (function(RGX_DEBURR, ARR_DEBURR, RGX_NON_SPACE) {
    function replaceDeburrChar() {
      return ARR_DEBURR[slice(arguments, 1).join(' ').search(RGX_NON_SPACE, '')];
    }
    deburr = function(str) {
      return str.replace(RGX_DEBURR, replaceDeburrChar);
    };
  })(
    /([\xc0-\xc5\u0100])|([\xe0-\xe5\u0101])|([\xc7\u0106\u010c])|([\xe7\u0107\u010d])|(\xd0\u0110)|(\xf0\u0111)|([\xc8-\xcb\u0112\u0116\u0118])|([\xe8-\xeb\u0113\u0117\u0119])|([\xcc-\xcf\u012a\u012e])|([\xec-\xef\u012b\u012f])|([\xd1\u0143])|([\xf1\u0144])|([\xd2-\xd6\xd8\u014c])|([\xf2-\xf6\xf8\u014d])|([\xd9-\xdc\u016a])|([\xf9-\xfc\u016b])|([\xdd\u0178])|([\xfd\xff])|(\xc6)|(\xe6)|(\xde)|(\xfe)|(\xdf)|(\u0152)|(\u0153)|(\u0141)|(\u0142)|([\u02B9-\u036F])/g,
    'A,a,C,c,D,d,E,e,I,i,N,n,O,o,U,u,Y,y,Ae,ae,Th,th,ss,Oe,oe,L,l,'.split(','),
    /\S/
  );
  
  /**
   * Defers calling a function until the current call stack is done.  Similar to
   * using the global <code>setTimeout</code> function.
   * @name defer
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/defer/2.23.0}
   * @param {Function} fn
   *   Function whose call will be deferred until after all other calls in the
   *   current stack are done.
   * @param {Array} [opt_args=[]]
   *   Array of the arguments to be passed to <code>fn</code>.
   * @returns {number}
   *   The <code>timeoutID</code> associated with the deferred call.  If passed to the
   *   global <code>clearTimeout</code> while the function is being deferred the deferred
   *   function will not be called.
   */
  function defer(fn, opt_args) {
    var objThis = this;
    opt_args = slice(opt_args || []);
    return setTimeout(function() { fn.apply(objThis, opt_args); }, 0);
  }
  
  /**
   * Converts radians to degrees.
   * @name degrees
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/degrees/2.23.0}
   * @param {number} radians
   *   The number representing radians of in angle which will be converted to
   *   degrees.
   * @returns {number}
   *   The equivalent of <code>radians</code> in degrees.
   */
  function degrees(radians) {
    return radians * 180 / Math.PI;
  }
  
  /**
   * Calls a function after a given amount of time.  This is very similar to the
   * global <code>setTimeout</code> function.
   * @name delay
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/delay/2.23.0}
   * @param {Function} fn
   *   Function to call after a certain amount of time.
   * @param {number} msDelay
   *   Delay in milliseconds before <code>fn</code> is actually called.
   * @param {Array} [opt_args=[]]
   *   An array of the arguments to pass to <code>fn</code>.
   * @returns {number}
   *   The <code>timeoutID</code> corresponding to this delayed function call.  The global
   *   <code>clearTimeout</code> function can be used to cancel this delayed call.
   */
  function delay(fn, msDelay, opt_args) {
    var objThis = this;
    opt_args = slice(opt_args || []);
    return setTimeout(function() { fn.apply(objThis, opt_args); }, msDelay);
  }
  
  /**
   * Tries to delete the value at the specified <code>path</code> starting at <code>root</code>.
   * @name deleteAt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/deleteAt/2.23.0}
   * @param {*} root
   *   Root object at which the <code>path</code> to delete from will start.
   * @param {Array} path
   *   Array of numbers/strings that represents the path to the value that
   *   should be deleted.
   * @returns {boolean}
   *   <code>true</code> if the value can be deleted from the specified <code>path</code> under
   *   <code>root</code>, otherwise <code>false</code>.
   */
  function deleteAt(root, path) {
    var objs = [root].concat(testAt(root, path)),
        l = path.length - 1;
    return (l + 1 && objs.length - 2 === l) ? delete objs[l][path[l]] : false;
  }
  
  /**
   * Deletes one or more keys from an object of some sort.
   * @name deleteKeys
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/deleteKeys/2.23.0}
   * @param {Object|undefined|null} obj
   *   If <code>undefined</code> or <code>null</code>, a partial function waiting for this value will
   *   be returned.  Otherwise this will be the object from which the specified
   *   keys will be deleted.
   * @param {Arrays} keys
   *   Array of keys to be deleted from <code>obj</code>.
   * @returns {Function|Object}
   *   If <code>obj</code> is <code>undefined</code> or <code>null</code> this will be a partial function which
   *   will simply accept the given <code>obj</code>.  The return value of the partial
   *   function is the same as the return value of this function in the case
   *   that <code>obj</code> is not <code>undefined</code> or <code>null</code>.  All keys that are found and
   *   successfully deleted will be returned within an object.
   */
  function deleteKeys(obj, keys) {
    function deleteKeys(obj) {
      obj = Object(obj);
      for (var value, key, result = {}, i = keys.length; i--; ) {
        if (has(obj, key = keys[i])) {
          value = obj[key];
          if (delete obj[key]) {
            result[key] = value;
          }
        }
      }
      return result;
    }
    return obj == undefined ? deleteKeys : deleteKeys(obj);
  }
  
  /**
   * Dices up a string splitting it on the passed in delimiter and placing the
   * results in an array while making sure that the empty string is converted to
   * an empty array.
   * @name dice
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/dice/2.23.0}
   * @param {string} str
   *   The string to be diced and turned into an array of strings.
   * @param {RegExp|string} delim
   *   The regular expression or string to be used as a delimiter in order to
   *   dice up <code>str</code>. The matched substrings will not be included in the strings
   *   returned in the array.
   * @param {number} [opt_limit=Infinity]
   *   If specified, this number represents the maximum number of times <code>str</code>
   *   will be split on <code>delim</code>. Unlike with <code>String.prototype.split()</code>, the
   *   remaining substring after this limit will simply be placed in the last
   *   element of the array.
   * @returns {Array.<string>}
   *   Returns an array of substrings formed by dicing (splitting) up <code>str</code> by
   *   <code>delim</code>. If <code>str</code> is an empty string, an empty array will be returned.
   */
  function dice(str, delim, opt_limit) {
    var arr = [], start = 0, i;
    if (str) {
      str.replace(
        (typeOf(delim) == 'RegExp' ? modRegExp : quoteRegExp)(delim, 'g'),
        function(m) {
          i = arguments;
          i = i[i.length - 2];
          if (!(opt_limit-- < 1)) {
            arr.push(str.slice(start, i));
            start = i + (m.length || 1);
          }
        }
      );
      arr.push(str.slice(start));
    }
    return arr;
  }
  
  /**
   * Creates a new array which is the difference of the two given arrays.
   * @name diffArrays
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/diffArrays/2.23.0}
   * @param {Array} array1
   *   Array containing the values that will be returned if they are not found
   *   in <code>array2</code>.
   * @param {Array} array2
   *   Array whose values will be compared to those in <code>array1</code> and only those
   *   that appear in <code>array1</code> and not in this array will be returned.
   * @returns {Array}
   *   A new array with values from <code>array1</code> that are not in <code>array2</code>.
   */
  function diffArrays(array1, array2) {
    array1 = slice(array1);
    array2 = slice(array2);
    for (var e1, i1 = 0, l1 = array1.length, i2, l2 = array2.length; i1 < l1; i1++) {
      e1 = array1[i1];
      for (i2 = 0; i2 < l2; i2++) {
        if (e1 === array2[i2]) {
          l1--;
          array1.splice(i1--, 1);
          l2--;
          array2.splice(i2--, 1);
          break;
        }
      }
    }
    return array1;
  }
  
  /**
   * Finds the differents between 2 dates.
   * @name diffDates
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/diffDates/2.23.0}
   * @param {Date} dEnd
   *   Ending date.
   * @param {Date} dStart
   *   Starting date.
   * @param {boolean} [opt_onlyRemainder=false]
   *   Determines whether or not the values returned will show the remainder
   *   values instead of the full values for each unit of measure.
   * @returns {Object}
   *   An object containing all of the following measurements in the
   *   corresponding object properties: <code>years</code>, <code>months</code>, <code>days</code>, <code>hours</code>,
   *   <code>minutes</code> and <code>seconds</code>.  If <code>opt_onlyRemainder</code> is specified as <code>true</code>
   *   all of the object's properties will not include the measures for the next
   *   highest unit of measure.  If <code>op_onlyRemainder</code> is supplied <code>weekDays</code>
   *   and <code>weeks</code> are added to this returned object.  If <code>opt_onlyRemainder</code> is
   *   not specified or is <code>false</code> all of the object's properties will
   *   individual calculations indicating the difference in time.
   */
  function diffDates(dEnd, dStart, opt_onlyRemainder) {
    var t = dEnd < dStart, multiplier = t ? -1 : 1;
    if (t) {
      t = dEnd;
      dEnd = dStart;
      dStart = t;
    }
  
    var years = dEnd.getFullYear() - dStart.getFullYear();
    t = new Date(+dStart);
    t.setFullYear(dEnd.getFullYear());
    if (t.getDate() != dStart.getDate() || t.getMonth() != dStart.getMonth()) {
      t.setDate(0);
    }
    if (dEnd < t) {
      years--;
      t.setFullYear(dEnd.getFullYear() - 1);
      t.setDate(dStart.getDate());
      if (t.getDate() != dStart.getDate()) {
        t.setDate(0);
      }
    }
    
    var months = (opt_onlyRemainder ? 0 : (years * 12)) + dEnd.getMonth() - dStart.getMonth();
    t = new Date(+dEnd);
    t.setFullYear(dStart.getFullYear());
    t.setDate(dEnd.getDate());
    t.setMonth(dStart.getMonth());
    if (t.getDate() != dEnd.getDate() || t.getMonth() != dStart.getMonth()) {
      t.setDate(0);
    }
    if (dStart > t) {
      --months;
      t.setDate(dEnd.getDate());
      t.setMonth(dStart.getMonth() + 1);
      if (t.getDate() != dEnd.getDate()) {
        t.setDate(0);
      }
    }
    if (opt_onlyRemainder || months < 0) {
      months = (months + 12) % 12;
    }
    
    t = (opt_onlyRemainder ? t : dEnd) - dStart;
  
    var days = Math.floor(t / 864e5);
    var hours = Math.floor(t / 36e5) % (opt_onlyRemainder ? 24 : Infinity);
    var minutes = Math.floor(t / 6e4) % (opt_onlyRemainder ? 60 : Infinity);
    var seconds = Math.floor(t / 1e3) % (opt_onlyRemainder ? 60 : Infinity);
    var ms = t % (opt_onlyRemainder ? 1e3 : Infinity);
  
    t = {
      start: multiplier + 1 ? dStart : dEnd,
      end: multiplier + 1 ? dEnd : dStart,
      years: multiplier * years,
      months: multiplier * months,
      days: multiplier * days,
      hours: multiplier * hours,
      minutes: multiplier * minutes,
      seconds: multiplier * seconds,
      milliseconds: multiplier * ms
    };
    if (opt_onlyRemainder) {
      t.weekDays = t.days % 7;
      t.weeks = Math.floor(multiplier * t.days / 7) * multiplier;
    }
    return t;
  }
  
  /**
   * Execute a function every so often.  Similar to <code>setTimeout</code>.
   * @name doEvery
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/doEvery/2.23.0}
   * @param {Function} fn
   *   Function to be executed every so often.
   * @param {number} msDelay
   *   Number of milliseconds between calls to <code>fn</code>.
   * @param {Array} [opt_args=[]]
   *   Arguments to be passed to <code>fn</code> every time it is called.
   * @param {boolean} [opt_immediate=false]
   *   Specifies whether or not an immediate call should occur.
   * @returns {number}
   *   The <code>timeoutID</code> associated with the ongoing process of running this
   *   function.  To stop calling <code>fn</code> you can use <code>clearInterval(timeoutID)</code>.
   */
  function doEvery(fn, msDelay, opt_args, opt_immediate) {
    function caller() { fn.apply(objThis, opt_args); }
    var objThis = this;
    opt_args = slice(opt_args || []);
    if (opt_immediate) {
      try { caller(); }
      catch (e) { setTimeout(function(){throw e;}, 0); }
    }
    return setInterval(caller, msDelay);
  }
  
  /**
   * Turns anything into an array.
   * @name toArray
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/toArray/2.23.0}
   * @param {*} [o]
   *   Any value to either convert to an array or wrap in an array.
   * @returns {Array}
   *   If no arguments were passed an empty array is returned.  If the argument
   *   passed was array-like it will be returned as an <code>Array</code>.  Otherwise the
   *   argument passed will simply be returned within an array.
   */
  function toArray(o) {
    return arguments.length ? isArrayLike(o) ? slice(o) : [o] : [];
  }
  
  /**
   * Creates HTML DOM objects.
   * @name dom
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/dom/2.23.0}
   * @param {string|Object} input
   *   The value that will be converted to DOM objects.  If a string, it will be
   *   interpreted as raw HTML.  Otherwise this must be an object specifying at
   *   least the <code>nodeName</code> (or <code>_</code>) property.  To represent an element with
   *   child nodes you can set the <code>children</code> (or <code>$</code>) property to an <code>input</code>
   *   object, <code>input</code> string, or array of <code>input</code> strings and/or objects that
   *   will be recursively interpreted.  The <code>html</code> property will be interpreted
   *   as the <code>innerHTML</code> property.  The <code>text</code> property will be interpreted ass
   *   the <code>innerText</code> and <code>textContent</code> properties.  The <code>cls</code> property will be
   *   interpreted as the element's class name.
   * @returns {Array.<Node>|HTMLElement}
   *   If <code>input</code> was a string an array of nodes represented in that string will
   *   be returned.  Otherwise the element represented by <code>input</code> will be
   *   returned.
   */
  var dom;
  (function(RGX_DASH, INNER_TEXT, TEXT_CONTENT, PROP_HASH) {
    function capAfterDash(m, afterDash) {
      return afterDash.toUpperCase();
    }
    dom = function(input) {
      var elem, realPropName, propName, propValue, i, l, j, c, style, stylePropName, kids;
      if (typeOf(input) == 'String') {
        elem = slice(dom({ _: 'DIV', html: input }).childNodes);
      }
      else {
        elem = __DOCUMENT.createElement(input.nodeName || input._);
        for (realPropName in input) {
          propValue = input[realPropName];
          if (has(input, realPropName) && (propName = has(PROP_HASH, realPropName) ? PROP_HASH[realPropName] : realPropName) != '_') {
            if (propName == 'style') {
              style = elem[propName];
              if (typeOf(propValue) == 'String') {
                style.cssText = propValue;
              }
              else {
                for (stylePropName in propValue) {
                  if (has(propValue, stylePropName)) {
                    style[stylePropName.replace(RGX_DASH, capAfterDash)] = propValue[stylePropName];
                  }
                }
              }
            }
            else if (propName == INNER_TEXT || propName == TEXT_CONTENT) {
              elem[TEXT_CONTENT] = elem[INNER_TEXT] = propValue;
            }
            else if (propName == '$') {
              propValue = toArray(propValue);
              for (i = 0, l = propValue.length; i < l;) {
                for (kids = toArray(dom(propValue[i++])), j = 0, c = kids.length; j < c;) {
                  elem.appendChild(kids[j++]);
                }
              }
            }
            else {
              elem[propName] = propValue;
              if (propName == realPropName && elem.getAttribute(propName) == undefined && 'function' != typeof propValue) {
                elem.setAttribute(propName, propValue);
              }
            }
          }
        }
      }
      return elem;
    };
  })(/-([^-])/g, 'innerText', 'textContent',
    {nodeName:'_',html:'innerHTML',text:'innerText',children:'$','for':'htmlFor','class':'className',cls:'className'});
  
  /**
   * Gets the day of the week for the specified date.  This is different from
   * <code>Date.prototype.getDay()</code> because it allows for the first day of the week
   * to be days other than Sunday.
   * @name dow
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/dow/2.23.0}
   * @param {Date} date
   *   Date object from which to pull the day of the week.
   * @param {number} [opt_firstDayOfWeek=0]
   *   Indicates what the first day of the week is.  If given it should be
   *   between <code>0</code> (for Sunday) and <code>6</code> (for Saturday).
   * @returns {number}
   *   A number between <code>0</code> and <code>6</code>.  <code>0</code> will always correspond to the value of
   *   <code>opt_firstDayOfWeek</code> whereas <code>1</code> will be the next day and so on.
   */
  function dow(date, opt_firstDayOfWeek) {
    return ((date.getDay() - (opt_firstDayOfWeek || 0)) % 7 + 7) % 7;
  }
  
  /**
   * Determines the latest date-time of the year, month, and week (Friday to
   * Thursday) of a specific <code>Date</code> object.
   * @name endOfFridayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfFridayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Friday to
   *   Thursday).
   * @returns {Date}
   *   The latest date-time of the year, month, and week (Friday to Thursday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, day, and hour of a
   * specific <code>Date</code> object.
   * @name endOfHour
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfHour/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, and hour.
   * @returns {Date}
   *   The latest date-time of the year, month, day, and hour contained by
   *   <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, day, hour, and minute
   * of a specific <code>Date</code> object.
   * @name endOfMinute
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfMinute/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, hour, and minute.
   * @returns {Date}
   *   The latest date-time of the year, month, day, hour, and minute contained
   *   by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, and week (Monday to
   * Sunday) of a specific <code>Date</code> object.
   * @name endOfMondayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfMondayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Monday to
   *   Sunday).
   * @returns {Date}
   *   The latest date-time of the year, month, and week (Monday to Sunday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year and month of a specific <code>Date</code>
   * object.
   * @name endOfMonth
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfMonth/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year and month.
   * @returns {Date}
   *   The latest date-time of the year and month contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, and week (Saturday to
   * Friday) of a specific <code>Date</code> object.
   * @name endOfSaturdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfSaturdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Saturday to
   *   Friday).
   * @returns {Date}
   *   The latest date-time of the year, month, and week (Saturday to Friday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, day, hour, minute, and
   * second of a specific <code>Date</code> object.
   * @name endOfSecond
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfSecond/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, hour, minute, and
   *   second.
   * @returns {Date}
   *   The latest date-time of the year, month, day, hour, minute, and second
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, and week (Sunday to
   * Saturday) of a specific <code>Date</code> object.
   * @name endOfSundayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfSundayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Sunday to
   *   Saturday).
   * @returns {Date}
   *   The latest date-time of the year, month, and week (Sunday to Saturday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, and week (Thursday to
   * Wednesday) of a specific <code>Date</code> object.
   * @name endOfThursdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfThursdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Thursday to
   *   Wednesday).
   * @returns {Date}
   *   The latest date-time of the year, month, and week (Thursday to Wednesday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, and week (Tuesday to
   * Monday) of a specific <code>Date</code> object.
   * @name endOfTuesdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfTuesdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Tuesday to
   *   Monday).
   * @returns {Date}
   *   The latest date-time of the year, month, and week (Tuesday to Monday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, and week (Wednesday to
   * Tuesday) of a specific <code>Date</code> object.
   * @name endOfWednesdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfWednesdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Wednesday to
   *   Tuesday).
   * @returns {Date}
   *   The latest date-time of the year, month, and week (Wednesday to Tuesday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year, month, and week (Sunday to
   * Saturday) of a specific <code>Date</code> object.
   * @name endOfWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Sunday to
   *   Saturday).
   * @returns {Date}
   *   The latest date-time of the year, month, and week (Sunday to Saturday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the latest date-time of the year of a specific <code>Date</code> object.
   * @name endOfYear
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfYear/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year.
   * @returns {Date}
   *   The latest date-time of the year contained by <code>date</code>.
   */
  
  /**
   * Determines if one date is in the same year, month and day as another date.
   * @name isSameDay
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameDay/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month and day will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, and week (Friday to
   * Thursday) as another date.
   * @name isSameFridayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameFridayWeek/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, and week (Friday to Thursday) will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, day, and hour as another
   * date.
   * @name isSameHour
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameHour/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, day, and hour will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, day, hour, and minute as
   * another date.
   * @name isSameMinute
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameMinute/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, day, hour, and minute will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, and week (Monday to
   * Sunday) as another date.
   * @name isSameMondayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameMondayWeek/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, and week (Monday to Sunday) will be returned.
   */
  
  /**
   * Determines if one date is in the same year and month as another date.
   * @name isSameMonth
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameMonth/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year and month will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, and week (Saturday to
   * Friday) as another date.
   * @name isSameSaturdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameSaturdayWeek/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, and week (Saturday to Friday) will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, day, hour, minute, and
   * second as another date.
   * @name isSameSecond
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameSecond/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, day, hour, minute, and second will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, and week (Sunday to
   * Saturday) as another date.
   * @name isSameSundayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameSundayWeek/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, and week (Sunday to Saturday) will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, and week (Thursday to
   * Wednesday) as another date.
   * @name isSameThursdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameThursdayWeek/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, and week (Thursday to Wednesday) will be
   *   returned.
   */
  
  /**
   * Determines if one date is in the same year, month, and week (Tuesday to
   * Monday) as another date.
   * @name isSameTuesdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameTuesdayWeek/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, and week (Tuesday to Monday) will be returned.
   */
  
  /**
   * Determines if one date is in the same year, month, and week (Wednesday to
   * Tuesday) as another date.
   * @name isSameWednesdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameWednesdayWeek/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, and week (Wednesday to Tuesday) will be
   *   returned.
   */
  
  /**
   * Determines if one date is in the same year, month, and week (Sunday to
   * Saturday) as another date.
   * @name isSameWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameWeek/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year, month, and week (Sunday to Saturday) will be returned.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and day
   * of a specific <code>Date</code> object.
   * @name spanOfDay
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfDay/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and day.
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and day contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and week
   * (Friday to Thursday) of a specific <code>Date</code> object.
   * @name spanOfFridayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfFridayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Friday to
   *   Thursday).
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and week (Friday to Thursday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, day, and
   * hour of a specific <code>Date</code> object.
   * @name spanOfHour
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfHour/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, and hour.
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, day, and hour contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, day,
   * hour, and minute of a specific <code>Date</code> object.
   * @name spanOfMinute
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfMinute/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, hour, and minute.
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, day, hour, and minute contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and week
   * (Monday to Sunday) of a specific <code>Date</code> object.
   * @name spanOfMondayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfMondayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Monday to
   *   Sunday).
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and week (Monday to Sunday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year and month of a
   * specific <code>Date</code> object.
   * @name spanOfMonth
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfMonth/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year and month.
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year
   *   and month contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and week
   * (Saturday to Friday) of a specific <code>Date</code> object.
   * @name spanOfSaturdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfSaturdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Saturday to
   *   Friday).
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and week (Saturday to Friday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, day,
   * hour, minute, and second of a specific <code>Date</code> object.
   * @name spanOfSecond
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfSecond/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, hour, minute, and
   *   second.
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, day, hour, minute, and second contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and week
   * (Sunday to Saturday) of a specific <code>Date</code> object.
   * @name spanOfSundayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfSundayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Sunday to
   *   Saturday).
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and week (Sunday to Saturday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and week
   * (Thursday to Wednesday) of a specific <code>Date</code> object.
   * @name spanOfThursdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfThursdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Thursday to
   *   Wednesday).
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and week (Thursday to Wednesday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and week
   * (Tuesday to Monday) of a specific <code>Date</code> object.
   * @name spanOfTuesdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfTuesdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Tuesday to
   *   Monday).
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and week (Tuesday to Monday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and week
   * (Wednesday to Tuesday) of a specific <code>Date</code> object.
   * @name spanOfWednesdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfWednesdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Wednesday to
   *   Tuesday).
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and week (Wednesday to Tuesday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year, month, and week
   * (Sunday to Saturday) of a specific <code>Date</code> object.
   * @name spanOfWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Sunday to
   *   Saturday).
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year,
   *   month, and week (Sunday to Saturday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest and latest date-times of the year of a specific
   * <code>Date</code> object.
   * @name spanOfYear
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spanOfYear/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year.
   * @returns {Array.<Date>}
   *   An array of two dates:  the earliest and latest date-times of the year
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and day of a specific
   * <code>Date</code> object.
   * @name startOfDay
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfDay/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and day.
   * @returns {Date}
   *   The earliest date-time of the year, month, and day contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and week (Friday to
   * Thursday) of a specific <code>Date</code> object.
   * @name startOfFridayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfFridayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Friday to
   *   Thursday).
   * @returns {Date}
   *   The earliest date-time of the year, month, and week (Friday to Thursday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, day, and hour of a
   * specific <code>Date</code> object.
   * @name startOfHour
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfHour/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, and hour.
   * @returns {Date}
   *   The earliest date-time of the year, month, day, and hour contained by
   *   <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, day, hour, and minute
   * of a specific <code>Date</code> object.
   * @name startOfMinute
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfMinute/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, hour, and minute.
   * @returns {Date}
   *   The earliest date-time of the year, month, day, hour, and minute
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and week (Monday to
   * Sunday) of a specific <code>Date</code> object.
   * @name startOfMondayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfMondayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Monday to
   *   Sunday).
   * @returns {Date}
   *   The earliest date-time of the year, month, and week (Monday to Sunday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year and month of a specific
   * <code>Date</code> object.
   * @name startOfMonth
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfMonth/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year and month.
   * @returns {Date}
   *   The earliest date-time of the year and month contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and week (Saturday to
   * Friday) of a specific <code>Date</code> object.
   * @name startOfSaturdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfSaturdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Saturday to
   *   Friday).
   * @returns {Date}
   *   The earliest date-time of the year, month, and week (Saturday to Friday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, day, hour, minute,
   * and second of a specific <code>Date</code> object.
   * @name startOfSecond
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfSecond/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, day, hour, minute, and
   *   second.
   * @returns {Date}
   *   The earliest date-time of the year, month, day, hour, minute, and second
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and week (Sunday to
   * Saturday) of a specific <code>Date</code> object.
   * @name startOfSundayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfSundayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Sunday to
   *   Saturday).
   * @returns {Date}
   *   The earliest date-time of the year, month, and week (Sunday to Saturday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and week (Thursday to
   * Wednesday) of a specific <code>Date</code> object.
   * @name startOfThursdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfThursdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Thursday to
   *   Wednesday).
   * @returns {Date}
   *   The earliest date-time of the year, month, and week (Thursday to
   *   Wednesday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and week (Tuesday to
   * Monday) of a specific <code>Date</code> object.
   * @name startOfTuesdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfTuesdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Tuesday to
   *   Monday).
   * @returns {Date}
   *   The earliest date-time of the year, month, and week (Tuesday to Monday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and week (Wednesday
   * to Tuesday) of a specific <code>Date</code> object.
   * @name startOfWednesdayWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfWednesdayWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Wednesday to
   *   Tuesday).
   * @returns {Date}
   *   The earliest date-time of the year, month, and week (Wednesday to
   *   Tuesday) contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year, month, and week (Sunday to
   * Saturday) of a specific <code>Date</code> object.
   * @name startOfWeek
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfWeek/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and week (Sunday to
   *   Saturday).
   * @returns {Date}
   *   The earliest date-time of the year, month, and week (Sunday to Saturday)
   *   contained by <code>date</code>.
   */
  
  /**
   * Determines the earliest date-time of the year of a specific <code>Date</code> object.
   * @name startOfYear
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startOfYear/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year.
   * @returns {Date}
   *   The earliest date-time of the year contained by <code>date</code>.
   */
  
  /**
   * Determines if one date is in the same year as another date.
   * @name isSameYear
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSameYear/2.23.0}
   * @param {Date} date1
   *   The date to be compared to <code>date2</code>.
   * @param {Date} [date2=undefined]
   *   The date to be compared to <code>date1</code>.  If <code>undefined</code> or not specified then
   *   a partial function will be returned which will accept <code>date2</code> as its only
   *   parameter.
   * @returns {boolean|Function}
   *   If <code>date2</code> is not given or is <code>undefined</code> a partial function will be
   *   returned which will accept <code>date2</code> as its only parameter.  If <code>date1</code> and
   *   <code>date2</code> are specified then a boolean indicating whether or not they are
   *   in the same year will be returned.
   */
  // Creates the following 56 functions:
  // isSameYear(), startOfYear(), endOfYear(), spanOfYear(), isSameMonth(), startOfMonth(), endOfMonth(), spanOfMonth(), isSameDay(), startOfDay(), endOfDay(), spanOfDay(), isSameHour(), startOfHour(), endOfHour(), spanOfHour(), isSameMinute(), startOfMinute(), endOfMinute(), spanOfMinute(), isSameSecond(), startOfSecond(), endOfSecond(), spanOfSecond(), isSameWeek(), startOfWeek(), endOfWeek(), spanOfWeek(), isSameSundayWeek(), startOfSundayWeek(), endOfSundayWeek(), spanOfSundayWeek(), isSameMondayWeek(), startOfMondayWeek(), endOfMondayWeek(), spanOfMondayWeek(), isSameTuesdayWeek(), startOfTuesdayWeek(), endOfTuesdayWeek(), spanOfTuesdayWeek(), isSameWednesdayWeek(), startOfWednesdayWeek(), endOfWednesdayWeek(), spanOfWednesdayWeek(), isSameThursdayWeek(), startOfThursdayWeek(), endOfThursdayWeek(), spanOfThursdayWeek(), isSameFridayWeek(), startOfFridayWeek(), endOfFridayWeek(), spanOfFridayWeek(), isSameSaturdayWeek(), startOfSaturdayWeek(), endOfSaturdayWeek(), spanOfSaturdayWeek()
  eval('Year Month Day Hour Minute Second Week Sunday Monday Tuesday Wednesday Thursday Friday Saturday'.split(' ').map(
    function(unit, i) {
      var code = 'b@Month(11) b.(1);a.(32);a.Date(0) b@Hours(23) b@Minutes(59) b@Seconds(59) b@Milliseconds(999)'.replace(/@/g, '.(0);a.').replace(/\.(\w*)\((?=\S+\.(\w+)\S+|)/g, '.set$1$2(').split(' ');
      if (i > 5) {
        var offset = i > 6 ? 14 - i : 7;
        code.push('b.setDate(b.getDate()-(b.getDay()+'
          + offset + ')%7);a.setDate(a.getDate()-(a.getDay()+'
          + offset + ')%7+6)');
        if (i > 6) {
          unit += 'Week';
        }
        i = 2;
      }
      code = 'function #(a){var b=new Date(a);a=new Date(a);@;return [b,a]}'.replace('@', code.slice(i).join(';'));
      return code.replace('){', ',c){if(void 0===c)return function(c){return #(a,c)};').replace(/\ba\.set;/g, '').replace('[b,a]', 'b<=c&&c<=a').replace(/#/g, 'isSame' + unit)
        + code.replace(/\ba\.set;|\[(b),a\]/g, '$1').replace('#', 'startOf' + unit)
        + code.replace(/\bb\.set;|\[b,(a)\]/g, '$1').replace('#', 'endOf' + unit)
        + code.replace('#', 'spanOf' + unit);
    }
  ).join(''));
  
  /**
   * Determines the latest date-time of the year, month, and day of a specific
   * <code>Date</code> object.
   * @name endOfDay
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endOfDay/2.23.0}
   * @param {Date} date
   *   The object from which to pull the year, month, and day.
   * @returns {Date}
   *   The latest date-time of the year, month, and day contained by <code>date</code>.
   */
  
  /**
   * Determines whether a string ends with the characters of a specified string.
   * @name endsWith
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/endsWith/2.23.0}
   * @param {string} string
   *   The string that will be searched.
   * @param {string} target
   *   The characters to be searched for at the end of <code>string</code>.
   * @param {number} [opt_length=string.length]
   *   If provided it is used as the length of <code>string</code>. If omitted, the default
   *   value is the length of <code>string</code>.
   * @returns {boolean}
   *   A boolean value indicating whether or not <code>target</code> is found at the end of
   *   <code>string</code>.
   */
  function endsWith(string, target, opt_length) {
    opt_length = opt_length != undefined ? opt_length || 0 : string.length;
    if (opt_length < 0) {
      opt_length += string.length;
      if (opt_length < 0) {
        opt_length = 0;
      }
    }
    return string.slice(opt_length - ('' + target).length, opt_length) === target;
  }
  
  /**
   * Is similar to <code>setAt()</code> but always ensures that the specified path will be
   * added to the root object.
   * @name ensureAt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ensureAt/2.23.0}
   * @param {*} root
   *   The object under which to ensure the path.
   * @param {Array} path
   *   An array of numbers or strings representing the path that must exist
   *   within <code>root</code>.
   * @param {*} [opt_value=undefined]
   *   If given this will be the value assigned to the specified path, otherwise
   *   <code>undefined</code> is assigned.
   * @returns {*}
   *   A reference to <code>root</code>.
   */
  function ensureAt(root, path, opt_value) {
    for (var k, o = root, i = 0, l = o == undefined ? i : path.length, m = l - 1; i < l; i++) {
      o = has(o, k = path[i])
        ? o[k]
        : (o[k] = i === m ? opt_value : {});
    }
    return root;
  }
  
  /**
   * Gets the index of a specified target value in a string, array or an object.
   * Unlike <code>Array.prototype.indexOf()</code>, this function works for finding <code>NaN</code>
   * and differentiates between <code>-0</code> and <code>0</code>.
   * @name indexOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/indexOf/2.23.0}
   * @param {Array|Object|string} subject
   *   The array, object or string in which <code>target</code> should be searched for.
   * @param {*} target
   *   The target value to search for within <code>subject</code>.
   * @param {number} [opt_start=0]
   *   The position at which to start the search for strings or arrays.  This
   *   will be ignored for objects.  Negative values will be calculated from the
   *   end.
   * @returns {number|string|undefined}
   *   If <code>subject</code> is an array or a string this will be a integer representing
   *   the first position at which <code>target</code> is found within <code>subject</code> or this
   *   will be <code>-1</code> which represents that <code>target</code> was not found.  If <code>subject</code>
   *   is an object this will be key at which <code>target</code> was first found (based on
   *   <code>Object.keys()</code>) or <code>undefined</code> if <code>target</code> was not found.
   */
  function indexOf(subject, target, opt_start) {
    var k, i, item, t = typeOf(subject), isNaN = target !== target, isNegZero = target === 0 && 1 / target < 0;
    if (t === 'String') {
      return subject.indexOf(target, opt_start);
    }
    else if (t === 'Array') {
      opt_start = ~~opt_start;
      if (opt_start < 0) {
        opt_start = Math.max(0, subject.length + opt_start);
      }
  
      for (i = subject.length; i-- > opt_start;) {
        item = subject[i];
        if (isNaN ? item !== item : (isNegZero ? item === 0 && 1 / item < 0 : (target === item))) {
          return i;
        }
      }
      return -1;
    }
  
    t = Object.keys(subject);
    for (i = t.length; i--;) {
      item = subject[k = t[i]];
      if (isNaN ? item !== item : (isNegZero ? item === 0 && 1 / item < 0 : (target === item))) {
        return k;
      }
    }
  }
  
  /**
   * Gets the keys of a specified value.
   * @name keys
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/keys/2.23.0}
   * @param {*} value
   *   Value for which you would like the property names retrieved.
   * @param {boolean} [opt_onlyNonEnums]
   *   If specified and is not <code>null</code> or <code>undefined</code> but is <code>false</code>-ish, all of
   *   the property names (enumerable or not) will be returned.  If this is
   *   <code>true</code>-ish only the non-enumerable property names will be returned.  If
   *   this is not given or is <code>null</code> or <code>undefined</code> all of the enumerable
   *   property names will be returned.
   * @returns {Array}
   *   An array of all of the keys of <code>arrOrObj</code>.
   */
  function keys(value, opt_onlyNonEnums) {
    var enumKeys = Object.keys(value = Object(value));
    return opt_onlyNonEnums == undefined
      ? enumKeys
      : (value = Object.getOwnPropertyNames(value), opt_onlyNonEnums)
        ? value.filter(function (key) { return indexOf(enumKeys, key) < 0; })
        : value;
  }
  
  /**
   * Gets an array of pairs (array of length 2) indicating all of the keys and
   * values within a given array or object.
   * @name entries
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/entries/2.23.0}
   * @param {*} value
   *   Value for which you would like the property names retrieved.
   * @param {boolean} [opt_onlyNonEnums]
   *   If specified and is not <code>null</code> or <code>undefined</code> but is <code>false</code>-ish, all of
   *   the property key/value pairs (enumerable or not) will be returned.  If
   *   this is <code>true</code>-ish only the non-enumerable property key/value pairs will
   *   be returned.  If this is not given or is <code>null</code> or <code>undefined</code> all of the
   *   enumerable property key/value pairs will be returned.
   * @returns {Array.<Array>}
   *   An array of pairs (arrays where the first item is the key and the second
   *   is the value).
   */
  function entries(value, opt_onlyNonEnums) {
    for (var result = keys(value, opt_onlyNonEnums), i = result.length; i--;) {
      result[i] = [result[i], value[result[i]]];
    }
    return result;
  }
  
  /**
   * Encodes a string to be used in a query part of a URL.
   * @name escape
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/escape/2.23.0}
   * @param {string} str
   *   String to encode.
   * @returns {string}
   *   Encoded string that can be used in the query part of a URL.
   */
  function escape(str) {
    return encodeURIComponent(str)
      .replace(/!/g, '%21')
      .replace(/'/g, '%27')
      .replace(/\(/g, '%28')
      .replace(/\)/g, '%29')
      .replace(/\*/g, '%2A')
      .replace(/%20/g, '+');
  }
  
  /**
   * Determines whether a specific test does not pass for all of the items in an
   * array.
   * @name notEveryWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/notEveryWhere/2.23.0}
   * @param {Array} array
   *   The array whose values should be checked against all of the rules.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {boolean}
   *   Returns <code>true</code> only if at least one of the values in <code>array</code> fails one of
   *   the given rules.  Otherwise <code>false</code> is returned.
   */
  
  /**
   * Determines whether a specific test doesn't pass for any of the items in an
   * array.
   * @name noWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/noWhere/2.23.0}
   * @param {Array} array
   *   The array whose values should be checked against all of the rules.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {boolean}
   *   Returns <code>true</code> if none of the values in <code>array</code> match all of the given
   *   rules.  Otherwise <code>false</code> is returned.
   */
  
  /**
   * Determines whether a specific test passes for at least one of the items in
   * an array.
   * @name someWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/someWhere/2.23.0}
   * @param {Array} array
   *   The array whose values should be checked against all of the rules.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {boolean}
   *   Returns <code>true</code> only if at least one of the values in <code>array</code> passes all
   *   of the given rules.  Otherwise <code>false</code> is returned.
   */
  
  /**
   * Creates functions for testing values.
   * @name testForNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/testForNot/2.23.0}
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>["details", "age", ">=", 18]</code> can be used to make sure that
   *   <code>object.details.age < 18</code> occurs (<code>object</code> would be passed to the
   *   function returned by <code>testFor()</code>).  If no comparator is supplied <code>===</code>
   *   will be used.  If one of the paths exists the entire test will fail.
   * @returns {Function}
   *   A function which can be used to test any value according to the given
   *   rule(s).  If all of the rules pass for the value passed into the returned
   *   function then the returned function will return <code>false</code>, otherwise it
   *   will return <code>true</code>.
   */
  // function testForNot() {
  //   for (var argNames = [], checks = [], rules = arguments, ri = 0, rl = rules.length; ri < rl; ri++) {
  //     var pathStart = 0,
  //       pathIndex = 0,
  //       path = rules[ri].slice(),
  //       value = rules[ri] = path.pop(),
  //       comparer = /^(([!=]=?|!?~|<|>)=|<|>|!?\(\))$/.test(path[path.length - 1])
  //         ? path.pop()
  //         : '===',
  //       pathLength = path.length;
  //     // argNames.push(...) is only in for-loop to save some code (1 character lol)
  //     for (argNames[ri] = 'a' + ri; pathIndex <= pathLength; pathIndex++) {
  //       for (var code = 'i', i = pathStart; i < pathIndex; i++) {
  //         code += '[' + JSON.stringify(path[i]) + ']';
  //       }
  //       code = pathIndex + 1 > pathLength
  //         ? /~|\(/.test(comparer)
  //           ? ((/!/.test(comparer) ? '!' : '') + 'a' + ri + (/~/.test(comparer) ? '.test(' : '(') + code + ')')
  //           : (value !== value && /^[!=]/.test(comparer))
  //             ? code + (/!/.test(comparer) ? '===' : '!==') + code // Allows for comparison of NaN
  //             : (code + comparer + 'a' + ri)
  //         : (code + '!=void 0');
  //       if (checks.indexOf(code) < 0) {
  //         checks.push(code);
  //       }
  //     }
  //   }
  //   return Function(argNames.join(','), 'return function(i){return !(' + checks.join('&&') + ')}').apply(0, rules);
  // }
  
  /**
   * Creates functions for testing values.
   * @name testFor
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/testFor/2.23.0}
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>["details", "age", ">=", 18]</code> can be used to make sure that
   *   <code>object.details.age >= 18</code> occurs (<code>object</code> would be passed to the
   *   function returned by <code>testFor()</code>).  If no comparator is supplied <code>===</code>
   *   will be used.  If one of the paths doesn't exist the entire test will
   *   fail.
   * @returns {Function}
   *   A function which can be used to test any value according to the given
   *   rule(s).  If all of the rules pass for the value passed into the returned
   *   function then the returned function will return <code>true</code>, otherwise it will
   *   return <code>false</code>.
   */
  // CODE FOR testFor(), testForNot()
  // people = Array(1000000).fill({}).map((x, i) => ({ name: `Person #${i + 1}`, age: ~~(Math.random() * 100), score: ~~(Math.random() * 51 + 49) }));
  // t = testFor(['age', '>=', 18], ['age', '<=', 21], ['name', '~=', /5/], ['score', '>', 95]);
  // for (var i = 0; i < 5; i++) {
  //   console.time('r0');
  //   r0 = people.filter(i => i != void 0 && i.age >= 18 && i.age <= 21 && /5/.test(i.name) && i.score > 95);
  //   console.timeEnd('r0');
  
  //   console.time('r1');
  //   r1 = people.filter(t)
  //   console.timeEnd('r1');
  // }
  // function testFor() {
  //   for (var argNames = [], checks = [], rules = arguments, ri = 0, rl = rules.length; ri < rl; ri++) {
  //     var pathStart = 0,
  //       pathIndex = 0,
  //       path = rules[ri].slice(),
  //       value = rules[ri] = path.pop(),
  //       comparer = /^(([!=]=?|!?~|<|>)=|<|>|!?\(\))$/.test(path[path.length - 1])
  //         ? path.pop()
  //         : '===',
  //       pathLength = path.length;
  //     // argNames.push(...) is only in for-loop to save some code (1 character lol)
  //     for (argNames[ri] = 'a' + ri; pathIndex <= pathLength; pathIndex++) {
  //       for (var code = 'i', i = pathStart; i < pathIndex; i++) {
  //         code += '[' + JSON.stringify(path[i]) + ']';
  //       }
  //       code = pathIndex + 1 > pathLength
  //         ? /~|\(/.test(comparer)
  //           ? ((/!/.test(comparer) ? '!' : '') + 'a' + ri + (/~/.test(comparer) ? '.test(' : '(') + code + ')')
  //           : (value !== value && /^[!=]/.test(comparer))
  //             ? code + (/!/.test(comparer) ? '===' : '!==') + code // Allows for comparison of NaN
  //             : (code + comparer + 'a' + ri)
  //         : (code + '!=void 0');
  //       if (checks.indexOf(code) < 0) {
  //         checks.push(code);
  //       }
  //     }
  //   }
  //   return Function(argNames.join(','), 'return function(i){return ' + checks.join('&&') + '}').apply(0, rules);
  // }
  eval(
    '##'
      .replace(
        /#/g,
        'function testFor(){for(var k=[],g=[],e=arguments,b=0,n=e.length;b<n;b++){var f=0,c=e[b].slice(),l=e[b]=c.pop(),d=/^(([!=]=?|!?~|<|>)=|<|>|!?\\(\\))$/.test(c[c.length-1])?c.pop():"===",m=c.length;for(k[b]="a"+b;f<=m;f++){for(var a="i",h=0;h<f;h++)a+="["+JSON.stringify(c[h])+"]";a=f+1>m?/~|\\(/.test(d)?(/!/.test(d)?"!":"")+"a"+b+(/~/.test(d)?".test(":"(")+a+")":l!==l&&/^[!=]/.test(d)?a+(/!/.test(d)?"===":"!==")+a:a+d+"a"+b:a+"!=void 0";0>g.indexOf(a)&&g.push(a)}}return Function(k.join(","),"return function(i){return "+g.join("&&")+"}").apply(0,e)}'
      )
      .replace(/r(.+?)(".g.+?\+")/, 'rNot$1!($2)')
  );
  
  /**
   * Retrieves a new array of all of the items in an array which match a list of
   * rules.
   * @name where
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/where/2.23.0}
   * @param {Array} array
   *   The array whose values should be checked against all of the rules.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {Array}
   *   An array containing all of the items from <code>array</code> that passed all of the
   *   specified rules.
   */
  
  /**
   * Retrieves a new array of all of the items in an array which do not match a
   * list of rules.
   * @name whereNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/whereNot/2.23.0}
   * @param {Array} array
   *   The array whose values should be checked against all of the rules.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {Array}
   *   An array containing all of the items from <code>array</code> that failed at least
   *   one of the specified rules.
   */
  
  /**
   * Determines whether a specific test passes for all of the items in an array.
   * @name everyWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/everyWhere/2.23.0}
   * @param {Array} array
   *   The array whose values should be checked against all of the rules.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {boolean}
   *   Returns <code>true</code> if all of the values in <code>array</code> match all of the given
   *   rules.  Otherwise <code>false</code> is returned.
   */
  // creates where(), whereNot(), someWhere(), everyWhere(), noWhere(), and notEveryWhere()
  eval('var w%,w%Not,someW%,everyW%,noW%,notEveryW%;(function(A,T,N,S){w@filter#};};w@filter#};};someW@some#};};everyW@every#};};noW@some#===!1};};notEveryW@every#===!1};};})'
    .replace(/@/g, '%=function(a){var t=T.apply(0,S(arguments,1));if(a==void 0)return function(a){return A.')
    .replace(/%/g, 'here')
    .replace(/(=.+?)T/, 'Not$1N')
    .replace(/#/g, '.call(a,t)')
    .replace(/\{([^\{]+?)\};/g, '$&$1')
  )(__EMPTY_ARRAY, testFor, testForNot, slice);
  
  /**
   * Calls a function and returns it so it can be called again.
   * @name exec
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/exec/2.23.0}
   * @param {Function} fn
   *   Function to be called and returned.  The return value of this function
   *   will be ignored when it is called.
   * @param {*} [opt_this]
   *   Value referenced by <code>this</code> in evaluation of <code>fn</code>.
   * @param {...*} [opt_args]
   *   Zero or more arguments to be passed to <code>fn</code>.
   * @returns {Function}
   *   After executing <code>fn</code> it will be returned.
   */
  function exec(fn, opt_this) { return fn.apply(opt_this, slice(arguments, 2)), fn; }
  
  /**
   * Gets all of the matches found when matching a regular expression against a
   * string.
   * @name matchAll
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/matchAll/2.23.0}
   * @param {string} str
   *   String against which the regular expression should be matched.
   * @param {RegExp} rgx
   *   Regular expression to match against <code>str</code>.
   * @param {function(Array,number)} [opt_fnMapper]
   *   If specified the return value of this function will always be appended to
   *   the array which will be returned by <code>matchAll()</code> unless <code>undefined</code> is
   *   the return value.  The 1st argument passed will be an array representing
   *   the matches array that was found.  The 2nd argument passed will be the
   *   amount of times this mapper has been called.
   * @returns {Array|null}
   *   If <code>rgx</code> matched a substring in <code>str</code> at least once and <code>opt_fnMapper</code>
   *   wasn't used, an array of all of the matches will be returned. Each match
   *   will at least have <code>match[0]</code>, <code>match.index</code> (the starting index of the
   *   found match within <code>str</code>), <code>match.source</code> (<code>rgx</code>) and <code>match.input</code>
   *   (<code>str</code>). If capture groups were specified in <code>rgx</code>, each will be found in
   *   its corresponding index starting with <code>match[1]</code>. If no matches were
   *   found, <code>null</code> is returned.
   */
  eval('exec:a,fmatch:f,a'.replace(/(\w+):(.,.)/g, 'function $1All($2,d){a=new RegExp(a.source,(a.flags||(a+"").replace(/[^]+\\//,"")).replace("g","")+"g");for(var b,g=0,c=[],e=0;b=a.exec(f);e===a.lastIndex&&++a.lastIndex,e=a.lastIndex)d&&(b=d(b,++g)),void 0!==b&&c.push(b);return c.length?c:null}'));
  
  /**
   * Gets all of the matches found when matching a regular expression against a
   * string.
   * @name execAll
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/execAll/2.23.0}
   * @param {RegExp} rgx
   *   Regular expression to match against <code>str</code>.
   * @param {string} str
   *   String against which the regular expression should be matched.
   * @param {function(Array,number)} [opt_fnMapper]
   *   If specified the return value of this function will always be appended to
   *   the array which will be returned by <code>matchAll()</code> unless <code>undefined</code> is
   *   the return value.  The 1st argument passed will be an array representing
   *   the matches array that was found.  The 2nd argument passed will be the
   *   amount of times this mapper has been called.
   * @returns {Array|null}
   *   If <code>rgx</code> matched a substring in <code>str</code> at least once and <code>opt_fnMapper</code>
   *   wasn't used, an array of all of the matches will be returned. Each match
   *   will at least have <code>match[0]</code>, <code>match.index</code> (the starting index of the
   *   found match within <code>str</code>), <code>match.source</code> (<code>rgx</code>) and <code>match.input</code>
   *   (<code>str</code>). If capture groups were specified in <code>rgx</code>, each will be found in
   *   its corresponding index starting with <code>match[1]</code>. If no matches were
   *   found, <code>null</code> is returned.
   */
  
  /**
   * Expands regular expressions by expanding the character classes.  Supports
   * <code>[:alnum:]</code>, <code>[:alpha:]</code>, <code>[:digit:]</code>, <code>[:punct:]</code>, <code>[:space:]</code>, or
   * <code>[:xdigit:]</code> and allows for addition of other character classes.
   * @name expandRegExp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/expandRegExp/2.23.0}
   * @param {RegExp|string} rgxOrCharClsName
   *   Regular expression or name of the character class to add to future
   *   regular expressions passed to this function.
   * @param {string} [opt_charClass]
   *   If <code>rgxOrCharClsName</code> is a string this should be defined and should be
   *   the definition of the character class without brackets.  This character
   *   class will be added and will be used to expand future regular expressions
   *   passed into this function.
   * @returns {RegExp|boolean}
   *   If <code>rgxOrCharClsName</code> is a regular expression this will be a modified
   *   version of it with the character classes expanded, otherwise this will be
   *   a boolean value indicating if the character class was successfully added.
   */
  var expandRegExp;
  (function(store, casingInitialized) {
    function addTo(groupName, index) {
      var arr = !has(store, groupName += 'case') ? store[groupName] = [] : store[groupName];
      var last = store['~' + groupName];
      if (last && last[last.length - 1] + 1 === index) {
        last[1] = index;
      }
      else {
        arr.push(store['~' + groupName] = [index]);
      }
    }
  
    function makeCasingRanges() {
      for (var ch, lower, upper, groupName, i = 0; i <= 0xFFFF; i++) {
        ch = String.fromCharCode(i);
        lower = ch.toLowerCase();
        upper = ch.toUpperCase();
        groupName = upper === lower ? 'no' : upper === ch ? 'upper' : lower === ch ? 'lower' : 'mix';
        addTo(groupName, i);
      }
      ['upper', 'lower', 'mix', 'no'].forEach(function (name) {
        name += 'case';
        store[name] = store[name].map(function(range) {
          return range.map(function(value) {
            return '\\u' + ('000' + value.toString(16)).slice(-4);
          }).join('-');
        }).join('');
      });
    }
  
    expandRegExp = function(rgxOrExtName, opt_extension) {
      if (typeOf(rgxOrExtName) === 'RegExp') {
        var oldSource,
            newSource = rgxOrExtName.source,
            flags = (rgxOrExtName + '').replace(/[^]+\/(\w*)$/, '$1');
        do {
          oldSource = newSource;
          newSource = oldSource.replace(
            /\[:(\w+):\]/g,
            function(m, name) {
              if (!casingInitialized && /^(upper|lower|mix|no)case$/.test(name)) {
                makeCasingRanges();
                casingInitialized = 1;
              }
              return has(store, name) ? store[name] : m;
            }
          );
        } while (oldSource !== newSource);
        rgxOrExtName = rgxOrExtName.source.replace(/\[\]/);
        return new RegExp(newSource, flags);
      }
      else if (oldSource = /^\w+$/.test(rgxOrExtName)) {
        store[rgxOrExtName] = opt_extension.replace(/\\?]/g, '\\]');
      }
      return oldSource;  // works cuz of hoisting
    };
  })({
    space: '\\s',
    alpha: 'A-Za-z',
    punct: '!"\#$%&\'()*+,\\-./:;<=>?@\\[\\\\\\]^_`{|}~',
    digit: '\\d',
    alnum: 'A-Za-z\\d',
    xdigit: 'A-Fa-f0-9'
  });
  
  /**
   * Adds extra properties to an object based on the properties owned by other
   * specified objects.
   * @name extend
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/extend/2.23.0}
   * @param {Object} objToExtend
   *   Object to be modified.
   * @param {...Object} objPropsX
   *   One or more arguments where each one's properties will be added to
   *   <code>objToExtend</code>.
   * @returns {Object}
   *   A reference to <code>objToExtend</code>.
   */
  function extend(objToExtend, objProps) {
    for (var k, args = arguments, i = 1; objProps = args[i++]; ) {
      for (k in objProps) {
        if (has(objProps, k)) {
          objToExtend[k] = objProps[k];
        }
      }
    }
    return objToExtend;
  }
  
  /**
   * Fill an array with a given value.
   * @name fill
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/fill/2.23.0}
   * @param {Array} arr
   *   Array to fill.
   * @param {*} value
   *   Value with which to fill <code>arr</code>.
   * @param {number} [opt_start=0]
   *   Starting position within <code>arr</code> at which to fill <code>arr</code>.  If it is negative
   *   it will be calculated from the end of <code>arr</code>.
   * @param {number} [opt_end=arr.length]
   *   Ending position within <code>arr</code> at which to no longer fill <code>arr</code>.  If it is
   *   negative it will be calculated from the end of <code>arr</code>.
   * @returns {Array}
   *   A reference to <code>arr</code>.
   */
  function fill(arr, value, opt_start, opt_end) {
    var len = arr.length;
    opt_start = opt_start < 0 ? Math.max(len + opt_start, 0) : (opt_start || 0);
    for (
      opt_end = opt_end < 0
        ? Math.max(len + opt_end, 0)
        : (opt_end || opt_end === 0 ? Math.min(opt_end, len) : len);
      opt_start < opt_end;
      opt_start++
    ) { arr[opt_start] = value; }
    return arr;
  }
  
  /**
   * Looks for the first index in an array whose given property matches the
   * criteria.
   * @name findIndexByProp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/findIndexByProp/2.23.0}
   * @param {Array} array
   *   Array to traverse.
   * @param {string} propName
   *   Name of the property of each <code>array</code> item to inspect.  If no such
   *   property exists for any <code>array</code> item that item will simply be skipped.
   * @param {*} tester
   *   Criteria by which to test the property of each item in <code>array</code>.  If this
   *   is a function it will be passed the value of the property and the return
   *   value will be coerced to a boolean to see if the property fits the
   *   criteria.  If this is a regular expression it will be tested against the
   *   property to see if the property fits the criteria.  Any other type of
   *   value will be checked to see if the property is this value to see if it
   *   fits the criteria.
   * @returns {number}
   *   The first index in <code>array</code> for which <code>tester</code> matches the specified
   *   property.  If <code>tester</code> never matches any of the properties <code>-1</code> will be
   *   returned.
   */
  
  /**
   * Determines whether or not a value is actually <code>NaN</code>.  Different from the
   * global <code>isNaN</code> function because it only tests for the presence of the <code>NaN</code>
   * value.
   * @name isNaN
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isNaN/2.23.0}
   * @param {number|*} value
   *   Value to test to see if it is <code>NaN</code>.
   * @returns {boolean}
   *   If <code>value</code> is <code>NaN</code> then <code>true</code> is returned, otherwise <code>false</code> is
   *   returned.
   */
  function isNaN(value) {
    return value !== value;
  }
  
  /**
   * Looks for the first value in an array whose given property matches the
   * criteria.
   * @name findByProp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/findByProp/2.23.0}
   * @param {Array} array
   *   Array to traverse.
   * @param {string} propName
   *   Name of the property of each <code>array</code> item to inspect.  If no such
   *   property exists for any <code>array</code> item that item will simply be skipped.
   * @param {*} tester
   *   Criteria by which to test the property of each item in <code>array</code>.  If this
   *   is a function it will be passed the value of the property and the return
   *   value will be coerced to a boolean to see if the property fits the
   *   criteria.  If this is a regular expression it will be tested against the
   *   property to see if the property fits the criteria.  Any other type of
   *   value will be checked to see if the property is this value to see if it
   *   fits the criteria.
   * @returns {*}
   *   The first item in <code>array</code> for which <code>tester</code> matches the specified
   *   property.  If <code>tester</code> never matches any of the properties <code>undefined</code>
   *   will be returned.
   */
  var findByProp = Function('T,I', ('return [find,findIndex,filter]').replace(/\w+(?=,|\])/g, 'function $&ByProp(d,e,a){var b=T(a),f="RegExp"===b,g="Function"===b||I(a)&&(a=isNaN);return d.$&(function(b,d){if(void 0==b)return!1;var c=b[e];return f?a.test(c):g?a(c):c===a})}'))(typeOf, isNaN),
      filterByProp = findByProp[2],
      findIndexByProp = findByProp[1];
  findByProp = findByProp[0];
  
  /**
   * Looks for all of the items in an array whose given property matches the
   * criteria.
   * @name filterByProp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterByProp/2.23.0}
   * @param {Array} array
   *   Array to traverse.
   * @param {string} propName
   *   Name of the property of each <code>array</code> item to inspect.  If no such
   *   property exists for any <code>array</code> item that item will simply be skipped.
   * @param {*} tester
   *   Criteria by which to test the property of each item in <code>array</code>.  If this
   *   is a function it will be passed the value of the property and the return
   *   value will be coerced to a boolean to see if the property fits the
   *   criteria.  If this is a regular expression it will be tested against the
   *   property to see if the property fits the criteria.  Any other type of
   *   value will be checked to see if the property is this value to see if it
   *   fits the criteria.
   * @returns {Array}
   *   An array of the items in <code>array</code> for which <code>tester</code> matches the specified
   *   property.
   */
  
  /**
   * Gets the keys of the specified object using <code>Object.keys()</code> and filters the
   * keys using a callback function.
   * @name filterKeys
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterKeys/2.23.0}
   * @param {*} obj
   *   The object whose key/value pairs should be examined and whose keys will
   *   potentially be returned.
   * @param {Function} tester
   *   The callback function which will be called for each key/value pair in
   *   <code>obj</code>.  This function will be passed (1) the key, (2) the value and (3) a
   *   reference to <code>obj</code>.  For each property the return value will be coerced
   *   to a boolean and will be used to see if the property name should be
   *   returned.
   * @param {boolean} [opt_negate=false]
   *   Optional value indicating whether or not to negate the results of each
   *   call to <code>tester()</code>.  If <code>false</code>-ish the <code>tester()</code> callback should return
   *   a <code>true</code>-ish value for any properties should be kept.  If <code>true</code>-ish the
   *   <code>tester()</code> callback should return a <code>false</code>-ish value for any properties
   *   that should be kept.
   * @returns {Array}
   *   An array of the keys retrieved from <code>obj</code>.
   */
  /** THE ORIGINAL CODE FOR filterKeys() **/
  // function filterKeys(obj, tester, opt_negate) {
  //   opt_negate = !opt_negate;
  //   for (var key, result = [], keys = Object.keys(obj), i = keys.length; i--;) {
  //     key = keys[i];
  //     if (!tester(key, obj[key], obj) !== opt_negate) {
  //       result.unshift(key);
  //     }
  //   }
  //   return result;
  // }
  
  /**
   * Gets the key/value pairs for the properties the specified object using
   * <code>Object.getOwnPropertyNames()</code> and filters them using a callback function.
   * @name filterPropEntries
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterPropEntries/2.23.0}
   * @param {*} obj
   *   The object whose properties should be examined and whose key/value pairs
   *   will potentially be returned.
   * @param {Function} tester
   *   The callback function which will be called for each key/value pair in
   *   <code>obj</code>.  This function will be passed (1) the property name, (2) the
   *   property value and (3) a reference to <code>obj</code>.  For each property the
   *   return value will be coerced to a boolean and will be used to see if the
   *   property key/value pair should be returned.
   * @param {boolean} [opt_negate=false]
   *   Optional value indicating whether or not to negate the results of each
   *   call to <code>tester()</code>.  If <code>false</code>-ish the <code>tester()</code> callback should return
   *   a <code>true</code>-ish value for any properties should be kept.  If <code>true</code>-ish the
   *   <code>tester()</code> callback should return a <code>false</code>-ish value for any properties
   *   that should be kept.
   * @returns {Array}
   *   An array of the property key/value pairs retrieved from <code>obj</code>.  Each item
   *   in the array will be an array where the first value in the subarray is
   *   the property name and the second item in the subarray is the value.
   */
  /** THE ORIGINAL CODE FOR filterPropEntries() **/
  // function filterPropEntries(obj, tester, opt_negate) {
  //   opt_negate = !opt_negate;
  //   for (var key, result = [], keys = Object.getOwnPropertyNames(obj), i = keys.length; i--;) {
  //     key = keys[i];
  //     if (!tester(key, obj[key], obj) !== opt_negate) {
  //       result.unshift([key, obj[key]]);
  //     }
  //   }
  //   return result;
  // }
  
  /**
   * Gets the property values of the specified object using
   * <code>Object.getPropertyNames()</code> and filters the values using a callback
   * function.
   * @name filterPropValues
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterPropValues/2.23.0}
   * @param {*} obj
   *   The object whose properties should be examined and whose values will
   *   potentially be returned.
   * @param {Function} tester
   *   The callback function which will be called for each key/value pair in
   *   <code>obj</code>.  This function will be passed (1) the value, (2) the property name
   *   and (3) a reference to <code>obj</code>.  For each property the return value will be
   *   coerced to a boolean and will be used to see if the property value should
   *   be returned.
   * @param {boolean} [opt_negate=false]
   *   Optional value indicating whether or not to negate the results of each
   *   call to <code>tester()</code>.  If <code>false</code>-ish the <code>tester()</code> callback should return
   *   a <code>true</code>-ish value for any properties should be kept.  If <code>true</code>-ish the
   *   <code>tester()</code> callback should return a <code>false</code>-ish value for any properties
   *   that should be kept.
   * @returns {Array}
   *   An array of the property values retrieved from <code>obj</code>.
   */
  /** THE ORIGINAL CODE FOR filterPropValues() **/
  // function filterPropValues(obj, tester, opt_negate) {
  //   opt_negate = !opt_negate;
  //   for (var key, result = [], keys = Object.getOwnPropertyNames(obj), i = keys.length; i--;) {
  //     key = keys[i];
  //     if (!tester(obj[key], key, obj) !== opt_negate) {
  //       result.unshift(obj[key]);
  //     }
  //   }
  //   return result;
  // }
  
  /**
   * Gets the property values of the specified object using <code>Object.keys()</code> and
   * filters the values using a callback function.
   * @name filterValues
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterValues/2.23.0}
   * @param {*} obj
   *   The object whose properties should be examined and whose values will
   *   potentially be returned.
   * @param {Function} tester
   *   The callback function which will be called for each key/value pair in
   *   <code>obj</code>.  This function will be passed (1) the value, (2) the property name
   *   and (3) a reference to <code>obj</code>.  For each property the return value will be
   *   coerced to a boolean and will be used to see if the property value should
   *   be returned.
   * @param {boolean} [opt_negate=false]
   *   Optional value indicating whether or not to negate the results of each
   *   call to <code>tester()</code>.  If <code>false</code>-ish the <code>tester()</code> callback should return
   *   a <code>true</code>-ish value for any properties should be kept.  If <code>true</code>-ish the
   *   <code>tester()</code> callback should return a <code>false</code>-ish value for any properties
   *   that should be kept.
   * @returns {Array}
   *   An array of the property values retrieved from <code>obj</code>.
   */
  /** THE ORIGINAL CODE FOR filterValues() **/
  // function filterValues(obj, tester, opt_negate) {
  //   opt_negate = !opt_negate;
  //   for (var key, result = [], keys = Object.keys(obj), i = keys.length; i--;) {
  //     key = keys[i];
  //     if (!tester(obj[key], key, obj) !== opt_negate) {
  //       result.unshift(obj[key]);
  //     }
  //   }
  //   return result;
  // }
  
  /**
   * Gets the property names of the specified object using
   * <code>Object.getOwnPropertyNames()</code> and filters the property names using a
   * callback function.
   * @name filterPropNames
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterPropNames/2.23.0}
   * @param {*} obj
   *   The object whose property should be examined and whose property names
   *   will potentially be returned.
   * @param {Function} tester
   *   The callback function which will be called for each property in <code>obj</code>.
   *   This function will be passed (1) the property name, (2) the property
   *   value and (3) a reference to <code>obj</code>.  For each property the return value
   *   will be coerced to a boolean and will be used to see if the property name
   *   should be returned.
   * @param {boolean} [opt_negate=false]
   *   Optional value indicating whether or not the negate the results of each
   *   call to <code>tester()</code>.  If <code>false</code>-ish the <code>tester()</code> callback should return
   *   a <code>true</code>-ish value for any properties should be kept.  If <code>true</code>-ish the
   *   <code>tester()</code> callback should return a <code>false</code>-ish value for any properties
   *   that should be kept.
   * @returns {Array}
   *   An array of the property names retrieved from <code>obj</code>.
   */
  /** THE ORIGINAL CODE FOR filterPropNames() **/
  // function filterPropNames(obj, tester, opt_negate) {
  //   opt_negate = !opt_negate;
  //   for (var key, result = [], keys = Object.getOwnPropertyNames(obj), i = keys.length; i--;) {
  //     key = keys[i];
  //     if (!tester(key, obj[key], obj) !== opt_negate) {
  //       result.unshift(key);
  //     }
  //   }
  //   return result;
  // }
  eval('QPropNamesRSTa,WUaXQKeysRkeysTa,WUaXQPropValuesRSTW,aUWXQValuesRkeysTW,aUWXQPropEntriesRSTa,WU[a,W]XQEntriesRkeysTa,WU[a,W]X'.replace(/[Q-UWX]/g, function(c) {
    return {
      Q: 'function filter',
      R: '(b,f,g){g=!g;for(var a,c=[],d=Object.',
      S: 'getOwnPropertyNames',
      T: '(b),e=d.length;e--;)a=d[e],!f(',
      U: ',b)!==g&&c.unshift(',
      W: 'b[a]',
      X: ');return c}',
    }[c];
  }));
  
  /**
   * Gets the key/value pairs for the properties the specified object using
   * <code>Object.keys()</code> and filters them using a callback function.
   * @name filterEntries
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterEntries/2.23.0}
   * @param {*} obj
   *   The object whose properties should be examined and whose key/value pairs
   *   will potentially be returned.
   * @param {Function} tester
   *   The callback function which will be called for each key/value pair in
   *   <code>obj</code>.  This function will be passed (1) the property name, (2) the
   *   property value and (3) a reference to <code>obj</code>.  For each property the
   *   return value will be coerced to a boolean and will be used to see if the
   *   property key/value pair should be returned.
   * @param {boolean} [opt_negate=false]
   *   Optional value indicating whether or not to negate the results of each
   *   call to <code>tester()</code>.  If <code>false</code>-ish the <code>tester()</code> callback should return
   *   a <code>true</code>-ish value for any properties should be kept.  If <code>true</code>-ish the
   *   <code>tester()</code> callback should return a <code>false</code>-ish value for any properties
   *   that should be kept.
   * @returns {Array}
   *   An array of the property key/value pairs retrieved from <code>obj</code>.  Each item
   *   in the array will be an array where the first value in the subarray is
   *   the property name and the second item in the subarray is the value.
   */
  /** THE ORIGINAL CODE FOR filterEntries() **/
  // function filterEntries(obj, tester, opt_negate) {
  //   opt_negate = !opt_negate;
  //   for (var key, result = [], keys = Object.keys(obj), i = keys.length; i--;) {
  //     key = keys[i];
  //     if (!tester(key, obj[key], obj) !== opt_negate) {
  //       result.unshift([key, obj[key]]);
  //     }
  //   }
  //   return result;
  // }
  
  /**
   * Filters the contents of an array and then returns the remaining values
   * mapped as different or possibly the same values.
   * @name filterMap
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/filterMap/2.23.0}
   * @param {Array} array
   *   The array which will be shallow copied, filtered and sent back with
   *   possibly modified values based on <code>func</code>.
   * @param {Function} func
   *   The function which will called for each value that is in a defined index
   *   in <code>array</code>.  The function will be passed (1) the value to be examined,
   *   (2) the index of that value and (3) a reference to <code>array</code>.  If the
   *   function returns any value that is not the same as <code>opt_badValue</code>, that
   *   value will be appended to the end of the array that will be returned.
   * @param {*} [opt_badValue=undefined]
   *   The value that if returned by <code>func</code> it will not be appended to the array
   *   to be returned.
   * @returns {Array}
   *   A non-sparse array of all of the values that were filtered and mapped
   *   back from <code>array</code> based on <code>func</code> and <code>opt_badValue</code>.
   */
  function filterMap(arr, fn, opt_badValue) {
    for (var result, value, isBadValueNaN = opt_badValue !== opt_badValue, ret = [], i = 0, l = arr.length; i < l; i++) {
      if (i in arr) {
        result = fn(value = arr[i], i, arr);
        if (isBadValueNaN ? result === result : result !== opt_badValue) {
          ret.push(result);
        }
      }
    }
    return ret;
  }
  
  /**
   * Get the nth index of the <code>target</code> in <code>subject</code>.
   * @name findNthIndex
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/findNthIndex/2.23.0}
   * @param {Array|string} subject
   *   Array or string to search for <code>target</code> in.
   * @param {Function|RegExp} target
   *   If <code>subject</code> is an array this must either be a function or a regular
   *   expression which will be tested against each element in <code>subject</code>. If
   *   <code>subject</code> is a string this must be a regular expression which will be
   *   tested against it to find all of the matches.
   * @param {number} n
   *   The number of the occurrence that will be searched for and returned.  <code>0</code>
   *   represents the first occurrence whereas <code>-1</code> represents the last.
   * @returns {number|undefined}
   *   If the nth occurrence of <code>target</code> can be found within <code>subject</code> the index
   *   will be returned otherwise <code>undefined</code> will be returned.
   */
  
  /**
   * Gets the nth value matching <code>target</code> in <code>subject</code>.
   * @name findNth
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/findNth/2.23.0}
   * @param {Array|string} subject
   *   Array or string to search for <code>target</code> in.
   * @param {Function|RegExp} target
   *   If <code>subject</code> is an array this must either be a function or a regular
   *   expression which will be tested against each element in <code>subject</code>.  If
   *   <code>subject</code> is a string this must be a regular expression which will be
   *   tested against it to find all of the matches.
   * @param {number} n
   *   The number of the occurrence that will be searched for and returned.  <code>0</code>
   *   represents the first occurrence whereas <code>-1</code> represents the last.
   * @returns {*}
   *   The nth occurrence of <code>target</code> within <code>subject</code>.  If not found
   *   <code>undefined</code> will be returned.
   */
  eval('var findNthIndex,findNth;(function(z,y){=function(b,c,e){var f=0>e,h="RegExp"===typeOf(c);var a="String"===typeOf(b);h&&(c=new RegExp(c.source,(c+"g").match(/\\w*$/)[0].replace(a?/g(.)/:/g()/g,"$1")));if(a){var g=[];b.replace(c,function(a){g.push(a)});var d=g.length;a=f?d+e:e;if(0<=a&&a<d)return g[a]}else{!f&&++e;var k=f?-1:1;d=b.length;for(a=f?d-1:0;f?0<=a:a<d;a+=k)if(has(b,a)&&(h?c.test(b[a]):c(b[a]))&&!(e-=k))return b[a]}};})'.replace(/=.+;/, 'findNthIndex$&findNth$&').replace('push(a)', 'push(arguments[arguments.length-2])').replace(/(return )b.a./, '$1a'))(typeOf, has);
  
  /**
   * Find the path to a specific value based on a callback function.
   * @name findPath
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/findPath/2.23.0}
   * @param {*} base
   *   The object in which you want to start looking to see if it a match
   *   according to <code>callback</code>.
   * @param {Function} callback
   *   The function that will be called for each value starting with <code>base</code> and
   *   traversing all of its descendants.  The arguments passed will be (1) the
   *   value be inspected, (2) the path as an array to that value and (3) a
   *   reference to <code>base</code>.  If this function returns a <code>true</code>-ish value then
   *   execution will stop and the path to this value will be returned.
   * @param {number} [opt_maxTimeMs=3000]
   *   The maximum amount of time to check before simply returning <code>undefined</code>.
   * @returns {Array<string>|boolean|undefined}
   *   If the desired value is found an array with the path to said <code>base</code>.  If
   *   the desired value is not found in the allotted time <code>undefined</code> is
   *   returned. In all other cases <code>false</code> is returned.
   */
  function findPath(base, callback, maxTimeMs=3000) {
    let startTime = +new Date;
    let toCheck = [{ value: base, path: [] }]
    while (entry = toCheck.shift()) {
      let value = entry.value;
      let path = entry.path;
      if (callback(value, path, base)) {
        return path;
      }
      if (value && 'object' === typeof value) {
        toCheck = toCheck.concat(Object.keys(value).map(key => ({ value: value[key], path: path.concat([key]) })));
      }
      if (new Date - startTime > maxTimeMs) {
        return undefined;
      }
    }
    return false;
  }
  
  /**
   * Finds the first item in an array which does not match the specified
   * criteria.
   * @name findWhereNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/findWhereNot/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from start to end to
   *   find the first item which does not match the given rules.  If <code>undefined</code>
   *   or <code>null</code> is given then a partial <code>findWhereNot()</code> function is created
   *   with just the rules that were given which will accept only the <code>array</code>
   *   parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {*}
   *   The first value in <code>array</code> which fails at least one of the specified
   *   rules.
   */
  
  /**
   * Finds the position of the first item in an array which matches the
   * specified criteria.
   * @name indexWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/indexWhere/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from start to end to
   *   find the position of the first item which matches the given rules.  If
   *   <code>undefined</code> or <code>null</code> is given then a partial <code>indexWhere()</code> function is
   *   created with just the rules that were given which will accept only the
   *   <code>array</code> parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {number}
   *   The index of the first value in <code>array</code> which passes all of the specified
   *   rules.  If no matches are found <code>-1</code> will be returned.
   */
  
  /**
   * Finds the position of the first item in an array which does not match the
   * specified criteria.
   * @name indexWhereNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/indexWhereNot/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from start to end to
   *   find the position of the first item which does not match the given rules.
   *   If <code>undefined</code> or <code>null</code> is given then a partial <code>indexWhereNot()</code>
   *   function is created with just the rules that were given which will accept
   *   only the <code>array</code> parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {number}
   *   The first position of the value in <code>array</code> which fails at least one of
   *   the specified rules.
   */
  
  /**
   * Finds the positions of the items in an array which match the specified
   * criteria.
   * @name indicesWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/indicesWhere/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from start to end to
   *   find the positions of the items which match the given rules.  If
   *   <code>undefined</code> or <code>null</code> is given then a partial <code>indicesWhere()</code> function
   *   is created with just the rules that were given which will accept only the
   *   <code>array</code> parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {Array}
   *   An array of positions of items in <code>array</code> which pass all of the specified
   *   rules.
   */
  
  /**
   * Finds the positions the items in an array which do not match the specified
   * criteria.
   * @name indicesWhereNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/indicesWhereNot/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from start to end to
   *   find the positions of the items which do not match the given rules.  If
   *   <code>undefined</code> or <code>null</code> is given then a partial <code>indicesWhereNot()</code>
   *   function is created with just the rules that were given which will accept
   *   only the <code>array</code> parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {Array}
   *   The positions of the values in <code>array</code> which fail at least one of the
   *   specified rules.
   */
  
  /**
   * Finds the position of the last item in an array which matches the specified
   * criteria.
   * @name lastIndexWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lastIndexWhere/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from end to start to
   *   find the position of the last item which matches the given rules.  If
   *   <code>undefined</code> or <code>null</code> is given then a partial <code>lastIndexWhere()</code> function
   *   is created with just the rules that were given which will accept only the
   *   <code>array</code> parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {number}
   *   The index of the last value in <code>array</code> which passes all of the specified
   *   rules.  If no matches are found <code>-1</code> will be returned.
   */
  
  /**
   * Finds the position of the last item in an array which does not match the
   * specified criteria.
   * @name lastIndexWhereNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lastIndexWhereNot/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from end to start to
   *   find the position of the last item which does not match the given rules.
   *   If <code>undefined</code> or <code>null</code> is given then a partial <code>lastIndexWhereNot()</code>
   *   function is created with just the rules that were given which will accept
   *   only the <code>array</code> parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {number}
   *   The last position of the value in <code>array</code> which fails at least one of the
   *   specified rules.
   */
  
  /**
   * Finds the last item in an array which matches the specified criteria.
   * @name lastWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lastWhere/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from end to start to
   *   find the last item which matches the given rules.  If <code>undefined</code> or
   *   <code>null</code> is given then a partial <code>lastWhere()</code> function is created with
   *   just the rules that were given which will accept only the <code>array</code>
   *   parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {*}
   *   The last value in <code>array</code> which passes all of the specified rules.
   */
  
  /**
   * Finds the last item in an array which does not match the specified
   * criteria.
   * @name lastWhereNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lastWhereNot/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from end to start to
   *   find the last item which does not match the given rules.  If <code>undefined</code>
   *   or <code>null</code> is given then a partial <code>lastWhereNot()</code> function is created
   *   with just the rules that were given which will accept only the <code>array</code>
   *   parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {*}
   *   The last value in <code>array</code> which fails at least one of the specified
   *   rules.
   */
  
  /**
   * Finds the first item in an array which matches the specified criteria.
   * @name findWhere
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/findWhere/2.23.0}
   * @param {Array|null|undefined} array
   *   If an array is given each item will be traversed from start to end to
   *   find the first item which matches the given rules.  If <code>undefined</code> or
   *   <code>null</code> is given then a partial <code>findWhere()</code> function is created with
   *   just the rules that were given which will accept only the <code>array</code>
   *   parameter.
   * @param {...Array} ruleX
   *   An array containing (1) the path, (2) optionally the comparator as a
   *   string and (3) the value to check the variable at that path against.
   *   Each part of the path should be a separate item in the array.  For
   *   example <code>[\"details\", \"age\", \">=\", 18]</code> can be used to make sure
   *   that <code>array[index].details.age >= 18</code> occurs.  If no comparator is
   *   supplied <code>===</code> will be used.  If one of the paths doesn't exist the
   *   entire test will fail.
   * @returns {*}
   *   The first value in <code>array</code> which passes all of the specified rules.
   */
  // creates findWhere(), indexWhere(), indicesWhere(), lastWhere(), lastIndexWhere(), findWhereNot(), indexWhereNot(), indicesWhereNot(), lastWhereNot(), and lastIndexWhereNot()
  // NOTE: Allows for partial calls by providing undefined or null as the first parameter.
  eval(
    'findZYXi]))Wa[i]VindexZYXi]))Wi;W-1VindicesZ,s=[]YXi]))s.push(i);WsVlastZ;l--;Xl]))Wa[l]VlastIndexZ;l--;Xl]))break;WlV'
      .replace(/[V-Z]/g, function (c) {
        return {
          Z: 'Where=function(a){var t=T.apply(0,S(arguments,1));if(a==void 0)return function(a){for(var l=a.length',
          Y: ',i=0;i<l;i++',
          X: ')if(t(a[',
          W: 'return ',
          V: '};};'
        }[c];
      })
      .replace(/.+/, '$&9;$&')
      .replace(/(=fu.+?if.)t(?=.+9;)/g, 'Not$1!t')
      .replace(/\{([^\{]+?)\};/g, '$&$1')
      .replace(
        /.+/,
        'find@index@indices@last@lastIndex@(function(T,S){$&})'.replace(
          /(\w+)@/g,
          'var $1WhereNot,$1Where;'
        )
      )
  )(testFor, slice);
  
  /**
   * Gets the value at the specified position in an array or an array-like
   * object.
   * @name nth
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/nth/2.23.0}
   * @param {Array|number} arrayOrN
   *   If an array or an array-like object is given this will be the object from
   *   which the value will be immediately retrieved.  If this is a number, this
   *   will be interpreted as <code>opt_n</code>, the position from which all values passed
   *   into the returned partial function will be retrieved.
   * @param {number} opt_n
   *   If given and <code>arrayOrN</code> is an array or an array-like object, this will be
   *   the position from which the value should be retrieved.  If this is a
   *   negative number the position will be calculated from the end of <code>arr</code>.
   *   If <code>arrayOrN</code> is a number this will become that value.
   * @returns {Function|*}
   *   If <code>arrayOrN</code> is an array or an array-like object the value at position
   *   <code>n</code> will be returned.  Otherwise if <code>arrayOrN</code> is a number a partial
   *   function that will already have the value of <code>opt_n</code> and will be awaiting
   *   the array will be returned.
   */
  function nth(arrayOrN, opt_n) {
    return opt_n != undefined
      ? arrayOrN[opt_n < 0 ? arrayOrN.length + opt_n : opt_n]
      : function(array) {
        return array[arrayOrN < 0 ? array.length + arrayOrN : arrayOrN];
      };
  }
  
  /**
   * Gets the last element in an array or an array-like object.
   * @name first
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/first/2.23.0}
   * @param {Array} array
   *   The array or array-like object from which to pull the first element.
   * @returns {*}
   *   The first value of <code>array</code>.
   */
  var first = nth(0);
  
  /**
   * Iterates through each item in an array or an object, passing the item to a
   * mapping function, and flattens the result into the array or object.
   * @name flatMap
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/flatMap/2.23.0}
   * @param {Array|Object} arrOrObj
   *   Array or object whose items should be iterated through and passed to the
   *   mapping function.
   * @param {Function} mapper
   *   Mapping function which will be passed (1) the item be analyzed, (2) the
   *   corresponding index or key for said item, and (3) a reference to
   *   <code>arrOrObj</code>.  The return value will be flattened into the array or object
   *   returned from <code>flatMap()</code>.
   * @param {*} [opt_thisArg]
   *   The value that will be referenced when using the <code>this</code> keyword within
   *   <code>mapper</code>.  If not given this will refer to the global object.
   * @returns {Array|Object}
   *   If <code>arrOrObj</code> was an array then an array will be returned, otherwise an
   *   object will be returned.  If an array, the values returned from <code>mapper</code>
   *   for each item will occupy the position of the original item in this new
   *   array.  If an object, the values returned from <code>mapper</code> will be used
   *   instead of the items that were passed in and the new object will use the
   *   key(s) from the objects returned for this new object.
   */
  function flatMap(arrOrObj, callback, thisArg) {
    if (Array.isArray(arrOrObj)) {
      for (var returned, insertI = 0, i = 0, l = arrOrObj.length, result = new Array(l); i < l; i++ , insertI++) {
        if (i in arrOrObj) {
          if (!Array.isArray(returned = callback.call(thisArg, arrOrObj[i], i, arrOrObj))) {
            returned = [returned];
          }
          result.splice.apply(result, [insertI, 1].concat(returned));
          insertI = insertI + returned.length - 1;
        }
      }
    }
    else {
      var result = {};
      for (var i in arrOrObj) {
        if (has(arrOrObj, i)) {
          extend(result, callback.call(thisArg, arrOrObj[i], i, arrOrObj));
        }
      }
    }
    return result;
  }
  
  /**
   * Create a flattened copy of an array.
   * @name flatten
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/flatten/2.23.0}
   * @param {Array} array
   *   Array to flatten.
   * @param {boolean|number} [opt_depth=1]
   *   If <code>true</code> or <code>Infinity</code> there will be no limit to the recursiveness of
   *   the flattening.  Otherwise, if given, this should be a number indicated
   *   how many levels down this array should be flattened.
   * @returns {Array}
   *   Flattened version of <code>array</code> up to the given <code>opt_depth</code>.
   */
  function flatten(array, opt_depth) {
    opt_depth = opt_depth == undefined ? 1 : opt_depth;
    var result = [];
    for (var nextDepth = opt_depth === true || (opt_depth - 1), i = 0, l = array.length; i < l; i++) {
      if (opt_depth > 0 && typeOf(array[i]) == 'Array') {
        result = result.concat(flatten(array[i], nextDepth));
      }
      else {
        result.push(array[i]);
      }
    }
    return result;
  }
  
  /**
   * Creates a new object with keys to all of the values regardless of the
   * level(excluding recursive objects). Keys for values on deeper levels will
   * be joined by a "." while any dots or backslashes in the original keys will
   * be escaped with a leading blackslash.
   * @name flattenKeys
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/flattenKeys/2.23.0}
   * @param {Object} obj
   *   The object whose keys should be flattened.
   * @param {boolean} [opt_keyedObjects=false]
   *   Determines whether or not to keep the objects as well as adding flattened
   *   keys.
   * @returns {Object}
   *   Returns a new object representing <code>obj</code> but having all nested keys
   *   represented by flat keys within the returned object.
   */
  function flattenKeys(obj, opt_keyedObjects) {
    function recurse(base, path, keys, keysCount, ancestors) {
      for (var innerKeys, innerKeysCount, v, k, i = 0; i < keysCount; i++) {
        v = base[k = keys[i]];
        innerKeys = (v && 'object' === typeof v) ? getKeys(v) : [];
        if (ancestors.indexOf(v) < 0) {
          k = k.replace(/\\|\./g, '\\$&');
          if (innerKeysCount = innerKeys.length) {
            recurse(v, path + k + '.', innerKeys, innerKeysCount, ancestors.concat([v]));
          }
          if (opt_keyedObjects || !innerKeysCount) {
            result[path + k] = v;
          }
        }
      }
    }
  
    var getKeys = Object.keys,
        result = {},
        keys = (obj && 'object' === typeof obj) ? getKeys(obj) : [],
        keysCount = keys.length;
    if (keysCount) {
      recurse(obj, '', keys, keysCount, [obj]);
    }
    return result;
  }
  
  /**
   * Creates a string representation of the date using the specified format
   * string.
   * @name formatDate
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/formatDate/2.23.0}
   * @param {Date} date
   *   Date object to be formatted as a string.
   * @param {string} strFormat
   *   If part of the string is wrapped in single quotes, those single quotes
   *   will be removed, any groupings of 2 consecutive single quotes will be
   *   replaced with just one and the other characters will appear in the
   *   returned string as is.  <code>YYYY</code> will be replaced by the full year (eg.
   *   2018).  <code>YY</code> will be replaced by a 2-digit representation of the year
   *   (eg. 18).  <code>MMMM</code> will be replaced by the name of the month (eg.
   *   September).  <code>MMM</code> will be replaced by the 1st 3 characters of the name
   *   of the month (eg. Sep).  <code>MM</code> will be replaced by a 2-digit
   *   representation of the month (eg. 09).  <code>M</code> will be replaced by a numeric
   *   representation of the month (eg. 9).  <code>DDDD</code> will be replaced by the name
   *   of the day of the week (eg. Sunday).  <code>DDD</code> will be replaced by the 1st 3
   *   characters of the name of the day of the week (eg. Sun).  <code>DD</code> will be
   *   replaced by a 2-digit representation of the day of the month (eg. 02).
   *   <code>D</code> will be replaced by a numeric representation of the day of the month
   *   (eg. 2).  <code>HH</code> will be replaced with a 24-hour 2-digit representation of
   *   the hour (eg. 15).  <code>H</code> will be replaced with a 24-hour numeric
   *   representation of the hour (eg. 15).  <code>hh</code> will be replaced with a
   *   12-hour 2-digit representation of the hour (eg. 03).  <code>h</code> will be
   *   replaced with a 12-hour numeric representation of the hour (eg. 3).  <code>mm</code>
   *   will be replaced with a 2-digit representation of the minutes (eg. 26).
   *   <code>m</code> will be replaced with a numeric representation of the minutes (eg.
   *   26).  <code>ss</code> will be replaced with a 2-digit representation of the seconds
   *   (eg. 01).  <code>s</code> will be replaced with a numeric representation of the
   *   seconds (eg. 1).  <code>A</code> will be replaced with "AM" or "PM".  <code>a</code> will be
   *   replaced with "am" or "pm".  <code>SSS</code> will be replaced with a 3-digit
   *   representation of the milliseconds (eg. 000).  <code>S</code> will be replaced with
   *   a numeric representation of the milliseconds (eg. 0).  <code>Z</code> will be
   *   replaced with a representation of the timezone offset that starts off
   *   with "+" or "-", followed by 2 digits that represent the hours offset,
   *   followed by 2 digits that represent the minutes offset.
   * @param {Object} [opt_overrides]
   *   Object that can override the preset values.  If the <code>months</code> property is
   *   defined it should be an array of 12 month names starting with January.
   *   If the <code>days</code> property is defined it should be an array of the 7 days of
   *   the week starting with Sunday.
   * @returns {string}
   *   A string representation of <code>date</code> where all of the rules for <code>strFormat</code>
   *   are followed.
   */
  var formatDate = (function(arrMonths, arrDays) {
    function prefixZeroes(num, end) {
      return ((new Array(end)).join(0) + Math.abs(num)).slice(-end);
    }
    return function(date, strFormat, opt_options) {
      opt_options = opt_options || {};
      var arrCustomMonths = opt_options.months || arrMonths,
          arrCustomDays = opt_options.days || arrDays,
          YYYY = date.getFullYear(),
          M = date.getMonth(),
          MMMM = arrCustomMonths[M++],
          DDDD = arrCustomDays[date.getDay()],
          D = date.getDate(),
          H = date.getHours(),
          h = (H % 12) || 12,
          m = date.getMinutes(),
          s = date.getSeconds(),
          S = date.getMilliseconds(),
          tzOffset = date.getTimezoneOffset(),
          subs = {
            YYYY: YYYY,
            YY: YYYY % 100,
            MMMM: MMMM,
            MMM: MMMM.slice(0, 3),
            MM : prefixZeroes(M, 2),
            M: M,
            DDDD: DDDD,
            DDD: DDDD.slice(0, 3),
            DD: prefixZeroes(D, 2),
            D: D,
            HH: prefixZeroes(H, 2),
            hh: prefixZeroes(h, 2),
            H: H,
            h: h,
            mm: prefixZeroes(m, 2),
            m: m,
            ss: prefixZeroes(s, 2),
            s: s,
            a: H < 12 ? 'am' : 'pm',
            A: H < 12 ? 'AM' : 'PM',
            SSS: prefixZeroes(S, 3),
            S: S,
            Z: (tzOffset < 0 ? '+' : '-') + prefixZeroes(tzOffset / 60, 2) + prefixZeroes(tzOffset % 60, 2)
          };
      return strFormat.replace(
        /YY(?:YY)?|M{1,4}|D{1,4}|HH?|hh?|mm?|ss?|a|A|S(?:SS)?|Z|'((?:[^']+|'')+)'/g,
        function(m, quoted) {
          return quoted ? quoted.replace(/''/g, "'") : subs[m]
        }
      );
    };
  })(
    'January February March April May June July August September October November December'.split(' '),
    'Sunday Monday Tuesday Wednesday Thursday Friday Saturday'.split(' ')
  );
  
  /**
   * Formats the time (number of milliseconds) given based on a format string.
   * If you want to format time using a <code>Date</code> object with a timezone different
   * from UTC (GMT) you should use <code>formatDate()</code>.
   * @name formatTime
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/formatTime/2.23.0}
   * @param {number} ms
   *   Number of milliseconds that will be represented in the returned string.
   * @param {string} format
   *   Format string where <code>W</code> (or <code>w</code>) will be replaced by the number of weeks,
   *   <code>D</code> (or <code>d</code>) by the number of days (maximum being 6), <code>H</code> (or <code>h</code>) by the
   *   number of hours (maximum being 23), <code>m</code> by the number of minutes (maximum
   *   being 59), <code>s</code> by the number of seconds (maximum being 59), and <code>S</code> by
   *   the number of milliseconds (maximum being 999).  Adding an exclamation
   *   point to the end of any of those characters (eg. <code>S!</code>) will result in the
   *   corresponding value not being truncated based on the next highest time
   *   increment.  Using <code>HH</code>, <code>hh</code>, <code>mm</code>, <code>ss</code> or <code>SSS</code> will result in leading
   *   zeroes for those corresponding values.  Instances of <code>-</code> will be removed
   *   only if <code>ms</code> is positive.  Instances of <code>+</code> will be replaced with <code>-</code>
   *   only if <code>ms</code> is negative, otherwise <code>+</code> will remain.  Wrapping a
   *   substring in single quotes or double quotes will cause that substring to
   *   be escaped and any instances of that same quote character being repeated
   *   once will be replaced with just one.  Wrapping a substring with the hash
   *   character (#) will result in a conditional string where any underlying
   *   substring wrapped in parentheses will only be returned if the preceding
   *   meta-group is not <code>1</code>.  If the the conditional substring is specified and
   *   the preceding meta-group is <code>1</code> then either nothing will be printed  or
   *   if the parenthesized group contains an unescaped vertical bar anything
   *   found thereafter will be printed.
   * @returns {string}
   *   The value of <code>ms</code> formatted based on <code>format</code>.
   */
  function formatTime(ms, format) {
    var lastNum, floor = Math.floor, isNegative = ms < 0 || 1 / ms < 0;
    ms = Math.abs(floor(ms));
    return format.replace(
      /(\\.)|'((?:[^']+|'')+)'|"((?:[^"]+|"")+)"|#((?:[^#]+|##)+)#|(\+)|(\-)|(([Ww])|([Dd])|(HH?|hh?)|(mm?)|(ss?)|(S(?:SS)?))(\!)?/g,
      function (all, escaped, inner, inner2, inner3, plus, minus, num, w, d, h, m, s, S, full) {
        if (escaped) {
          return escaped;
        }
        if (num) {
          lastNum = all = S
            ? !full ? ms % 1e3 : ms
            : s
              ? (all = floor(ms / 1e3), !full ? all % 60 : all)
              : m
                ? (all = floor(ms / 6e4), !full ? all % 60 : all)
                : h
                  ? (all = floor(ms / 36e5), !full ? all % 24 : all)
                  : d
                    ? (all = floor(ms / 864e5), !full ? all % 7 : all)
                    : floor(ms / 6048e5);
          num = num.length;
          return (full || num === 1) ? all : ('00' + all).slice(-num);
        }
  
        inner = inner || inner2 || inner3;
  
        var quote = inner3 ? '#' : inner2 ? '"' : "'";
  
        return quote
          ? (
              (quote === '#' && lastNum != undefined)
              ? inner.replace(/\(((?:[^\\\|\(\)]|\\.)*)(?:\|((?:[^\\\|\(\)]|\\.)*))?\)/g, lastNum === 1 ? '$2' : '$1')
              : inner
            ).split(quote + quote).join(quote)
          : isNegative ? '-' : plus ? '+' : '';
      }
    ).replace(/\\(.)/g, '$1');
  }
  
  /**
   * Gets the fractional part of a number, removing the integral part.
   * @name frac
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/frac/2.23.0}
   * @param {number} num
   *   The number from which the fractional part will be pulled.
   * @returns {number}
   *   Returns <code>num</code> without the integral part while keeping the sign.
   */
  function frac(num) {
    return +(+num).toExponential().replace(
      /(-?)(\d+(\.?)\d*)e(.+)/,
      function(m, neg, num, dot, offset) {
        var zeroes = Array(Math.abs(offset) + 2).join('0');
        num = (zeroes + num + (dot ? '' : '.') + zeroes).split('.');
        return +(neg + '.' + num.join('').slice(+offset + num[0].length));
      }
    );
  }
  
  /**
   * Creates a string by using the specified sequence of code points.
   * @name fromCodePoints
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/fromCodePoints/2.23.0}
   * @param {...Array|number} codePoints
   *   If an array is passed in as the first parameter then all of the items in
   *   the array will be used as the code points from which the string is the be
   *   created.  Otherwise each argument passed should be a number representing
   *   a code point from which a string will be built.
   * @returns {string}
   *   The string built from the specified code points.
   */
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/fromCodePoint#Polyfill
  function fromCodePoints(inCodePoints) {
    var codePoints = ('object' === typeof inCodePoints && inCodePoints) || arguments;
    var codeUnits = [], codeLen = 0, result = "", fromCharCode = String.fromCharCode;
    for (var codePoint, index = 0, len = codePoints.length; index < len; ++index) {
      codePoint = +codePoints[index];
      // correctly handles all cases including `NaN`, `-Infinity`, `+Infinity`
      // The surrounding `!(...)` is required to correctly handle `NaN` cases
      // The (codePoint>>>0) === codePoint clause handles decimals and negatives
      if (!(codePoint < 1114111 && (codePoint>>>0) === codePoint)) {
        throw RangeError("Invalid code point: " + codePoint);
      }
      if (codePoint <= 65535) { // BMP code point
        codeLen = codeUnits.push(codePoint);
      }
      else { // Astral code point; split in surrogate halves
        // https://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
        codePoint -= 65536;
        codeLen = codeUnits.push(
          (codePoint >> 10) + 55296,  // highSurrogate
          (codePoint % 1024) + 56320 // lowSurrogate
        );
      }
      if (codeLen >= 16383) {
        result += fromCharCode.apply(0, codeUnits);
        codeUnits.length = 0;
      }
    }
    return result + fromCharCode.apply(0, codeUnits);
  }
  
  /**
   * Decodes any <code>%##</code> encoding in the given string. Plus symbols (<code>'+'</code>) are
   * decoded to a space character.
   * @name unescape
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/unescape/2.23.0}
   * @param {string} str
   *   String to be decoded.
   * @returns {string}
   *   Decoded version of <code>str</code>.
   */
  function unescape(str) {
    return decodeURIComponent(str.replace(/%(?![\dA-F]{2})/gi, '%25').replace(/\+/g, '%20'));
  }
  
  /**
   * Takes a serialized URL parameter string and turns it into an object.
   * @name fromParams
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/fromParams/2.23.0}
   * @param {string} str
   *   The serialized URL parameter string to turned into an object.
   * @param {Function} [opt_parser]
   *   Optional function that parses each value found in <code>str</code>.  For each value
   *   found this function will be passed (1) the value to be parsed and (2) the
   *   path to this value as an array and the return value will be used as the
   *   value to store in the object returned by <code>fromParams()</code>.
   * @returns {Object}
   *   The object version of <code>str</code>.
   */
  function fromParams(str, opt_parser) {
    var result = {};
    str.replace(
      /([^&=]+)(?:=([^&]*))?/g,
      function(match, key, value) {
        var base = result;
        value = unescape(value);
  
        if (!opt_parser) {
          value = /^-?\d+(\.\d+)?$/.test(value)
            ? +value
            : value === 'true'
              ? true
              : value === 'false'
                ? false
                : /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d.\d{3}Z$/.test(value)
                  ? new Date(value)
                  : value;
        }
        unescape(key)
          .match(/(^(?:[^\[\]\\]+|\\.)+)|\[((?:[^\[\]\\]+|\\.)+)\]/g)
          .forEach(function (key, index, keys) {
            key = (index ? key.slice(1, -1) : key).replace(/\\(.)/g, '$1');
            if (keys.length - 1 === index) {
              base[key] = opt_parser
                ? opt_parser(value, keys)
                : value;
            }
            else if (has(base, key)) {
              base = base[key];
            }
            else {
              base[key] = base = {};
            }
          });
      }
    );
    return result;
  }
  
  /**
   * Gets the nearest 32-bit single precision float representation of a number.
   * @name fround
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/fround/2.23.0}
   * @param {number} doubleFloat
   *   A Number. If the parameter is of a different type, it will get converted
   *   to a number or to <code>NaN</code> if it cannot be converted.
   * @returns {number}
   *   The nearest 32-bit single precision float representation of the given
   *   number.
   */
  var fround;
  (function (array) {
    fround = function(x) {
      return array[0] = x, array[0];
    };
  })(new Float32Array(1));
  
  /**
   * Finds the largest integer which evenly divides the two specified integers.
   * @name gcd
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/gcd/2.23.0}
   * @param {number} int1
   *   The first of two integers for which the greatest common denominator
   *   should be found.
   * @param {number} int2
   *   The second of two integers for which the greatest common denominator
   *   should be found.
   * @returns {number}
   *   Returns the largest integer which evenly divides <code>int1</code> and <code>int2</code>.
   */
  function gcd(int1, int2) {
    return int2 ? gcd(int2, int1 % int2) : int1;
  }
  
  /**
   * Get the value of the specified property name.
   * @name get
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/get/2.23.0}
   * @param {Object|undefined} obj
   *   Object to get the property from.
   * @param {string|undefined} [opt_name=undefined]
   *   Name of the property.
   * @param {*} [opt_default=undefined]
   *   The default value to return if the specified property (<code>opt_name</code>) is not
   *   owned by <code>obj</code>.
   * @returns {*}
   *   If both <code>obj</code> and <code>opt_name</code> are specified then either the owned property
   *   will be returned or <code>opt_default</code> will be returned.  If <code>obj</code> is
   *   <code>undefined</code> a partial function will be returned that will accept an
   *   object to always pull the same property name (<code>opt_name</code>) from and an
   *   optional different default value.  If <code>opt_name</code> was not specified or is
   *   <code>undefined</code> a partial function will be returned that will accept a name
   *   for a property to always pull from the same object (<code>obj</code>) and a
   *   different default value.
   */
  function get(obj, opt_name, opt_default) {
    var emptyIndex = obj != undefined ? opt_name == undefined ? 1 : -1 : 0;
    return (emptyIndex + 1)
      ? Function('g,a,b,c', 'return function(x,y){return g(a,x,b,arguments.length>1?y:c)}'.replace(emptyIndex ? 'b,' : 'a,', ''))(get, obj, opt_name, opt_default)
      : has(obj, opt_name)
        ? obj[opt_name]
        : opt_default;
  }
  
  /**
   * Get the value starting at root and traversing the given path.
   * @name getIn
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/getIn/2.23.0}
   * @param {*} root
   *   The root object from which to start traversing <code>path</code>.
   * @param {Array} path
   *   An array of names indicating the path under <code>root</code> that should be
   *   traversed.
   * @param {*} [opt_default]
   *   The default value that will be returned if <code>path</code> cannot be fully
   *   traversed.
   * @returns {*}
   *   If the path cannot be fully traversed, <code>opt_default</code> will be returned.
   *   Otherwise the value found at <code>path</code> will be returned.
   */
  // function getIn(root, path, opt_default) {
  //   for (
  //     var targetType, target = root, i = 0, l = path.length;
  //     target != undefined
  //       && (('object' === (targetType = typeof target) || 'function' === targetType)
  //         ? path[i] in target
  //         : (undefined !== target[path[i]]))
  //       && i < l;
  //     i++
  //   ) {
  //     target = target[path[i]];
  //   }
  //   return i === l ? target : opt_default;
  // }
  
  /**
   * Determine if a path can be fully traversed starting at a given root value.
   * @name hasIn
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/hasIn/2.23.0}
   * @param {*} root
   *   The root object from which to start traversing <code>path</code>.
   * @param {Array} path
   *   An array of names indicating the path under <code>root</code> that should be
   *   traversed.
   * @returns {boolean}
   *   If the path can be fully traversed <code>true</code> will be returned, otherwise
   *   <code>false</code> will be returned.
   */
  // function hasIn(root, path, nothing) {
  //   for (
  //     var targetType, target = root, i = 0, l = path.length;
  //     target != undefined
  //       && (('object' === (targetType = typeof target) || 'function' === targetType)
  //         ? path[i] in target
  //         : (undefined !== target[path[i]]))
  //       && i < l;
  //     i++
  //   ) {
  //     target = target[path[i]];
  //   }
  //   return i === l;
  // }
  
  /**
   * Validate a proposed path and determine how much of it can actually be
   * traversed.
   * @name pathIn
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/pathIn/2.23.0}
   * @param {*} root
   *   The root object from which to start traversing <code>path</code>.
   * @param {Array} path
   *   An array of names indicating the path under <code>root</code> that should be
   *   traversed.
   * @returns {Array}
   *   The path that can actually be traversed.
   */
  // function pathIn(root, path, nothing) {
  //   for (
  //     var targetType, target = root, i = 0, l = path.length, result = [];
  //     target != undefined
  //       && (('object' === (targetType = typeof target) || 'function' === targetType)
  //         ? path[i] in target
  //         : (undefined !== target[path[i]]))
  //       && i < l;
  //     i++
  //   ) {
  //     target = target[path[i]];
  //     result.push(path[i]);
  //   }
  //   return result;
  // }
  
  /**
   * Sets a value under a root object after traversing the given path.
   * @name setIn
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/setIn/2.23.0}
   * @param {*} root
   *   The root object from which to start traversing <code>path</code>.
   * @param {Array} path
   *   An array of names indicating the path under <code>root</code> that should be
   *   traversed.
   * @param {*} value
   *   The value to set under <code>root</code> at the given <code>path</code>.
   * @returns {*}
   *   A reference to <code>root</code>.  Even if the value cannot be set <code>root</code> will still
   *   be returned.
   */
  // function setIn(root, path, value) {
  //   for (
  //     var targetType, target = root, i = 0, l = path.length - 1;
  //     target != undefined
  //       && (('object' === (targetType = typeof target) || 'function' === targetType)
  //         ? path[i] in target
  //         : (undefined !== target[path[i]]))
  //       && i < l;
  //     i++
  //   ) {
  //     target = target[path[i]];
  //   }
  //   if (target != undefined) {
  //     target[path[i]] = value;
  //   }
  //   return root;
  // }
  
  /**
   * Get all of the values in root at each step of the given path.
   * @name getAllIn
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/getAllIn/2.23.0}
   * @param {*} root
   *   The root object from which to start traversing <code>path</code>.
   * @param {Array} path
   *   An array of names indicating the path under <code>root</code> that should be
   *   traversed.
   * @returns {Array}
   *   An array with the value found at each step of <code>path</code> starting at <code>root</code>.
   *   Since values may not exist past a certain point, this array may be
   *   shorter than <code>path</code>.
   */
  // // ORIGINAL SOURCE CODE:  getAllIn(), getIn(), hasIn(), pathIn(), setIn()
  // function getAllIn(root, path, nothing) {
  //   for (
  //     var targetType, target = root, i = 0, l = path.length, result = [];
  //     target != undefined
  //       && (('object' === (targetType = typeof target) || 'function' === targetType)
  //         ? path[i] in target
  //         : (undefined !== target[path[i]]))
  //       && i < l;
  //     i++
  //   ) {
  //     target = target[path[i]];
  //     result.push(target);
  //   }
  //   return result;
  // }
  eval('ZgetAllInY,o=[]X,o.push(t)Wo}ZgetInYXWi===l?t:v}ZhasInYXWi===l}ZsetInY-1X;void 0!=t&&(t[p[i]]=v)Wr}ZpathInY,o=[]X,o.push(p[i])Wo}'
    .replace(/[W-Z]/g, function(c) {
      return {
        Z: 'function ',
        Y: '(r,p,v){for(var T,t=r,i=0,l=p.length',
        X: ';void 0!=t&&("object"===(T=typeof t)||"function"===T?p[i]in t:void 0!==t[p[i]])&&i<l;i++)t=t[p[i]]',
        W: ';return '
      }[c];
    })
  );
  
  /**
   * Retrieves one or more cookies.
   * @name getCookie
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/getCookie/2.23.0}
   * @param {string|RegExp|undefined|null} [name=undefined]
   *   Either the name of the cookie to get or a regular expression matching the
   *   names of all of the cookies to get.  If <code>undefined</code> or <code>null</code> is supplied
   *   that means all cookies should be retrieved.
   * @param {*} [opt_defaultValue=undefined]
   *   The default value to return in the case that <code>name</code> was a string but the
   *   corresponding cookie doesn't exist.
   * @returns {*}
   *   If <code>name</code> is a string and matches the name of a cookie, that cookie's
   *   value will be returned as a string.  If <code>name</code> is a string but doesn't
   *   match the name of a cookie <code>opt_defaultValue</code> will be returned.  If
   *   <code>name</code> is of type <code>RegExp</code> an object containing all of the cookies with
   *   names that match the specified regular expression will be returned.  If
   *   <code>name</code> is <code>undefined</code> or <code>null</code> an object containing all of the cookies
   *   will be returned.  If an object is returned each key will represent the
   *   name of each cookie and each value will be the value of that cookie.
   */
  function getCookie(name, opt_defaultValue) {
    if(typeOf(name) === 'RegExp') {
      var ret = getCookie();
      for(var key in ret) {
        if(!name.test(key)) {
          delete ret[key];
        }
      }
    }
    else if(name == undefined) {
      var ret = {};
      __DOCUMENT.cookie.replace(/(?:^|;\s*)(.+?)(?=;\s*|$)/g, function($0, $1) {
        $1 = $1.match(/(.*?)=(.*)/);
        ret[decodeURIComponent($1[1])] = decodeURIComponent($1[2]);
      });
    }
    else {
      name = __DOCUMENT.cookie.match(new RegExp("(?:^|;\\s*)" + encodeURIComponent(name) + "=(.*?)(?:;\\s*|$)"));
      if(name) {
        var ret = decodeURIComponent(name[1]);
      }
    }
    return ret == undefined ? opt_defaultValue : ret;
  }
  
  /**
   * Gets a reference to the global object.
   * @name getGlobal
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/getGlobal/2.23.0}
   * @returns {global}
   *   A reference to the global object.
   */
  function getGlobal () {
    return __GLOBAL;
  }
  
  /**
   * Tests to see if the given <code>path</code> exists under a specific <code>root</code> object.
   * @name hasAt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/hasAt/2.23.0}
   * @param {*} root
   *   Object to start at as the root of the proposed <code>path</code>.
   * @param {Array} path
   *   An array of strings and/or numbers that represent the path to be tested
   *   under <code>root</code>.
   * @returns {boolean}
   *   <code>true</code> if the specified <code>path</code> is found under <code>root</code>, otherwise <code>false</code>.
   */
  function hasAt(root, path) {
    return path.length > 0 && testAt(root, path).length === path.length;
  }
  
  /**
   * Determines if there is a cookie with the specified name.
   * @name hasCookie
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/hasCookie/2.23.0}
   * @param {string} name
   *   The name of the proposed cookie.
   * @returns {boolean}
   *   <code>true</code> if a cookie with the specified <code>name</code> is found.
   */
  function hasCookie(name) {
    return (';' + __DOCUMENT.cookie).replace(/;\s+/g, ';').indexOf(';' + encodeURIComponent(name) + '=') >= 0;
  }
  
  /**
   * Creates a string representation of an ID (non-negative integer) based on
   * the given hash value and alphabet.
   * @name hashID
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/hashID/2.23.0}
   * @param {number} intID
   *   Non-negative integer that will be turned into a string.
   * @param {number} [opt_intHash=0]
   *   Non-negative integer that will be used to offset the characters used to
   *   represent <code>intID</code>.
   * @param {string} [opt_strAlpha="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"]
   *   All of the characters that can be used to represent <code>intID</code>.  Defaults to
   *   the digits 0 to 9 followed by A to Z followed by a to z.
   * @returns {string}
   *   The string representation of <code>intID</code> based on the hash and the alphabet.
   */
  function hashID(intID, opt_intHash, opt_strAlpha) {
    intID = !(intID >= 0) ? 0 : Math.round(intID);
    opt_intHash = Math.max(0, Math.min(Math.round(opt_intHash || 0), 9007199254740991));
    opt_strAlpha = opt_strAlpha || '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  
    var encodedID = '';
    var FULL_ALPHA_LENGTH = opt_strAlpha.length;
  
    do {
      var newAlpha = '';
      for (var x, i = opt_intHash; opt_strAlpha; i += opt_intHash) {
        newAlpha += opt_strAlpha.charAt(x = i % opt_strAlpha.length);
        opt_strAlpha = opt_strAlpha.slice(0, x) + opt_strAlpha.slice(x + 1);
      }
      opt_strAlpha = newAlpha;
  
      encodedID = opt_strAlpha.charAt(intID % FULL_ALPHA_LENGTH) + encodedID;
    } while(intID = ~~(intID / FULL_ALPHA_LENGTH));
    return encodedID;
  }
  
  /**
   * Tests to see if all of the given property names are owned by the specified
   * object.
   * @name hasKeys
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/hasKeys/2.23.0}
   * @param {Object|null|undefined} obj
   *   If <code>undefined</code> or <code>null</code>, a partial function waiting for this value will
   *   be returned.  Otherwise this will be the object from which the specified
   *   keys will be deleted.
   * @param {Array} keys
   *   Array of keys to see if they are owned by <code>obj</code>.
   * @returns {boolean|Function}
   *   If <code>obj</code> is <code>undefined</code> or <code>null</code> this will be a partial function which
   *   will simply accept the given <code>obj</code>.  The return value of the partial
   *   function is the same as the return value of this function in the case
   *   that <code>obj</code> is not <code>undefined</code> or <code>null</code>.  If all of the keys are owned by
   *   <code>obj</code>, <code>true</code> will be returned, otherwise <code>false</code>.
   */
  function hasKeys(obj, keys) {
    function hasKeys(obj) {
      obj = Object(obj);
      for (var i = keys.length; i--; ) {
        if (!has(obj, keys[i])) {
          return false;
        }
      }
      return true;
    }
    return obj == undefined ? hasKeys : hasKeys(obj);
  }
  
  /**
   * Converts a normal string into a string that can be treated as raw HTML.
   * @name htmlify
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/htmlify/2.23.0}
   * @param {string} str
   *   The string to be converted to raw HTML.
   * @param {boolean} [optKeepWhitespace=false]
   *   If set to <code>true</code> the returned string will convert multiple, consecutive
   *   spaces to non-breaking spaces followed by one normal space. If set to
   *   <code>true</code>, line breaks will be converted to <code><br /></code>.
   * @returns {string}
   *   HTMLified version of <code>str</code>.
   */
  function htmlify(str, opt_keepWhitespace) {
    str = str.replace(/&/g, '&amp;')
             .replace(/</g, '&lt;')
             .replace(/>/g, '&gt;')
             .replace(/"/g, '&quot;')
             .replace(/'/g, '&#x27;')
             .replace(/`/g, '&#x60;');
    return opt_keepWhitespace
      ? str.replace(/\r?\n|\r/g, '<br />')
           .replace(/\t/g, '    ')
           .replace(/^ | (?= )| $/g, '&nbsp;')
      : str;
  }
  
  /**
   * Determines the square root of the sum of the squares of the arguments
   * passed.
   * @name hypot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/hypot/2.23.0}
   * @param {...number} numX
   *   Each of the numbers that will be squared and summed together.
   * @returns {number}
   *   The square root of the sum of the squares of the arguments passed.
   */
  function hypot() {
    var val, ret = 0, args = arguments, i = args.length;
    while (i--) {
      ret += (val = +args[i]) * val;
    }
    return Math.sqrt(ret);
  }
  
  /**
   * Tests <code>subject</code> against <code>control</code> and if it results in a <code>false</code>-ish value
   * <code>falsyValue</code> will be passed back.  Otherwise, if <code>opt_truthyValue</code> is given
   * it will be passed back, but if not <code>subject</code> will be passed back.
   * @name ifFail
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifFail/2.23.0}
   * @param {*} subject
   *   Value to test.
   * @param {Function|RegExp} control
   *   If this is a function it will be called with <code>subject</code> being the only
   *   argument.  Otherwise, this regular expression will be tested against
   *   <code>subject</code>.
   * @param {*} falsyValue
   *   Value to be returned if testing <code>subject</code> against <code>control</code> results in a
   *   <code>false</code>-ish value.
   * @param {*} [opt_truthyValue=subject]
   *   Value to be returned if testing <code>subject</code> against <code>control</code> results in a
   *   <code>true</code>-ish value.
   * @returns {*}
   *   <code>falsyValue</code> will be returned if testing <code>subject</code> against <code>control</code>
   *   results in a <code>false</code>-ish value.  Otherwise, if <code>opt_truthyValue</code> is given
   *   it will be returned, but if not given <code>subject</code> will be returned.
   */
  
  /**
   * Tests <code>subject</code> against <code>control</code> and if it results in a <code>true</code>-ish value
   * <code>truthyValue</code> will be passed back.  Otherwise, if <code>opt_falsyValue</code> is given
   * it will be passed back, but if not <code>subject</code> will be passed back.
   * @name ifPass
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifPass/2.23.0}
   * @param {*} subject
   *   Value to test
   * @param {Function|RegExp} control
   *   If this is a function it will be called with <code>subject</code> being the only
   *   argument.  Otherwise, this regular expression will be tested against
   *   <code>subject</code>.
   * @param {*} truthyValue
   *   Value to be returned if testing <code>subject</code> against <code>control</code> results in a
   *   <code>true</code>-ish value.
   * @param {*} [opt_falsyValue=subject]
   *   Value to be returned if testing <code>subject</code> against <code>control</code> results in a
   *   <code>false</code>-ish value.
   * @returns {*}
   *   <code>truthyValue</code> will be returned if testing <code>subject</code> against <code>control</code>
   *   results in a <code>true</code>-ish value.  Otherwise, if <code>opt_falsyValue</code> is given
   *   it will be returned, but if not given <code>subject</code> will be returned.
   */
  
  /**
   * Tests <code>subject</code> against <code>control</code> and if it results in a <code>false</code>-ish value
   * <code>falsyValue</code> will be passed back.  Otherwise, if <code>opt_truthyValue</code> is given
   * it will be passed back, but if not <code>control</code> will be passed back.
   * @name ifFails
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifFails/2.23.0}
   * @param {Function|RegExp} control
   *   If this is a function it will be called with <code>subject</code> being the only
   *   argument.  Otherwise, this regular expression will be tested against
   *   <code>subject</code>.
   * @param {*} subject
   *   Value to test.
   * @param {*} falsyValue
   *   Value to be returned if testing <code>subject</code> against <code>control</code> results in a
   *   <code>false</code>-ish value.
   * @param {*} [opt_truthyValue=subject]
   *   Value to be returned if testing <code>subject</code> against <code>control</code> results in a
   *   <code>true</code>-ish value.
   * @returns {*}
   *   <code>falsyValue</code> will be returned if testing <code>subject</code> against <code>control</code>
   *   results in a <code>false</code>-ish value.  Otherwise, if <code>opt_truthyValue</code> is given
   *   it will be returned, but if not given <code>control</code> will be returned.
   */
  
  /**
   * If the first argument is in the second argument (array) the third argument
   * is passed back, otherwise the fourth argument (or if not given the first
   * argument) is passed back.
   * @name ifIn
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifIn/2.23.0}
   * @param {*} subject
   *   Value to see if it is in <code>arrControls</code>.
   * @param {Array} arrControls
   *   Array of values to test <code>subject</code> against.
   * @param {*} trueValue
   *   Value to be returned if <code>subject</code> is found within <code>arrControls</code>.
   * @param {*} [opt_falseValue=subject]
   *   Value to be returned if <code>subject</code> is not found within <code>arrControls</code>.
   * @returns {*}
   *   <code>trueValue</code> will be returned if <code>subject</code> is found within <code>arrControls</code>.
   *   Otherwise, if <code>opt_falseValue</code> is given it will be returned, but if not
   *   given <code>subject</code> will be returned.
   */
  
  /**
   * If the second argument is in the first argument (array) the third argument
   * is passed back, otherwise the fourth argument (or if not given the first
   * argument) is passed back.
   * @name ifIncludes
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifIncludes/2.23.0}
   * @param {Array} arrControls
   *   Array of values to test <code>subject</code> against.
   * @param {*} subject
   *   Value to see if it is in <code>arrControls</code>.
   * @param {*} trueValue
   *   Value to be returned if <code>subject</code> is found within <code>arrControls</code>.
   * @param {*} [opt_falseValue=subject]
   *   Value to be returned if <code>subject</code> is not found within <code>arrControls</code>.
   * @returns {*}
   *   <code>trueValue</code> will be returned if <code>subject</code> is found within <code>arrControls</code>.
   *   Otherwise, if <code>opt_falseValue</code> is given it will be returned, but if not
   *   given <code>arrControls</code> will be returned.
   */
  
  /**
   * If the first and second arguments are different the third argument is
   * passed back, otherwise the fourth argument (or if not given the first
   * argument) is passed back.
   * @name ifNot
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifNot/2.23.0}
   * @param {*} subject
   *   Value to test.
   * @param {*} control
   *   Value that is being tested against.
   * @param {*} falseValue
   *   Value to be returned if <code>control</code> and <code>subject</code> are different.
   * @param {*} [opt_trueValue=subject]
   *   Value to be returned if <code>control</code> and <code>subject</code> are the same.
   * @returns {*}
   *   <code>opt_trueValue</code> will be returned if <code>control</code> is different from
   *   <code>subject</code>.  Otherwise, if <code>falseValue</code> is given it will be returned, but
   *   if not given <code>subject</code> will be returned.
   */
  
  /**
   * If the first argument is not in the second argument (array) the third
   * argument is passed back, otherwise the fourth argument (or if not given the
   * first argument) is passed back.
   * @name ifOut
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifOut/2.23.0}
   * @param {*} subject
   *   Value to see if it is in <code>arrControls</code>.
   * @param {Array} arrControls
   *   Array of values to test <code>subject</code> against.
   * @param {*} falseValue
   *   Value to be returned if <code>subject</code> is not found within <code>arrControls</code>.
   * @param {*} [opt_trueValue=subject]
   *   Value to be returned if <code>subject</code> is found within <code>arrControls</code>.
   * @returns {*}
   *   <code>falseValue</code> will be returned if <code>subject</code> is not found within
   *   <code>arrControls</code>.  Otherwise, if <code>opt_trueValue</code> is given it will be
   *   returned, but if not given <code>subject</code> will be returned.
   */
  
  /**
   * Tests <code>subject</code> against <code>control</code> and if it results in a <code>true</code>-ish value
   * <code>truthyValue</code> will be passed back.  Otherwise, if <code>opt_falsyValue</code> is given
   * it will be passed back, but if not <code>control</code> will be passed back.
   * @name ifPasses
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifPasses/2.23.0}
   * @param {Function|RegExp} control
   *   If this is a function it will be called with <code>subject</code> being the only
   *   argument.  Otherwise, this regular expression will be tested against
   *   <code>subject</code>.
   * @param {*} subject
   *   Value to test
   * @param {*} truthyValue
   *   Value to be returned if testing <code>subject</code> against <code>control</code> results in a
   *   <code>true</code>-ish value.
   * @param {*} [opt_falsyValue=control]
   *   Value to be returned if testing <code>subject</code> against <code>control</code> results in a
   *   <code>false</code>-ish value.
   * @returns {*}
   *   <code>truthyValue</code> will be returned if testing <code>subject</code> against <code>control</code>
   *   results in a <code>true</code>-ish value.  Otherwise, if <code>opt_falsyValue</code> is given
   *   it will be returned, but if not given <code>control</code> will be returned.
   */
  
  /**
   * If the first and second arguments are the same the third argument is passed
   * back, otherwise the fourth argument (or if not given the first argument) is
   * passed back.
   * @name ifIs
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifIs/2.23.0}
   * @param {*} subject
   *   Value to test.
   * @param {*} control
   *   Value that is being tested against.
   * @param {*} trueValue
   *   Value to be returned if <code>control</code> and <code>subject</code> are the same.
   * @param {*} [opt_falseValue=subject]
   *   Value to be returned if <code>control</code> and <code>subject</code> are different.
   * @returns {*}
   *   <code>trueValue</code> will be returned if <code>control</code> is <code>subject</code>.  Otherwise, if
   *   <code>opt_falseValue</code> is given it will be returned, but if not given <code>subject</code>
   *   will be returned.
   */
  var ifIs = Function(
        'I,O',
        ',function(a,b,c,d){return?c:(arguments.length>3?d:a)}'
          .replace(/(.+return)(.+)/, '~`_#%'.replace(/./g, '$$1$&$$2$$1$&$$2'))
          .replace(/~/g, '!(@)') // ifIs() & ifNot()
          .replace(/`/g, '!(I(b,a)>=0)') // ifIn() && ifOut
          .replace(/_/g, '!(I(a,b)>=0)') // ifIncludes() && ifExcludes()
          .replace(/#/g, '~b`b.test(a):b(a))') // ifPass() && ifFail()
          .replace(/%/g, '~a`a.test(b):a(b))') // ifPasses() && ifFails()
          .replace(/~(.)`/g, '!(O.toString.call($1)==="[object RegExp]"?') // Fix ifPass(), ifPasses(), ifFail() & ifFails()
          .replace(/!([^!]+(!|$))/g, ' $1')
          .replace(/@/g, 'a!==a?b!==b:a===0?1/a===1/b:a===b') // Fix ifIs() & ifNot() (including NaNs and 0s)
          .replace(/,(.+)/, 'return[$1]')
      )(indexOf, __EMPTY_OBJECT),
      ifNot = ifIs[1],
      ifIn = ifIs[2],
      ifOut = ifIs[3],
      ifIncludes = ifIs[4],
      ifExcludes = ifIs[5],
      ifPass = ifIs[6],
      ifFail = ifIs[7],
      ifPasses = ifIs[8],
      ifFails = ifIs[9];
  ifIs = ifIs[0];
  
  /**
   * If the second argument is not in the first argument (array) the third
   * argument is passed back, otherwise the fourth argument (or if not given the
   * first argument) is passed back.
   * @name ifExcludes
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ifExcludes/2.23.0}
   * @param {Array} arrControls
   *   Array of values to test <code>subject</code> against.
   * @param {*} subject
   *   Value to see if it is in <code>arrControls</code>.
   * @param {*} falseValue
   *   Value to be returned if <code>subject</code> is not found within <code>arrControls</code>.
   * @param {*} [opt_trueValue=subject]
   *   Value to be returned if <code>subject</code> is found within <code>arrControls</code>.
   * @returns {*}
   *   <code>falseValue</code> will be returned if <code>subject</code> is not found within
   *   <code>arrControls</code>.  Otherwise, if <code>opt_trueValue</code> is given it will be
   *   returned, but if not given <code>arrControls</code> will be returned.
   */
  
  /**
   * Determines the 32-bit product of the arguments passed similar to the way it
   * would be calculated in C.
   * @name imul
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/imul/2.23.0}
   * @param {...number} numX
   *   All of the numbers for which you would like to calculate the 32-bit
   *   product.
   * @returns {number}
   *   The C-like 32-bit product of all of the numbers passed in.
   */
  function imul() {
    // Using the lo/hi version because the 0x001fffff/0xffc00000 version doesn't
    // always give the correct answer if the operands are placed in reverse order.
    for (var a, aLo, bLo, args = arguments, i = args.length, b = ~~args[--i]; i-- > 0; ) {
      a = b;
      b = args[i];
      aLo = a & 0xffff;
      bLo = b & 0xffff;
      b = ((aLo * bLo) + (((((a >>> 16) & 0xffff) * bLo + aLo * ((b >>> 16) & 0xffff)) << 16) >>> 0) | 0);
    }
    return b;
  }
  
  /**
   * Determines whether or not a <code>target</code> is present within a given string,
   * array or object.  Unlike <code>Array.prototype.includes()</code>, this function
   * differentiates between <code>-0</code> and <code>0</code>.
   * @name includes
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/includes/2.23.0}
   * @param {Array|Object|string} subject
   *   The array, object or string in which <code>target</code> should be searched for.
   * @param {*} target
   *   The target value to search for within <code>subject</code>.
   * @param {number} [opt_start=0]
   *   The position at which to start the search for strings or arrays.  This
   *   will be ignored for objects.  Negative values will be calculated from the
   *   end.
   * @returns {boolean}
   *   A boolean value indicating if <code>target</code> is present within <code>subject</code> at or
   *   after <code>opt_start</code>.
   */
  function includes(subject, target, opt_start) {
    subject = indexOf(subject, target, opt_start);
    return 'number' === typeof subject ? subject >= 0 : (subject !== undefined);
  }
  
  /**
   * Indexes the <code>target</code> object based on the items found in <code>subject</code> and the
   * <code>callback</code> function.
   * @name index
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/index/2.23.0}
   * @param {Array|Object} subject
   *   Array or object to traverse.
   * @param {Object} target
   *   Object that will be augmented based on the items traversed in <code>subject</code>.
   * @param {Function} callback
   *   Function called for each item within <code>subject</code>.  In order to augment
   *   <code>target</code>, the return value must be an object (or array) that either has
   *   the <code>key</code> and <code>value</code> properties defined, <code>0</code> and <code>1</code> (for an array)
   *   values defined, or <code>k</code> and <code>v</code> properties defined.  All other types of
   *   values returned from this function will be ignored.
   * @returns {Object|Array}
   *   A reference to <code>target</code>.
   */
  var index = (function(kNames, vNames) {
    return function (subject, target, callback) {
      forOf(subject, function (value, key) {
        if (value = callback.call(target, value, key, subject)) {
          for (var kName, vName, i = 0; kName = kNames[i], vName = vNames[i]; i++) {
            if (has(value, kName) && has(value, vName)) {
              target[value[kName]] = value[vName];
              i = 2;
            }
          }
        }
      });
      return target;
    };
  })(
    ['k', 0, 'key'],
    ['v', 1, 'value', false]
  );
  
  /**
   * Determine where 2 strings begin to differ from each other.
   * @name indexOfDiff
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/indexOfDiff/2.23.0}
   * @param {string} str1
   *   A string to be compared to another string.
   * @param {string} str2
   *   A string to be compared to <code>str1</code>.
   * @returns {number}
   *   If <code>str1</code> matches <code>str2</code>, <code>-1</code> will be returned. In all other cases, the
   *   first index at which the corresponding character in <code>str1</code> is not the
   *   same as that found in <code>str2</code> will be returned.
   */
  function indexOfDiff(str1, str2) {
    var splitLen = Math.ceil(Math.min(str1.length, str2.length) / 2),
        s1_1 = str1.slice(0, splitLen), s2_1 = str2.slice(0, splitLen);
    return str1 != str2
      ? splitLen
        ? s1_1 != s2_1
          ? splitLen - 1 && indexOfDiff(s1_1, s2_1)
          : (indexOfDiff(str1.slice(splitLen), str2.slice(splitLen)) + splitLen)
        : 0
      : -1;
  }
  
  /**
   * Gets the information for this version of YourJS.
   * @name toString
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/toString/2.23.0}
   * @returns {string}
   *   A quick summary of the YourJS object simply indicating the version and
   *   the variable name.
   */
  function toString() {
    return 'YourJS v' + __VERSION + ' (' + __VARIABLE_NAME + ')';
  }
  
  /**
   * Gets the information for this version of YourJS.
   * @name info
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/info/2.23.0}
   * @returns {Object}
   *   An object containing a <code>name</code> property and a <code>version</code> property.
   */
  function info() {
    return { name: __VARIABLE_NAME, version: __VERSION, toString: toString };
  }
  
  /**
   * Inserts a value into an array at the given position.
   * @name insertInto
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/insertInto/2.23.0}
   * @param {*} value
   *   Value to insert into <code>array</code>.
   * @param {Array} array
   *   Array into which a value will be inserted.
   * @param {number} position
   *   Position at which to insert the value within <code>array</code>.  A negative value
   *   will mean that the position is counted from the end of <code>array</code>.  A
   *   position of <code>-0</code> means that <code>value</code> will be added to the end of <code>array</code>.
   * @returns {*}
   *   A reference to <code>value</code>.  This is useful when chaining.
   */
  
  /**
   * Inserts a value into an array at the given position.
   * @name insert
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/insert/2.23.0}
   * @param {Array} array
   *   Array into which a value will be inserted.
   * @param {*} value
   *   Value to insert into <code>array</code>.
   * @param {number} position
   *   Position at which to insert the value within <code>array</code>.  A negative value
   *   will mean that the position is counted from the end of <code>array</code>.  A
   *   position of <code>-0</code> means that <code>value</code> will be added to the end of <code>array</code>.
   * @returns {Array}
   *   A reference to <code>array</code>.  This is useful when chaining.
   */
  eval('Intoc,aa,c'.replace(/([^,]*)((.),.)/g, 'function insert$1($2,b){a.splice(0==b&&0>1/b?a.length:b,0,c);return $3}'));
  
  /**
   * Takes a number and converts it to a twos-complement 32-bit signed integer.
   * @name int32
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/int32/2.23.0}
   * @param {number} number
   *   Number to be converted to a twos-complement 32-bit signed integer.
   * @returns {number}
   *   Returns the specified number as a twos-complement 32-bit signed integer.
   */
  
  /**
   * Takes a number and converts it to a 16-bit unsigned integer.
   * @name uint16
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/uint16/2.23.0}
   * @param {number} number
   *   Number to be converted to a 16-bit unsigned integer.
   * @returns {number}
   *   Returns the specified number as a 16-bit unsigned integer.
   */
  
  /**
   * Takes a number and converts it to a 32-bit unsigned integer.
   * @name uint32
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/uint32/2.23.0}
   * @param {number} number
   *   Number to be converted to a 32-bit unsigned integer.
   * @returns {number}
   *   Returns the specified number as a 32-bit unsigned integer.
   */
  
  /**
   * Takes a number and converts it to a 8-bit unsigned integer.
   * @name uint8
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/uint8/2.23.0}
   * @param {number} number
   *   Number to be converted to a 8-bit unsigned integer.
   * @returns {number}
   *   Returns the specified number as a 8-bit unsigned integer.
   */
  
  /**
   * Takes a number and converts it to a twos-complement 8-bit signed integer.
   * @name int8
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/int8/2.23.0}
   * @param {number} number
   *   Number to be converted to a twos-complement 8-bit signed integer.
   * @returns {number}
   *   Returns the specified number as a twos-complement 8-bit signed integer.
   */
  // Creates the following functions:  int8(), uint8(), int16(), uint16(), int32(), and uint32()
  // Works in IE10+
  eval('uUi32I32uUi16I16uUi8I8'.replace(/(u?)([UI]i?)(\d\d?)/g, 'var $1int$3;(function(a){$1int$3=function(x){return a[0]=x,a[0]}})(new $2nt$3Array(1));'));
  
  /**
   * Takes a number and converts it to a twos-complement 16-bit signed integer.
   * @name int16
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/int16/2.23.0}
   * @param {number} number
   *   Number to be converted to a twos-complement 16-bit signed integer.
   * @returns {number}
   *   Returns the specified number as a twos-complement 16-bit signed integer.
   */
  
  /**
   * Gets a new array that will be the intersection of two arrays.
   * @name intersect
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/intersect/2.23.0}
   * @param {Array} array1
   *   First array to be intersected with the second array.
   * @param {Array} array2
   *   Second array to be intersected with the first array.
   * @returns {Array}
   *   An array with all of the values that <code>array1</code> and <code>array2</code> share while
   *   each value only appears as many times as the minimum amount of times it
   *   is found in either array.
   */
  function intersect(array1, array2) {
    return diffArrays(array1, diffArrays(array1, array2));
  }
  
  /**
   * Creates the inverse of an object by making a new object where the keys are
   * the values and the values are the keys.
   * @name invert
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/invert/2.23.0}
   * @param {*} obj
   *   Object whose keys and values will be inverted and returned.
   * @param {*} [opt_initial={}]
   *   Object to which the value/key pairs should be added.  If not given an
   *   empty object will be used.
   * @returns {*}
   *   Adds the value/key pairs found in <code>obj</code> to <code>opt_initial</code> where all of the
   *   keys added will be the values from <code>obj</code> and all of the corresponding
   *   values will be the keys.
   */
  function invert(obj, opt_initial) {
    for (var result = opt_initial || {}, pairs = entries(obj), i = 0, l = pairs.length; i < l; i++) {
      result[pairs[i][1]] = pairs[i][0];
    }
    return result;
  }
  
  /**
   * Tests to see if a number can be the operand of a bitwise operator without
   * losing integral precision.
   * @name isBitable
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isBitable/2.23.0}
   * @param {number} value
   *   Value to see if it can be an operand for a bitwise operator without
   *   losing integral precision.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> will not lose integral precision when used as an
   *   operand for a bitwise operator.  Otherwise <code>false</code>.
   */
  function isBitable(value) {
    return ~~value == Math.trunc(value);
  }
  
  /**
   * Determines if the argument that is passed in is an integer in the range of
   * <code>-9007199254740991</code> and <code>9007199254740991</code>.
   * @name isSafeInt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSafeInt/2.23.0}
   * @param {number} value
   *   Value to test out.
   * @returns {boolean}
   *   Boolean indicating if <code>value</code> is an integer within the range of
   *   <code>-9007199254740991</code> and <code>9007199254740991</code>.
   */
  var isSafeInt;
  (function(MAX_POSITIVE_INT) {
    isSafeInt = function(value) {
      return 'number' == typeof value && value % 1 == 0 && Math.abs(value) <= MAX_POSITIVE_INT;
    };
  })(Math.pow(2,53) - 1);
  
  /**
   * Determines if a number is a prime integer.
   * @name isPrime
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isPrime/2.23.0}
   * @param {number} num
   *   Number to be tested.
   * @returns {boolean}
   *   <code>true</code> if <code>num</code> is a positive integer that is only divisible by itself
   *   and <code>1</code>.  Otherwise <code>false</code>.
   */
  function isPrime(num) {
    if(!isSafeInt(num)) {
      throw new TypeError("expected a finite integer");
    }
    if(num < 2) {
      return false;
    }
    if(num < 4) {
      return true;
    }
    if(num % 2 == 0 || num % 3 == 0) {
      return false;
    }
    for(var max = parseInt(Math.sqrt(num)) + 2, check = 6; check <= max; check += 6) {
      if(num % (check - 1) == 0 || num % (check + 1) == 0) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Determines if a number is a positive integer that is divisible by other
   * integers apart from <code>1</code> and itself.
   * @name isComposite
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isComposite/2.23.0}
   * @param {number} num
   *   Number to be tested.
   * @returns {boolean}
   *   <code>true</code> if <code>num</code> is divisible by another integer other than <code>1</code> and
   *   itself, otherwise <code>false</code>.
   */
  function isComposite(num) {
    return num > 1 && !isPrime(num);
  }
  
  /**
   * Determines if an array, arguments list, other type of list or collection is
   * empty. Also can be used on objects to ensure that it doesn't have any
   * properties explicitly set.
   * @name isEmpty
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isEmpty/2.23.0}
   * @param {*} value
   *   Value to be checked.
   * @returns {boolean}
   *   Returns <code>false</code> if <code>value</code> is an empty array, arguments list, node list,
   *   element collection, or string. Returns <code>false</code> if <code>value</code> has no
   *   properties of its own defined. In all other cases, <code>true</code> is returned.
   */
  function isEmpty(value) {
    var isNotArrayLike = !isArrayLike(value = Object(value));
    for (var k in value) {
      if (has(value, k) && (isNotArrayLike || ~~k == k)) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Determines if a value is an <code>Error</code> object.
   * @name isError
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isError/2.23.0}
   * @param {*} value
   *   Value to be tested.
   * @param {boolean} [opt_testForAnyError=false]
   *   Indicates whether or not any type of object whose type name ends with
   *   <code>"Error"</code> will count as an error object.
   * @returns {boolean}
   *   <code>true</code> if <code>opt_testForAnyError</code> is <code>false</code>-ish or not given and <code>value</code>
   *   is an <code>Error</code> object.  <code>true</code> if <code>opt_testForAnyError</code> is <code>true</code>-ish and
   *   the type name of <code>value</code> ends with <code>"Error"</code>.  Otherwise <code>false</code>.
   */
  function isError(value, opt_testForAnyError) {
    return typeOf(value).slice(opt_testForAnyError ? -5 : 0) == 'Error';
  }
  
  /**
   * Basically used to determine if something is falsy or is an empty collection
   * of sorts.
   * @name isFalsy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isFalsy/2.23.0}
   * @param {*} value
   *   Value to check.
   * @param {boolean} [opt_strict=false]
   *   Ordinarilly <code>isFalsy()</code> will check if a value is falsy or is an empty
   *   collection. If this argument is <code>true</code>-ish <code>isFalsy()</code> will not check
   *   <code>value</code> to see if it is an empty collection but will only return the
   *   equivalent of <code>!value</code>.
   * @returns {boolean}
   *   Returns <code>false</code> if <code>value</code> is an empty array, arguments list, node list,
   *   element collection, or string. Returns <code>false</code> if <code>value</code> is an object of
   *   type <code>Object</code> which has no properties of its own defined. Returns <code>false</code>
   *   if <code>value</code> is falsy. In all other cases, <code>true</code> is returned.
   */
  function isFalsy(value, opt_strict) {
    return !value || (!opt_strict && (isArrayLike(value) || value.constructor == Object) && isEmpty(value));
  }
  
  /**
   * Tests a value to see if it is a number that is also finite.
   * @name isFinite
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isFinite/2.23.0}
   * @param {number|*} value
   *   The value to be tested to see if it is a number and is finite.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is a number and is finite, otherwise <code>false</code> is
   *   returned.
   */
  function isFinite(value) {
    return 'number' == typeof value && __GLOBAL.isFinite(value);
  }
  
  /**
   * Determines if a value is a floating point number.
   * @name isFloat
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isFloat/2.23.0}
   * @param {*} value
   *   Value to be tested.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is a floating point number, otherwise <code>false</code>.
   */
  function isFloat(value) {
    return ('number' == typeof value) && !!(value % 1);
  }
  
  /**
   * Determines if an array, array-like object, or an object is full of items
   * that match a specific type or tester function.
   * @name isFullOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isFullOf/2.23.0}
   * @param {Array|Object} arrOrObj
   *   Array, array-like object, or object whose items will be tested.
   * @param {string|Function} tester
   *   If a string is specified it will be assumed to be the type that all
   *   values in <code>arrOrObj</code> must match.  Otherwise this must be a function that
   *   will accept (1) the item value, (2) the item index, and (3) a reference
   *   to <code>arrOrObj</code> and the return value of this function will coerced to a
   *   boolean to determine if the value matched.
   * @param {boolean} [opt_trueOnEmpty=false]
   *   Indicates whether or not an empty <code>arrOrObj</code> will allow this call to
   *   <code>isFullOf()</code> to evaluate to <code>true</code>.
   * @returns {boolean}
   *   <code>true</code> if all of the items in <code>arrOrObj</code> match <code>tester</code> or if <code>arrOrObj</code>
   *   is empty and <code>opt_trueOnEmpty</code> is <code>true</code>-ish.  Otherwise <code>false</code>.
   */
  function isFullOf(arrOrObj, tester, opt_trueOnEmpty) {
    var result = true, isStrTester = typeOf(tester) == 'String';
    var count = forOf(arrOrObj, function(value, key, arrOrObj, end) {
      if (isStrTester ? typeOf(value) != tester : !tester(value, key, arrOrObj)) {
        result = false;
        end();
      }
    });
    return result && (count > 0 || !!opt_trueOnEmpty);
  }
  
  /**
   * Determines if a value is within a given range.
   * @name isInRange
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isInRange/2.23.0}
   * @param {number|string} value
   *   Value to be checked.
   * @param {number|string} lowerBounds
   *   Lower bounds against which <code>value</code> will be checked.
   * @param {number|string} upperBounds
   *   Upper bounds against which <code>value</code> will be checked.
   * @param {boolean} [opt_excludeBounds=false]
   *   Indicates whether or not equality to the bounds should prevent the
   *   <code>value</code> from being considered within the range.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is greater than or equal <code>lowerBound</code> and less than or
   *   equal <code>upperBound</code> (equality test is ignored if <code>opt_excludeBounds</code> is
   *   <code>true</code>-ish), otherwise <code>false</code>.
   */
  function isInRange(value, lowerBound, upperBound, opt_excludeBounds) {
    return opt_excludeBounds
      ? lowerBound < value && value < upperBound
      : (lowerBound <= value && value <= upperBound);
  }
  
  /**
   * Determines if a value is an integer.
   * @name isInt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isInt/2.23.0}
   * @param {*} value
   *   Value to be tested.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is an integer, otherwise <code>false</code>.
   */
  function isInt(value) {
    return ('number' == typeof value) && value % 1 == 0;
  }
  
  /**
   * Checks to see if a value is of a specific kind.
   * @name isKind
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isKind/2.23.0}
   * @param {*} value
   *   Value to determine if it matches any of the type names given.
   * @param {...string} kindName
   *   Possible kind names to match <code>value</code> against.
   * @returns {boolean}
   *   A boolean indicating if the kinds of <code>value</code> matched against any of the
   *   <code>kindName</code> arguments passed.
   */
  function isKind(value) {
    value = kindsOf(value);
    for (
      var args = slice(arguments), argc = args.length, i = 1;
      i < argc && indexOf(value, args[i]) < 0;
      i++
    );
    return i < argc;
  }
  
  /**
   * Creates a partial isKind() function.
   * @name isKindFor
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isKindFor/2.23.0}
   * @param {...string} kindName
   *   One or more kind names to check against values.
   * @param {boolean} [opt_negate=false]
   *   If specified the returned function's return value will always be negated.
   * @returns {Function}
   *   A function that can be used to see if any value passed to it is one of
   *   the kind names passed in as <code>kindName</code>. The return value will indicate if
   *   one of the kind names matches the kind of the value passed to it. If
   *   <code>opt_negate</code> is <code>true</code> the return value of this function will be the
   *   opposite.
   */
  function isKindFor() {
    var args = slice(arguments),
      argc = args.length,
      negate = argc > 1 && 'boolean' === typeof args[argc - 1] && (argc-- , args.pop());
    return function (value) {
      return isKind.apply(isKind, [value].concat(args)) !== negate;
    };
  }
  
  /**
   * Determine if the specified year is a leap year.
   * @name isLeapYear
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isLeapYear/2.23.0}
   * @param {Date|number} [opt_year=new Date()]
   *   The year to be tested.  If a date is specified the year from that date
   *   will be used.  If nothing is specified the current year will be used.
   * @returns {boolean}
   *   <code>true</code> if <code>year</code> is a leap year, otherwise <code>false</code>.
   */
  function isLeapYear(opt_year) {
    if ('number' != typeof opt_year) {
      opt_year = (opt_year == undefined ? new Date : opt_year).getFullYear();
    }
    return opt_year % (opt_year % 100 ? 4 : 400) == 0;
  }
  
  /**
   * Determines if a string only has characters without casing.
   * @name isNoCase
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isNoCase/2.23.0}
   * @param {string} str
   *   The value to be checked.
   * @returns {boolean}
   *   <code>true</code> if <code>str</code> only has characters without casing. Otherwise <code>false</code> is
   *   returned.
   */
  
  /**
   * Determines if a string doesn't have lower cased characters.
   * @name isUpperCase
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isUpperCase/2.23.0}
   * @param {string} str
   *   The value to be checked.
   * @param {boolean} [opt_allowNoCasing=false]
   *   If <code>true</code>, characters without casing found in <code>str</code> will be viewed as
   *   upper cased characters.
   * @returns {boolean}
   *   <code>true</code> if <code>str</code> only has cased characters that are upper cased or if
   *   <code>opt_allowNoCasing</code> is <code>true</code> and <code>str</code> only has characters without
   *   casing. Otherwise <code>false</code> is returned.
   */
  
  /**
   * Determines if a string has both upper cased and lower cased characters.
   * @name isMixCase
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isMixCase/2.23.0}
   * @param {string} str
   *   The value to be checked.
   * @param {boolean} [opt_allowNoCasing=false]
   *   If <code>true</code>, characters without casing found in <code>str</code> will be viewed as
   *   mixed cased characters.
   * @returns {boolean}
   *   <code>true</code> if <code>str</code> has mixed cased characters or if <code>opt_allowNoCasing</code> is
   *   <code>true</code> and <code>str</code> only has characters without casing. Otherwise <code>false</code> is
   *   returned.
   */
  eval('Mix   Lower Upper No'.replace(/\w+/g, function(name, i) {
    return 'function is' + name + 'Case(s,n){s=0 in arguments?s+"":"";return(s=(s==s.toLowerCase()?6:0)+(s==s.toUpperCase()?12:0))==' + i + '||(!!n&&s>12);}';
  }));
  
  /**
   * Determines if a string doesn't have upper cased characters.
   * @name isLowerCase
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isLowerCase/2.23.0}
   * @param {string} str
   *   The value to be checked.
   * @param {boolean} [opt_allowNoCasing=false]
   *   If <code>true</code>, characters without casing found in <code>str</code> will be viewed as
   *   lower cased characters.
   * @returns {boolean}
   *   <code>true</code> if <code>str</code> only has cased characters that are lower cased or if
   *   <code>opt_allowNoCasing</code> is <code>true</code> and <code>str</code> only has characters without
   *   casing. Otherwise <code>false</code> is returned.
   */
  
  /**
   * Determines if a value is negative.
   * @name isNegative
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isNegative/2.23.0}
   * @param {number} value
   *   The value to test to see if it is negative.
   * @param {boolean} [opt_includeNegZero=false]
   *   Value indicating whether or not <code>-0</code> should be counted as negative.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is negative or if <code>value</code> is <code>-0</code> and
   *   <code>opt_includeNegZero</code> is <code>true</code>-ish.  Otherwise <code>false</code>.
   */
  function isNegative(value, opt_includeNegZero) {
    return value < 0 || (!!opt_includeNegZero && 1 / value < 0);
  }
  
  /**
   * Determines if a value is either <code>null</code> or <code>undefined</code>.
   * @name isNil
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isNil/2.23.0}
   * @param {*} value
   *   The value to be checked.
   * @returns {boolean}
   *   Returns <code>true</code> if value is either <code>null</code> or <code>undefined</code>. Otherwise
   *   <code>false</code> is returned.
   */
  function isNil(value) {
    return value == undefined;
  }
  
  /**
   * Indicates if a value is a numeric string.
   * @name isNumeric
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isNumeric/2.23.0}
   * @param {*} value
   *   Value to test.
   * @returns {boolean}
   *   Boolean indicating whether or not <code>value</code> is a numeric string.
   */
  function isNumeric(value) {
    return  'string' == typeof value && !value != __GLOBAL.isFinite(Number(value));
  }
  
  /**
   * Gets the date in the ISO-8601 standard format: <code>YYYY-MM-DDTHH:mm:ss.sssZ</code>
   * @name isoDate
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isoDate/2.23.0}
   * @param {Date} [opt_date=new Date()]
   *   Date to convert to ISO-8601 format.  If not given the current date/time
   *   is used.
   * @returns {string}
   *   <code>opt_date</code> as a string in the ISO-8601 format: <code>YYYY-MM-DDTHH:mm:ss.sssZ</code>
   */
  function isoDate(opt_date) {
    opt_date = opt_date || new Date;
    return opt_date.getUTCFullYear() + "-"
      + ("0" + (opt_date.getUTCMonth() + 1) + "-").slice(-3)
      + ("0" + opt_date.getUTCDate() + "T").slice(-3)
      + ("0" + opt_date.getUTCHours() + ":").slice(-3)
      + ("0" + opt_date.getUTCMinutes() + ":").slice(-3)
      + ("0" + opt_date.getUTCSeconds() + ".").slice(-3)
      + ("00" + opt_date.getUTCMilliseconds() + "Z").slice(-4);
  }
  
  /**
   * Creates functions for testing if a value "is" (or "is not") a value.
   * @name isOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isOf/2.23.0}
   * @param {*} value
   *   The value that the returned function will always be used to check
   *   against.
   * @param {boolean} [opt_negate=false]
   *   Determines if the returned function will always check if passed values
   *   are NOT <code>value</code>.
   * @param {boolean} [opt_traverseArray=false]
   *   If <code>value</code> is an array, the returned function will check the passed value
   *   against all of the items within <code>value</code>.
   * @returns {Function}
   *   Function that accepts one argument which will be checked against <code>value</code>.
   *   The function will return a boolean value when called indicating if it
   *   matches <code>value</code> or not (also depending on the value of <code>opt_negate</code>).
   */
  function isOf(value, opt_negate, opt_traverseArray) {
    var parts, xs = [];
  
    parts = (value = (opt_traverseArray && nativeType(value) === 'Array') ? value : [value]).map(function (x, i) {
      return (x !== x ? 'v!==v' : 'v===x').replace('x', xs[i] = 'x' + i);
    });
  
    return Function(
      xs.join(','),
      ('return function(v){return' + (opt_negate ? '!' : '') + '(' + parts.join('||') + ')}')
    ).apply(0, value);
  }
  
  /**
   * Determines if a value is positive.
   * @name isPositive
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isPositive/2.23.0}
   * @param {number} value
   *   Value to be tested as to whether or not it is positive.
   * @param {boolean} [opt_includeZero=false]
   *   Value indicating whether or not <code>0</code> should be counted as positive.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is negative or if <code>value</code> is <code>-0</code> and
   *   <code>opt_includeNegZero</code> is <code>true</code>-ish.  Otherwise <code>false</code>.
   */
  function isPositive(value, opt_includeZero) {
    return value > 0 || (!!opt_includeZero && 1 / value > 0);
  }
  
  /**
   * Determine whether or not a RegExp matches an entire string (or the start of
   * a string).
   * @name isRegExpMatch
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isRegExpMatch/2.23.0}
   * @param {RegExp} rgx
   *   The regular expression to be used.
   * @param {string} [opt_str]
   *   If specified, <code>rgx</code> will be matched against the string to see if the
   *   regular expression matches from the beginning. If not specified, a
   *   function will be returned that will be used to match against <code>rgx</code>
   *   whenever it is called.
   * @param {boolean} [opt_onlyCheckStart=false]
   *   If <code>true</code>, <code>rgx</code> must only match the beginning of <code>opt_str</code>. If <code>false</code>,
   *   <code>rgx</code> must match the entire string <code>opt_str</code>.
   * @returns {boolean|Function}
   *   If <code>opt_str</code> is given and is not <code>null</code> or <code>undefined</code>, a boolean will be
   *   returned. If <code>opt_onlyCheckStart</code> is <code>true</code>-ish the value returned will
   *   indicate if <code>rgx</code> simply matched from the beginning of <code>opt_str</code>. If
   *   <code>opt_onlyCheckStart</code> is <code>false</code>-ish, the value returned will indicate if
   *   <code>rgx</code> matched all of <code>opt_str</code>. If <code>opt_str</code> is not given or <code>null</code> or
   *   <code>undefined</code> a function to match against <code>rgx</code> will be returned which will
   *   accept a string as the 1st argument that will act as <code>opt_str</code> would have
   *   and a 2nd optional argument which will act as <code>opt_onlyCheckStart</code> would
   *   have and will return a boolean indicating if <code>rgx</code> matches <code>str</code>.
   */
  function isRegExpMatch(rgx, opt_str, opt_onlyCheckStart) {
    rgx = RegExp(rgx.source + '|([\\S\\s])', (rgx + '').replace(/[\s\S]+\/|g/g, '') + 'g');
    function f(str, opt_checkStartOnly) {
      rgx.lastIndex = undefined;
      opt_checkStartOnly = 1 in arguments ? opt_checkStartOnly : opt_onlyCheckStart;
      var isMatch = false, match, keepGoing = 1;
      while ((match = rgx.exec(str)) && keepGoing) {
        isMatch = slice(match, -1)[0] == undefined;
        keepGoing = isMatch && !opt_checkStartOnly;
      }
      return isMatch;
    }
    return opt_str == undefined ? f : f(opt_str, opt_onlyCheckStart);
  }
  
  /**
   * Test for space characters starting at the beginning of a string.
   * @name isSpace
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isSpace/2.23.0}
   * @param {string} str
   *   The string to be tested.
   * @param {boolean} [opt_onlyCheckStart=false]
   *   If <code>true</code>, only the beginning of the string is checked to see if it
   *   starts with a space character. If <code>false</code>, all of the string is checked
   *   to see if it only contains space characters.
   * @returns {boolean}
   *   Returns <code>true</code> if <code>opt_onlyCheckStart</code> is <code>true</code>-ish and <code>str</code> starts
   *   with a space character or if <code>opt_onlyCheckStart</code> is <code>false</code>-ish and
   *   <code>str</code> only contains space characters. Otherwise <code>false</code> is returned.
   */
  var isSpace = isRegExpMatch(/[ \xA0\u1680\u180E\u2000-\u200A\u2028\u2029\u202F\u205F\u3000]/);
  
  /**
   * Determines if a value is of a specific type or one of a list of specific
   * types.
   * @name isType
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isType/2.23.0}
   * @param {*} value
   *   Value to determine if it matches any of the type names given.
   * @param {...string} typeName
   *   Possible type names to match <code>value</code> against.
   * @returns {boolean}
   *   A boolean indicating if the type of <code>value</code> matched against any of the
   *   <code>typeName</code> arguments passed.
   */
  function isType(value) {
    value = typeOf(value);
    for (var a = arguments, i = a.length; --i && a[i] != value; );
    return i > 0;
  }
  
  /**
   * Creates a partial <code>isType()</code> function.
   * @name isTypeFor
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isTypeFor/2.23.0}
   * @param {...string} typeNames
   *   One or more type names to check against values.
   * @param {boolean} [opt_negate=false]
   *   If specified the returned function's return value will always be negated.
   * @returns {Function}
   *   A function that can be used to see if any value passed to it is one of
   *   the type names passed in as <code>typeNames</code>.  The return value will indicate
   *   if one of the type names matches the type of the value passed to it.  If
   *   <code>opt_negate</code> is <code>true</code> the return value of this function will be the
   *   opposite.
   */
  function isTypeFor() {
    var args = slice(arguments),
      argc = args.length,
      negate = argc > 1 && 'boolean' === typeof args[argc - 1] && (argc-- , args.pop());
    return function (value) {
      value = typeOf(value);
      for (var i = 0; i < argc && args[i] != value; i++);
      return i < argc !== negate;
    };
  }
  
  /**
   * Test for whitespace characters starting at the beginning of a string.
   * @name isWhitespace
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/isWhitespace/2.23.0}
   * @param {string} str
   *   The string to be tested.
   * @param {boolean} [opt_onlyCheckStart=false]
   *   If <code>true</code>, only the beginning of the string is checked to see if it
   *   starts with a whitespace character. If <code>false</code>, all of the string is
   *   checked to see if it only contains whitespace characters.
   * @returns {boolean}
   *   Returns <code>true</code> if <code>opt_onlyCheckStart</code> is <code>true</code>-ish and <code>str</code> starts
   *   with a whitespace character or <code>if opt_onlyCheckStart</code> is <code>false</code>-ish and
   *   <code>str</code> only contains whitespace characters. Otherwise <code>false</code> is returned.
   */
  var isWhitespace = isRegExpMatch(/[\t-\r\x1C- \u1680\u180E\u2000-\u200A\u2028\u2029\u205F\u3000]/);
  
  /**
   * Glues the values of an array together to form a string.
   * @name join
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/join/2.23.0}
   * @param {Array} arr
   *   The array containing the values to be glued together.
   * @param {string} [opt_delimiter=","]
   *   The string to delimit each value in <code>arr</code>.
   * @param {string} [opt_beforeEach=""]
   *   The string to prepend to each value in <code>arr</code>.
   * @param {string} [opt_afterEach=""]
   *   The string to append to each value in <code>arr</code>.
   * @returns {string}
   *   A string representation of <code>arr</code> with all of the values prefixed by
   *   <code>opt_beforeEach</code>, suffixed by <code>opt_afterEach</code> and separated by
   *   <code>opt_delimiter</code>.
   */
  function join(arr, opt_delimiter, opt_beforeEach, opt_afterEach) {
    opt_beforeEach = opt_beforeEach || '';
    opt_afterEach = opt_afterEach || '';
    return arr.length
      ? opt_beforeEach + arr.join(
          opt_afterEach
          + (opt_delimiter != undefined ? opt_delimiter : ',')
          + opt_beforeEach
        ) + opt_afterEach
      : '';
  }
  
  /**
   * Joins the values of an array to make a path string.
   * @name joinPath
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/joinPath/2.23.0}
   * @param {Array} arrPath
   *   The array containing the path that should be converted to a string.
   * @returns {string}
   *   The string version of <code>arrPath</code> where the path is joined by dots and
   *   where any dots in the path are escaped.
   */
  function joinPath(arrPath) {
    return arrPath.map(function (x) {
      return (x + '').replace(/\\|\./g, '\\$&');
    }).join('.');
  }
  
  /**
   * Adds a JSON-P script to the page to be executed once the script has been
   * retrieved.
   * @name jsonp
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/jsonp/2.23.0}
   * @param {string} url
   *   URL of the JSON-P script without the callback parameter provided.
   * @param {Function} callback
   *   Function to be called by the JSON-P script.
   * @param {string} [opt_callbackParamName="callback"]
   *   Name of the parameter that will be passed the callback function's name.
   * @returns {HTMLScriptElement}
   *   The script element that is added to the page.
   */
  function jsonp(url, callback, opt_callbackParamName) {
    for (
      var parent = __DOCUMENT.getElementsByTagName('head')[0] || __DOCUMENT.body,
          script = __DOCUMENT.createElement('script'),
          callbackName = '__jsonp';
      has(__GLOBAL, callbackName = (callbackName + Math.random()).replace('.', ''));
    );
    __GLOBAL[callbackName] = function() {
      delete __GLOBAL[callbackName];
      callback.apply(this, arguments);
    };
    script.src = (url + '&'
      + encodeURIComponent(opt_callbackParamName || 'callback')
      + '=' + callbackName).replace(/(^[^\?&]*)&/, '$1?');
    script.type = 'text/JavaScript';
    parent.appendChild(script);
    return script;
  }
  
  /**
   * Maximizes the dimensions while keeping the same aspect ratio.
   * @name keepAspectRatio
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/keepAspectRatio/2.23.0}
   * @param {number} width
   *   The base width.
   * @param {number} height
   *   The base height.
   * @param {number} tagetWidth
   *   The target width.
   * @param {number} targetHeight
   *   The target height.
   * @param {boolean} [opt_maximize=false]
   *   If <code>false</code>-ish, the minimum dimensions will be found where at least one
   *   of the dimensions matches the corresponding target dimension. If
   *   <code>true</code>-ish, the maximum dimensions will be found where at least one of
   *   the dimensions matches the corresponding target dimension.
   * @returns {Object}
   *   Object containing the minimum or maximum calculated width and height
   *   while keeping the same aspect ratio provided.  Eg. <code>{ height: 400, width:
   *   320 }</code>.
   */
  function keepAspectRatio(width, height, targetWidth, targetHeight, opt_maximize) {
    targetWidth = Math[opt_maximize ? 'max' : 'min'](targetHeight * width / height, targetWidth);
    return { width: targetWidth, height: targetWidth * height / width };
  }
  
  
  /**
   * Tests two or more objects to make sure they have the same keys.
   * @name keysMatch
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/keysMatch/2.23.0}
   * @param {*} obj1
   *   First object whose keys will be compared to the keys of the other objects
   *   passed in.
   * @param {...*} objX
   *   The other objects whose keys will be compared to the keys of <code>obj1</code>.
   * @returns {boolean}
   *   A boolean value indicating whether or not the keys of the objects that
   *   were passed in matched each other.
   */
  function keysMatch(obj1) {
    for (var j, keys2, keys1 = Object.keys(Object(obj1)).sort(), args = arguments, i = args.length; --i; ) {
      keys2 = Object.keys(Object(args[i])).sort();
      j = keys1.length;
      if (j !== keys2.length) {
        return false;
      }
      while (j--) {
        if (keys1[j] !== keys2[j]) {
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * Gets the last element in an array or an array-like object.
   * @name last
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/last/2.23.0}
   * @param {Array} array
   *   Array or array-like object from which to retrieve the last value.
   * @returns {*}
   *   The last value in <code>array</code>.
   */
  var last = nth(-1);
  
  /**
   * Finds the smallest integer that is evenly divisible by the two specified
   * integers.
   * @name lcm
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lcm/2.23.0}
   * @param {number} int1
   *   The first of two integers that will be evenly divisible by the returned
   *   least common multiple.
   * @param {number} int2
   *   The second of two integers that will be evenly divisible by the returned
   *   least common multiple.
   * @returns {number}
   *   The smallest integer that is evenly divisible by <code>int1</code> and <code>int2</code>.
   */
  function lcm(int1, int2) {
    return Math.abs(int1 * int2) / gcd(int1, int2);
  }
  
  /**
   * Creates a wrapper function which when called will call the given function,
   * but only a maximum amount of times as specified.
   * @name limit
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/limit/2.23.0}
   * @param {Function} fn
   *   The function which will only be called a limited amount of times.
   * @param {number} [opt_callLimit=1]
   *   The maximum amount of times the returned wrapper function can be called.
   * @param {string} opt_errorMessage
   *   If given, once the call limit is reached every subsequent call to the
   *   returned wrapper function will throw an error with this message.  If not
   *   given no error will be thrown.
   * @returns {Function}
   *   A wrapper function that is limited in the amount of times it will call
   *   <code>fn</code>.
   */
  function limit(fn, opt_callLimit, opt_errorMessage) {
    if (opt_callLimit == undefined) {
      opt_callLimit = 1;
    }
    return function() {
      if (opt_callLimit-- > 0) {
        return fn.apply(this, arguments);
      }
      else if (opt_errorMessage) {
        throw new Error(opt_errorMessage);
      }
    };
  }
  
  /**
   * Gets the logarithm of any number in the specified base.  NOTE:  this
   * function works best for values that don't rely on a base of <code>Math.E</code>.  If
   * you want to find natural log it would be best to simply use <code>Math.log()</code>.
   * @name log
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/log/2.23.0}
   * @param {number} x
   *   The number for which the logarithm will be calculated.
   * @param {number} base
   *   The base of the calculated logarithm.
   * @returns {number}
   *   The logarithm of <code>x</code> for the specified <code>base</code>.
   */
  var log;
  (function(CACHE_LOG_INVERSE_BASE) {
    log = function(x, opt_base) {
      opt_base = opt_base || Math.E;
      return Math.log(x)
        * (
          CACHE_LOG_INVERSE_BASE[opt_base] = CACHE_LOG_INVERSE_BASE.hasOwnProperty(opt_base)
            ? CACHE_LOG_INVERSE_BASE[opt_base]
            : (5 / Math.log(Math.pow(opt_base, 5)))
        );
    };
  })({});
  
  /**
   * Gets the base 2 logarithm of a number.
   * @name log2
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/log2/2.23.0}
   * @param {number} x
   *   A number.
   * @returns {number}
   *   The base 2 logarithm of the given number. If the number is negative,
   *   <code>NaN</code> is returned.
   */
  // Math.log2() and Math.log10() don't exist in IE.
  eval('102'.replace(/..?/g, 'function log$&(x){return Math.log(x)*Math.LOG$&E}'));
  
  /**
   * Gets the base 10 logarithm of a number.
   * @name log10
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/log10/2.23.0}
   * @param {number} x
   *   A number.
   * @returns {number}
   *   The base 2 logarithm of the given number. If the number is negative,
   *   <code>NaN</code> is returned.
   */
  
  /**
   * Progressively loops the values within an array on each iteration while at
   * the same time retrieving one of the end values of the specified array.
   * @name loop
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/loop/2.23.0}
   * @param {Array} array
   *   Array from which to pull the value which will also be modified because
   *   the value will then be placed on the opposite end.
   * @param {boolean} [opt_reverse=false]
   *   Boolean indicating that instead of retrieving the first value, the last
   *   value should be retrieved and then placed at the beginning of <code>array</code>.
   * @returns {*}
   *   If <code>opt_reverse</code> is <code>true</code>-ish the last value in <code>array</code> will be returned
   *   and also placed at the beginning of <code>array</code>.  Otherwise the first value
   *   in <code>array</code> will be returned and also placed at the end of <code>array</code>.
   */
  function loop(array, opt_reverse) {
    var first = array[opt_reverse ? 'pop' : 'shift']();
    if (array.length) {
      array[opt_reverse ? 'unshift' : 'push'](first);
    }
    return first;
  }
  
  /**
   * Takes a value that will then be upper-cased.
   * @name upperCase
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/upperCase/2.23.0}
   * @param {string|*} value
   *   The string or value to be upper-cased.
   * @returns {string}
   *   <code>value</code> coerced to a string with all of the characters upper-cased.
   */
  
  /**
   * Takes a value that will then be lower-cased.
   * @name lowerCase
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lowerCase/2.23.0}
   * @param {string|*} value
   *   The string or value to be lower-cased.
   * @returns {string}
   *   <code>value</code> coerced to a string with all of the characters lower-cased.
   */
  eval('LlowUupp'.replace(/(.)(.(..))/g, 'function $2erCase(s){return(arguments.length?s+"":"").to$1$3erCase()}'));
  
  /**
   * Lower-cases all characters after the first one.
   * @name lowerRest
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lowerRest/2.23.0}
   * @param {string} string
   *   String that should be returned making sure that all characters after the
   *   first are lower-cased.
   * @returns {string}
   *   <code>string</code> with the first character as is followed by all the rest
   *   lower-cased.
   */
  
  /**
   * Upper-cases the first character of a string.
   * @name upperFirst
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/upperFirst/2.23.0}
   * @param {string} string
   *   String that should be returned with the first character upper-cased.
   * @returns {string}
   *   <code>string</code> with the first character upper-cased and any remaining
   *   characters kept as is.
   */
  
  /**
   * Upper-cases all characters after the first one.
   * @name upperRest
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/upperRest/2.23.0}
   * @param {string} string
   *   String that should be returned making sure that all characters after the
   *   first are upper-cased.
   * @returns {string}
   *   <code>string</code> with the first character as is followed by all the rest
   *   upper-cased.
   */
  
  /**
   * Lower-cases the first character of a string.
   * @name lowerFirst
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/lowerFirst/2.23.0}
   * @param {string} string
   *   String that should be returned with the first character lower-cased.
   * @returns {string}
   *   <code>string</code> with the first character lower-cased and any remaining
   *   characters kept as is.
   */
  // Generates lowerFirst(), lowerRest(), upperFirst(), and upperRest()
  eval("function st(s){return s.slice(0,1)erCase()".replace(/(\w+ )(.{13}(.{8})....)(.+)/g,"$1lowerFir$2.toLow$4+$31);}$1lowerRe$2+$31).toLow$4;}$1upperFir$2.toUpp$4+$31);}$1upperRe$2+$31).toUpp$4}"));
  
  /**
   * Creates a new array or object corresponding to the array-like structure or
   * object supplied where the keys to the new array or object are determined by
   * a callback function.
   * @name mapKeys
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/mapKeys/2.23.0}
   * @param {Array|Object} objectOrArray
   *   The array, array-like structure or object whose items or properties will
   *   be traversed by <code>callback</code>.
   * @param {Function} callback
   *   Function that produces replacement keys for each key of <code>objectOrArray</code>.
   *   The first argument passed will be current value of the item or property.
   *   The second argument will be the index or property name.  The third
   *   argument will be a reference to <code>objectOrArray</code>.  The return value will
   *   be used as the corresponding key to be set in the array or object
   *   returned by <code>mapKeys()</code>.
   * @param {*} [opt_thisArg=global]
   *   Value to use as this when executing <code>callback</code>.
   * @returns {Array|Object}
   *   If <code>objectOrArray</code> is an array-like structure then a new array with the
   *   modified keys will be returned.  Otherwise a new object with the modified
   *   keys will be returned.
   */
  
  /**
   * Creates a new array or object corresponding to the array-like structure or
   * object supplied where the keys and values to the new array or object are
   * determined by a callback function.
   * @name mapPairs
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/mapPairs/2.23.0}
   * @param {Array|Object} objectOrArray
   *   The array, array-like structure or object whose items or properties will
   *   be traversed by <code>callback</code>.
   * @param {Function} callback
   *   Function that produces a key and value pair that will be used to define
   *   each item or property in the structure (<code>Array</code> or <code>Object</code>)
   *   corresponding to <code>objectOrArray</code>.  The first argument passed will be
   *   current value of the item or property.  The second argument will be the
   *   index or property name.  The third argument will be a reference to
   *   <code>objectOrArray</code>.  If the return value is <code>false</code>-ish the property or item
   *   will not be added to the object returned by <code>mapPairs()</code>.  Otherwise the
   *   return value should be an array where the first value is the key and the
   *   second value is the value of the item or property that should be added to
   *   the object returned by <code>mapPairs()</code>.
   * @param {*} [opt_thisArg=global]
   *   Value to use as this when executing <code>callback</code>.
   * @returns {Array|Object}
   *   If <code>objectOrArray</code> is an array-like structure then a new array with the
   *   modified keys and values will be returned.  Otherwise a new object with
   *   the keys and values will be returned.
   */
  
  /**
   * Creates a new array or object with the results of calling a provided
   * function on every item or property in the given object.
   * @name map
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/map/2.23.0}
   * @param {Array|Object} objectOrArray
   *   The array, array-like structure or object whose items or properties will
   *   be traversed by <code>callback</code>.
   * @param {Function} callback
   *   Function that produces a replacement item or property for each
   *   corresponding one <code>objectOrArray</code>.  The first argument passed will be
   *   current value of the item or property.  The second argument will be the
   *   index or property name.  The third argument will be a reference to
   *   <code>objectOrArray</code>.  The return value will be used to replace each item or
   *   property.
   * @param {*} [opt_thisArg=global]
   *   Value to use as this when executing <code>callback</code>.
   * @returns {Array|Object}
   *   If <code>objectOrArray</code> is an array-like structure then a new array with the
   *   modified contents will be returned.  Otherwise a new object with the
   *   modified contents will be returned.
   */
  /*
  // Original code for map(), mapKeys() and mapPairs():
  function map(obj, fn, opt_thisObj) {
    for (
      var k, isArray = isArrayLike(obj), result = isArray ? [] : {}, arrKeys = Object.keys(obj), i = 0, l = arrKeys.length;
      i < l;
    ) {
      k = arrKeys[i++];
      result[k] = fn.call(opt_thisObj, obj[k], isArray ? +k : k, obj);
    }
    return result;
  }
  function mapKeys(obj, fn, opt_thisObj) {
    for (
      var k, isArray = isArrayLike(obj), result = isArray ? [] : {}, arrKeys = Object.keys(obj), i = 0, l = arrKeys.length;
      i < l;
    ) {
      k = arrKeys[i++];
      result[fn.call(opt_thisObj, obj[k], isArray ? +k : k, obj)] = obj[k];
    }
    return result;
  }
  function mapPairs(obj, fn, opt_thisObj) {
    for (
      var r, k, isArray = isArrayLike(obj), result = isArray ? [] : {}, arrKeys = Object.keys(obj), i = 0, l = arrKeys.length;
      i < l;
    ) {
      k = arrKeys[i++];
      if (r = fn.call(opt_thisObj, obj[k], isArray ? +k : k, obj)) {
        result[r[0]] = r[1];
      }
    }
    return result;
  }
  */
  var mapPairs, mapKeys, map = Function('I', "return[@a=e[f++],d[a]=#;return d},@a=e[f++],d[#]=b[a];return d},@if(a=e[f++],a=#)d[a[0]]=a[1];return d}]".replace(/@/g,"function(b,g,h){for(var a,c=I(b),d=c?[]:{},e=Object.keys(b),f=0,k=e.length;f<k;)").replace(/#/g,"g.call(h,b[a],c?+a:a,b)"))(isArrayLike);
  mapPairs = map[2];
  mapKeys = map[1];
  map = map[0];
  
  /**
   * Get the (first) maximum value in an array determined by <code>valuer</code>.
   * @name maxBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/maxBy/2.23.0}
   * @param {Array} array
   *   Array which contains the maximum value that should be found.
   * @param {Function|Array|number|string} valuer
   *   If this is a function it will be passed each item in <code>array</code> and the
   *   corresponding values will be used to rank the items.  In all other cases
   *   this will be used as a path within each item in <code>array</code> whose property at
   *   that path will be used to rank the item.
   * @returns {*}
   *   The (first) maximum value within <code>array</code>.
   */
  
  /**
   * Get the index of the (first) maximum value in an array.
   * @name maxIndex
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/maxIndex/2.23.0}
   * @param {Array} array
   *   Array which contains the maximum value that should be found and for which
   *   the corresponding index should be returned.
   * @returns {number}
   *   The index of the (first) maximum value within <code>array</code>.  If <code>array</code> is
   *   empty <code>-1</code> will be returned.
   */
  
  /**
   * Get the index of the (first) maximum value in an array determined by
   * <code>valuer</code>.
   * @name maxIndexBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/maxIndexBy/2.23.0}
   * @param {Array} array
   *   Array which contains the maximum value that should be found and for which
   *   the corresponding index should be returned.
   * @param {Function|Array|number|string} valuer
   *   If this is a function it will be passed each item in <code>array</code> and the
   *   corresponding values will be used to rank the items.  In all other cases
   *   this will be used as a path within each item in <code>array</code> whose property at
   *   that path will be used to rank the item.
   * @returns {number}
   *   The index of the (first) maximum value within <code>array</code>.  If <code>array</code> is
   *   empty <code>-1</code> will be returned.
   */
  
  /**
   * Get the (first) minimum value in an array.
   * @name min
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/min/2.23.0}
   * @param {Array} array
   *   Array which contains the minimum value that should be found.
   * @returns {*}
   *   The (first) minimum value within <code>array</code>.
   */
  
  /**
   * Get the (first) minimum value in an array determined by <code>valuer</code>.
   * @name minBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/minBy/2.23.0}
   * @param {Array} array
   *   Array which contains the minimum value that should be found.
   * @param {Function|Array|number|string} valuer
   *   If this is a function it will be passed each item in <code>array</code> and the
   *   corresponding values will be used to rank the items.  In all other cases
   *   this will be used as a path within each item in <code>array</code> whose property at
   *   that path will be used to rank the item.
   * @returns {*}
   *   The (first) minimum value within <code>array</code>.
   */
  
  /**
   * Get the index of the (first) minimum value in an array determined by
   * <code>valuer</code>.
   * @name minIndexBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/minIndexBy/2.23.0}
   * @param {Array} array
   *   Array which contains the minimum value that should be found and for which
   *   the corresponding index should be returned.
   * @param {Function|Array|number|string} valuer
   *   If this is a function it will be passed each item in <code>array</code> and the
   *   corresponding values will be used to rank the items.  In all other cases
   *   this will be used as a path within each item in <code>array</code> whose property at
   *   that path will be used to rank the item.
   * @returns {number}
   *   The index of the (first) minimum value within <code>array</code>.  If <code>array</code> is
   *   empty <code>-1</code> will be returned.
   */
  
  /**
   * Get the (first) minimum value and the (first) maximum value in an array.
   * @name minMax
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/minMax/2.23.0}
   * @param {Array} array
   *   Array which contains the minimum value and the maximum value that should
   *   be found.
   * @returns {*}
   *   An object containing a <code>min</code> property which will be the (first) minimum
   *   value in <code>array</code>.  The object will also contain a <code>max</code> property which
   *   will be the (first) maximum value in <code>array</code>.  If <code>array</code> is empty
   *   <code>undefined</code> will be the value of the <code>min</code> and <code>max</code> properties.
   */
  
  /**
   * Get the (first) maximum value in an array determined by <code>valuer</code>.
   * @name minMaxBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/minMaxBy/2.23.0}
   * @param {Array} array
   *   Array which contains the minimum and maximum values that should be found.
   * @param {Function|Array|number|string} valuer
   *   If this is a function it will be passed each item in <code>array</code> and the
   *   corresponding values will be used to rank the items.  In all other cases
   *   this will be used as a path within each item in <code>array</code> whose property at
   *   that path will be used to rank the item.
   * @returns {*}
   *   An object containing a <code>min</code> property which will be the (first) minimum
   *   value in <code>array</code>.  The object will also contain a <code>max</code> property which
   *   will be the (first) maximum value in <code>array</code>.  If <code>array</code> is empty
   *   <code>undefined</code> will be the value of the <code>min</code> and <code>max</code> properties.
   */
  
  /**
   * Get the index of the (first) minimum value and the index of the (first)
   * maximum value in an array.
   * @name minMaxIndex
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/minMaxIndex/2.23.0}
   * @param {Array} array
   *   Array which contains the minimum and maximum values that should be found
   *   and for which the corresponding indices should be returned.
   * @returns {Object}
   *   An object containing a <code>min</code> property which will be the index of the
   *   (first) minimum value in <code>array</code>.  The object will also contain a <code>max</code>
   *   property which will be the index of the (first) maximum value in <code>array</code>.
   *   If <code>array</code> is empty <code>-1</code> will be the value of the indices.
   */
  
  /**
   * Get the index of the (first) minimum value and the index of the (first)
   * maximum value in an array determined by <code>valuer</code>.
   * @name minMaxIndexBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/minMaxIndexBy/2.23.0}
   * @param {Array} array
   *   Array which contains the minimum and maximum values that should be found
   *   and for which the corresponding indices should be returned.
   * @param {Function|Array|number|string} valuer
   *   If this is a function it will be passed each item in <code>array</code> and the
   *   corresponding values will be used to rank the items.  In all other cases
   *   this will be used as a path within each item in <code>array</code> whose property at
   *   that path will be used to rank the item.
   * @returns {Object}
   *   An object containing a <code>min</code> property which will be the index of the
   *   (first) minimum value in <code>array</code>.  The object will also contain a <code>max</code>
   *   property which will be the index of the (first) maximum value in <code>array</code>.
   *   If <code>array</code> is empty <code>-1</code> will be the value of the indices.
   */
  
  /**
   * Get the index of the (first) minimum value in an array.
   * @name minIndex
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/minIndex/2.23.0}
   * @param {Array} array
   *   Array which contains the minimum value that should be found and for which
   *   the corresponding index should be returned.
   * @returns {number}
   *   The index of the (first) minimum value within <code>array</code>.  If <code>array</code> is
   *   empty <code>-1</code> will be returned.
   */
  // requires getAt()
  // Generates minIndex, minIndexBy, min, minBy, maxIndex, maxIndexBy, max, maxBy, minMax, minMaxBy, minMaxIndex and minMaxIndexBy
  eval(
    "minIndex t.min!min a[t.min]!maxIndex t.max!max a[t.max]!minMax{min:a[t.min],max:a[t.max]}!".replace(
      /(\w+)([^!]+)!/g,
      " By,i".replace(
        /\W|(..)(..)/g,
        "@ $$1$1(a$2){var t=#$1(a$2);return$$2}"
      )
    ).replace(
      /.+/,
      '$&var #,#By;#=@(G){#=@(h){for(var a,c,e,f,g,b=0,k=h.length;b<k;b++)a=h[b],b?a<c?(c=a,e=b):a>f&&(f=a,g=b):(c=f=a,e=g=b);return{min:e%,max:g%}};#By=@(h,a){var t=typeof a;a="string"===t||"number"===t?[a]:a;for(var c,e,f,g,b,k="@"===t,d=0,l=h.length;d<l;d++)c=k?a(h[d]):G(h[d],a),d?c<e?(e=c,f=d):c>g&&(g=c,b=d):(e=g=c,f=b=d);return{min:f%,max:b%}}}'
    ).replace(/#/g, 'minMaxIndex').replace(/@/g, 'function').replace(/(.)%/g, '$1===undefined?-1:$1')
  )(getAt)
  
  /**
   * Get the (first) maximum value in an array.
   * @name max
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/max/2.23.0}
   * @param {Array} array
   *   Array which contains the maximum value that should be found.
   * @returns {*}
   *   The (first) maximum value within <code>array</code>.
   */
  
  /**
   * Get the remainder after dividing 2 numbers while making sure the remainder
   * is always between 0 and the divisor..
   * @name mod
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/mod/2.23.0}
   * @param {number} dividend
   *   Number to be divided.
   * @param {number} divisor
   *   Number to divide by.
   * @returns {number}
   *   Remainder after dividing the dividend by the divisor.
   */
  function mod(dividend, divisor) {
    return (dividend % divisor + divisor) % divisor;
  }
  
  /**
   * Modifies a <code>Date</code> object with either offsets or specific times.
   * @name modDate
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/modDate/2.23.0}
   * @param {Date} date
   *   The date to be modified.
   * @param {Object} objOptions
   *   The object which specifies how to modify <code>date</code>.  The properties will be
   *   read and used to modify <code>date</code> (or a copy of <code>date</code> if <code>objOptions</code> is
   *   <code>true</code>-ish).  For example to offset the year by 2 years into the future
   *   you can specify <code>{ year: 2 }</code>.  To actually set a specific unit of time
   *   you can prefix the name of the property with <code>$</code>.  For example if this is
   *   <code>{ $day: 3, $month: 6 }</code> then the month will be set to July (because the
   *   month property of a <code>Date</code> object is zero-based) and the day of the month
   *   will be set to <code>3</code>.  Valid properties are <code>year</code> (or <code>fullYear</code>),
   *   <code>month</code>, <code>day</code> (or <code>date</code>), <code>hour</code>, <code>minute</code> (or <code>min</code>), <code>second</code> (or
   *   <code>sec</code>), or <code>millisecond</code> (or <code>ms</code>).  You can also use the plural form of
   *   those property names.  Another set of properties is <code>dow</code> or <code>dow0</code>
   *   (Sunday to Saturday), <code>dow1</code> (Monday to Sunday), <code>dow2</code> (Tuesday to
   *   Monday), <code>dow3</code> (Wednesday to Tuesday), <code>dow4</code> (Thursday to Wednesday),
   *   <code>dow5</code> (Friday to Thursday) and <code>dow6</code> (Saturday to Friday), all of which
   *   allow for setting the day of the week.  Additionally you can prefix those
   *   names with a <code>$</code> to actually set the value instead of simply offsetting
   *   those values.
   * @returns {Date}
   *   If <code>objOptions.clone</code> is <code>true</code>-ish then a copy of <code>date</code> will be
   *   modified and returned.  Otherwise the same <code>date</code> that was passed will be
   *   modified and returned.
   */
  var modDate;
  (function (MAP) {
    modDate = function (date, options) {
      if (options.clone) {
        date = new Date(date);
      }
      for (var setter, getter, isSetOnly, k, v, keys = Object.keys(options), i = keys.length; i--;) {
        v = options[k = keys[i]];
        isSetOnly = /^\$/.test(k);
        k.replace(/^\$|\b(ms)$|s$/g, '$1')
          .replace(/^((milli)?sec(ond)?|min(ute)?|hour)$/, '$&s')
          .replace(/./, function (a) { return a.toUpperCase(); })
          .replace(/^(DOW|dow)([0-6])?$|^.+$/i, function (k, dow, offset) {
            if (dow) {
              dow = (date.getDay() + -(offset || 0) + 7) % 7;
              v = ((v % 7) + 7) % 7;
              date.setDate(date.getDate() + (isSetOnly ? v - dow : ((dow + v) % 7 - dow)));
            }
            else {
              k = MAP[k] || k;
              if ('function' === typeof date[setter = 'set' + k] && 'function' === typeof date[getter = 'get' + k]) {
                date[setter](!isSetOnly ? date[getter]() + v : v);
              }
            }
          });
      }
      return date;
    };
  })({ Ms: 'Milliseconds', Day: 'Date', Year: 'FullYear', Secs: 'Seconds', Mins: 'Minutes' });
  
  /**
   * Takes an array or an object and traverses all of its key/value pairs
   * allowing for modifications while in the end returning a reference to that
   * same array or object.
   * @name modEach
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/modEach/2.23.0}
   * @param {Array|Object} obj
   *   Array or object that will be traversed.
   * @param {Function} fn
   *   Function that can make changes for each value within <code>obj</code>.  For each
   *   value within <code>obj</code> this function will be called with (1) the value, (2)
   *   the key and (3) a reference to <code>obj</code>.
   * @param {boolean} [opt_mapReturns=false]
   *   Indicates whether or no the return value from <code>fn</code> should be assigned to
   *   that corresponding key for <code>obj</code>.
   * @returns {Array|Object}
   *   A reference to <code>obj</code>.
   */
  function modEach(obj, fn, opt_mapReturns) {
    forOf(obj, function(v, k, obj) {
      v = fn(v, k, obj);
      if (opt_mapReturns) {
        obj[k] = v;
      }
    });
    return obj;
  }
  
  /**
   * Parses a URL to get the parameters specified in the query string.
   * @name parseQS
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/parseQS/2.23.0}
   * @param {string} url
   *   The URL to parse.
   * @returns {Object}
   *   An object in which the keys are the keys of the parameters found in <code>url</code>
   *   and the values are the corresponding values found in <code>url</code>. If a
   *   parameter key is specified twice or ends in <code>"[]"</code>, that property will be
   *   an array of all of the found values for that key.
   */
  function parseQS(url) {
    var vars = {};
    url.replace(/\?[^#]+/, function(query) {
      query.replace(/\+/g, ' ').replace(/[\?&]([^=&#]+)(?:=([^&#]*))?/g, function(m, key, value, arrIndicator, alreadyDefined, lastValue) {
        key = decodeURIComponent(key);
        arrIndicator = key.slice(-2) == '[]';
        value = value && decodeURIComponent(value);
        alreadyDefined = has(vars, key);
        lastValue = vars[key];
        vars[key] = (arrIndicator || alreadyDefined)
          ? typeOf(lastValue) == 'Array'
            ? lastValue.concat([value])
            : alreadyDefined
              ? [lastValue, value]
              : [value]
          : value;
      });
    });
    return vars;
  }
  
  /**
   * Modify a URL.
   * @name modURL
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/modURL/2.23.0}
   * @param {string} [opt_url=location.href]
   *   URL to modify and return.  If <code>undefined</code> or <code>null</code> is specified this
   *   will default to the page's location.
   * @param {Object} options
   *   An object indicating how to modify <code>opt_url</code>.  Each property name should
   *   correspond to a valid <code>URL</code> property name.  If a function is given as a
   *   property name it will be executed and the return value will be used as
   *   that property of the URL.  Specifying a function for the <code>"search"</code>
   *   property will cause the function to be called with an object representing
   *   the current search parameters and the return result will be used to
   *   indicate what the new search parameters will be.  Specifying an object
   *   for the <code>"search"</code> property will provide the ability to set individual
   *   search parameters with values or callback functions.  If <code>options.search</code>
   *   is an object and has any properties set to <code>undefined</code> or <code>null</code> this
   *   will indicate that those properties should be removed, but any search
   *   parameters that are not defined as properties of <code>options.search</code> will
   *   remain.
   * @returns {string}
   *   The modified form of <code>opt_url</code>.
   */
  function modURL(opt_url, options) {
    var URL = __GLOBAL.URL;
    var url = opt_url || __GLOBAL.location.href;
    try {
      url = new URL(url);
    }
    catch (a) {
      a = __DOCUMENT.createElement('a');
      a.href = url;
      url = new URL(a.href);
    }
    // Loop through input options...
    for (var optionKey in options) {
      if (has(options, optionKey)) {
        var optionValue = options[optionKey];  // input option
        if (optionKey === 'search') {
          var hasSearchFunc = 'function' === typeof optionValue;
          if (hasSearchFunc) {
            optionValue = optionValue(parseQS(url[optionKey]));
          }
          if ('object' === typeof optionValue) {
            var urlSearch = parseQS(url[optionKey]), arrNewSearch = [];
            // If options.search was not a function then loop through the search
            // params of the URL to be modified and only keep the ones that are
            // not specified to be changed via options.search.
            if (!hasSearchFunc) {
              for (var qsKey in urlSearch) {
                if (has(urlSearch, qsKey) && !has(optionValue, qsKey)) {
                  arrNewSearch.push([qsKey, urlSearch[qsKey]]);
                }
              }
            }
            // Loop through the search params that are to be added or modified and
            // add any that are not specified as null or undefined
            for (var searchKey in optionValue) {
              if (has(optionValue, searchKey)) {
                var searchValue = optionValue[searchKey];
                var newSearchValue = 'function' === typeof searchValue ? searchValue(urlSearch[searchKey], urlSearch) : searchValue;
                if (newSearchValue != undefined) {
                  (nativeType(newSearchValue) === 'Array' ? newSearchValue : [newSearchValue]).forEach(function(searchValue) {
                    arrNewSearch.push([searchKey, searchValue]);
                  });
                }
              }
            }
            optionValue = arrNewSearch.reduce(function(search, pair) {
              return search + (search ? '&' : '?') + escape(pair[0]) + '=' + escape(pair[1]);
            }, '');
          }
        }
        url[optionKey] = 'function' === typeof optionValue ? optionValue(url[optionKey]) : optionValue;
      }
    }
    return url.href;
  }
  
  /**
   * Gets a multiple of a number that approximates another number.
   * @name multOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/multOf/2.23.0}
   * @param {number} factor
   *   The number to get a multiple of.
   * @param {number} target
   *   The number that the multiple of should approximate.
   * @param {boolean} [opt_roundOut=false]
   *   If <code>true</code>-ish the return value will be at least the same as <code>target</code> or
   *   further away from <code>0</code> than <code>target</code>, otherwise it will be <code>target</code> or
   *   closer to <code>0</code> than <code>target</code>.
   * @returns {number}
   *   A multiple of <code>factor</code> that approximates <code>target</code>.  If <code>opt_roundOut</code> is
   *   <code>true</code>-ish this will be a value that is either <code>target</code> or a little
   *   further away from <code>0</code> than <code>target</code>.  If <code>opt_roundOut</code> is <code>false</code>-ish
   *   this will be a value that is either <code>target</code> or a little closer to <code>0</code>
   *   than <code>target</code>.
   */
  function multOf(factor, target, opt_roundOut) {
    return Math[(target < 0 || 1 / target < 0) === !opt_roundOut ? 'ceil' : 'floor'](target / factor) * factor;
  }
  
  /**
   * Reverts the global variable to which <code>YourJS</code> is initially assigned by the
   * library back to its value prior to defining YourJS.
   * @name noConflict
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/noConflict/2.23.0}
   * @returns {Object}
   *   <code>YourJS</code> object.
   */
  var noConflict;
  (function(previousValue, alreadyRun) {
    noConflict = function() {
      if (!alreadyRun) {
        alreadyRun = 1;
        __GLOBAL[__VARIABLE_NAME] = previousValue;
      }
      return YourJS;
    };
  })(__GLOBAL[__VARIABLE_NAME]);
  
  /**
   * Apply the boolean negation operator to a value.
   * @name not
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/not/2.23.0}
   * @param {*} x
   *   The value to use boolean negation on.
   * @returns {boolean}
   *   <code>true</code> if <code>x</code> was <code>false</code>-like or <code>false</code> if <code>x</code> was <code>true</code>-like.
   */
  function not(x) { return !x; }
  
  /**
   * Gets the a <code>Date</code> object representing the current date/time.
   * @name now
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/now/2.23.0}
   * @returns {Date}
   *   A <code>Date</code> object representing the current date/time.  Executing arithmetic
   *   on this function without invoking it will result in getting the number of
   *   milliseconds since January 1, 1970 GMT (eg. <code>+YourJS.now ===
   *   Date.now()</code>).
   */
  function now() { return new Date; }
  now.valueOf = function() { return +new Date; };
  
  /**
   * Creates a new object with only the specified keys copied from the specified
   * object to this new object.
   * @name only
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/only/2.23.0}
   * @param {Object|undefined} opt_subject
   *   If <code>undefined</code> this indicates that a partial function will be returned.
   *   Otherwise this is the object from which the properties should be copied.
   * @param {Array|Function} keys
   *   If this is an array it must be the keys of the subject to be copied to
   *   the new object.  If this is a function it will be called for each
   *   key/value pair (receiving the key as argument 1 and the value as argument
   *   2) and whenever the return value is <code>true</code>-ish the key/value pair will be
   *   copied to the new object.
   * @param {*} [opt_initial={}]
   *   If given this is the object that will be returned and into which the
   *   properties will be copied.
   * @returns {*}
   *   If <code>opt_subject</code> was given <code>opt_initial</code> with the specified keys copied
   *   into it will be returned.  Otherwise a partial function will be returned
   *   which will accept (1) the mandatory object from which to copy the
   *   properties and (2) the optional initial object that will be augmented and
   *   returned.
   */
  function only(opt_subject, keys, opt_initial) {
    function f(subject, opt_initial) {
      var result = {};
      var arrKeys = 'function' === typeof keys
        ? entries(subject).reduce(function (carry, kv) {
          if (keys(kv[0], kv[1])) {
            carry.push(kv[0]);
          }
          return carry;
        }, [])
        : keys;
      return arrKeys.reduce(function (result, key) {
        if (has(subject, key)) {
          result[key] = subject[key];
        }
        return result;
      }, opt_initial || {});
    }
  
    return keys ? f(opt_subject, opt_initial) : ((keys = opt_subject), f);
  }
  
  /**
   * Get the ordinal for an integer (<code>st</code>, <code>nd</code>, <code>rd</code>, or <code>th</code>).
   * @name ordinalize
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/ordinalize/2.23.0}
   * @param {number} num
   *   Number for which to get the ordinal.
   * @param {boolean} [opt_excludeNum=false]
   *   Indicates whether or not to exclude <code>num</code> from the return value.
   * @returns {string}
   *   If <code>opt_excludeNum</code> is <code>false</code>-ish a string version of <code>num</code> with the
   *   appropriate ordinal string will be returned.  If <code>opt_excludeNum</code> is
   *   <code>true</code>-ish just the ordinal corresponding to <code>num</code> will be returned.
   */
  function ordinalize(num, opt_excludeNum) {
    var abs = num < -num ? -num : num, ones = abs % 10;
    return (opt_excludeNum ? '' : num) + ((abs % 100 - ones - 10 ? [0,'st','nd','rd'][ones] : 0) || 'th');
  }
  
  /**
   * Confines a number to a specified range by looping number that are too large
   * back to the beginning of the range and numbers that are too small back to
   * the end of the range (just like in Pacman).
   * @name pacman
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/pacman/2.23.0}
   * @param {number} x
   *   The number that you want to confine to the specified range.
   * @param {number} inclusive
   *   An extreme of the range to which to confine <code>x</code>.  This number IS included
   *   in the range of valid numbers.
   * @param {number} exclusive
   *   An extreme of the range to which to confine <code>x</code>.  This number IS NOT
   *   included in the range of valid numbers.  If <code>x</code> is equal to this number,
   *   <code>inclusive</code> will be returned from this function.
   * @returns {number}
   *   A number representing <code>x</code> within the range <code>inclusive</code> and <code>exclusive</code>
   *   where <code>x</code> can be <code>inclusive</code> but cannot be <code>exclusive</code>.
   */
  function pacman(x, inclusive, exclusive) {
    exclusive -= inclusive;
    return ((x - inclusive) % exclusive + exclusive) % exclusive + inclusive;
  }
  
  /**
   * Appends a string with a filler string so that the length of the string is
   * always at least a certain length.
   * @name padEnd
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/padEnd/2.23.0}
   * @param {string} str
   *   The string to append a filler string to.
   * @param {number} maxLength
   *   The maximum length allowed for the returned string. If this is less than
   *   <code>str.length</code>, this will be set to <code>str.length</code>.
   * @param {string} [opt_fillString=" "]
   *   The string to repeatedly append to <code>str</code> until the length reaches
   *   <code>maxLength</code>.
   * @returns {string}
   *   Returns <code>str</code> with the filler string appended to it as many times as
   *   necessary until <code>maxLength</code> is reached.
   */
  function padEnd(str, maxLength, opt_fillString) {
    str += '';
  
    var filler, fillLen, stringLength = str.length;
  
    return maxLength > stringLength
      ? (
          filler = (opt_fillString !== undefined ? opt_fillString + '' : '') || ' ',
          fillLen = maxLength - stringLength,
          str + (new Array(Math.ceil(fillLen / filler.length) + 1)).join(filler).slice(0, fillLen)
        )
      : str;
  }
  
  /**
   * Prepends a string with a filler string so that the length of the string is
   * always at least a certain length.
   * @name padStart
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/padStart/2.23.0}
   * @param {string} str
   *   The string to prepend a filler string to.
   * @param {number} maxLength
   *   The maximum length allowed for the returned string. If this is less than
   *   <code>str.length</code>, this will be set to <code>str.length</code>.
   * @param {string} [opt_fillString=" "]
   *   The string to repeatedly prepend to <code>str</code> until the length reaches
   *   <code>maxLength</code>.
   * @returns {string}
   *   Returns <code>str</code> with the filler string prepended to it as many times as
   *   necessary until <code>maxLength</code> is reached.
   */
  function padStart(str, maxLength, opt_fillString) {
    str += '';
  
    var filler, fillLen, stringLength = str.length;
  
    return maxLength > stringLength
      ? (
          filler = (opt_fillString !== undefined ? opt_fillString + '' : '') || ' ',
          fillLen = maxLength - stringLength,
          (new Array(Math.ceil(fillLen / filler.length) + 1)).join(filler).slice(0, fillLen) + str
        )
      : str;
  }
  
  /**
   * Takes an array of arrays (each inner array being a key-value pair) and
   * returns the object with the keys and values set accordingly.
   * @name pair
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/pair/2.23.0}
   * @param {Array<Array>} pairs
   *   An array of arrays where the inner arrays are key-value pairs.  The keys
   *   and values will be assigned to <code>opt_objToAugment</code>.
   * @param {Object} [opt_objToAugment={}]
   *   If specified this is the object that will be augmented according to the
   *   keys and values found in <code>pairs</code>.
   * @returns {Object}
   *   A reference to <code>opt_objToAugment</code>.
   */
  function pair(pairs, opt_objToAugment) {
    opt_objToAugment = opt_objToAugment || {};
    for (var i = 0, l = pairs.length; i < l; i++) {
      opt_objToAugment[pairs[i][0]] = pairs[i][1];
    }
    return opt_objToAugment;
  }
  
  /**
   * Creates a copy of a function with the left-most arguments preset.
   * @name partial
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/partial/2.23.0}
   * @param {Function} fn
   *   The function whose left-most arguments should be preset.
   * @param {Array} arrPresetArgs
   *   An array containing all of the arguments to always be passed to the
   *   returned version of <code>fn</code>.
   * @param {boolean} [opt_presetFromRight=false]
   *   Boolean indicating if the preset arguments should always be passed after
   *   all other arguments.  If <code>false</code> all preset arguments will precede the
   *   other arguments passed.
   * @returns {Function}
   *   A wrapper function which essentially acts as <code>fn</code> with the
   *   <code>arrPresetArgs</code> already being sent.  The arguments in <code>arrPresetArgs</code>
   *   will be sent as the left-most arguments if <code>opt_presetFromRight</code> is not
   *   specified or <code>false</code>-ish, otherwise they will be sent as the right-most
   *   arguments.
   */
  function partial(fn, arrPresetArgs, opt_presetFromRight) {
    return opt_presetFromRight
      ? function() { return fn.apply(this, slice(arguments).concat(arrPresetArgs)); }
      : function() { return fn.apply(this, arrPresetArgs.concat(slice(arguments))); };
  }
  
  /**
   * Creates a partial function where the arguments in any position can be set.
   * Also allows for setting the value of <code>this</code>.
   * @name partialAny
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/partialAny/2.23.0}
   * @param {Function} fn
   *   Function to return a partial version of.
   * @param {Object} objArgPresets
   *   Object whose properties will be used to preset the arguments within the
   *   partial function.  Each property name that is a positive integer will be
   *   used to fill in arguments from the left while property names that are
   *   negative integers (including <code>-0</code>) will be used to fill in arguments from
   *   the right.  If a property is named <code>this</code> it will be used to specify the
   *   <code>this</code> context object of the partial function.
   * @returns {Function}
   *   A partial version of <code>fn</code> with the arguments specified in <code>objArgPresets</code>
   *   already set.
   */
  function partialAny(fn, objArgPresets) {
    var objThis, objThisGiven, arrPresets = [];
    forIn(objArgPresets, function(value, key) {
      if (key == 'this') {
        objThis = value;
        objThisGiven = 1;
      }
      else if (isFinite(key = +key) && key == ~~key) {
        arrPresets.push([key, value]);
      }
    });
    arrPresets.sort(function(a, b) {
      a = 1 / a[0];
      b = 1 / b[0];
        return (a < 0 && b < 0) ? b - a : (a - b);
    });
    return function() {
      for (var preset, arrArgs = slice(arguments), i = arrPresets.length; i--; ) {
        preset = arrPresets[i];
        arrArgs.splice(preset[0] || (1/preset[0] < 0 ? Infinity : 0), 0, preset[1]);
      }
      return fn.apply(objThisGiven ? this : objThis, arrArgs);
    };
  }
  
  /**
   * Pastes a string into another string while possibly replacing part of the
   * string being pasted into.
   * @name paste
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/paste/2.23.0}
   * @param {string} str
   *   String to be modified.
   * @param {string} newPart
   *   String to be pasted into <code>str</code>.
   * @param {number} startIndex
   *   Index within <code>str</code> indicating where to paste <code>newPart</code>.
   * @param {number} [opt_endIndex=startIndex]
   *   Index within <code>str</code> indicating where the end of <code>newPart</code> will end.
   * @returns {string}
   *   A copy of <code>str</code> modified so that <code>newPart</code> is pasted into it from
   *   <code>startIndex</code> to <code>opt_endIndex</code>.
   */
  function paste(str, newPart, startIndex, opt_endIndex) {
    str = cut(str, startIndex, opt_endIndex == undefined ? startIndex : opt_endIndex);
    return str.slice(0, startIndex) + newPart + str.slice(startIndex);
  }
  
  /**
   * Creates a copy of the given array making each item the value of the given
   * property name.
   * @name pluck
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/pluck/2.23.0}
   * @param {Array} arr
   *   Array to copy.
   * @param {string|number} propName
   *   Name of the property of each array item that will replace the
   *   corresponding array item.  If this is a negative number and the array
   *   items are arrays this will be used as the index counting from the end of
   *   those arrays.
   * @returns {Array}
   *   Copy of <code>arra</code> where each item is replaced by one of its properties as
   *   specified by <code>propName</code>.
   */
  function pluck(arr, propName) {
    for (var item, negPropName = propName < 0 && ~~propName === propName, result = [], i = arr.length; i--;) {
      item = arr[i];
      if (has(arr, i)) {
        result[i] = item == undefined
          ? item
          : item[(negPropName && isFinite(item.length)) ? propName + item.length : propName];
      }
    }
    return result;
  }
  
  /**
   * Periodically tests for a condition to be met before executing some code.
   * @name poll
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/poll/2.23.0}
   * @param {Function} fnTest
   *   Polling function which will be executed every <code>opt_interval</code> until it
   *   returns a <code>true</code>-ish value or the <code>opt_timeout</code> time elapses.  The
   *   arguments passed will be (1) a prevention function that when called will
   *   prevent further tests from occurring and (2) the number of times this
   *   function was called.  Calling the prevention function will only prevent
   *   subsequent polls from happening, it will not indicate that the test was a
   *   success or failure.
   * @param {Function} fnOnFound
   *   Function to execute once <code>fnTest</code> evaluates to a <code>true</code>-ish value.  The
   *   arguments passed will be (1) the return value from the success call to
   *   <code>fnTest</code> and (2) the number of times <code>fnTest</code> was called.
   * @param {number} [opt_interval=100]
   *   Amount of milliseconds to wait between tests.
   * @param {number} [opt_timeout=Infinity]
   *   Amount of milliseconds to wait timing out the poll.
   * @returns {boolean}
   *   Indicating if the first call to <code>fnTest</code> was a success.
   */
  function poll(fnTest, fnOnFound, opt_interval, opt_timeout) {
    var isDone, iteration = 0, self = this, start = new Date;
    opt_timeout = opt_timeout == undefined ? Infinity : opt_timeout;
    opt_interval = opt_interval == undefined ? 100 : opt_interval;
    function cancel() {
      isDone = true;
    }
    function fnTestWrapper() {
      try {
        var result = fnTest.call(self, cancel, ++iteration);
        if (result) {
          isDone = result;
          fnOnFound.call(self, result, iteration);
          result;
        }
        return !!result;
      }
      catch (e) {
        throw e;
      }
      finally {
        if (!isDone && (new Date - start) < opt_timeout) {
          setTimeout(fnTestWrapper, opt_interval);
        }
      }
    }
    return fnTestWrapper();
  }
  
  /**
   * Takes a URL and goes to it using the POST method.
   * @name postURL
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/postURL/2.23.0}
   * @param {string} url
   *   The URL with the GET parameters to go to.
   * @param {Array|Object} [opt_data]
   *   Data to be passed when posting to the URL.  If this is an array, it
   *   should be an array of arrays where the inner arrays' first elements are
   *   keys and the second elements are values.  If this is an object, the keys
   *   are interpreted as the keys and the values as values.  If the values are
   *   some type of object or array they will be JSON stringified.
   * @param {string} [opt_target='_self']
   *   If specified it will be used as the target frame for where to POST to.
   * @param {HTMLDocument} [opt_ownerDocument=window.document]
   *   If specified it will be the <code>ownerDocument</code> under which the form will be
   *   created to POST <code>url</code> to.
   * @returns {HTMLFormElement}
   *   The HTML form element that is temporarily to created to post to the
   *   specified URL.
   */
  function postURL(url, opt_data, opt_target, opt_ownerDocument) {
    opt_ownerDocument = opt_ownerDocument || __DOCUMENT;
  
    var body = opt_ownerDocument.body,
      form = extend(opt_ownerDocument.createElement("FORM"), {
        method: 'POST',
        action: url
      });
    if (opt_target) {
      form.target = opt_target;
    }
    form.style.display = "none";
  
    // If opt_data is an array treat it as an array of key/value pairs, otherwise
    // get an array of key/value pairs and traverse them all.
    (Array.isArray(opt_data) ? opt_data : entries(opt_data)).forEach(function (pair) {
      form.appendChild(extend(opt_ownerDocument.createElement("TEXTAREA"), {
        name: pair[0],
        value: 'object' === typeof pair[1] ? JSON.stringify(pair[1]) : pair[1]
      }));
    });
  
    body.appendChild(form);
    form.submit();
    body.removeChild(form);
    return form;
  }
  
  /**
   * Always ensures that a string starts with a specific substring.
   * @name prefix
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/prefix/2.23.0}
   * @param {string|undefined|null} str
   *   This will be the string to which the prefix will be prepended unless the
   *   test proves that it doesn't need to be prepended.  Specifying <code>undefined</code>
   *   or <code>null</code> will cause a partial function to be returned.
   * @param {string} strPrefix
   *   This will be the prefix to add to <code>str</code> if it needs to be added.
   * @param {RegExp} [opt_rgxTester]
   *   Regular expression to test against the string to be prefixed.  If the
   *   string tests negative against this regular expression <code>strPrefix</code> will be
   *   prepended to the beginning of it.
   * @returns {string|function(string)}
   *   If <code>str</code> is not given a partial function will be returned which will
   *   await the string value of <code>str</code>.  If <code>opt_rgxTester</code> isn't given, the
   *   <code>str</code> will be returned and it will be prefixed with <code>strPrefix</code> if it
   *   isn't already at the beginning of it.  If <code>opt_rgxTester</code> is given and
   *   <code>str</code> tests negative against it <code>str</code> prefixed with <code>strPrefix</code> will be
   *   returned.  Otherwise <code>str</code> will be returned.
   */
  function prefix(str, strPrefix, opt_rgxTester) {
    return str == undefined
      ? function(str) { return prefix(str, strPrefix, opt_rgxTester); }
      : (opt_rgxTester ? !opt_rgxTester.test(str) : str.indexOf(strPrefix))
        ? strPrefix + str
        : str;
  }
  
  /**
   * Gets the value of a property.
   * @name prop
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/prop/2.23.0}
   * @param {Object} obj
   *   Object whose property will be retrieved.
   * @param {string} [opt_name]
   *   Name of the property to get.  If <code>obj[opt_name]</code> references a function it
   *   will be called to get the property value.
   * @returns {Function|*}
   *   If both <code>obj</code> and <code>opt_name</code> are given, the property <code>obj[opt_name]</code> will
   *   be returned.  If <code>opt_name</code> is not given a partial function is returned
   *   which will await the value of <code>opt_name</code>.  If the property to be returned
   *   is a function it will be evaluated and the return value will be returned
   *   instead.
   */
  function prop(obj, opt_name) {
    function getProp(name) {
      name = obj[name];
      return 'function' == typeof name ? name.call(obj) : name;
    }
    obj = obj || Object(obj);
    return arguments.length > 1 ? getProp(opt_name) : getProp;
  }
  
  /**
   * Gets the value of a property.
   * @name propOf
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/propOf/2.23.0}
   * @param {string} name
   *   Name of the property to get.  If <code>obj[opt_name]</code> references a function it
   *   will be called to get the property value.
   * @param {Object} [opt_obj]
   *   Object whose property will be retrieved.
   * @returns {Function|*}
   *   If both <code>opt_obj</code> and <code>name</code> are given, the property <code>opt_obj[name]</code> will
   *   be returned.  If <code>opt_obj</code> is not given a partial function is returned
   *   which will await the value of <code>opt_obj</code>.  If the property to be returned
   *   is a function it will be evaluated and the return value will be returned
   *   instead.
   */
  function propOf(name, opt_obj) {
    function getPropOf(obj) {
      var prop = (obj || Object(obj))[name];
      return 'function' == typeof prop ? prop.call(obj) : prop;
    };
    return arguments.length > 1 ? getPropOf(opt_obj) : getPropOf;
  }
  
  /**
   * Converts degrees to radians.
   * @name radians
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/radians/2.23.0}
   * @param {number} degrees
   *   The number representing degrees of an angle which will be converted to
   *   radians.
   * @returns {number}
   *   The equivalent of <code>degrees</code> in radians.
   */
  function radians(degrees) {
    return degrees * Math.PI / 180;
  }
  
  /**
   * Either generates a random number or pulls a random value from an array.
   * @name random
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/random/2.23.0}
   * @param {Array|number} arrOrMinOrMax
   *   If an array, a random item from it will be returned.  If this is a number
   *   and <code>opt_max</code> is either not given or boolean this will be seen as the
   *   maximum value.  Otherwise this will be seen as the minimum value that can
   *   be returned.
   * @param {number|boolean} [opt_max]
   *   If this is a number it will be interpreted as the maximum value.  The
   *   maximum value will never be returned because a random number between the
   *   minimum value (inclusive) and the maximum value (exclusive) is always
   *   returned.  If this is a boolean it will be used as <code>opt_truncate</code> and the
   *   maximum will come from <code>arrOrMinOrMax</code> thus setting <code>0</code> to
   *   <code>arrOrMinOrMax</code>.
   * @param {boolean} [opt_truncate=false]
   *   If <code>true</code>-ish and <code>arrOrMinOrMax</code> wasn't an array the returned value will
   *   be truncated.
   * @returns {*}
   *   If <code>arrOrMinOrMax</code> is an array a random value from the array will be
   *   returned, otherwise a random number in the given range will be returned.
   */
  function random(arrOrMinOrMax, opt_max, opt_round) {
    if ('number' == typeof arrOrMinOrMax) {
      if ('boolean' == typeof opt_max) {
        opt_round = opt_max;
        opt_max = undefined;
      }
      if (opt_max === undefined) {
        opt_max = arrOrMinOrMax;
        arrOrMinOrMax = 0;
      }
      arrOrMinOrMax = Math.random() * (opt_max - arrOrMinOrMax) + arrOrMinOrMax;
      return opt_round ? Math.trunc(arrOrMinOrMax) : arrOrMinOrMax;
    }
    return arrOrMinOrMax[random(0, arrOrMinOrMax.length, 1)];
  }
  
  /**
   * Creates an array of numbers within a given range.
   * @name range
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/range/2.23.0}
   * @param {number} startOrStop
   *   The starting point of the range to return.  If this is the argument
   *   argument passed the value will be used as <code>opt_stop</code> and this will become
   *   <code>0</code>.
   * @param {number} [opt_stop=startOrStop]
   *   The non-inclusive boundary of the range.  This value will not be included
   *   in the returned array.  The value is pulled from <code>startOrStop</code> if not
   *   specified.
   * @param {number} [opt_step=1]
   *   The difference between each subsequent number in the range.
   * @returns {Array.<number>}
   *   Returns an array of all of the numbers in the range. If <code>opt_step</code> >= <code>0</code>
   *   but <code>startOrStop</code> >= <code>opt_stop</code>, an empty array will be returned. Also,
   *   if <code>opt_step</code> <= <code>0</code>, but <code>startOrStop</code> <= <code>opt_stop</code>, an empty array
   *   will be returned.
   */
  function range(startOrStop, opt_stop, opt_step) {
    if (arguments.length < 2) {
      opt_stop = startOrStop;
      startOrStop = 0;
    }
    for (var ret = [], t = (opt_step = opt_step || 1) > 0; t ? startOrStop < opt_stop : startOrStop > opt_stop; startOrStop += opt_step) {
      ret.push(startOrStop);
    }
    return ret;
  }
  
  /**
   * Creates a new wrapper function that will call the original the arguments in
   * a different order.
   * @name rearg
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/rearg/2.23.0}
   * @param {Function} fn
   *   Function to be wrapped and called with the arguments in a different
   *   order.
   * @param {Array.<number>} indices
   *   Indices of the arguments to be passed.  If the you want <code>fn(a,b,c)</code> to
   *   always be called as <code>fn(c,a,b)</code> you can supply <code>[2,0,1]</code>.
   * @param {number} [opt_restIndex=indices.length]
   *   The starting index of the rest of the arguments that will be allowed to
   *   be passed in after those specified by <code>indices</code>.  If <code>Infinity</code>, no more
   *   arguments will be passed no matter how many are sent to the wrapper
   *   function.  If <code>undefined</code>, <code>indices.length</code> will be used.
   * @returns {Function}
   *   The wrapper function that will call <code>fn</code> with the arguments in the
   *   specified order.
   */
  function rearg(fn, indices, opt_restIndex) {
    var normalIndices = indices.map(function(v,i){return '$' + i;}).join(','),
        modifiedIndices = indices.map(function(v, i) {
          return '$' + (v == undefined ? i : v);
        }).join(',');
    return Function('f,s', 'return function(' + normalIndices + '){return f.apply(this, [' + modifiedIndices + '].concat(s(arguments,' + (opt_restIndex == undefined ? indices.length : opt_restIndex) + ')))}')(fn,slice);
  }
  
  /**
   * Modifies keys of an object or augments the object with more keys for the
   * same values.
   * @name rekey
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/rekey/2.23.0}
   * @param {Array|Object} obj
   *   The object or array to be cloned, possibly augmented and changed.  If not
   *   given then a partial function will be returned by <code>rekey()</code>.
   * @param {Array|Object} keyMap
   *   Specifies how to update the clone of <code>obj</code>.  If this is an object the
   *   keys will correspond to the current keys and values will correspond to
   *   the new keys.  If this is an array then each value must be an array where
   *   the first item is used to match current keys and the second item is used
   *   to define the new keys.  An array at times may be preferred so that you
   *   can target one or more current keys using a function (which is passed
   *   just passed each current key and should return a value indicating whether
   *   it should be modified) or a regular expression.  If the property values
   *   or each array item's second value is a function, it will be called with
   *   the value of the current key that is to be modified or augmented and the
   *   return value will be used as the new key or additional key.
   * @param {boolean} [opt_keepOriginals=false]
   *   If <code>true</code> then the returned object or array will be an augmented version
   *   of <code>obj</code>.  Otherwise any keys that are found in <code>obj</code> because of <code>keyMap</code>
   *   values will simply be replaced according to the corresponding values in
   *   <code>keyMap</code>.
   * @returns {Function|Array|Object}
   *   If <code>obj</code> is <code>undefined</code> or <code>null</code> a partial function which will allow for
   *   <code>obj</code> to be defined will be returned.  If <code>obj</code> is array-like a new array
   *   will be returned.  Otherwise a new object will be returned.  In the case
   *   that an array or an object is returned the keys will be based on the keys
   *   from <code>obj</code> and the values determined by <code>keyMap</code>.
   */
  // has(), isArrayLike()
  function rekey(obj, keyMap, opt_keepOriginals) {
    obj = Object(obj);
    // Allow for partial call...
    if (obj == undefined) {
      return function(obj) {
        return rekey(obj, keyMap, opt_keepOriginals);
      };
    }
  
    var isArray = isArrayLike(obj); 
    var newObj = isArray ? [] : {};
    var excludedOriginals = [];
    (
      Array.isArray(keyMap)
        ? keyMap
        : Object.keys(keyMap).map(function (key) {
            return [key, keyMap[key]];
          })
    ).forEach(function (pair) {
      var oldKey = pair[0];
      var oldKeyType = typeof oldKey;
      var testId = oldKeyType === 'function' ? 1 : 'string,boolean,number'.indexOf(oldKeyType) >= 0 ? 2 : 0;
      var newKey = pair[1];
      var newKeyIsFunc = 'function' === typeof newKey;
      for (var keyInObj in obj) {
        if (has(obj, keyInObj = isArray ? +keyInObj : keyInObj)) {
          if (testId ? testId === 1 ? oldKey(keyInObj) != undefined : oldKey == keyInObj : oldKey.test(keyInObj)) {
            newObj[keyInObj.replace(testId ? /^[^]*$/ : oldKey, newKeyIsFunc ? newKey(keyInObj) : newKey)] = obj[keyInObj];
            if (!opt_keepOriginals) {
              excludedOriginals.push(keyInObj);
            }
          }
        }
      }
    });
    for (var keyInObj in obj) {
      if (has(obj, keyInObj) && excludedOriginals.indexOf(isArray ? +keyInObj : keyInObj) < 0 && !has(newObj, keyInObj)) {
        newObj[keyInObj] = obj[keyInObj];
      }
    }
    return newObj;
  }
  
  /**
   * Remove all occurrences of a specific value from a given array.
   * @name removeAll
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/removeAll/2.23.0}
   * @param {Array} array
   *   The array from which to remove the target value.
   * @param {*} target
   *   The target value that should be removed from <code>array</code>.
   * @returns {Array}
   *   A reference to <code>array</code>.
   */
  
  /**
   * Remove a specific value from a given array starting at the end of the
   * array.
   * @name removeRight
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/removeRight/2.23.0}
   * @param {Array} array
   *   The array from which to remove the target value.
   * @param {*} target
   *   The target value that should be removed from <code>array</code>.
   * @param {number} [opt_maxRemoval=1]
   *   The maximum number of times to remove <code>target</code> from <code>array</code>.
   * @returns {Array}
   *   A reference to <code>array</code>.
   */
  
  /**
   * Remove a specific value from a given array.
   * @name remove
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/remove/2.23.0}
   * @param {Array} array
   *   The array from which to remove the target value.
   * @param {*} target
   *   The target value that should be removed from <code>array</code>.
   * @param {number} [opt_maxRemoval=1]
   *   The maximum number of times to remove <code>target</code> from <code>array</code>.
   * @returns {Array}
   *   A reference to <code>array</code>.
   */
  // // ORIGINAL CODE FOR remove(), removeRight() and removeAll():
  // function remove(array, target, opt_maxRemovals) {
  //   opt_maxRemovals = opt_maxRemovals != undefined ? ~~opt_maxRemovals || 0 : 1;
  //   for (var v, isnan = target !== target, i = -1, l = array.length; ++i < l && 0 < opt_maxRemovals;) {
  //     if (i in array) {
  //       v = array[i];
  //       if (isnan ? v !== v : target === v) {
  //         array.splice(i, 1);
  //         i--;
  //         f--;
  //         opt_maxRemovals--;
  //       }
  //     }
  //   }
  //   return array;
  // }
  eval('ZYXW=-1,fV++d<f&&0<bU,d--,f--,b--TZRightYXWV0<d--&&0<bU,b--TZAllYWV0<d--UT'.replace(
    /[T-Z]/g,
    function (c) {
      return {
        Z: 'function remove',
        Y: '(a,e,b){',
        X: 'b=void 0!=b?~~b||0:1;',
        W: 'for(var c,g=e!==e,d',
        V: '=a.length;',
        U: ';)d in a&&(c=a[d],g?c!==c:e===c)&&(a.splice(d,1)',
        T: ');return a}'
      }[c];
    }
  ));
  
  /**
   * Removes a value from an array at the specified position.
   * @name removeAt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/removeAt/2.23.0}
   * @param {Array} array
   *   Array from which a value will be removed.
   * @param {number} position
   *   Position within <code>array</code> from where the value should be removed.  If this
   *   is negative the position will be calculated from the end of <code>array</code>.
   * @returns {Array}
   *   A reference to <code>array</code>.  This is useful when chaining.
   */
  function removeAt(array, position) {
    array.splice(position, 1);
    return array;
  }
  
  /**
   * Takes the name of a cookie and sets its value.
   * @name setCookie
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/setCookie/2.23.0}
   * @param {string} name
   *   Name of the cookie to set.
   * @param {string} value
   *   Value of the cookie to set.  If not a string, it will be coerced into a
   *   string.
   * @param {Object} [opt_options={path:'/'}]
   *   Object whose property values can be used to specify additional options.
   *   If the cookie is meant to expire at a certain time you can set the
   *   <code>expires</code> property to a <code>Date</code> object or the number representation of
   *   such a date.  If you want to specify the maximum amount of seconds until
   *   the cookie expires you can set the <code>maxAge</code> (or <code>max-age</code>) property to
   *   the number of seconds.  If you want to specify the hosts to which the
   *   cookie will be sent you can set the <code>domain</code> property.  If a domain is
   *   specified, subdomains are always included.  If you want to specify the
   *   URL path that under which the cookie will be saved you can specify it in
   *   the <code>path</code> property.  If you want to set a secure cookie while on a
   *   secure site (<code>https:</code>) you can set the <code>secure</code> property to <code>true</code>.
   * @returns {boolean}
   *   Boolean indicating whether or not the cookie was set.
   */
  function setCookie(name, value, opt_options) {
    opt_options = opt_options || {};
    var maxAge = opt_options['max-age'] || opt_options.maxAge,
      expires = maxAge ? +new Date + maxAge * 1000 : opt_options.expires,
      path = opt_options.path,
      domain = opt_options.domain,
      secure = opt_options.secure;
    __DOCUMENT.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value)
      + (expires ? "; expires=" + (new Date(expires)).toGMTString() : "")
      + (path ? "; path=" + path : "; path=/")
      + (domain ? "; domain=" + opt_options.domain : "")
      + (secure ? "; secure" : "");
    return hasCookie(name);
  }
  
  /**
   * Removes the specified cookie(s).
   * @name removeCookie
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/removeCookie/2.23.0}
   * @param {string|RegExp|undefined|null} [name=undefined]
   *   Either the name of the cookie to remove or a regular expression matching
   *   the names of all of the cookies to remove.  If <code>undefined</code> or <code>null</code> is
   *   supplied that means all cookies should be removed.
   * @param {Object} [opt_options={"path":"/"}]
   *   Object whose property values can be used to specify additional options.
   *   If you want to specify the hosts for which the cookie will be removed you
   *   can set the <code>domain</code> property.  If a domain is specified, subdomains are
   *   always included.  If you want to specify the URL path from under which
   *   the cookie will be removed you can specify it in the <code>path</code> property.  If
   *   you want to remove a secure cookie while on a secure site (<code>https:</code>) you
   *   can set the <code>secure</code> property to <code>true</code>.
   * @returns {*}
   *   If <code>name</code> is a string and matches the name of a cookie, that cookie's
   *   value will be returned as a string.  If <code>name</code> is a string but doesn't
   *   match the name of a cookie <code>undefined</code> will be returned.  If <code>name</code> is of
   *   type <code>RegExp</code> an object containing all of the cookies with names that
   *   match the specified regular expression will be returned.  If <code>name</code> is
   *   <code>undefined</code> or <code>null</code> an object containing all of the cookies will be
   *   returned.  If an object is returned each key will represent the name of
   *   each removed cookie and each value will be the value of that removed
   *   cookie.
   */
  function removeCookie(name, opt_options) {
    var key, cookies, ret = getCookie(name);
    if('string' == typeof ret) {
      (cookies = {})[name] = ret;
    }
    else {
      cookies = ret;
    }
    opt_options = opt_options || {};
    for(key in cookies) {
      if (has(cookies, key)) {
        setCookie(key, '', {
          path: opt_options.path,
          domain: opt_options.domain,
          secure: opt_options.secure,
          expires: new Date(0)
        });
      }
    }
    return ret;
  }
  
  /**
   * Creates a object where the keys and values are based on key/value pair
   * returned by the specified pairing function.
   * @name repair
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/repair/2.23.0}
   * @param {*} obj
   *   Object whose key/value pairs will be sent to the pairing function.
   * @param {Function} fnPairer
   *   Pairing function that will take (1) an array containing each key/value
   *   pair within <code>obj</code> and (2) a reference to <code>obj</code>.  If something is returned
   *   that is not <code>undefined</code> or <code>null</code> it should be an array containing the
   *   key/value pair to add to <code>opt_initial</code>.
   * @param {*} [opt_initial={}]
   *   Object to return from <code>repair()</code> function which will contain the newly
   *   paired key/value pairs.  If not specified an empty object will be used.
   * @returns {*}
   *   A reference to <code>opt_initial</code> which has the key/value pairs returned by
   *   <code>fnPairer</code> added to it.
   */
  function repair(obj, fnPairer, opt_initial) {
    opt_initial = opt_initial || {};
    forOf(obj, function(value, key) {
      var result = fnPairer([key, value], obj);
      if (result) {
        opt_initial[result[0]] = result[1];
      }
    });
    return opt_initial;
  }
  
  /**
   * Produces a function that when called will simply return what was passed in.
   * @name repay
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/repay/2.23.0}
   * @param {...*} [input]
   *   Zero or more arguments that will be returned by function made by this
   *   <code>repay</code> function.
   * @returns {Function}
   *   A function that when called will return all <code>input</code> arguments originally
   *   passed to the <code>repay</code> function.  If only one <code>input</code> was passed, calling
   *   this returned function will simply return that one <code>input</code> argument.  In
   *   all other cases an array containing all <code>input</code> arguments will be
   *   returned when this returned function is called.
   */
  function repay(input) {
    var args = arguments;
    return Function('s,i,a', 'return function(){return ' + (args.length - 1 ? 's(a)' : 'i') + '}')(slice, input, args);
  }
  
  /**
   * Creates a new string which is the given string repeated the specified
   * number of times.
   * @name repeat
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/repeat/2.23.0}
   * @param {string} string
   *   String to be repeated.
   * @param {number} count
   *   Number of times that <code>string</code> should be repeated.
   * @returns {string}
   *   A new string which will be <code>string</code> repeated <code>count</code> times.
   */
  function repeat(string, count) {
    return Array(count + 1).join(string);
  }
  
  /**
   * Replace targeted substrings with new strings.
   * @name replace
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/replace/2.23.0}
   * @param {string} subject
   *   String to be modified and returned.
   * @param {Array|Object|string} target
   *   If this is a string each instance of this string will be found within
   *   <code>subject</code> and replaced by the result of <code>replacer</code>.  If this is an array
   *   each of the contents will be searched for within <code>subject</code> and replaced
   *   by the result of <code>replacer</code>.  If this is an object each key will be
   *   searched for and replaced by the result of the corresponding value.  If
   *   the corresponding value of an object's property is a function it will be
   *   executed the same way <code>replacer</code> would be.
   * @param {function(Array,number,Function)|string} replacer
   *   If a function is supplied it will be called for each <code>target</code> found with
   *   argument 1 being the match array (including <code>match.input</code>, <code>match.index</code>,
   *   and <code>match.source</code>), argument 2 being the iteration count and argument 3
   *   being a function that when executed will immediately stop replacing
   *   values.  If a string is supplied it will replace all instances of
   *   <code>target</code> within <code>subject</code>.  This parameter can be omitted and will be
   *   ignored if <code>target</code> is an object.
   * @returns {string}
   *   <code>subject</code> with all of the targeted substrings replaced with new strings
   *   as specified either by <code>target</code> or <code>replacer</code>.
   */
  var replace;
  (function(breakError) {
    function end() { throw breakError; }
    replace = function(subject, target, replacer) {
      var match, matches, i, l,
          count = 0,
          replacement = replacer,
          targetType = typeOf(target = 'string' == typeof target ? [target] : target),
          targetIsRegExp = targetType == 'RegExp',
          isObject = !targetIsRegExp && targetType != 'Array',
          rgx = targetIsRegExp ? target : new RegExp(
            (isObject ? Object.keys(target) : target).map(function(v){return quoteRegExp(v);}).join('|'),
            'g'
          ),
          result = subject;
  
      try {
        for (matches = matchAll(subject, rgx), i = 0, l = matches.length; i < l; i++) {
          match = matches[i];
          if (isObject) {
            replacer = target[match];
          }
          if ('function' == typeof replacer) {
            replacement = replacer.call(result, match, ++count, end);
          }
          result = result.slice(0, match.index) + replacement + result.slice(match.index + match[0].length);
        }
      }
      catch (e) {
        if (e != breakError) {
          throw e;
        }
      }
  
      return result;
    };
  })({});
  
  /**
   * Replace multiple instances of a <code>target</code> string with a <code>replacement</code>
   * string.
   * @name replaceMany
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/replaceMany/2.23.0}
   * @param {string} subject
   *   The string to be searched and modified.
   * @param {string} target
   *   The string to look for and replace within <code>subject</code>.
   * @param {string} replacement
   *   The string to replace instances of <code>target</code> within <code>subject</code>.
   * @param {number} [opt_limit=Infinity]
   *   The maximum number of occurrences of <code>target</code> to replace.  If negative it
   *   indicates how many occurrences of <code>target</code> from the end to not replace.
   *   If not specified all occurrences will be replaced.
   * @returns {string}
   *   A new version of <code>subject</code> with a maximum of <code>opt_limit</code> number of
   *   occurrences of <code>target</code> replaced by <code>replacement</code>.
   */
  function replaceMany(subject, target, replacement, opt_limit) {
    var arr = subject.split(target);
    var first = arr.slice(
      0,
      opt_limit = opt_limit == undefined ? Infinity : opt_limit < 0 ? ~~opt_limit - 1 : ~~opt_limit
    );
    var rest = arr.slice(opt_limit);
    return first.join(replacement) + ((0 in first && 0 in rest) ? replacement : '') + rest.join(target + '');
  }
  
  /**
   * Creates a wrapper function that if called with more arguments than
   * specified, those arguments will be passed in as an array in the final
   * argument.
   * @name restParam
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/restParam/2.23.0}
   * @param {Function} fn
   *   The function to call with the normal parameters followed by the rest
   *   parameter in its place as specified by <code>opt_start</code>.
   * @param {number} [opt_start=fn.length-1]
   *   The index within the arguments passed to the returned function that will
   *   start the summary of the rest parameter.
   * @returns {Function}
   *   Returns a function that when invoked will call <code>fn</code>, but if there are any
   *   that have an index greater or equal to <code>opt_start</code> they will be passed in
   *   as the last parameter which will be an array.
   */
  function restParam(fn, opt_start) {
    opt_start = opt_start == undefined ? fn.length - 1 : opt_start;
    return function() {
      var args = slice(arguments);
      return fn.apply(this, args.length < opt_start ? args : slice(args, 0, opt_start).concat([slice(args, opt_start)]));
    }
  }
  
  /**
   * Takes an array (or an array-like object) or a string, copies it, and
   * returns the copy in reverse order.
   * @name reverse
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/reverse/2.23.0}
   * @param {Array|string} arrOrStr
   *   If this is an array or an array-like object a copy of it will be made as
   *   an array and then the copy will be reversed.  If this is a string a copy
   *   will be made with all of the characters put in reverse order.
   * @returns {Array|string}
   *   Returns a copy of <code>arrOrStr</code> in reverse order.
   */
  function reverse(arrOrStr) {
    return 'string' === typeof arrOrStr
      ? arrOrStr.split('').reverse().join('')
      : slice(arrOrStr).reverse();
  }
  
  /**
   * Determines the nth-root of a number.
   * @name root
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/root/2.23.0}
   * @param {number} number
   *   Number to be rooted.
   * @param {number} n
   *   The degree to which <code>number</code> should be rooted (eg. <code>3</code> for cube root).
   * @returns {number}
   *   <code>number</code> to the <code>n</code>th-root.
   */
  function root(number, n) {
    var result = Math.pow(Math.abs(number), 1 / n);
    return (number < 0 && n % 2) ? -result : result;
  }
  
  /**
   * Rounds a number towards <code>0</code>.
   * @name roundOut
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/roundOut/2.23.0}
   * @param {number} num
   *   Number to round away from <code>0</code>.
   * @param {number} [opt_precision=0]
   *   The precision (as an integer) with which to round the number away from
   *   <code>0</code>.
   * @returns {number}
   *   <code>num</code> rounded away from <code>0</code>.
   */
  
  /**
   * Rounds a number towards <code>0</code>.
   * @name roundIn
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/roundIn/2.23.0}
   * @param {number} num
   *   Number to round towards <code>0</code>.
   * @param {number} [opt_precision=0]
   *   The precision (as an integer) with which to round the number towards <code>0</code>.
   * @returns {number}
   *   <code>num</code> rounded towards <code>0</code>.
   */
  eval('var roundIn,roundOut;');
  'In<Out>'.replace(/(\w+)(.)/g, function(_, fnEnding, op) {
    eval('(function(c,f){round' + fnEnding + '=function(n,p){return(+n' + op + '0?c:f)(n,p)}})')(ceil, floor);
  });
  
  /**
   * Determines if two or more numbers have the same sign (negative or
   * positive).
   * @name sameSign
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/sameSign/2.23.0}
   * @param {number} number1
   *   First number to be compared to all of the other passed numbers.
   * @param {number} number2
   *   Second number to be compared to all of the other numbers passed.
   * @param {...number} [numberX]
   *   All additional numbers, if any, to be compared to <code>number1</code>.
   * @returns {boolean}
   *   Returns <code>true</code> if all of the numbers are positive or all of them are
   *   negative, otherwise <code>false</code> is returned.
   */
  function sameSign() {
    for (
      var args = arguments, b = args[0], mustBeNeg = b === b ? b < 0 || 1 / b < 0 : b, i = args.length;
      b = args[--i], i && mustBeNeg === (b === b ? b < 0 || 1 / b < 0 : b);
    );
    return !i;
  }
  
  /**
   * Takes an array and returns a new array with some or all of the original
   * values not usually in the same order.
   * @name sample
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/sample/2.23.0}
   * @param {Array} arr
   *   Array to create a new sample array from.
   * @param {number} [opt_count=1]
   *   Number of items to put in the sample array.  Cannot exceed <code>arr.length</code>.
   * @returns {Array}
   *   Sample array of random elements picked from <code>arr</code>.
   */
  // Done this way to account for partially or fully empty arrays
  function sample(arr, opt_count) {
    var t, j, result = [], l = arr.length, i = l;
    if (l) {
      for (opt_count = Math.max(1, Math.min(opt_count || 1, l)); i--, result[i] = i;);
      for (i = l; opt_count > l - i; ) {
        j = Math.round(Math.random() * --i);
        if (i && i != j) {
          t = result[i];
          result[i] = result[j];
          result[j] = t;
        }
        if (has(arr, result[i])) {
          result[i] = arr[result[i]];
        }
        else {
          delete result[i];
        }
      }
    }
    return slice(result, -opt_count);
  }
  
  /**
   * Scales a number using an input range and and an output range to give a
   * proportional output.
   * @name scale
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/scale/2.23.0}
   * @param {number} x
   *   The value to be scaled.
   * @param {number} minX
   *   The lower bound of <code>x</code>.
   * @param {number} maxX
   *   The upper bound of <code>x</code>.
   * @param {number} minReturn
   *   The lower bound of the return value which directly corresponds to <code>minX</code>.
   * @param {number} maxReturn
   *   The upper bound of the return value which directly corresponds to <code>maxX</code>.
   * @returns {number}
   *   Returns <code>x</code> scaled based on the range of <code>minX</code> to <code>maxX</code> being
   *   proportional to <code>minReturn</code> to <code>maxReturn</code>.  If <code>x</code> is within the range
   *   of <code>minX</code> and <code>maxX</code> the return value will be within the range of
   *   <code>minReturn</code> and <code>maxReturn</code>.  If <code>x</code> is outside of the range of <code>minX</code> to
   *   <code>maxX</code> then the return value will also be outside of the <code>minReturn</code> to
   *   <code>maxReturn</code> range.  If <code>minReturn</code> and <code>maxReturn</code> are the same
   *   <code>minReturn</code> will be returned.
   */
  function scale(x, minX, maxX, minReturn, maxReturn) {
    return (x - minX) * (maxReturn - minReturn) / (maxX - minX) + minReturn;
  }
  
  /**
   * Set the value of a property on an object.
   * @name set
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/set/2.23.0}
   * @param {*|undefined} obj
   *   Object whose property will be set.
   * @param {string} [opt_name=undefined]
   *   Name of the property to be set.
   * @param {*} [opt_value]
   *   Value to which the property will be set.
   * @param {boolean} [opt_returnObj=false]
   *   If this is <code>true</code>-ish and a partial value is not to be returned then
   *   <code>obj</code> will be returned.
   * @returns {*}
   *   If the first 3 arguments are given the previous property value will be
   *   returned unless <code>opt_returnObj</code> is <code>true</code>-ish in which case <code>obj</code> will be
   *   returned.  If one or more arguments are missing a partial function will
   *   be returned that will accept the remaining arguments.  The value of
   *   <code>opt_returnObj</code> will be ignored for partial functions.
   */
  function set(obj, opt_name, opt_value, opt_returnObj) {
    var t1, t2,
        needsObj = obj == undefined,
        needsName = opt_name == undefined,
        needsValue = arguments.length < 3;
    if (needsObj || needsName || needsValue) {
      t1 = ['b','a'];
      t2 = { x: needsObj, y: needsName, z: needsValue };
      return Function(
        's,x,y,z',
        'return function(a,b){return s(x,y,z)}'.replace(/x|y|z/g, function(m){return t2[m]?t1.pop():m}).replace(t1[0] ? '' : ',b', '')
      )(set, obj, opt_name, opt_value, opt_returnObj);
    }
    t1 = obj[opt_name];
    obj[opt_name] = opt_value;
    return opt_returnObj ? obj : t1;
  }
  
  /**
   * Tries to set a value at the given <code>path</code> under the given <code>root</code> object.
   * @name setAt
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/setAt/2.23.0}
   * @param {*} root
   *   Object to start at as the root of the proposed <code>path</code>.
   * @param {Array} path
   *   An array of strings and/or numbers that represent the path to be
   *   traversed under <code>root</code>.
   * @param {*} value
   *   The value to be assigned under the specified <code>path</code> within the <code>root</code>
   *   object.
   * @returns {boolean}
   *   <code>true</code> if <code>value</code> is successfully set under the given path, otherwise
   *   <code>false</code>.
   */
  function setAt(root, path, value) {
    var test = testAt(root, path),
        i = path.length - 1,
        setIt = i >= 0 && test.length >= i;
    if (setIt = setIt && !isPrimitive(root = test[i - 1])) {
      root[path[i]] = value;
    }
    return setIt;
  }
  
  /**
   * Creates a new array with the items in a random order.
   * @name shuffle
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/shuffle/2.23.0}
   * @param {Array} arr
   *   Array to copy and shuffle.
   * @returns {Array}
   *   Shuffled copy of <code>arr</code>.
   */
  // Done this way to account for partially or fully empty arrays
  function shuffle(arr) {
    for (var t, j, result = [], l = arr.length, i = l; i--, result[i] = i;);
    for (i = l; i; ) {
      j = Math.round(Math.random() * --i);
      if (i && i != j) {
        t = result[i];
        result[i] = result[j];
        result[j] = t;
      }
      if (has(arr, result[i])) {
        result[i] = arr[result[i]];
      }
      else {
        delete result[i];
      }
    }
    return result;
  }
  
  /**
   * Determines whether a value is positive, negative, a form of zero (<code>-0</code> or
   * <code>0</code>) or not a number.
   * @name sign
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/sign/2.23.0}
   * @param {number} x
   *   Value that should be interpreted as a number to determine if it is
   *   positive or negative.
   * @returns {number}
   *   Returns <code>NaN</code> if <code>x</code> isn't recognized as a number.  If <code>x</code> is zero <code>x</code>
   *   will be returned.  If <code>x</code> is negative <code>-1</code> is returned, otherwise <code>1</code> is
   *   returned.
   */
  function sign(x) {
    x = +x;
    return x ? x < 0 ? -1 : 1 : x;
  }
  
  /**
   * Specifies whether or not the number is negative (meaning it has a bit
   * indicating the sign of the number).
   * @name signbit
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/signbit/2.23.0}
   * @param {number} x
   *   The value to be observed.
   * @returns {boolean}
   *   Returns <code>true</code> if <code>x</code> is <code>-0</code> or less than <code>0</code>, otherwise returns
   *   <code>false</code>.
   */
  function signbit(x) {
    return x < 0 || 1 / x < 0;
  }
  
  /**
   * Shallow copies and sorts an array given specific criteria.
   * @name sortBy
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/sortBy/2.23.0}
   * @param {Array} array
   *   Array to be sorted.
   * @param {string|Array.<string|number>|Function} criteria
   *   Criteria by which a shallow copy of <code>array</code> will be sorted.  If a string
   *   or an array is supplied it will be used as the path to each individual
   *   array element that will be keyed off of to do the sort.
   * @param {Function} [opt_comparer=YourJS.compare]
   *   Comparison function used to compare each array element criteria.
   * @returns {Array}
   *   A shallow copy of the array sorted according to the specified criteria
   *   and comparison function.
   */
  function sortBy(array, criteria, opt_comparer) {
    // If criteria is a string split it on ".".
    var criteriaType = typeof criteria;
    if (criteriaType === 'string') {
      criteria = criteria.split('.');
    }
  
    // If no comparer given use YourJS's compare() function.
    opt_comparer = opt_comparer || compare;
  
    // 1. Turn all values in array into meta values containing v (real value) and
    //    c (value to compare).
    // 2. Sort the c values.
    // 3. return the sorted array values (v)
    return array
      .map(function(v) {
        return {
          v: v,
          c: criteriaType === 'function'
            ? criteria(v)
            : criteria.reduce(function (criteria, key) {
                if (criteria != undefined) {
                  return criteria[('number' === typeof key && key < 0 && !has(criteria, key) && 'number' === typeof criteria.length) ? criteria.length + key : key];
                }
              }, v)
        };
      })
      .sort(function(a, b) { return opt_comparer(a.c, b.c); })
      .map(function(a) { return a.v; });
  }
  
  /**
   * Creates an array with the span of numbers going from <code>first</code> and ending at
   * <code>last</code> if possible depending on the specified step value.
   * @name span
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/span/2.23.0}
   * @param {number} first
   *   First number to be processed for the returned array.
   * @param {number} last
   *   Last number to be processed for the returned array.
   * @param {number} [opt_step=1]
   *   Defaults to <code>1</code> if not given or if <code>0</code> or <code>NaN</code> is specified.  The
   *   difference between each subsequent number to be processed for the
   *   returned array.
   * @param {Function} [opt_mapper]
   *   Function to call for each number in the sequence and whose return value
   *   will be used as the value added to the returned array.  If specified this
   *   function will be called for every number in the span, receiving it as the
   *   one and only argument.
   * @returns {Array}
   *   An array containing the sequence of numbers starting at <code>first</code> and
   *   ending at <code>last</code>.  If <code>first</code> is less than <code>last</code> and <code>opt_step</code> is less
   *   than <code>0</code> or if <code>last</code> is less than <code>first</code> and <code>opt_step</code> is greater than
   *   <code>0</code> an empty array will be returned.  If <code>opt_mapper</code> is given the array
   *   will contain the sequence of mapped.
   */
  function span(first, last, opt_step, opt_mapper) {
    opt_step = +opt_step || 1;
    for (var result = [], mult = opt_step < 0 ? -1 : 1; mult * (last - first) >= 0; first += opt_step) {
      result.push(opt_mapper ? opt_mapper(first) : first);
    }
    return result;
  }
  
  /**
   * Duplicates an array or a string and adds and/or removes one or more values
   * from it. NOTE: This is different from the traditional splice function
   * because it doesn't modify the array, but rather it returns a modified
   * duplicate.
   * @name splice
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/splice/2.23.0}
   * @param {Array|string} arrOrStr
   *   The array or string to be duplicated and modified.
   * @param {number} start
   *   The position at which to start modifying <code>arrOrStr</code>. If this number is
   *   negative the position will be calculated from the end of <code>arrOrStr</code>.
   * @param {number} [opt_length=Infinity]
   *   Indicates the amount of indices that should be removed from <code>arrOrStr</code>.
   *   If negative this will be used as if it were an index counting from the
   *   end of <code>arrOrStr</code>.
   * @param {Array|string} [opt_replacement]
   *   If specified, this will be inserted at <code>start</code>. If <code>arrOrStr</code> is an
   *   array, this should be an array of the values to insert at <code>start</code>. If
   *   <code>arrOrStr</code> is a string, this should be the string to insert at <code>start</code>.
   * @returns {Array|string}
   *   The modified duplicate of <code>arrOrStr</code>.
   */
  function splice(arrOrStr, start, opt_length, opt_replacement) {
    opt_length = opt_length != undefined
      ? opt_length < 0
        ? arrOrStr.length - start + opt_length
        : opt_length
      : Infinity;
    start = start < 0 ? Math.max(0, arrOrStr.length + start) : start;
    if (isArrayLike(arrOrStr)) {
      return slice(arrOrStr, 0, start).concat(opt_replacement || [], slice(arrOrStr, start + opt_length));
    }
    return arrOrStr.slice(0, start)
      + (opt_replacement === undefined ? '' : opt_replacement)
      + arrOrStr.slice(start + opt_length);
  }
  
  /**
   * Takes a path as a string and splits it into an array.
   * @name splitPath
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/splitPath/2.23.0}
   * @param {string} path
   *   The string path that will be split into its parts.
   * @returns {Array<string>}
   *   The string path that was passed in as an array of strings.
   */
  function splitPath(path) {
    return (path + '.').match(/([^\\.]|\\.)*\./g).map(function(x) {
      return x.slice(0, -1).replace(/\\(.)/g, '$1');
    });
  }
  
  /**
   * Calls a function while flattening first-level array-like arguments so as
   * the spread those individual values and use them as individual arguments.
   * @name spread
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spread/2.23.0}
   * @param {Function} fn
   *   Function to which the spread values will be passed.
   * @param {...*} [args]
   *   Arguments that will be passed to <code>fn</code>.  If any of the arguments are
   *   arrays or array-like they will be flattened and passed to <code>fn</code> so as the
   *   spread those values.
   * @returns {*}
   *   Return value after calling <code>fn</code> with the specified <code>args</code>.
   */
  function spread(fn) {
    return fn.apply(this, slice(arguments, 1).reduce(function(args, arg) {
      return args.concat(isArrayLike(arg) ? slice(arg) : [arg]);
    }, []));
  }
  
  /**
   * Passes an array as the arguments to a construct thusly allowing a variable
   * number of arguments to be passed to the constructor.
   * @name spreadNew
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/spreadNew/2.23.0}
   * @param {Function} constructor
   *   Constructor function to be called with a variable number of arguments.
   * @param {Array} args
   *   Array or array-like object that will be treated as the arguments to pass
   *   to <code>constructor</code>.
   * @returns {*}
   *   Constructed object.
   */
  function spreadNew(constructor, args) {
    return new(constructor.bind.apply(constructor, [null].concat(slice(args))));
  }
  
  /**
   * Determines whether a string starts with the characters of a specified
   * string.
   * @name startsWith
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/startsWith/2.23.0}
   * @param {string} string
   *   The string that will be searched.
   * @param {string} target
   *   The characters to be searched for at the start of <code>string</code>.
   * @param {number} [opt_fromIndex=0]
   *   The position in <code>string</code> at which to begin searching for <code>target</code>.
   * @returns {boolean}
   *   A boolean indicating whether or not <code>target</code> is at the start of <code>string</code>.
   */
  // Similar to https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith
  // Different from String.prototype.startsWith() because 3rd arg can be negative.
  function startsWith(string, target, opt_fromIndex) {
    opt_fromIndex = opt_fromIndex || 0;
    if (opt_fromIndex < 0) {
      opt_fromIndex += string.length;
      if (opt_fromIndex < 0) {
        opt_fromIndex = 0;
      }
    }
    return string.slice(opt_fromIndex, opt_fromIndex + ('' + target).length) === target;
  }
  
  /**
   * Allows for an array or an array-like structure to be stepped through in the
   * order specified after each iteration.
   * @name step
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/step/2.23.0}
   * @param {Array} array
   *   Array or array-like object to step through.
   * @param {number} [opt_stepValue=1]
   *   If given this will indicate the amount by which to increase the index of
   *   the value to be checked next.  For example, if you always want to
   *   traverse an array backwards you can supply <code>-1</code>.
   * @param {Function} [opt_callback]
   *   If not given <code>opt_stepValue</code> will be the only value used for the step
   *   value.  If given this function will be called on each iteration.  This
   *   will be passed (1) the value, (2) the index of the value and (3) <code>array</code>.
   *   The return value will be used as the new step value.  If nothing,
   *   <code>undefined</code> or <code>null</code> is returned then <code>opt_stepValue</code> will be used as
   *   the next step value.
   * @param {number} [opt_startIndex]
   *   If not given this will default to <code>array.length - 1</code> if <code>opt_stepValue</code>
   *   is <code>-1</code>, otherwise this will default to <code>0</code>.  This represents the
   *   starting index.
   * @returns {Array}
   *   An array of the values found in the indices that were traversed.
   */
  // Example at:  https://www.yourjs.com/console/?gist=b93e3d6df9070b4f5fee28a9331c626d&file=step-example.js
  function step(array, opt_stepValue, opt_callback, opt_startIndex) {
    opt_stepValue = ~~opt_stepValue || 1;
    var count = array.length;
    var i = (opt_startIndex == undefined || isNaN(opt_startIndex = ~~opt_startIndex))
      ? opt_stepValue < 0
        ? count - 1
        : 0
      : opt_startIndex;
    i = (~~i % count + count) % count;
    for (var newStep, result = []; 0 <= i && i < array.length; i += opt_stepValue) {
      newStep = opt_callback && opt_callback(array[i], i, array);
      result.push(array[i]);
      opt_stepValue = newStep != undefined ? ~~newStep || 0 : opt_stepValue;
    }
    return result;
  }
  
  /**
   * Substitute values into strings where the corresponding placeholders have
   * been entered.
   * @name sub
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/sub/2.23.0}
   * @param {string} template
   *   The string containing placeholders to be filled in and returned. A
   *   placeholder must correspond to a value in <code>opt_subs</code> and its name must be
   *   surrounded in curly braces (eg. <code>"Hello {name}!"</code> contains the <code>name</code>
   *   placeholder). If a placeholder refers to a number, a ternary operator can
   *   be used (eg. <code>"You have {apples} apple{apples?s:}"</code>). What appears
   *   between the <code>?</code> and the <code>:</code> will replace the placeholder if the variable
   *   before the <code>?</code> is not <code>1</code>. What appears after the <code>:</code> will replace the
   *   placeholder if the variable before the <code>?</code> is <code>1</code>. A 4-ary (AKA
   *   quaterary) operator can also be used if a placeholder refers to a number
   *   (eg. <code>"You have {apples?{apples}:one:no} apple{apples?s:}"</code>). When using
   *   a 4-ary operator, whatever appears after the second <code>:</code> will replace the
   *   placeholder if the variable before the <code>?</code> is <code>0</code>.  If a placeholder is
   *   not ternary or 4-ary but ends with <code>#</code>, <code>opt_funcs</code> will be called on the
   *   value and the return value will be used.  If a placeholder is not ternary
   *   or 4-ary but ends with <code>#</code> followed by a name (eg. <code>#ordinalize</code>), the
   *   function with that property name under <code>opt_funcs</code> will be called for the
   *   value and the return value will replace the placeholder.  If a
   *   placeholder evaluates to a function the function will be called all of
   *   the ternary or 4-ary values as arguments.  Nested expressions are
   *   supported.
   * @param {Array|Object} [opt_subs=global]
   *   Array or object from which to pull the values to be inserted into
   *   <code>template</code>.
   * @param {Array.<Function>|Object.<Function>|Function} [opt_funcs=YourJS]
   *   If this is a function it can be used to modify the values filled in
   *   within <code>template</code>.  If this is an object or an array its properties or
   *   array items can be referenced to modify the values filled in within
   *   <code>template</code>.
   * @returns {string}
   *   Returns <code>template</code> with all of the valid placeholders filled in with
   *   their substitutions as found in <code>opt_subs</code>.
   */
  function sub(template, opt_subs, opt_funcs) {
    opt_subs = opt_subs || __GLOBAL;
    opt_funcs = opt_funcs || YourJS;
    for (
      var result;
      result !== template;
      template = template.replace(
        /\{([\w\$]+)(?:\?((?:\\.|[^\\\{\}:])*):((?:\\.|[^\\\{\}:])*)(?::((?:\\.|[^\\\{\}:])*))?|#([\w\$]+))?\}/ig,
        function(match, arg, multi, one, none, fn) {
          if (has(opt_subs, arg)) {
            arg = opt_subs[arg];
            if ('function' == typeof arg) {
              arg = arg(multi, one, none, match);
            }
            match = multi === undefined ? arg : (arg == 1 ? one : ((!arg && none !== undefined) ? none : multi));
            if (fn != undefined) {
              fn = fn ? opt_funcs[fn] : opt_funcs;
              if ('function' == typeof fn) {
                match = fn(match);
              }
            }
          }
          return match;
        }
      )
    ) {
      result = template;
    }
    return result.replace(/\\(\W)/g, '$1');
  }
  
  /**
   * Always ensures that a string ends with a specific substring.
   * @name suffix
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/suffix/2.23.0}
   * @param {string|undefined|null} str
   *   This will be the string to which the suffix will be appended unless the
   *   test proves that it doesn't need to be appended.  Specifying <code>undefined</code>
   *   or <code>null</code> will cause a partial function to be returned.
   * @param {string} strSuffix
   *   This will be the suffix to add to <code>str</code> if it needs to be added.
   * @param {RegExp} [opt_rgxTester]
   *   Regular expression to test against the string to be suffixed.  If the
   *   string tests negative against this regular expression <code>strSuffix</code> will be
   *   appended to the end of it.
   * @returns {string|function(string)}
   *   If <code>str</code> is not given a partial function will be returned which will
   *   await the string value of <code>str</code>.  If <code>opt_rgxTester</code> isn't given, the
   *   <code>str</code> will be returned and it will be suffixed with <code>strSuffix</code> if it
   *   isn't already at the end of it.  If <code>opt_rgxTester</code> is given and <code>str</code>
   *   tests negative against it <code>str</code> suffixed with <code>strSuffix</code> will be
   *   returned.  Otherwise <code>str</code> will be returned.
   */
  function suffix(str, strSuffix, opt_rgxTester) {
    return str == undefined
      ? function(str) { return suffix(str, strSuffix, opt_rgxTester); }
      : (opt_rgxTester ? !opt_rgxTester.test(str) : str.slice(-strSuffix.length) != strSuffix)
        ? str + strSuffix
        : str;
  }
  
  /**
   * Used to make a file size human readable.  Takes the number of bytes or bits
   * and converts it into a string indicating the number in the appropriate
   * range (eg. KB, MB, etc.).
   * @name suffixFileSize
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/suffixFileSize/2.23.0}
   * @param {number} count
   *   Number of bytes or bits to represent as a string.
   * @param {boolean} [opt_countInBits=false]
   *   Specifies whether <code>count</code> is given in bits instead of bytes.
   * @returns {string}
   *   A string indicating <code>count</code> with the appropriate measure (eg. kb, Mb,
   *   etc.).
   */
  var suffixFileSize;
  (function(Math, SUFFIXES) {
    suffixFileSize = function(count, opt_countInBits) {
      var div = opt_countInBits ? 3 : 10,
          base = opt_countInBits ? 10 : 2,
          level = ~~Math.min(Math['log' + base](count < 0 ? -count : count) / div, 8);
      count = (count / Math.pow(base, div * level)).toFixed(2).replace(/\.?0+$/, '')
        + ' ' + SUFFIXES[level];
      return opt_countInBits ? count : count.toUpperCase();
    };
  })(Math, ['b','kb','Mb','Gb','Tb','Pb','Eb','Zb','Yb']);
  
  /**
   * Gets the sum of an array of numbers.
   * @name sum
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/sum/2.23.0}
   * @param {Array.<number>} arr
   *   Array of numbers to be summed up.
   * @returns {number}
   *   Sum of the array of numbers.
   */
  function sum(arr) {
    for (var result = 0, i = arr.length; i--; result += arr[i]) {}
    return result;
  }
  
  /**
   * Swaps the specified properties between the target object and the source
   * object.
   * @name swapProps
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/swapProps/2.23.0}
   * @param {*} target
   *   Target object from which the specified properties will be swapped.
   * @param {*} source
   *   Source object from which the specified properties will be swapped.
   * @param {Array} keys
   *   Array of keys for the properties that should be swapped between <code>source</code>
   *   and <code>target</code>.
   * @param {boolean} [opt_dontDelete=false]
   *   Indicates whether properties that dont exist should be deleted between
   *   objects.  If <code>true</code>, non-existent properties will be set to <code>undefined</code>.
   * @returns {*}
   *   Returns a reference to <code>target</code>.
   */
  function swapProps(target, source, props, opt_dontDelete) {
    source = Object(source);
    for (var t, copyToSource, prop, oTarget = Object(target), i = props.length; i--;) {
      prop = props[i];
      copyToSource = prop in oTarget;
      t = oTarget[prop];
      if ((prop in source) || opt_dontDelete) {
        oTarget[prop] = source[prop];
      }
      else {
        delete oTarget[prop];
      }
      if (copyToSource || opt_dontDelete) {
        source[prop] = t;
      }
      else {
        delete source[prop];
      }
    }
    return target;
  }
  
  /**
   * Creates timed functions which on every successful execution will indicate
   * the amount of time it took to execute.
   * @name time
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/time/2.23.0}
   * @param {Function} func
   *   Function to wrap and be timed.
   * @param {Array|Function} times
   *   If this is an array, on each successful execution of <code>func</code> an object
   *   will be added to the end containing the following properties:  <code>elapsed</code>
   *   (time taken), <code>start</code> (starting time stamp), <code>end</code> (ending time stamp),
   *   <code>returnValue</code>, <code>function</code> (a reference to <code>func</code>), <code>this</code> (context object
   *   passed to <code>func</code>), and <code>arguments</code> (those passed to <code>func</code>).  If this is
   *   a function, upon successful execution of <code>func</code> it will be called and
   *   passed the aforementioned object.
   * @returns {Function}
   *   A wrapped version of <code>func</code> which will take the context and arguments and
   *   pass them to <code>func</code> and then return the return value from <code>func</code>.  All
   *   timing information will be given to <code>times</code>.
   */
  function time(fn, times) {
    var timesIsFunction = 'function' === typeof times;
    return function() {
      var startTime = +new Date,
          result = fn.apply(this, arguments),
          endTime = +new Date,
          timingData = {
            elapsed: endTime - startTime,
            start: startTime,
            end: endTime,
            returnValue: result,
            'function': fn,
            'this': this,
            'arguments': arguments
          };
      timesIsFunction ? times(timingData) : times.push(timingData);
      return result;
    };
  }
  
  /**
   * Gets a string indicating how long ago a given date was in the largest unit
   * possible.
   * @name timeAgo
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/timeAgo/2.23.0}
   * @param {Date} dateTime
   *   Date in the past.
   * @param {Date} [opt_currDateTime=new Date()]
   *   Current date.  If not given defaults to the current date and time.
   * @returns {string}
   *   A string indicating in the largest unit possible how long ago <code>dateTime</code>
   *   happened compared to <code>opt_currDateTime</code>.
   */
  function timeAgo(dateTime, opt_currDateTime) {
    opt_currDateTime = new Date(opt_currDateTime || new Date) - new Date(dateTime);
    return '31536e6year2592e6month864e5day36e5hour6e4minute1e3second'.replace(/(\d+e\d)([a-z]+)/g, function(m, ms, interval) {
      if (dateTime != undefined) {
        ms = opt_currDateTime / +ms;
        if (ms >= 1 || interval == 'second') {
          dateTime = undefined;
          return ~~ms + ' ' + interval + (~~ms - 1 ? 's' : '') + ' ago';
        }
      }
      return '';
    }) || undefined;
  }
  
  /**
   * Capitalizes the first letter of each word in a string. Also commonly known
   * as <code>toProperCase()</code>.
   * @name titleCase
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/titleCase/2.23.0}
   * @param {string} str
   *   The string which will be title cased.
   * @param {function(string, number, string)} [opt_fnFilter]
   *   If specified, this function will be passed every word (along with the
   *   position and the original string) and should return <code>true</code> if the word
   *   should be title cased, otherwise <code>false</code> should be returned.
   * @returns {string}
   *   <code>str</code> with all of the 1st letter of each word capitalized (unless
   *   filtered out by <code>opt_fnFilter</code>).
   */
  var titleCase;
  (function(RGX_WORD) {
    titleCase = function (str, opt_fnFilter) {
      return str.replace(RGX_WORD, function(word, start, rest, index) {
        return (!opt_fnFilter || opt_fnFilter(word, index, str) ? start.toUpperCase() : start) + rest;
      });
    };
  })(/(\S)((?:\B\S)*)/g);
  
  /**
   * Converts the given number into a string representing the number in the
   * specified base.
   * @name toBase
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/toBase/2.23.0}
   * @param {number} [opt_num]
   *   The number that should be represented in a specific base.
   * @param {number} base
   *   A number in the range from <code>2</code> to <code>36</code>.
   * @returns {string|Function}
   *   If only one argument is given a partial function will be returned which
   *   will take that original argument as <code>base</code>, will accept one argument when
   *   called which will be interpreted as the number to convert to the
   *   specified <code>base</code>.  Otherwise converts the first arguments to the
   *   specified <code>base</code> and returns the number as a string.
   */
  function toBase(opt_num, base) {
    return base
      ? (+opt_num).toString(base)
      : function(x) { return (+x).toString(opt_num); };
  }
  
  /**
   * Converts a string or a number to a boolean value.
   * @name toBool
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/toBool/2.23.0}
   * @param {string|number} value
   *   The value to be parsed.
   * @returns {boolean}
   *   If <code>value</code> is <code>"false"</code>, <code>"no"</code>, <code>"off"</code>, <code>0</code> or the empty string then
   *   <code>false</code> will be returned.  In all other cases <code>true</code> will be returned.
   */
  function toBool(value) {
    return!/^(no|off|false|0|)$/i.test((value || '') + '');
  }
  
  /**
   * Toggles values in an array, adding the values that are missing from the
   * array and removing those that are there.
   * @name toggle
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/toggle/2.23.0}
   * @param {Array} array
   *   The array whose values should be toggled. A copy of this array with the
   *   values toggled will be returned.
   * @param {Array} valuesToToggle
   *   The array of values to either add to (if they are present) or remove from
   *   (if they are missing) <code>array</code>.
   * @param {Function} [opt_fnTestEquality]
   *   If not given strict equality (<code>===</code>) will be used to compare values. If
   *   specified, this function will be used to determine if two values are
   *   equal. The first argument will be the value within <code>array</code> to be tested
   *   and the second will be the value within <code>valuesToToggle</code> to be tested.
   * @returns {Array}
   *   A duplicate of <code>array</code> with the common values of <code>valuesToToggle</code> removed
   *   and the missing values added.
   */
  function toggle(array, valuesToToggle, opt_fnTestEquality) {
    array = slice(array);
    valuesToToggle = slice(valuesToToggle);
    for (var j, i = array.length, valuesCount = valuesToToggle.length; i--;) {
      for (j = valuesCount; j--;) {
        if (opt_fnTestEquality ? opt_fnTestEquality(array[i], valuesToToggle[j]) : (array[i] === valuesToToggle[j])) {
          array.splice(i, 1);
          valuesToToggle.splice(j, 1);
          valuesCount--;
        }
      }
    }
    return array.concat(valuesToToggle);
  }
  
  /**
   * Takes an object and turns it into a serialized URL parameter string.
   * @name toParams
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/toParams/2.23.0}
   * @param {Object} obj
   *   The object to be serialized as a URL parameter string.
   * @param {Function} [opt_formatter]
   *   Optional formatter function. Used to format the values that will be
   *   displayed for each value from <code>obj</code>.  For each value found in <code>obj</code> this
   *   function will be passed (1) the value and (2) the path to that value as
   *   an array.  The return value will be used as the value added to the
   *   serialized string.  If <code>undefined</code> is returned or if nothing is returned
   *   then nothing will be added to the parameter serialization for that object
   *   property.
   * @returns {string}
   *   A URL parameter string representing <code>obj</code>.
   */
  function toParams(obj, opt_formatter) {
    var encodedKey, encodedValue, k, parts = [];
    obj = flattenKeys(obj);
    for (k in obj) {
      if (has(obj, k)) {
        encodedKey = escape(('.' + k).replace(
          /\.((?:[^\\\.]+|\\.)*)/g,
          function (match, name, i) {
            if (/(^|\\)\[|\]/.test(name)) {
              name = name.replace(/\[|\]/g, '\\$&');
            }
            if (i) {
              name = '[' + name + ']';
            }
            return name;
          }
        ));
        encodedValue = obj[k];
        if (opt_formatter) {
          encodedValue = opt_formatter(encodedValue, k.match(/(?:[^\\]+|\\.)+/g).map(function (k) { return k.replace(/\\(.)/g, '$1'); }));
          encodedValue = encodedValue && escape(encodedValue);
        }
        else {
          encodedValue = escape(
            encodedValue == undefined
              ? ''
              : nativeType(encodedValue) !== 'Date'
                ? encodedValue + ''
                : encodedValue.toJSON()
          );
        }
        if (encodedValue !== undefined) {
          parts.push(encodedKey + '=' + encodedValue);
        }
      }
    }
    return parts.join('&');
  }
  
  /**
   * Removes all whitespace characters or the specified characters from the
   * beginning of a string.
   * @name trimLeft
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/trimLeft/2.23.0}
   * @param {string} [opt_str=undefined]
   *   The string to return with all whitespace characters or specified
   *   characters stripped from the beginning of it.
   * @param {string} [opt_chars="\t\n\v\f\r \xA0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF"]
   *   Characters to remove from the beginning of the string.
   * @returns {Function|string}
   *   If <code>opt_str</code> is not given a function will be returned that will take one
   *   string argument and trim the beginning of that string using the
   *   characters from <code>opt_chars</code>.  If <code>opt_str</code> is given it will be returned
   *   with all leading characters found in <code>opt_chars</code> stripped off.
   */
  function trimLeft(opt_str, opt_chars) {
    opt_chars = 'string' == typeof opt_chars
      ? new RegExp('^[' + quoteRegExp(opt_chars) + ']+')
      : /^[\t-\r \xA0\u1680\u180E\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF]+/;
    return opt_str == undefined ? function(str) { return str.replace(opt_chars, ''); } : opt_str.replace(opt_chars, '');
  }
  
  /**
   * Removes all whitespace characters or the specified characters from the end
   * of a string.
   * @name trimRight
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/trimRight/2.23.0}
   * @param {string} [opt_str]
   *   The string to return with all whitespace characters or specified
   *   characters stripped from the end of it.
   * @param {string} [opt_chars="\t\n\v\f\r \xA0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF"]
   *   Characters to remove from the end of the string.
   * @returns {Function|string}
   *   If <code>opt_str</code> is not given a function will be returned that will take one
   *   string argument and trim the end of that string using the characters from
   *   <code>opt_chars</code>.  If <code>opt_str</code> is given it will be returned with all trailing
   *   characters found in <code>opt_chars</code> stripped off.
   */
  function trimRight(opt_str, opt_chars) {
    opt_chars = 'string' == typeof opt_chars
      ? new RegExp('[' + quoteRegExp(opt_chars) + ']+$')
      : /[\t-\r \xA0\u1680\u180E\u2000-\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF]+$/;
    return opt_str == undefined ? function(str) { return str.replace(opt_chars, ''); } : opt_str.replace(opt_chars, '');
  }
  
  /**
   * Removes all whitespace characters or the specified characters from the
   * beginning of a string.
   * @name trim
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/trim/2.23.0}
   * @param {string} [opt_str=undefined]
   *   String to return with all whitespace characters or specified characters
   *   stripped from the beginning and end of it.
   * @param {string} [opt_chars="\t\n\v\f\r \xA0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u2028\u2029\u202F\u205F\u3000\uFEFF"]
   *   Characters to remove from the beginning of the string.
   * @returns {Function|string}
   *   If <code>opt_str</code> is not given a function will be returned that will take one
   *   string argument and trim the beginning and end of that string using the
   *   characters from <code>opt_chars</code>. If <code>opt_str</code> is given it will be returned
   *   with all leading and trailing characters found in <code>opt_chars</code> stripped
   *   off.
   */
  function trim(opt_str, opt_chars) {
    return opt_str == undefined
      ? function(str) { return trimLeft(trimRight(str, opt_chars), opt_chars); }
      : trimLeft(trimRight(opt_str, opt_chars), opt_chars);
  }
  
  /**
   * Only gets the integral part of a number.
   * @name trunc
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/trunc/2.23.0}
   * @param {number} num
   *   The number for which only the integral part shall be returned.
   * @returns {number}
   *   The integral part of <code>num</code> while keeping the sign.
   */
  function trunc(num) {
    return Math[num < 0 ? 'ceil' : 'floor'](num);
  }
  
  /**
   * Keeps trying to execute a function until it has success or until the
   * timeout is reached.
   * @name tryUntil
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/tryUntil/2.23.0}
   * @param {Function} fn
   *   Function to keep trying to run.
   * @param {number} [opt_timeout=Infinity]
   *   Amount of milliseconds to wait before giving up on running <code>fn</code>.
   * @param {number} [opt_interval=100]
   *   Amount of milliseconds to wait between each try.
   * @returns {boolean}
   *   <code>true</code> is returned if no error is thrown the first time <code>fn</code> is called.
   *   Otherwise <code>false</code> is returned.
   */
  function tryUntil(fn, opt_timeout, opt_interval) {
    var start = new Date, iteration = 0, self = this;
    opt_timeout = opt_timeout == undefined ? Infinity : opt_timeout;
    opt_interval = opt_interval == undefined ? 100 : opt_interval;
    function fnWrapper() {
      try {
        fn.call(self, ++iteration);
        return true;
      }
      catch (e) {
        if (new Date - start < opt_timeout) {
          setTimeout(fnWrapper, opt_interval);
        }
        return false;
      }
    }
    return fnWrapper();
  }
  
  /**
   * Unbinds a value from a function (such as a prototypal function) and creates
   * a wrapper function that when called will use the 1st argument as the bound
   * value.
   * @name unbind
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/unbind/2.23.0}
   * @param {Function} fn
   *   Function to be unbound.
   * @param {Array} [opt_args=[]]
   *   Array of arguments to precede any additionally arguments passed to the
   *   returned wrapper function.
   * @returns {Function}
   *   A wrapper function which when called will bound the first argument passed
   *   to it to <code>fn</code> and then pass the rest of the arguments in after any
   *   specified in <code>opt_args</code>.
   */
  function unbind(fn, opt_args) {
    opt_args = slice(opt_args || []);
    return function(objThis) {
      return fn.apply(objThis, opt_args.concat(slice(arguments, 1)));
    };
  }
  
  /**
   * Creates a string representation of the given object or primitive.
   * @name uneval
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/uneval/2.23.0}
   * @param {*} subject
   *   Object or primitive whose string interpretation will be formulated and
   *   returned.
   * @param {number} [opt_spaceCount=0]
   *   If given and not <code>0</code> this causes non-empty objects and/or arrays to be
   *   split onto multiple lines and indented by the amount of spaces given
   *   here.
   * @returns {string}
   *   A string representation of <code>subject</code>.
   */
  var uneval = (function(RGX_BEGIN_WHITESPACE, RGX_LINE, RGX_BEGIN_NON_BLANK, EMPTY, EMPTY_TIMES_START) {
    EMPTY_TIMES_START = EMPTY + ' \xD7 ';
    function recursiveUneval(obj, arrAncestors, spaces, newline, partSep) {
      var skipFirstLine, minLeadSpace, key, pairs, parts, strWrap, isNotAncestor, emptyCount, l, i,
          arrNewAncestors = arrAncestors.concat([obj]),
          typeName = typeOf(obj);
      if (typeName === 'String') {
        return JSON.stringify(obj);
      }
      if (typeName === 'Number') {
        return ((!obj && 1 / obj < 0) ? '-' : '') + obj;
      }
      else if (typeName === 'Date') {
        return 'new Date(' + (+obj) + ')';
      }
      else if (typeName === 'Function') {
        minLeadSpace = Infinity;
        return (obj + '').replace(RGX_BEGIN_WHITESPACE, function(m, i, mLen) {
          if ((mLen = m.length) || i) {
            minLeadSpace = Math.min(minLeadSpace, mLen);
          }
          else {
            skipFirstLine = true;
          }
          return m;
        }).replace(RGX_LINE, function(m, i) {
          return (!i && skipFirstLine) ? m : m.slice(minLeadSpace);
        });
      }
      else if (typeName === 'Array' || typeName === 'Object' || typeName === '*unknown') {
        parts = [];
        strWrap = typeName === 'Array' ? '[]' : '{}';
        isNotAncestor = indexOf(arrAncestors, obj) < 0;
        i = 0;
        if (isNotAncestor) {
          if (strWrap === '[]') {
            for (emptyCount = 0, l = obj.length; i < l; i++) {
              if (has(obj, i)) {
                if (emptyCount) {
                  parts.push(emptyCount - 1 ? EMPTY_TIMES_START + emptyCount : EMPTY);
                  emptyCount = 0;
                }
                parts.push(recursiveUneval(obj[i], arrNewAncestors, spaces, newline, partSep));
              }
              else {
                emptyCount += 1;
              }
            }
            if (emptyCount) {
              parts.push(emptyCount - 1 ? EMPTY_TIMES_START + emptyCount : EMPTY);
            }
          }
          else {
            pairs = entries(obj).sort(function(a,b){return a<=b?a<b?-1:0:1});
            for (l = pairs.length; i < l; i++) {
              key = pairs[i][0];
              parts.push(
                (isValidVarName(key) ? key : recursiveUneval(key, [], spaces, newline, partSep))
                + ': '
                + recursiveUneval(pairs[i][1], arrNewAncestors, spaces, newline, partSep)
              );
            }
          }
        }
        return strWrap.charAt(0)
          + (isNotAncestor
              ? (newline + parts.join(partSep) + newline).replace(RGX_BEGIN_NON_BLANK, spaces)
              : '\u2026'
          )
          + strWrap.charAt(1);
      }
      return obj + '';
    }
  
    return function(obj, opt_spaceCount) {
      var spaces = opt_spaceCount ? Array(opt_spaceCount + 1).join(' ') : '',
          newline = opt_spaceCount ? '\n' : ''
          partSep = ',' + (newline || ' ');
      return recursiveUneval(obj, [], spaces, newline, partSep);
    };
  })(/^[\s\xA0]*/gm, /^.+$/gm, /^(?!$)/gm, 'empty');
  
  /**
   * Gets an ID (non-negative integer) by decoding the given hashed ID.
   * @name unhashID
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/unhashID/2.23.0}
   * @param {string} hashedID
   *   The hashed ID that must be parsed to get the original ID.
   * @param {number} [opt_intHash=0]
   *   Non-negative integer that will be used to find the offset of the
   *   characters used to represent <code>hashedID</code>.
   * @param {string} [opt_strAlpha="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"]
   *   All of the characters that were used to encode <code>hashedID</code>.  Defaults to
   *   the digits 0 to 9 followed by A to Z followed by a to z.
   * @returns {number}
   *   The ID (non-negative integer) that was used to produce <code>hashedID</code> based
   *   on the hash and the alphabet.
   */
  function unhashID(hashedID, opt_intHash, opt_strAlpha) {
    opt_intHash = Math.max(0, Math.min(Math.round(opt_intHash || 0), 9007199254740991));
    opt_strAlpha = opt_strAlpha || '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  
    var id = 0;
    var FULL_ALPHA_LENGTH = opt_strAlpha.length;
  
    for (var iter = 0, l = hashedID.length; iter < l; iter++) {
      var newAlpha = '';
      for (var x, i = opt_intHash; opt_strAlpha; i += opt_intHash) {
        newAlpha += opt_strAlpha.charAt(x = i % opt_strAlpha.length);
        opt_strAlpha = opt_strAlpha.slice(0, x) + opt_strAlpha.slice(x + 1);
      }
      opt_strAlpha = newAlpha;
  
      id += opt_strAlpha.indexOf(hashedID.slice(-1)) * Math.pow(FULL_ALPHA_LENGTH, iter);
      hashedID = hashedID.slice(0, -1);
    }
    return id;
  }
  
  /**
   * Creates a new array which will contain all of the items of the two given
   * arrays.  Different from concatenation because the number of times a value
   * occurs will never exceed the maximum number of times it was found in one of
   * the arrays.
   * @name union
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/union/2.23.0}
   * @param {Array} array1
   *   First array to union with the second array.
   * @param {Array} array2
   *   Second array to union with the first array.
   * @returns {Array}
   *   An array of all of the items from <code>array1</code> and <code>array2</code> but limiting the
   *   occurrences to never exceeding the maximum number of times the value
   *   occurred in either array.
   */
  function union(array1, array2) {
    return array1.concat(diffArrays(array2, array1));
  }
  
  /**
   * Creates a new version of an array with all of the duplicate values removed.
   * @name uniquify
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/uniquify/2.23.0}
   * @param {Array} array
   *   Array to copy and then modify to get rid of all duplicates from within
   *   the copy.
   * @returns {Array}
   *   A copy of <code>array</code> with all of the duplicates removed.
   */
  function uniquify(array) {
    array = slice(array);
    for (var e1, i1 = 0, l = array.length; i1 < l; i1++) {
      e1 = array[i1];
      for (i2 = i1 + 1; i2 < l; i2++) {
        if (is(e1, array[i2])) {
          l--;
          array.splice(i2--, 1);
        }
      }
    }
    return array;
  }
  
  /**
   * Creates a copy of an array with all of the consecutive items up to but not
   * including the first value <code>false</code>-ish value.
   * @name whileExists
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/whileExists/2.23.0}
   * @param {Array} array
   *   Array or array-like object containing values that will be copied.
   * @param {boolean} [opt_fromEnd=false]
   *   Indicates that the values should be copied starting from the end, not
   *   from the beginning as normal.
   * @returns {Array}
   *   If <code>opt_fromEnd</code> is not given or is <code>false</code>-ish, an array of all of the
   *   consecutive <code>true</code>-ish values starting at the beginning of <code>array</code> will
   *   be returned.  Otherwise an array of all of the consecutive <code>true</code>-ish
   *   values starting at the end of <code>array</code> will be returned.
   */
  eval('while until'.replace(/\w+/g, function(m, i) {
    return 'function ' + m
      + 'Exists(b,c){for(var f=c?"unshift":"push",d=[],g=c?-1:1,e=b.length,a=c?~~(e-1):0;'
      + (i ? '!' : '') + 'b[a]&&-1<a&&a<e;a+=g)d[f](b[a]);return d}';
  }));
  
  /**
   * Creates a copy of an array with all of the consecutive items up to but not
   * including the first value <code>true</code>-ish value.
   * @name untilExists
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/untilExists/2.23.0}
   * @param {Array} array
   *   Array or array-like object containing values that will be copied.
   * @param {boolean} [opt_fromEnd=false]
   *   Indicates that the values should be copied starting from the end, not
   *   from the beginning as normal.
   * @returns {Array}
   *   If <code>opt_fromEnd</code> is not given or is <code>false</code>-ish, an array of all of the
   *   consecutive <code>false</code>-ish values starting at the beginning of <code>array</code> will
   *   be returned.  Otherwise an array of all of the consecutive <code>false</code>-ish
   *   values starting at the end of <code>array</code> will be returned.
   */
  
  /**
   * Creates a date using universal time.
   * @name utc
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/utc/2.23.0}
   * @param {Date|number|string} yearOrValue
   *   Either a <code>Date</code> object whose properties will be interpreted as UTC
   *   properties, or the number of milliseconds since the beginning of Jaunary
   *   1, 1970 GMT, or a string version of the date, or the year.  If this is to
   *   be understood as the year additional arguments must also be passed.
   * @param {number} [opt_month]
   *   An integer between <code>0</code> and <code>11</code> representing the month.
   * @param {number} [opt_day]
   *   An integer between <code>1</code> and <code>31</code> representing the month.
   * @param {number} [opt_hour]
   *   An integer between <code>0</code> and <code>23</code> representing the hour.
   * @param {number} [opt_min]
   *   An integer between <code>0</code> and <code>59</code> representing the minute.
   * @param {number} [opt_sec]
   *   An integer between <code>0</code> and <code>59</code> representing the second.
   * @param {number} [opt_ms]
   *   An integer between <code>0</code> and <code>999</code> representing the millisecond.
   * @returns {Date}
   *   Specified UTC date/time as a <code>Date</code> object.
   */
  function utc(yearOrValue, opt_month, opt_day, opt_hour, opt_min, opt_sec, opt_ms) {
    var d;
    if (arguments.length < 2) {
      d = 'object' == typeof yearOrValue ? yearOrValue : new Date(yearOrValue);
      d = Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds());
    }
    else {
      d = Date.UTC(yearOrValue, opt_month, opt_day == undefined ? 1 : opt_day, opt_hour || 0, opt_min || 0, opt_sec || 0, opt_ms || 0);
    }
    return new Date(d);
  }
  
  /**
   * Gets an array of all of the values within a given array or object.
   * @name values
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/values/2.23.0}
   * @param {*} value
   *   Value for which you would like the property names retrieved.
   * @param {boolean} [opt_onlyNonEnums]
   *   If specified and is not <code>null</code> or <code>undefined</code> but is <code>false</code>-ish, all of
   *   the properties (enumerable or not) will be returned.  If this is
   *   <code>true</code>-ish only the non-enumerable properties will be returned.  If this
   *   is not given or is <code>null</code> or <code>undefined</code> all of the enumerable properties
   *   will be returned.
   * @returns {Array}
   *   An array of all of the values in <code>value</code>.
   */
  function values(value, opt_onlyNonEnums) {
    for (var result = keys(value, opt_onlyNonEnums), i = result.length; i--;) {
      result[i] = value[result[i]];
    }
    return result;
  }
  
  /**
   * Creates a waiter function which takes orders and serves orders in a way and
   * calls a ready function when all orders have been served.
   * @name wait
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/wait/2.23.0}
   * @param {Function} fnReady
   *   Function to be called when the waiter function is has been called to
   *   serve the last remaining order.  When called this function will receive
   *   all of the orders ever made as the 1st argument and the final order as
   *   the 2nd.
   * @param {boolean} [opt_clearOrdersAfterReady=false]
   *   If <code>true</code>-ish the orders array will be cleared after the <code>fnReady</code>
   *   function is called.  This prevents previous orders from showing up if
   *   <code>fnReady</code> is called multiple times.
   * @returns {Function}
   *   A waiter function which take and serve orders.  The 1st argument passed
   *   to this function should be a boolean indicating that an order is going
   *   in.  The 2nd argument passed to this function can be any value that
   *   represents an order.  When this function is called a boolean will be
   *   returned indicating whether or not the call was successful.  The only way
   *   <code>false</code> can be returned by this function is if an order is to be served
   *   that doesn't exist.
   */
  function wait(fnReady, opt_clearOrdersAfterReady) {
    var orders = [], allOrders = [];
    return function(opt_makeAnOrder, opt_order) {
      var index;
      if (opt_makeAnOrder) {
        orders.push(opt_order);
        allOrders.push(opt_order);
      }
      else {
        index = orders.findIndex(function(order) {
          return is(order, opt_order);
        });
        if (index >= 0) {
          orders.splice(index, 1);
          if (!orders.length) {
            try {
              fnReady(slice(allOrders), opt_order);
            }
            catch (e) {
              setTimeout(function(){throw e},0);
            }
            if (opt_clearOrdersAfterReady) {
              allOrders = [];
            }
          }
        }
      }
      return !(index < 0);
    }
  }
  
  /**
   * Creates a function which takes values that will be interpreted as booleans
   * and will return the correspond string (or value).
   * @name yes
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/yes/2.23.0}
   * @param {*} [opt_yesValue="Yes"]
   *   The value that will be returned when <code>true</code>-ish values are passed to the
   *   returned function.
   * @param {*} [opt_noValue="No"]
   *   The value that will be returned when <code>false</code>-ish values are passed to the
   *   returned function.
   * @param {*} [opt_maybeValue]
   *   The value that will be returned when neither <code>true</code> no <code>false</code> is passed
   *   to the returned function.  If not given then all values will be
   *   interpreted as <code>true</code> or <code>false</code>.
   * @returns {Function}
   *   The function which will accept one value that will be interpreted as a
   *   boolean and will return <code>opt_yesValue</code>, <code>opt_noValue</code> or
   *   <code>opt_maybeValue</code>.
   */
  function yes(opt_yesValue, opt_noValue, opt_maybeValue) {
    opt_yesValue = opt_yesValue == undefined ? 'Yes' : opt_yesValue;
    opt_noValue = opt_noValue == undefined ? 'No' : opt_noValue;
    return function(value) {
      return (opt_maybeValue != undefined && value != true && value != false)
        ? opt_maybeValue
        : value
          ? opt_yesValue
          : opt_noValue;
    };
  }
  
  /**
   * Takes one or more arrays and does a kind of matrix inverse option on them.
   * @name zip
   * @memberof JS
   * @function
   * @see {@link http://yourjs.com/docs/view/zip/2.23.0}
   * @param {...Array} array
   *   One or more arrays whose array items will be used to create the returned
   *   array.
   * @returns {Array}
   *   An array of arrays where the first array contains the first item of each
   *   array and the second array contains the second item of each array and so
   *   on.
   */
  function zip() {
    for (var j, i = 0, arrays = arguments, maxArrLen = 0, result = [], argc = arrays.length; i < argc; i++) {
      maxArrLen = Math.max(maxArrLen, arrays[i].length);
    }
    for (i = 0; i < maxArrLen; i++) {
      result[i] = [];
      for (j = 0; j < argc; j++) {
        result[i][j] = arrays[j][i];
      }
    }
    return result;
  }

  /**
   * YourJS object.
   * @name JS
   * @namespace
   * @global
   */
  YourJS = {add:add,after:after,afterAll:afterAll,alias:alias,andBits:andBits,applyOf:applyOf,around:around,array:array,attempt:attempt,avg:avg,before:before,best:best,bind:bind,bindCompare:bindCompare,blockify:blockify,call:call,callOf:callOf,calls:calls,canDefault:canDefault,cap:cap,capProto:capProto,cbrt:cbrt,ceil:ceil,chain:chain,chunk:chunk,clamp:clamp,closestMultOf:closestMultOf,codePointAt:codePointAt,commaNumber:commaNumber,compact:compact,compare:compare,compareTitle:compareTitle,compose:compose,construct:construct,copyProps:copyProps,copySign:copySign,cos:cos,cot:cot,count:count,countBy:countBy,countCodePoints:countCodePoints,csc:csc,css:css,curry:curry,cut:cut,debounce:debounce,deburr:deburr,defaulter:defaulter,defaultTo:defaultTo,defer:defer,degrees:degrees,delay:delay,deleteAt:deleteAt,deleteKeys:deleteKeys,dice:dice,diffArrays:diffArrays,diffDates:diffDates,divide:divide,doEvery:doEvery,dom:dom,dow:dow,endOfDay:endOfDay,endOfFridayWeek:endOfFridayWeek,endOfHour:endOfHour,endOfMinute:endOfMinute,endOfMondayWeek:endOfMondayWeek,endOfMonth:endOfMonth,endOfSaturdayWeek:endOfSaturdayWeek,endOfSecond:endOfSecond,endOfSundayWeek:endOfSundayWeek,endOfThursdayWeek:endOfThursdayWeek,endOfTuesdayWeek:endOfTuesdayWeek,endOfWednesdayWeek:endOfWednesdayWeek,endOfWeek:endOfWeek,endOfYear:endOfYear,endsWith:endsWith,ensureAt:ensureAt,entries:entries,eq:eq,eqs:eqs,escape:escape,everyWhere:everyWhere,exec:exec,execAll:execAll,expandRegExp:expandRegExp,extend:extend,fill:fill,filter:filter,filterByProp:filterByProp,filterEntries:filterEntries,filterKeys:filterKeys,filterMap:filterMap,filterOut:filterOut,filterPropEntries:filterPropEntries,filterPropNames:filterPropNames,filterPropValues:filterPropValues,filterValues:filterValues,findByProp:findByProp,findIndexByProp:findIndexByProp,findNth:findNth,findNthIndex:findNthIndex,findPath:findPath,findWhere:findWhere,findWhereNot:findWhereNot,first:first,flatMap:flatMap,flatten:flatten,flattenKeys:flattenKeys,floor:floor,forEach:forEach,forIn:forIn,formatDate:formatDate,formatTime:formatTime,forOf:forOf,frac:frac,fromCodePoints:fromCodePoints,fromParams:fromParams,fround:fround,fullNumber:fullNumber,gcd:gcd,get:get,getAllIn:getAllIn,getAt:getAt,getCookie:getCookie,getGlobal:getGlobal,getIn:getIn,groupBy:groupBy,gt:gt,gte:gte,has:has,hasAt:hasAt,hasCookie:hasCookie,hashID:hashID,hasIn:hasIn,hasKeys:hasKeys,htmlify:htmlify,hypot:hypot,ifExcludes:ifExcludes,ifFail:ifFail,ifFails:ifFails,ifIn:ifIn,ifIncludes:ifIncludes,ifIs:ifIs,ifNot:ifNot,ifOut:ifOut,ifPass:ifPass,ifPasses:ifPasses,imul:imul,includes:includes,index:index,indexBy:indexBy,indexOf:indexOf,indexOfDiff:indexOfDiff,indexWhere:indexWhere,indexWhereNot:indexWhereNot,indicesWhere:indicesWhere,indicesWhereNot:indicesWhereNot,info:info,insert:insert,insertInto:insertInto,int16:int16,int32:int32,int8:int8,intersect:intersect,invert:invert,is:is,isArguments:isArguments,isArray:isArray,isArrayLike:isArrayLike,isBitable:isBitable,isBoolean:isBoolean,isComposite:isComposite,isDate:isDate,isEmpty:isEmpty,isError:isError,isFalsy:isFalsy,isFinite:isFinite,isFloat:isFloat,isFullOf:isFullOf,isFunction:isFunction,isInRange:isInRange,isInt:isInt,isKind:isKind,isKindFor:isKindFor,isLeapYear:isLeapYear,isLowerCase:isLowerCase,isMixCase:isMixCase,isNaN:isNaN,isNegative:isNegative,isNil:isNil,isNoCase:isNoCase,isNot:isNot,isNull:isNull,isNumber:isNumber,isNumeric:isNumeric,isObject:isObject,isoDate:isoDate,isOf:isOf,isPositive:isPositive,isPrime:isPrime,isPrimitive:isPrimitive,isPrototype:isPrototype,isRegExp:isRegExp,isRegExpMatch:isRegExpMatch,isSafeInt:isSafeInt,isSameDay:isSameDay,isSameFridayWeek:isSameFridayWeek,isSameHour:isSameHour,isSameMinute:isSameMinute,isSameMondayWeek:isSameMondayWeek,isSameMonth:isSameMonth,isSameSaturdayWeek:isSameSaturdayWeek,isSameSecond:isSameSecond,isSameSundayWeek:isSameSundayWeek,isSameThursdayWeek:isSameThursdayWeek,isSameTuesdayWeek:isSameTuesdayWeek,isSameWednesdayWeek:isSameWednesdayWeek,isSameWeek:isSameWeek,isSameYear:isSameYear,isSpace:isSpace,isString:isString,isType:isType,isTypeFor:isTypeFor,isUndefined:isUndefined,isUnknown:isUnknown,isUpperCase:isUpperCase,isValidVarName:isValidVarName,isWhitespace:isWhitespace,join:join,joinPath:joinPath,jsonp:jsonp,keepAspectRatio:keepAspectRatio,keys:keys,keysMatch:keysMatch,kindsOf:kindsOf,last:last,lastIndexWhere:lastIndexWhere,lastIndexWhereNot:lastIndexWhereNot,lastWhere:lastWhere,lastWhereNot:lastWhereNot,lcm:lcm,limit:limit,log:log,log10:log10,log2:log2,loop:loop,lowerCase:lowerCase,lowerFirst:lowerFirst,lowerRest:lowerRest,lt:lt,lte:lte,map:map,mapKeys:mapKeys,mapPairs:mapPairs,matchAll:matchAll,max:max,maxBy:maxBy,maxIndex:maxIndex,maxIndexBy:maxIndexBy,min:min,minBy:minBy,minIndex:minIndex,minIndexBy:minIndexBy,minMax:minMax,minMaxBy:minMaxBy,minMaxIndex:minMaxIndex,minMaxIndexBy:minMaxIndexBy,mixin:mixin,mod:mod,modDate:modDate,modEach:modEach,modRegExp:modRegExp,modURL:modURL,multiply:multiply,multOf:multOf,nativeType:nativeType,ne:ne,nes:nes,noConflict:noConflict,not:not,notEveryWhere:notEveryWhere,now:now,noWhere:noWhere,nth:nth,only:only,orBits:orBits,ordinalize:ordinalize,pacman:pacman,padEnd:padEnd,padStart:padStart,pair:pair,parseQS:parseQS,partial:partial,partialAny:partialAny,paste:paste,pathIn:pathIn,pluck:pluck,poll:poll,postURL:postURL,prefix:prefix,prop:prop,propOf:propOf,quoteRegExp:quoteRegExp,radians:radians,random:random,range:range,rearg:rearg,rekey:rekey,rem:rem,remove:remove,removeAll:removeAll,removeAt:removeAt,removeCookie:removeCookie,removeRight:removeRight,repair:repair,repay:repay,repeat:repeat,replace:replace,replaceMany:replaceMany,restParam:restParam,reverse:reverse,root:root,round:round,roundIn:roundIn,roundOut:roundOut,sameSign:sameSign,sample:sample,scale:scale,sec:sec,set:set,setAt:setAt,setCookie:setCookie,setIn:setIn,shuffle:shuffle,sign:sign,signbit:signbit,sin:sin,slice:slice,someWhere:someWhere,sortBy:sortBy,span:span,spanOfDay:spanOfDay,spanOfFridayWeek:spanOfFridayWeek,spanOfHour:spanOfHour,spanOfMinute:spanOfMinute,spanOfMondayWeek:spanOfMondayWeek,spanOfMonth:spanOfMonth,spanOfSaturdayWeek:spanOfSaturdayWeek,spanOfSecond:spanOfSecond,spanOfSundayWeek:spanOfSundayWeek,spanOfThursdayWeek:spanOfThursdayWeek,spanOfTuesdayWeek:spanOfTuesdayWeek,spanOfWednesdayWeek:spanOfWednesdayWeek,spanOfWeek:spanOfWeek,spanOfYear:spanOfYear,splice:splice,splitPath:splitPath,spread:spread,spreadNew:spreadNew,startOfDay:startOfDay,startOfFridayWeek:startOfFridayWeek,startOfHour:startOfHour,startOfMinute:startOfMinute,startOfMondayWeek:startOfMondayWeek,startOfMonth:startOfMonth,startOfSaturdayWeek:startOfSaturdayWeek,startOfSecond:startOfSecond,startOfSundayWeek:startOfSundayWeek,startOfThursdayWeek:startOfThursdayWeek,startOfTuesdayWeek:startOfTuesdayWeek,startOfWednesdayWeek:startOfWednesdayWeek,startOfWeek:startOfWeek,startOfYear:startOfYear,startsWith:startsWith,step:step,sub:sub,subtract:subtract,suffix:suffix,suffixFileSize:suffixFileSize,sum:sum,swapProps:swapProps,tan:tan,testAt:testAt,testFor:testFor,testForNot:testForNot,throttle:throttle,time:time,timeAgo:timeAgo,titleCase:titleCase,toArray:toArray,toBase:toBase,toBool:toBool,toCodePoints:toCodePoints,toggle:toggle,toParams:toParams,toString:toString,trim:trim,trimLeft:trimLeft,trimRight:trimRight,trunc:trunc,tryUntil:tryUntil,typeOf:typeOf,uint16:uint16,uint32:uint32,uint8:uint8,unbind:unbind,unescape:unescape,uneval:uneval,unhashID:unhashID,union:union,uniquify:uniquify,untilExists:untilExists,upperCase:upperCase,upperFirst:upperFirst,upperRest:upperRest,utc:utc,values:values,wait:wait,where:where,whereNot:whereNot,whileExists:whileExists,xorBits:xorBits,yes:yes,zip:zip};

  __callsAfterDefs.forEach(function(fn) { fn(); });

  // Add to browser/node environment correctly.
  if(typeof exports !== 'undefined') {
    if(typeof module !== 'undefined' && module.exports) {
      exports = module.exports = YourJS;
    }
    (exports[__VARIABLE_NAME] = YourJS)[__VARIABLE_NAME] = undefined;
  } 
  else {
    __GLOBAL[__VARIABLE_NAME] = YourJS;
  }
})("2.23.0.yn", "JS");
