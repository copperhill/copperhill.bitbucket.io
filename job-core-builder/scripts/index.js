$(function () {
  const STEP_TYPES = {
    mapping: {
      source: {
        endpoint: null,
        method: null,
        params: []
      },
      output: {
        endpoint: null,
        method: null,
        params: []
      },
      mappings: []
    },
    method: {
      endpoint: null,
      method: null,
      params: []
    }
  };
  const STEP_TYPE_NAMES = JS.keys(STEP_TYPES).sort();

  function clonify(value) {
    return JSON.parse(JSON.stringify(value));
  }

  vueApp = new Vue({
    el: '#vueApp',
    data: {
      STEP_TYPE_NAMES,
      job: {
        name: null,
        logIndentSize: 2,
        logIndentWithTab: false,
        logDetails: true,
        localAwsSettings: {
          profile: null
        },
        preferLocal: false,
        endpoints: [],
        steps: []
      },
      jsonImport: null
    },
    computed: {
      jsonExport() {
        let {job} = this;
        return JSON.stringify({
          name: job.name,
          logIndentSize: ~~job.logIndentSize,
          logIndentWithTab: job.logIndentWithTab,
          logDetails: job.logDetails,
          localAwsSettings: job.localAwsSettings,
          preferLocal: job.preferLocal,
          endpoints: job.endpoints.reduce((endpoints, endpoint) => {
            endpoints[endpoint.id] = endpoint = JS.extend({}, endpoint);
            if (!endpoint.connectMethod) {
              delete endpoint.connectMethod;
            }
            endpoint.connectParamMap = endpoint.connectParamMap.reduce(
              (carry, {key, value}) => JS.set(carry, key, value, true),
              {}
            );
            delete endpoint.id;
            return endpoints;
          }, {}),
          steps: job.steps.map(step => {
            step = clonify(step);
            JS.extend(step, step.typeProps);
            delete step.typeProps;
            if (step.type === 'method') {
              step.params = step.params.reduce((carry, {key, value}) => JS.set(carry, key, value, true), {});
            }
            return step;
          })
        }, null, 2);
      },
      jsonExportHref() {
        return 'data:text/plain;charset=utf-8,' + encodeURIComponent(this.jsonExport);
      }
    },
    mounted() {
      this.updateTooltips();
    },
    updated() {
      this.updateTooltips();
    },
    methods: {
      addStep() {
        this.job.steps.push(this.updateStepFromType({
          name: null,
          isDisabled: false,
          type: STEP_TYPE_NAMES[0],
          logBefore: null,
          logAfter: null
        }));
      },
      updateStepFromType(step) {
        return JS.set(step, 'typeProps', clonify(STEP_TYPES[step.type]), true);
      },
      removeStep(stepIndex) {
        this.job.steps.splice(stepIndex, 1);
      },
      getTargetDisplayName(endpointId) {
        let endpoint = this.job.endpoints.find(endpoint => endpoint.id === endpointId);
        return endpoint ? endpoint.id + ' (' + endpoint.connector + ')' : '';
      },
      addMethodParams(root, path) {
        JS.getIn(root, path.concat(['params'])).push({ key: null, value: null });
      },
      removeMethodParams(root, path, paramIndex) {
        JS.getIn(root, path.concat(['params'])).splice(paramIndex, 1);
      },
      addTarget() {
        this.job.endpoints.push({
          id: null,
          awsSecretId: null,
          localSecretPath: null,
          connector: null,
          connectMethod: null,
          connectParamMap: []
        });
      },
      removeTarget(endpointIndex) {
        this.job.endpoints.splice(endpointIndex, 1);
      },
      addToTargetConnectParamMap(endpoint) {
        endpoint.connectParamMap.push({
          key: null,
          value: null
        });
      },
      removeTargetConnectParamMap(endpoint, connectParamIndex) {
        endpoint.connectParamMap.splice(connectParamIndex, 1);
      },
      updateTooltips() {
        $('[tooltip]', this.$el).each((i, el) => $(el).tooltip({
          title: el.getAttribute('tooltip'),
          placement: 'bottom'
        }));
      },
      showImportModal() {
        $(this.$refs.modalImport).modal('show');
      },
      showExportModal() {
        $(this.$refs.modalExport).modal('show');
      },
      loadFileInto() {
        let self = this;
        let dataPath = Array.from(arguments);
        JS.dom({
          _: 'input',
          type: 'file',
          cls: 'd-none',
          onchange() {
            let file = this.files[0];
            if (file) {
              JS.extend(new FileReader(), {
                onload: e => JS.setIn(self, dataPath, e.endpoint.result),
                onerror: e => console.error(e)
              }).readAsText(file, 'UTF-8');
            }
          }
        }).click();
      },
      importJSON() {
        let job = JSON.parse(this.jsonImport);

        let props = ['name', 'logIndentSize', 'logDetails', 'logIndentWithTab', 'localAwsSettings', 'preferLocal'];
        JS.copyProps(this.job, job, props);
        this.job.endpoints = JS.entries(job.endpoints).map(([id, endpoint]) =>
          JS.extend(
            { connectMethod: null },
            endpoint,
            { id, connectParamMap: JS.entries(endpoint.connectParamMap).map(([key, value]) => ({key, value})) }
          )
        );
        this.job.steps = job.steps.map(step => {
          if (step.type === 'method') {
            let typeProps = JS.deleteKeys(step, ['method', 'params', 'endpoint']);
            step.typeProps = typeProps;
            typeProps.params = JS.entries(typeProps.params).map(([key, value]) => ({ key, value }));
          }
          return step;
        });

        $(this.$refs.modalImport).modal('hide');
      }
    }
  });
});